# MavNRC
> Last modified: By Jeremi Levesque 2022-08-24

## Starting references
1. [Quick overview/description of the general architecture of the aircraft](Documents/Architecture.md)
2. [In depth description of the MavNRC Module](Documents/Description.md)

## Description
The MavNRC is kind of the central piece managing mission information and a few other informations between the different systems in the aircraft. This module is multithreaded and uses shared memory for some information sharing between the systems. For a more in-depth description of the MavNRC, please refer to the [previous section](#starting-references).

## How to build/run
The MavNRC is currently supported on both Windows and Mac operating systems.
### On Windows
MavNRC is using the *pthread* library which is originally a UNIX threading library. The adapted version of the pthread library is located under the [pthread-win](pthread-win/) folder of the project. Everything should already be linked properly to reference this library in the Visual Studio project. The project currently uses Visual Studio 2019, but it probably won't be that hard to upgrade to VS 2022 when the time comes since almost everything is compatible.

1. Open the [VS Solution](winMavNrc.sln)
2. Build & Run the project

### On Mac
1. Open the [XCode project](mavNrc/mavNrc.xcodeproj)
2. Build & Run the project
3. You might want to deactivate Doxygen documentation warnings to reduce the number of warnings. To do that (in XCode) : Go in the project settings > Build Settings > Apple Clang - Warnings - All languages > Documentation Comments > Select **No**

> N.B: If new files were added to the project on Windows only development, you might have to add/link those new files to your XCode project. And if you create new files on the Mac Dev Environment, you should add/link those new files to the Visual Studio Project.

## Connect multiple instances of QGroundControl to MavNRC
MavNRC can handle multiple instances of GCS that communicates the Mavlink protocol under the same subnet. In order to connect to the MavNRC's data, you have to setup a new connection inside of QGroundControl. To setup a new communication link, follow these instructions :

1. Application Settings > Comm Links > Add
2. Enter a representative name for this link
3. (Optional) You can check the box to auto connect on launch so you don't have to connect the link manually every time
4. Select the type of the link to be UDP
5. Replace the pre-written port to be the one on which MavNRC broadcasts (e.g. 12346)
6. Click OK and then make sure your new link is in the state *Connected*

> N.B: If it doesn't work, make sure you're not on VPN & make sure that the MavNRC is running and broadcasting on the same port than the one you're listening to. Alternatively, you can try adding the following server address to the link : *broadcastAddr:broadcastPort* (ex: 192.168.50.255:12346)

> N.B: When you connect, it is important to identify the instance with a unique mavlink SYS_ID. Otherwise, if the SYS_ID is already connected, the QGC should not be authorized to communicate with MavNRC. Lastly, the **SYS_ID 255 is restricted to the local instance of QGC**.

The communication should work normally once the link is properly connected.

## Permission levels for instances of GCS 
A permission level regarding the mavlink messages to be sent has been implemented based on the SYS_ID of the GCS instance. If you're trying to send a message that you lack the permissions for, MavNRC will deny the message and send an alert to let the remote user that this action was restricted to his level of permission. Also, on connection with MavNRC, the remote instance will receive an alert message that let's it know what permissions it has.

This functionality still needs to be defined completely and right now it's only in an exploratory state: it was implemented to see what if this was something possible to to, but it's not currently in use. If the message is unknown/unimplemented by the system, the access to this message is denied by default. To change anything related to the permissions of messages, you should refer to the [remotePermissions file](winMavNrc/remotePermissions.c).

The following levels of permissions were determined arbitrarily, but they were implemented for demonstration purposes:

- **Admin** (255-250)
    - Have full access to all messages
- **Semi** (249-240)
    - Same as read-only, but can upload a new mission.
- **Read-only** (239-230)
    - Can only read the mission data and vehicle information. This level of permission cannot set or modify anything that will affect the aircraft or other instances of GCS.