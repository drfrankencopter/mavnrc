# Architecture Description
> Last modified: By Jeremi Levesque 2022-08-24
## Introduction
The CVLAD project is a multi-machine & multi-modules (multiples apps) that is developped iteratively across different builds. The following document is based on the understanding of the architecture based on Jeremi Levesque's perspective, a second year student in the summer 2022. The document was last updated on August of 2022 and may need to be updated in the near future as the architecture of the team changes. The document is only to give a perspective on the project and what systems are involved and what are their general uses when flying the Bell-412.

If you are not familiar with every concept in this project, I included a separate file with links that I found useful when self-learning certain of these concepts.

## Architecture
The following diagram quickly illustrates what the architecture of the project looks like. This diagram evolves quickly so it might already be outdated. Please note that I do not have experience on each of the systems, so the diagram may vary on certain sections.

Each plain rectangle in the following diagram represents a physical process (not a thread, but a separate executable) and after a quick look, you can already understand that this projects heavily relies on communication between each process. All the network communication relies on UDP sockets.

![NRC Architecture](CVLAD-Architecture.png)

Please note that each dotted rectangle represents a physical machine in the aircraft. This means that Boss/NavDisplay aren't on the same computer as the MavNRC in the aircraft. I will detail quickly describe each module and then take a more in-depth look in some of the more important sections that I've worked on.

**N.B**: This graph can vary when running everything locally. The NavDisplay can use the shared memory on the MavNRC computer without needing the BossHelper to handle the communications.

1. **MavNRC :** It's kind of the central piece to the puzzle. MavNRC holds the mission information when a mission is uploaded into the system. A mission is composed of different types of waypoints and it describes what path the pilot/system should follow (with specified speeds, altitudes, yaw, etc.). The MavNRC holds almost all the information related to such a mission and is in charge of sharing and managing that mission and other services to other processes needing the mission data. For more details about this module, check out [this file describing most of it's features](Description.md).

2. **Shared Memory :** On the MavNRC side, there are two types of shared memory, but the one I'm doing to detail is the shared memory holding the mission information. Just as described in the MavNRC above, the mission is stored in this shared memory so it can be used by other processes on the same machine. The shared memory on the Boss/NavDisplay side is used the same way as it is on the MavNRC machine. This way, you can run the NavDisplay locally so it can use the MavNRC's shared memory.

3. **BossHelper :** Receives the entire mission from MavNRC via the network (using UDP sockets) and populates the shared memory on the local computer with the new mission information.

4. **Boss :** It's basically a UI to reflect the state of the aircraft to the pilot. It's the primary display for the pilot. It was co-developped with the US Army, so access to the code of this app is usually restricted for most students. Kris Ellis might be able to better explain what every component of this UI does as you might have to encounter it.

5. **QGC :** QGroundControl is an open-source software that is primarily designed for mission planning and as a Ground Control Service for drones. It is built on top of the Mavlink protocol which was also designed primarily for drones, but everything is used for an actual aircraft in this context.

    We have access to the QGroundControl (QGC) source code since it's an open source software, so a few modifications were made to fit the needs of the project. It is mostly used currently for mission planning and to load the missions in the aircraft. All missions are usually pre-planned and ran in the simulator before going for a physical test and QGC provides such functionnalities in the Plan View. It currently is also used as a top-down view of the aircraft to give an overview of the mission to the pilot in the front of the aircraft.

    The integrity of QGC was written in Qt/QML which is a C++ (Qt) backend and a QML front-end (which is sort of a javascript framework).

    Since the modifications specific to the CVLAD project were pushed onto a new repository, we will have to resynchronise the repository every once in a while to get the latest features from the public open source repository. You can do such an update by cloning our private QGC repository and changing the remote URL to be the public API's URL. You can then pull/merge the newest changes from the daily build on the master. Make sure everything works as before, rechange the remote URL to be the private repository so you can push the new version.

    To look at the modifications done on the official version of QGC, you should look the separate file listing each modification called [QGCChanges.md](QGCChanges.md).

    You should very much look at the documentation and the open source repo if you need to dive deeper to better understand what's happening in the code. To understand the UI, it's pretty self-explanatory, but some details/guides are also given in the documentation.
    
    - Here's the repo: https://github.com/mavlink/qgroundcontrol
    - Here's the doc: https://dev.qgroundcontrol.com/master/en/
    - **A very important link to install/setup the QGC developement env:** https://dev.qgroundcontrol.com/master/en/getting_started/index.html

6. **NavDisplay :** The NavDisplay is a top down view of the aircraft and the environnement/map around it. It's meant to replace the instance of QGC that is running in the front of the aircraft for the pilot. It offers a better top-down view of the aircraft with a higher refresh rate and the possibility to use vectorized maps which offer smooth zooming/dezooming and can allow us to turn the heading of the underlying map while keeping the labels orientation up right (which was not possible with QGC's raster maps). Since this project was fully built by the CVLAD team, it's a lighter software than QGC thus easier to customize and add new features. We also don't always have to get it up to date with QGC's buggy daily builds.

    The NavDisplay implementation is pretty much self explanatory, but everything should be well documented in the NavDisplay repository. We took the time to revisit & cleanup & comment most of the code, so you can look around starting with the README.

7. **Peregrine :** It's the system around the Lidar used on the aircraft. It is mostly used to scan and find a safe landing point when approaching/hoving the landing zone.


## Mavlink
Mavlink is the underlying protocol to communicate most of the information to control the aircraft between systems, to communicate any information about the mission or the status of the systems. It is a protocol based on UDP packets mostly implemented in C. To get a better understanding of this protocol and how it affects our system, I would suggest reading a few of the following links:

- Root of the mavlink documentation website (you will find almost everything you need related to mavlink somewhere in there) : https://mavlink.io/en/
- Mission protocol microservice (most used between applications/QGCs) : https://mavlink.io/en/services/mission.html
- Heartbeat protocol (used everywhere there's mavlink) : https://mavlink.io/en/services/heartbeat.html
- You might encounter some issues with the parameter protocol which can be worth a read if you have extra free-time, but it's only used by QGC when connecting to the MavNRC for the first time : https://mavlink.io/en/services/parameter.html
- The ping protocol might be useful if you want to mesure the speed of the connection: https://mavlink.io/en/services/ping.html
- The entire Guide is useful, but I would strongly suggest the following two sections of the guide to get started : https://mavlink.io/en/guide/serialization.html and https://mavlink.io/en/guide/routing.html


## Other systems (Peregrine, Supervisor, Offline planner, etc.)
I (Jeremi) did not have the chance to work on/with these systems enought to give a precise description of what they do unfortunately. Other members of the team would probably give a better description than I so please refer to them if you have any questions regarding those systems.