# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 14:38:50 2021

@author: James Horner
"""

from xml.etree import ElementTree as ET
from datetime import datetime
from simplekml import Kml, Color
from polycircles import polycircles

import json
import sys
import os 


'''Global variable for the information that should be printed if the user specifies a help switch.'''
HELP_INFO = '''\n#####################    Help for the ExtractPlan python script.    ##################### 
              
This script is intended to be used to extract a flight plan from a QGroundControl 
PLAN file and use the information to generate a ForeFlight plan in FPL format 
and optionally a KML file for geofence data.

Usage:

py ExtractPlan.py [-plan][PLAN_FILE] [-fpl][FPL_FILE] [-kml][KML_FILE]

All the switches are optional and the program will resort to default behaviour if
a switch is not specified.

    [-plan][PLAN_FILENAME]:
        Switch followed by the name (with full path and extension) of the PLAN file 
        that the flight plan and geofence data will be extracted from. If this switch 
        is not used the user will be prompted to enter a PLAN file name.
        
    [-fpl][FPL_FILENAME]:
        Switch followed by name (with full path and extension) of the file that the 
        FPL formatted flight plan will be written to. If this switch is not used 
        the FPL file will be saved to the same directory as the PLAN file and will have 
        the same name but with the FPL extension.
    
    [-kml][KML_FILENAME]:
        Switch followed by the name (with full path and extension) of the KML file 
        that the geofence data will be written to. If this switch is not used the 
        program will not save the geofence data. If this swtich is used without 
        specifying a file name it will be inferred from the PLAN file name, akin
        to the FPL default behaviour.
'''

'''Global variable for the command line switches that can be used with the script.'''
SWITCHES = ['-plan', '-fpl', '-kml']

'''Global variable for the meters to feet conversion.'''
METERS_TO_FEET = 3.28084

"""
@brief Function validFile is used to determine if a file name is a valid input argument.

@details The function takes in a file name, expected extension, and whether the file should 
already exist. If the file should already exist, the function checks if the file exists and 
has the correct extension. If the file is not expected to exist, the function checks if the 
directory is valid and the file name has the correct extension.

@param file_name String name of the file to check if valid.
@param extension String extension that the file should have (without the dot).
@param exists Boolean for whether the file should already exist or not.

@returns Boolean True is the file name is valid, False otherwise.
"""
def validFile(file_name, extension, exists):
    #If the file should already exist,
    if exists:
        #Return True if the end of the file name matches the extension and file exists.
        return file_name[-len(extension):] == extension and os.path.isfile(file_name)
    
    #Else if the is not epected to exist,
    else:    
        #Return True if the end of the file name matches the extension and the folder the file is in exists.
        return file_name[-len(extension):] == extension and os.path.isdir(file_name[:file_name.rfind('\\')])



'''
@brief Function verifyFile is used to check if a file name is valid and if not get a valid file 
name from the user.

@details The function takes a file name, extension, and whether the file should already exist and 
if the file name is invalid retrieves a file name from the user until a valid file is supplied. By
using this function it is guaranteed that it will return a file name that is valid according to the 
input arguments.

@param file_name String name of the file to verify.
@param extension String extension that the file should have (without the dot).
@param exists Boolean for whether the file should already exist or not.

@returns String file name that meets the input criteria. If the file name is valid to start with it 
will just be returned, otherwise a different, valid, file name will be returned.
'''
def verifyFile(file_name, extension, exists):
    #Temporary variable for the file name to return.
    ret_file_name = ''
    #If the input file name is not valid,
    if not validFile(file_name, extension, exists):
        #Indicate that the file name is invalid.
        input_valid = False
        #While the file name is still invalid,
        while(not input_valid):
            #Get a new file name from the user.
            if exists:
                ret_file_name = input(extension.upper() + ' file either does not exist or has an incorrect extension. \nPlease try again: ').replace('\"', '')
            else:
                ret_file_name = input(extension.upper() + ' file either has an invalid directory or has an incorrect extension. \nPlease try again: ').replace('\"', '')
                
            #Check if the file is valid, if it is the while loop will be exited.
            input_valid = validFile(ret_file_name, extension, exists)
            
        #Tell the user that a valid file name was entered.
        print('Valid ' + extension.upper() + ' file entered. Continuing with execution.')
        
    #Else if the file name was valid to begin with,
    else:
        #set the return file name to be the original.
        ret_file_name = file_name
        
    #Return the temporary return file name variable.
    return ret_file_name

    

'''
@brief Function parseArgs is used to parse the command line arguments passed to the script and determine what behaviour should be used.

@details The function takes the raw list of input arguments from sys.argv and extracts the determines what behaviour the script should 
follow based on what switches are used and what arguments are supplied alongside the scripts. After parsing the arguments the function 
returns a tuple with the names of the PLAN, FPL, and optionally the KML file that should the script output should be read from and 
written to. 

@param args List of command line arguments supplie to the script from sys.argv

@returns Tuple of the form (PLAN_FILE, FPL_FILE, KML_FILE) that conatins the file names that should be used for the rest of the execution.
'''
def parseArgs(args):
    #Declare variables for the file names that will be returned in a tuple.
    plan_file_name = ''
    fpl_file_name = ''
    kml_file_name = ''
    #Declare a variable for whether the function should infer the name of the KML file from the
    #name of the PLAN file, as is the case if the -fpl switch is not used.
    infer_kml_name = False
    
    #If the command line arguments contain a help switch of some sort,
    if '-help' in args or 'help' in args or 'h' in args or '-h' in args:
        #Print the help information, wait for the user to press enter, then exit.
        print(HELP_INFO)
        input('Press Enter to exit.')
        exit(0)
        
    #For each argument in the list,
    for a in range(len(args)):
        #If the argument starts with a dash, it could be a switch,
        if args[a][0] == '-':
            #If it is in the valid switches,
            if args[a].lower() in SWITCHES:
                
                #If it is the -plan switch and there is an argument after the switch that isn't another switch,
                if args[a].lower() == '-plan' and (a + 1) < len(args) and args[a + 1][0] != '-':
                    #Get the name of the PLAN file from the next argument.
                    plan_file_name = verifyFile(args[a + 1], 'plan', True)
                
                #Else if it is the -fpl switch and there is an argument after the switch that isn't another switch,
                elif args[a].lower() == '-fpl' and (a + 1) < len(args) and args[a + 1][0] != '-':
                    #Get the name of the FPL file from the next argument.
                    fpl_file_name = verifyFile(args[a + 1], 'fpl', False)
                    
                #Else if it is the -kml switch,
                elif args[a].lower() == '-kml':
                    #If there is an argument after the switch that isn't another switch,
                    if (a + 1) < len(args) and args[a + 1][0] != '-':
                        #Get the name of the KML file from the next argument.
                        kml_file_name = verifyFile(args[a + 1], 'kml', False)
                    #Else if there is not an argument after the switch or it is another switch,
                    else:
                        #Flag that the name of the KML file should be infered from the name of the PLAN file.
                        infer_kml_name = True
                        
            #If the switch argument is not in the list of valid switches,
            else:
                #Tell the user that an invalid switch was used, print the valid switches, then exit.
                print('Invalid switch used, valid switches are: ')
                for s in SWITCHES:
                    print('\t' + s)
                exit(0)
        
    #If the PLAN file name is still empty after parsing the input arguments, meaning the -plan switch wasn't used or 
    #a file name wasn't given after the switch.
    if plan_file_name == '':
        #Get a PLAN file name ffrom the user and verfiy that it is valid.
        plan_file_name = input('PLAN file was not specified correctly in arguments, please enter a PLAN file: ')
        plan_file_name = verifyFile(plan_file_name.replace('\"', ''), 'plan', True)
    
    #If the FPL file name is still empty after parsing the input arguments, meaning the -fpl switch wasn't used or 
    #a file name wasn't given after the switch.
    if fpl_file_name == '':
        #Infer the FPL file name from the PLAN file name and directory.
        fpl_file_name = plan_file_name[:-4] + 'fpl'
    
    #If the -kml switch was used but a file name wasn't specified after the switch,
    if infer_kml_name:
        #Infer the KML file name from the PLAN file name and directory.
        kml_file_name = plan_file_name[:-4] + 'kml'
            
    #Return the file names in a tuple that can unpacked on the calling code's end.
    return (plan_file_name, fpl_file_name, kml_file_name)
     


'''
@brief Function writeFPL is used to write flight plan data into a file in the FPL format.

@details The function takes the mission data from a JSON PLAN file and converts it to the XML based FPL format then saves the 
result in a file specified by file_name. 

@param file_name String name of the file to save the flight plan into.
@param mission_data Dictionary containing the flight plan data from a JSON based PLAN file.

@returns None.
'''
def writeFPL(file_name, mission_data):
    #Using ElementTree start constructing the XML data starting with the root 'flight-plan' element.
    root = ET.Element('flight-plan')
    root.set('xmlns' ,'http://www8.garmin.com/xmlschemas/FlightPlan/v1')
    #Create the necessary sub elements of 'flight-plan' according to the PLAN XSD.
    created = ET.SubElement(root, 'created')
    created.text = datetime.now().isoformat(timespec='seconds') + 'Z'
    wp_table = ET.SubElement(root, 'waypoint-table')
    
    #Declare a waypoint id variable that will be combined with the WP id from the PLAN
    wp_id = 'WP'
    #Declare a list that will contain contain all the waypoint subelements for later processing
    wps = []
    
    #For all mission items in the PLAN file,
    for w in mission_data['mission']['items']:
        #If the mission item is a SimpleItem and is either a waypoint, landing, or takeoff command,
        if w['type'] == 'SimpleItem' and w['command'] in [16, 21, 22]:
            #Create a new 'waypoint' subelement.
            wp = ET.SubElement(wp_table, 'waypoint')
            
            #Create the necessary sub elements of 'waypoint' according to the PLAN XSD.
            wp_identifier = ET.SubElement(wp, 'identifier')
            #Use the 'doJumpId' element from the item so the waypoint numbers match up in QGC and ForeFlight.
            wp_identifier.text = wp_id + str(w['doJumpId'])
            
            wp_type = ET.SubElement(wp, 'type')
            wp_type.text = 'USER WAYPOINT'
            
            wp_cc = ET.SubElement(wp, 'country-code')
            
            #Add the lat/lon subelements that specify the waypoint location.
            lat = ET.SubElement(wp, 'lat')
            lat.text = str(w['params'][4])
            lon = ET.SubElement(wp, 'lon')
            lon.text = str(w['params'][5])
                        
            comment = ET.SubElement(wp, 'comment')

            alt = ET.SubElement(wp, 'elevation-ft')
            alt.text = str(w['params'][6] * METERS_TO_FEET)

            #Add the waypoint to the list of waypoints.            
            wps.append(wp)
    
    #Create the route that will be displayed in ForeFlight.
    route = ET.SubElement(root, 'route')
    route_name = ET.SubElement(route, 'route-name')
    #Set the route name to be the first waypoint id to the last waypoint id.
    route_name.text = wps[0].find('identifier').text + ' TO ' + wps[-1].find('identifier').text
    fpi = ET.SubElement(route, 'flight-plan-index')
    fpi.text = '1'
    #For each waypoint,
    for w in wps:
        #Add a route point for each waypoint.
        rp = ET.SubElement(route, 'route-point')
        
        rp_identifier = ET.SubElement(rp, 'waypoint-identifier')
        rp_identifier.text = w.find('identifier').text
        
        rp_type = ET.SubElement(rp, 'waypoint-type')
        rp_type.text = w.find('type').text
        
        rp_cc = ET.SubElement(rp, 'waypoint-country-code')
    
    #Create a ElementTree structure that has a write to file method.
    elems = ET.ElementTree(root)
    
    #Write the ElementTree to the file specified by file_name.
    with open(file_name, 'wb+') as fpl_file:
        elems.write(fpl_file, encoding='utf-16')
        
    return



'''
@brief Function writeKML is used to write geofence data into a file in the KML format.

@details The function takes the geofence data from a mission in a JSON PLAN file and converts it to the XML based KML 
format then saves the result in a file specified by file_name. 

@param file_name String name of the file to save the geofence data into.
@param mission_data Dictionary containing the flight plan data from a JSON based PLAN file.

@returns None.
'''
def writeKML(file_name, mission_data):
    #If the mission_data contains geofences,
    if len(mission_data['geoFence']['circles']) > 0 or len(mission_data['geoFence']['polygons']) > 0:
        #Create an instance of simplkml.
        kml = Kml(open=1)
        
        index = 0
        #For each polygonal geofence in the mission,
        for p in mission_data['geoFence']['polygons']:
            #Create a new KML multi geometry object and add a polygon to it.
            multigeo = kml.newmultigeometry(name='Polygon Geofence ' + str(index))
            poly = multigeo.newpolygon()
            
            #Get the boundary of the geofence as a list of coordinates in the form (lat, lon).
            boundary = []
            for v in p['polygon']:
                boundary.append((v[1], v[0]))
            boundary.append(boundary[0])
            
            #Set the outer boundary of the polygon to be the boundary.
            poly.outerboundaryis = boundary
            
            #Style the polygon based on whether it should be included or excluded.
            multigeo.style.linestyle.color = Color.red
            if p['inclusion']:
                multigeo.style.polystyle.fill = 0
            else:
                multigeo.style.polystyle.fill = 1
                multigeo.style.polystyle.color = Color.changealpha("77", Color.orange)

            index += 1
        
        index = 0
        #For each circular geofence in the mission,
        for c in mission_data['geoFence']['circles']:
            #Create a new KML multi geometry object and add a polygon to it.
            multigeo = kml.newmultigeometry(name='Circular Geofence ' + str(index))
            poly = multigeo.newpolygon()
            
            #Create a polygonal approximation of the circular geofence.
            pc = polycircles.Polycircle(latitude = c['circle']['center'][0],
                                        longitude = c['circle']['center'][1], 
                                        radius = c['circle']['radius'], 
                                        number_of_vertices = 10 + int((c['circle']['radius'] * 2 * 3.1415) / 10))
            
            #Set the multigeomtery polygon boundary to be the polycircle boundary.
            poly.outerboundaryis = pc.to_kml()

            #Style the polygon based on whether it should be included or excluded.
            multigeo.style.linestyle.color = Color.red
            if c['inclusion']:
                multigeo.style.polystyle.fill = 0
            else:
                multigeo.style.polystyle.fill = 1
                multigeo.style.polystyle.color = Color.changealpha("77", Color.orange)
                
            index += 1
        
        #Save the geofences in a KML file specified by file_name.
        kml.save(file_name)
        
    #If the mission data does not conatin any geofences,
    else:
        #Let the user know that no KML file will be generated.
        print('No geofences specified in PLAN file so KML file will not be generated.')

    return

    
''' Main function '''
def main(args):
    #Extract the file names from the command line arguments.
    plan_file_name, fpl_file_name, kml_file_name = parseArgs(args)
            
    #Open the plan file in read mode and load it as JSON.
    with open(plan_file_name, 'r') as plan_file:
        plan_data = json.load(plan_file)
        
        #Write the PLAN file data into an FPL file.
        writeFPL(fpl_file_name, plan_data)
        
        #If there is a KML file name,
        if kml_file_name != '':
            #Write the geofence data into a KML file.
            writeKML(kml_file_name, plan_data)
            
    return
        


main(sys.argv)
    
    
    
    
    
    