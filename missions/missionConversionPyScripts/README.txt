ExtractPlan install requirements:
1. First download and install the latest python release from https://www.python.org/downloads/ making sure to use the add python to your PATH in the installer.
2. Open a command prompt and run the command "pip install simplekml" to install the KML library for the geofences.
3. Next run "pip install polycircles" to install the library that's used to approximate circular geofences in KML.
4. All finished and you should be good to go. 

Running the ExtractPlan script:
1. Open a command prompt in the directory where you have saved the script. This can be done by opening the containing folder in file explorer and typing "cmd" in the address bar.
2. In the command prompt run the "py ExtractPlan.py -help" which will bring up a guide to using the script with a description of the different available switches.


