#pragma once
#include "log.h"
#include <stdint.h>		 // Needed for fixed width types e.g. uint32_t
#include <stdio.h>       // added this to handle the use of the FILE specifier

// KE made this a struct, as it seems to make more sense
typedef struct {
	int recording;	// Set to 1 when recording
	int filenum;	// Contains the current filenmuber
	int	start_recording;	// Flag to trigger start of recording
	int	stop_recording;		// flag to trigger stop recording
	int		event_on;		// Set to 1 to signal event on
	int		event_number;	// Incremented Event number
	float	kb_logged;	// kb logged to disk
	char    frl_filename[256]; // The name of our output frl file
	char	flightDataRootDir[256];	// Still need to figure out the differences between these
	char	filepath_string[256];
} logControl_t;

extern logControl_t logControl_global;

extern uint32_t event_mark_global;   // The event mark is bitwise or combinations of several possibel event sources:
									// Bit0 = FBW Engage/Disengage
									// Bit1 = SimEngage
									// Bit2 = EPEvent (EP Col FS0, bottom right toggle switch)
									// NOTE March 25 2014: The above was the intent, but it turns out that transcribe doesn't support this, and would need to be re-written. That's a job for a later date....


extern logfile_t file[MAX_LOGFILES];
