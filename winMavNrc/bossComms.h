#pragma once
#include "geo.h"

#pragma pack(push,4)
typedef struct {
#define WPT_PREVIEW_LENGTH 3
	int lpNo; // What landing point we are on within the current mission (if 0 means no LP data yet)
	double lpLat; // lat lon coordinates of the LP
	double lpLon;
	int missionNo; // The number of the current mission we are running...saved in mavNrc processMissionItemInt()
	int missionItemNo;
	int isMissionStarted; // 1 = yes, 0 = no
	int isLandingLeg; // 1 = yes, true, 0 = not the landing leg
	double lat;  // WGS 84 Latitude of the Item
	double lon;  // WGS 84 longitude of the item
	float  alt;  // MSL Alt of the item in meters
	float  yaw;  // Yaw in deg
	float  speed; // Speed in m/s
	float  horzAcceptRadiusM; // horizontal Acceptance radius in meters
	float  vertAcceptRadiusM; // vertical acceptance radius in meters
	int	previewLength; // the actual # of waypoints in the preview (must be less than WPT_PREVIEW_LENGTH
	double latNext[WPT_PREVIEW_LENGTH]; // latitude of next (up to 3) waypoints for path drawing
	double lonNext[WPT_PREVIEW_LENGTH]; // longitude of next (up to 3) waypoints
	char description[10]; // Text description of the item (leave all text to the end
} bossMissionData_t;

typedef struct {
	int lpNo; // What landing point we are on within the current mission (if 0 means no LP data yet)
	double lpLat; // lat lon coordinates of the LP
	double lpLon;
	int missionNo; // The number of the current mission we are running...saved in mavNrc processMissionItemInt()
	int missionItemNo;
	int isMissionStarted; // 1 = yes, 0 = no
	int isLandingLeg; // 1 = yes, true, 0 = not the landing leg
	double lat;  // WGS 84 Latitude of the Item
	double lon;  // WGS 84 longitude of the item
	float  alt;  // MSL Alt of the item in meters
	float  yaw;  // Yaw in deg
	float  speed; // Speed in m/s
	float  horzAcceptRadiusM; // horizontal Acceptance radius in meters
	float  vertAcceptRadiusM; // vertical acceptance radius in meters
} bossMissionDataToDisk_t;
#pragma pack(pop)



void *sendBossMissionThread(void *arg);
int sendMissionItemToBoss(bossMissionData_t myMissionData);
void writeBossMissionDataToDisk(bossMissionData_t myBossData);
// Boss related functions
void updateBossMissionData(void);
void updateBossLpData(double lat, double lon);
void clearBossMissionData(void); // clear the gloabl saving boss mission data

bossMissionData_t updateBossMissionDataForItem(int itemNo, bossMissionData_t prevScannedMissionData);
extern float get_bearing_to_next_waypointKE(double lat_now, double lon_now, double lat_next, double lon_next, double *bearing);//located in geo.cpp

extern float get_distance_to_point_global_wgs84(double lat_now, double lon_now, float alt_now, double lat_next, double lon_next, float alt_next, float *dist_xy, float *dist_z);  //located in geo.cpp
