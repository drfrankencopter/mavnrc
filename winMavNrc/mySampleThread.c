#include <stdio.h>
#include "threads.h"
#include "mySampleThread.h"
#include "sleep.h"



void *mySampleThread1(void *arg) {
	//This is just a dumb thread that echos to the screen a few times
	int i;
	printf("mySample Thread1 has been spawned\n");
	for (i = 0; i < 100; i++) {
		printf("My sample thread1 is writing to console: %d\n", i);
		usleep(100000); // About 10 Hz
	}
	return 0; // I'm done!
} // end sampleThread

void *mySampleThread2(void *arg) {
	//This is just a dumb thread that echos to the screen a few times
	int i;
	printf("mySample Thread2 has been spawned\n");
	for (i = 0; i < 50; i++) {
		printf("My sample thread2 is writing to console: %d\n", i);
		usleep(200000); // About 5 Hz
	}
	return 0; // I'm done!
} // end sampleThread