/*
    Mission Protocol Helper Functions
    
    File name   : sendToRemoteThread.c
    Date        : 1/19/2022 2:06:49 PM
    Author      : Levesqueje
    Description : This file is holding a thread used to send periodic information to all remote instances.
                  This thread is launched from the recvFromRemoteThread once the socket to send/recv is initialized
                  correctly.
    TODOS : Not noted.
*/

// Standard librairies includes
#include <string.h>

#ifdef _WIN32
#include <ws2tcpip.h>
#endif

// External installed librairies <>
#include <standard/mavlink.h>

// Project documents includes ""
#include "equivalenceCOREsharedMem.h" // Shared memory global access
#include "socket.h"
#include "sendToRemoteThread.h"
#include "NrcDefaults.h"
#include "updateVehicleMavLinkStatusThread.h"
#include "sleep.h"

#define HEARTBEAT_RATE 1  // 1 Hz

// Ugly global pointer
extern missionData_t* mission_data;
extern remotes_infos_t remotesInfo;
/*
    sendToRemoteThread
    Description : This thread is the main thread used to communicate with every remote instance of QGroundControl.
                  It uses the recvFromRemoteThread's socket to send periodic information to the remote instances.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          : None

*/
void sendToRemoteThread(void* arg) {
    int sockfd = *(int*)arg;        // Same socket as in the recvFromRemoteThread.c
    struct sockaddr_in broadcastAddr;
    struct sockaddr_in gcsAddr;
    int timeCounter = 0;
    
    printf("SEND TO REMOTE THREAD LAUNCHED\n");

    
    while (1) {
        double interval = 0.01; // Executes the loop each 0,01 seconds
        int nbSamplesPerHz = (int)1/0.01;
        usleep(interval/1e-06);
        
        if (++timeCounter == nbSamplesPerHz)
            timeCounter = 0;

        if ((timeCounter % nbSamplesPerHz/HEARTBEAT_RATE) == 0) {
            mavlink_message_t heartbeatMsg;
            mavlink_msg_heartbeat_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &heartbeatMsg, MAV_TYPE_HELICOPTER, MAV_AUTOPILOT_GENERIC_MISSION_FULL, 0, 0, MAV_STATE_UNINIT);
            sendMavlinkMessage(sockfd, &heartbeatMsg, &broadcastAddr, sizeof(broadcastAddr));
            
            //verifyRemotesConnection(&mission_data->remotesInfo);
        }
        
        if (remotesInfo.numRemotes > 0) {
            
            if (mission_data->newMissionFromGcs) {
                // If there's a new mission, send the new mission count to the remote instances of GCS
                printf("SENDING NEW MISSION COUNT TO ALL REMOTES\n");
                sendMissionCount(sockfd, mission_data, &broadcastAddr, sizeof(broadcastAddr), 0, 0, MAV_MISSION_TYPE_MISSION);
                mission_data->newMissionFromGcs = 0;
            }
            else if (mission_data->newMissionFromRemote) {
                // If there's a new mission coming from a remote instance, send to everyone plus to the local GCS
                int resRemote = sendMissionCount(sockfd, mission_data, &broadcastAddr, sizeof(broadcastAddr), 0, 0, MAV_MISSION_TYPE_MISSION);
                int resGcs = sendMissionCount(sockfd, mission_data, &gcsAddr, sizeof(gcsAddr), 0, 0, MAV_MISSION_TYPE_MISSION);
                if (resGcs && resRemote)
                    mission_data->newMissionFromRemote = 0;
            }
            
        }
        else if (mission_data->newMissionFromGcs || mission_data->newMissionFromRemote) {
            // No remotes, but there's a new mission. Don't send it.
            mission_data->newMissionFromGcs = 0;
            mission_data->newMissionFromRemote = 0;
        }
    }
    
#ifdef _WIN32
    WSACleanup();
#endif
}

/*
    sendMessageToAllRemotes
    Description : This function sends a mavlink message to all remotes.
                
                  Easily editable to allow broadcasting/multicasting messages.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : 1 if all messages were sent successfully, 0 if at least a message failed
    Inputs          :
        - sockfd : file descriptor of the socket to send from
        - msg : mavlink message to send to all remotes
        - remotes : shared struct holding the remotes list and the number of remotes connected

*/
int sendMessageToAllRemotes(int sockfd, const mavlink_message_t* msg, const remotes_infos_t* remotes) {
    // We send mavlink messages one by one to all the remotes.
    // In order to avoid sending one by one to all the remotes, we would need
    // to find a way to bypass the loopback on a broadcast socket or find a way
    // to make QGC subscribe to a multicast group.
    //
    // When I tried to broadcast, every QGC instance respond on the broadcast address.
    // This means that every other remote instance gets all the traffic from every other
    // instance. This thread will also receive every packet that it sends and this lead to a
    // flood of messages on the recvfrom on the socket. There was a lot of delay and there were
    // problems communicating the mission information.
    //
    // Multicast worked liked a charm with a helper program on top of each QGC instance to handle
    // the subscription to the multicast group. We aren't taking this approach because we want
    // to be able to upgrade QGC easily and because it's way more simple.
    int i, res = 1;
    for (i = 0; i < remotes->numRemotes; ++i) {
        if (!sendMavlinkMessage(sockfd, msg, &remotes->remotes[i].addr, remotes->remotes[i].addrLen))
            res = 0;
    }
    return 0;
}

/*
    verifyRemotesConnection
    Description : This function is usually called periodically to verify that there's a response
                  (heartbeat) from each remote connected. If the number of expected heartbeats exceeds
                  a certain limit, MavNRC deletes the unresponsive remote from its list of remotes.
 
                  This allows to stop sending information to a remote instance that's not listening.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - remotesInfo : Struct holding the list of remotes and the number of remotes
*/
//void verifyRemotesConnection(remotes_infos_t* remotesInfo) {
//    int i;
//
//    for (i = 0; i < remotesInfo->numRemotes; ++i) {
//        if (remotesInfo->remotes[i].misses > 8) {
//            // After 8 misses, disconnect it
//            LOG("DELETING REMOTE\n");
//
//            // If we are at the end, just set it to zero and decrement the num
//            if (i == remotesInfo->numRemotes-1) {
//                memset(&remotesInfo->remotes[i], 0, sizeof(remotesInfo->remotes[i]));
//            }
//            else {
//                // Otherwise, shift all thoses on the right to the left by 1
//                int j;
//                for (j = i; i < remotesInfo->numRemotes-1; ++j) {
//                    remotesInfo->remotes[j] = remotesInfo->remotes[j+1];
//                }
//            }
//            --remotesInfo->numRemotes;
//        }
//    }
//}
