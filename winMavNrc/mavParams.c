#include <stdio.h>
#ifdef _WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#include "mavParams.h"
#pragma clang diagnostic pop
#include "NrcDefaults.h"
#include "mavProtocol.h"
#include <stdlib.h>
int numMavParams = 0;
mavlink_param_value_t myMavParams[MAXPARAMS];

// This will have some helper functions for dealing with parameters


int readParamsFromDisk(void) {
	// This tries to read the parameter file from disk (sample files are contained in the QGC source)

#ifdef _WIN32
	char *myParamsFileName = "\\defaultParams.txt";
#else
    char *myParamsFileName = "/defaultParams.txt";
#endif

	char buff[FILENAME_MAX]; // Filename Max defined in stdio.h
	GetCurrentDir(buff, FILENAME_MAX);
	printf("Reading defaultParams from working dir: %s\n", buff);

	strcat(buff, myParamsFileName);

	FILE *myParamsFd = fopen(buff, "r");
	
	int numParamsFound = 0;
	if (myParamsFd != NULL) {
		rewind(myParamsFd); // Go to start of file
		while (numParamsFound < MAXPARAMS) {
			int sysId, compId, paramType;
			float paramVal;
			char paramName[32] = { 0 };
			char *retVal = fgets(buff,FILENAME_MAX, myParamsFd);
			if (retVal == NULL) break; // EOF or Error
			printf("Read Line: %s\n", buff);
			// Now process the line
			if (buff[0] != '#') { // Iglore comment lines begining with #
				int scanRet = sscanf(buff, "%d\t%d\t%s\t%f\t%d\n", &sysId, &compId, paramName, &paramVal, &paramType);
				if (scanRet == 5) {
					printf("Added this param: %s to struct\n", paramName);
					// Now add it to the parameters list
					myMavParams[numParamsFound].param_count = 0;				  // We don't know the final number of parameters while parsing
					myMavParams[numParamsFound].param_index = numParamsFound;
					myMavParams[numParamsFound].param_value = paramVal;
					strncpy(myMavParams[numParamsFound].param_id, paramName, 16); // At most 16 bytes including null
					myMavParams[numParamsFound].param_type = paramType;
					numParamsFound++;
					numMavParams = numParamsFound;					
				}
			}
		}
	} else {
		printf("ERROR: Couldn't open defaultParams.txt from %s\n", buff);
        return -1;
	}

	// Re-parse the parameters to update their parameters
	for (int i = 0; i < numMavParams; ++i) {
		myMavParams[i].param_count = numMavParams;
	}

    return 0;
}

int findParamByName(char paraName[16]) {
	//Looks through the parameter table to find a match
	// returns the index associated with the match, or -1 if not found
	bool paramFound = false;
	int i;
	for (i=0; i < MAXPARAMS; i++) { // Loop thru
		if (strcmp(paraName, myMavParams[i].param_id) == 0) {
			// Match
			paramFound = true;
			break; //out of the for loop
		}
	}
	if (paramFound) return i;
	else return -1; // Not found
}

/*
    sendParametersThread
    Description : This function represents a separate thread that will be launched in order to communicate the parameters
                  over the network to a specified address.

                  It was chosen to send the parameters in a separate thread, because the mavlink documentation recommends to allow
                  pauses after each parameter sent. Sending the parameters in a separate thread allows to continue the communication
                  on the 'main' thread without sleeping it.

				  UPDATE : This said, we had to remove the sleeps in the thread because it was doing some weird behavior on other machines
						   that parameters wouldn't be uploaded properly onto the QGC instance. This caused problems when uploading a mission
						   and when starting a new mission.

    Author			: Jeremi Levesque
    Date			: 02/07/2022
    Return value	: None
    Inputs			:
        - a : struct of parameters holding the following attributes
            > sockfd : File descriptior of the socket from which we want to send the parameters from
            > shm      : Pointer to the shared_memory
            > to     : pointer to the Gcs Address

*/
void* sendParametersThread(void* a) {
    sendParametersArgs_t* args = a;

    int sockfd = args->sockfd;
    struct sockaddr_in* to = &args->to;
    mavlink_message_t msg;
    int i;

    for (i = 0; i < args->numParams; ++i) {
        // Loop through all our parameters in the array
        mavlink_msg_param_value_pack(
            MY_MAV_SYS_ID,
            MY_MAV_COMP_ID,
            &msg,
            args->params[i].param_id,
            args->params[i].param_value,
            args->params[i].param_type,
            args->params[i].param_count,
            args->params[i].param_index);
        printf("PARAM : ");
        sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
    }
    
    free(a);

    return NULL;
}
