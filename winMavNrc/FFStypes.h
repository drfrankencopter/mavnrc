#pragma once
#include "node_configuration.h" // Needed for FCC NUM CHS
#pragma pack(push,4)
typedef struct { // contains all the data read by the FFS - mainly used by FCC as input to control systems
	double ffs_system_time;
	uint32_t ffs_switches;
	float ffs_control_position_lateral;
	float ffs_control_position_long;
	float ffs_control_position_pedal;
	float ffs_control_position_collective;
	float ffs_commanded_position_lateral;
	float ffs_commanded_position_long;
	float ffs_commanded_position_pedal;
	float ffs_commanded_position_collective;
	float ffs_applied_force_lateral;
	float ffs_applied_force_long;
	float ffs_applied_force_pedal;
	float ffs_applied_force_collective;
	float ffs_trim_position_lateral;
	float ffs_trim_position_long;
	float ffs_trim_position_pedal;
	float ffs_trim_position_collective;
	float sidearm_applied_force_lateral;
	float sidearm_applied_force_long;
	float sidearm_applied_force_pedal;
	float sidearm_applied_force_collective;
	float collective_thumb_wheel_pot;
} ffs_output_t; // called the same in Bell 205 ICD

typedef struct { // contains configuration info for the FFS to be displayed on the CDU
	double ffs_system_time;
	uint32_t ffs_engaged;
	float lateral_mass;
	float longitudinal_mass;
	float pedal_mass;
	float collective_mass;
	float lateral_gradient;
	float longitudinal_gradient;
	float pedal_gradient;
	float collective_gradient;
	float lateral_damping;
	float longitudinal_damping;
	float pedal_damping;
	float collective_damping;
	float lateral_breakout;
	float longitudinal_breakout;
	float pedal_breakout;
	float collective_breakout;
	float lateral_friction;
	float longitudinal_friction;
	float pedal_friction;
	float collective_friction;
	char ffs_stick_model[32];
} ffs_configuration_t; // called the same in Bell 205 ICD

typedef struct {	// defines parameters in the notch
	float 	size;
	float	mass;
	float	damping;
	float	friction;
	float	frat;
} ffs_notch_params_t;

typedef struct {
	float	mass;
	float	gradient;
	float	damping;
	float	friction;
	float	frat;
	float	breakout;
	float	throw_limit;
	float	trim_rate;
	ffs_notch_params_t notch;	
} ffs_axis_params_t;


typedef struct { // FFS Parameters for sending an entire model file via Ethernet
	ffs_axis_params_t	lat;
	ffs_axis_params_t	lon;
	ffs_axis_params_t	ped;
	ffs_axis_params_t	col;
} ffs_model_params_t;	
	
// Note that ffs205_analog_input_t is used as part of the next type (ffs205_analog) which then includes
// the dio ports and d2a's

//used for sending on network
// KE Change Here Jan 31, 2020 removing attribute(packed) in favour of pragma(pack,1)
#pragma pack(push,1)
typedef struct {
	#ifdef DBL_TIME
		#ifdef SPLIT_DBL
			float time;
			float time_dec;
		#else
			double time;
		#endif
	#else
		float time;
	#endif
	float ch[FFS205_NUM_CHS];	// analog inputs
} ffs205_analog_input_t;	// based on a variant of analog_t but with no chars or d2a's

typedef struct {
	#ifdef DBL_TIME
		#ifdef SPLIT_DBL
			float time;
			float time_dec;
		#else
			double time ;
		#endif
	#else
		float time;
	#endif
	float ch[FFS205_NUM_CHS];	// analog inputs
	unsigned char dio_port_a;
	unsigned char dio_port_b;
	unsigned char dio_port_c;
	float command[FFS205_NUM_D2A];	// Commanded actuator position (in)
	float d2a[FFS205_NUM_D2A] ;		// Moog valve command volts
} ffs205_analog_t;	// based on a variant of analog_t

// used for writing to dat file
typedef struct {
	#ifdef DBL_TIME
		#ifdef SPLIT_DBL
			float time ;
			float time_dec ;
		#else
			double time ;
		#endif
	#else
		float time ;
	#endif
	float ch[FFS205_NUM_CHS/2];	// analog inputs
	unsigned char dio_port_a0;	// Note here that the difference between this struct and the 
	unsigned char dio_port_a1;	// ffs_analog_t struct is that the dio's are broken out into
	unsigned char dio_port_a2;	// one byte chars for each discrete IO. This is because FRL format
	unsigned char dio_port_a3;	// does not support a single bit data type
	unsigned char dio_port_a4;
	unsigned char dio_port_a5;
	unsigned char dio_port_a6;
	unsigned char dio_port_a7;
	unsigned char dio_port_b0;
	unsigned char dio_port_b1;
	unsigned char dio_port_b2;
	unsigned char dio_port_b3;
	unsigned char dio_port_b4;
	unsigned char dio_port_b5;
	unsigned char dio_port_b6;
	unsigned char dio_port_b7;
	unsigned char dio_port_c0;
	unsigned char dio_port_c1;
	unsigned char dio_port_c2;
	unsigned char dio_port_c3;
	unsigned char dio_port_c4;
	unsigned char dio_port_c5;
	unsigned char dio_port_c6;
	unsigned char dio_port_c7;
	float command[FFS205_NUM_D2A] ;	// Commanded actuator position (in)
	float d2a[FFS205_NUM_D2A];		// Moog valve command volts
} ffs205_analog_file_t;	// based on a variant of ffs205 analog_t, but tailored to write to FRL file format
#pragma pack(pop)

typedef struct {
	float pos_cmd;	// Actuator position command (inches)
	float proportional;	// proportional gain
	float derivative;	// derivative gain
	float deriv_filt_freq;	// break point of 1st order derivative filter
	float integral;		// integral gain
	float integral_limit;	// limit of integral command (volts)
	float bias;				// bias (in mv)
    float dither_amplitude;
    float dither_freq;
    float limiter_freq;			// Frequency in Hz for the high frequency force limiter
    float limiter_thresh; 		// Threshold force (in lbs) over which limiting starts
    float limiter_attack_time; 	// Time constant (seconds) for starting limiting
    float limiter_release_time; // Time constant (seconds) for returning to nominal gain
    float limiter_ratio;		// Controls the slope of limiting...higher #'s mean more effect (more hf gain reduction)
} ffs_control_t;


typedef struct {
	ffs_control_t	lateral;		// lateral control
	ffs_control_t	longitudinal;	// longitudinal control
	ffs_control_t	pedal;			// pedal control
	ffs_control_t	collective;		// collective control
    uint32_t   		control_mode;   // 0= use default gain values, 1 = feel.c model...force loop closure, 2 = simulink position cmd position loop closure
    uint32_t   		request_control_data;   // set to 1 for "send control data packet"
} ffs205_control_test_t;	// Used for testing servo actuators including gain setting

typedef struct {
    float proportional;
    float integral;
    float derivative;
    float total_volts;
    float hf_gain_reduction;	// The amount of gain reduction applied to high frequencies in the force limiter
} ffs205_axis_control_components_t; // Used for monitoring the internal workings of the closed loop controller

typedef struct {
    ffs205_axis_control_components_t lateral;
    ffs205_axis_control_components_t longitudinal;
    ffs205_axis_control_components_t pedal;
    ffs205_axis_control_components_t collective;
} ffs205_control_components_t;
#pragma pack(pop)

// Model storage struct
typedef struct auxilliary_models {
	char modelname[32];				/* The name of the stick/pedal model*/
	double param [ 52 ];            /* parameters                           */
	int flag;                       /* On if model loaded in this location  */
} auxmod_t;	// Use this type for storing model parameters


