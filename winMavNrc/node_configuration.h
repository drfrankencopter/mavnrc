#ifndef NODE_CONFIGURATION_H_
#define NODE_CONFIGURATION_H_

#define ANALOG_CHAN_LOW				0
#define ANALOG_CHAN_HIGH			31
#define ANALOG_FSAMP				5000
#define ANALOG_IRQ_DIVISOR			5
#define ANALOG_OUTPUT_DIVISOR		50
#define ANALOG_NUM_CHS				(ANALOG_CHAN_HIGH-ANALOG_CHAN_LOW+1)

#define HMU205_CHAN_LOW				0
#define HMU205_CHAN_HIGH			31
#define HMU205_FSAMP				5000
#define HMU205_IRQ_DIVISOR			5
#define HMU205_OUTPUT_DIVISOR		50
#define HMU205_NUM_CHS				(HMU205_CHAN_HIGH-HMU205_CHAN_LOW+1)

#define FCC205_CHAN_LOW				0
#define FCC205_CHAN_HIGH			31
#define FCC205_FSAMP				5000		// This is the speed A/D samples at
#define FCC205_IRQ_DIVISOR			5		
#define FCC205_OUTPUT_DIVISOR		50			
#define FCC205_NUM_CHS				(FCC205_CHAN_HIGH-FCC205_CHAN_LOW+1)

/*
#define FFS205_CHAN_LOW				0
#define FFS205_CHAN_HIGH			15
#define FFS205_FSAMP				5000
#define FFS205_IRQ_DIVISOR			3		// This controls how many samples per chan get filled in the FIFO before an interrupt is generated
#define FFS205_OUTPUT_DIVISOR		1		// // This is a divider on how often the analog thread (including feel() is run)..currently 2.0kHz
#define FFS205_NUM_CHS				(FFS205_CHAN_HIGH-FFS205_CHAN_LOW+1)
#define FFS205_NUM_D2A				4
#define FFS_OUTPUT_PACKET_DIVISOR 	17		// This determines how often packets are sent (FSAMP/IRQ_DIV/OUTPUT_DIV/PACKET_DIV). Currently 100 Hz
#define FFS_CONFIG_PACKET_DIVISOR	333		// This determines how often config packets are sent (FSAMP/IRQ_DIV/OUTPUT_DIV/PACKET_DIV)...should be 5 Hz		
*/


//#define FFS205_CHAN_LOW				0
//#define FFS205_CHAN_HIGH			31
//#define FFS205_FSAMP				5000
//#define FFS205_IRQ_DIVISOR			5		// This controls how many samples per chan get filled in the FIFO before an interrupt is generated
//#define FFS205_OUTPUT_DIVISOR		50		// // This is a divider on how often the analog thread (including feel() is run)..currently 2.0kHz
#define FFS205_NUM_CHS				32
#define FFS205_NUM_D2A				4
//#define FFS_OUTPUT_PACKET_DIVISOR 	1		// This determines how often packets are sent (FSAMP/IRQ_DIV/OUTPUT_DIV/PACKET_DIV). Currently 100 Hz
//#define FFS_CONFIG_PACKET_DIVISOR	10		// This determines how often config packets are sent (FSAMP/IRQ_DIV/OUTPUT_DIV/PACKET_DIV)...should be 5 Hz		


#define N_HARMONICS					5
#define N_TERMS						(2*N_HARMONICS+1)
#define N_STATES					(2*N_TERMS)

#define B205_NUM_BLADES				1
#define B205_AZ_OFFSET				0.0

#define B412_NUM_BLADES				4
#define B412_AZ_OFFSET				283.0
#define B412_NUM_STRAIN				8

#ifdef BELL205
	#define	M						B205_NUM_BLADES
	#define AZ_OFFSET				B205_AZ_OFFSET
#elif defined BELL412
	#define	M						B412_NUM_BLADES
	#define AZ_OFFSET				B412_AZ_OFFSET
#endif

#define ROTOR_FREQ_NOM				5.4
#define OMEGA_ROTOR					(ROTOR_FREQ_NOM*2.0*M_PI)
#define DEG2RAD						(M_PI/180.0)
#define RAD2DEG						(180.0/M_PI)

#endif /*NODE_CONFIGURATION_H_*/
