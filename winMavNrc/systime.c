//
//  systime.c
//  iNS Test
//
//  Created by Kris Ellis on 11-11-07.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#include <time.h>
#include <math.h>
#include <stdint.h>
#include "systime.h"
#include <stdio.h>

#if defined (_WIN32) || defined (_WIN64)
#include <windows.h>
LONGLONG init_time;
#elif __APPLE__
#include <mach/mach_time.h>		// Note this is for mac use. Linux not yet supported
#include <unistd.h>
static double seconds_since_midnight;
static struct timespec ts;
static uint64_t start_time_tics;
#else
#include <unistd.h>
#endif

// For shared mem
#ifdef _WIN32
//Some hacks here for windows shared memory access
#include <windows.h>
#include <conio.h>

HANDLE hMapFileSysTime;    // Global for saving the handle to the shared memory
TCHAR sysTimeMemName[] = TEXT("sysTimeSharedMem");    // As also defined in the python code

#else
#include <fcntl.h>  // Needed for read/write definitions
#include <unistd.h> // needed for close()
#include <sys/mman.h>   // For POSIX shared memory via shm_open
const char *sysTimeMemName = "/sysTimeSharedMem";

static int hMapFileSysTime; // Regular file descriptor for Linux/Mac

#endif // End MacOs specif stuff

// Note this file has been hacked extensively versus the DRP code base. Lines have been
// commented instead of deleted for tracibility
//#include <rtai_lxrt.h> // Can't include real time stuff in iphone

static uint8_t isSysTimeSharedMemConnected = 0; // Default to not connected

sysTimeInit_t *sysTimeInitSharedMem;
#define BUF_SIZE_SYS_TIME sizeof(sysTimeInit_t)    //A convenience define, big enough to hold our data

static int time_initialized = 0; // Default to not initialized so 1st call to SysTime can init

double SysTime(void)
{
	if (!time_initialized) {
		Init_SysTime();
		time_initialized = 1;
	}

#if defined (_WIN32) || defined (_WIN64)
	// Insert windows timer code here
	LARGE_INTEGER freq, time;

  	QueryPerformanceFrequency(&freq);
  	QueryPerformanceCounter(&time);  	

    return (double)((double)(time.QuadPart-init_time) / (double)freq.QuadPart);; 
#elif __APPLE__
    // Had to comment out the real time clock call
	// return rt_get_time_ns()/1e9 - systime_offset;
    
    // This code is largely sourced from Apple's developer site, Q&A1398 "Mach Absolute Time Units"
    //  I've used the code base that does not require the CoreServices foundation to be included
    static mach_timebase_info_data_t sTimebaseInfo;
    uint64_t current_time_tics, elapsed;
    
    current_time_tics = mach_absolute_time();   // Call to get current time in ticks since last reset
    elapsed = current_time_tics - start_time_tics;
    // If this is the 1st time we've run we need to get the timebase. This is done my checking the
    // denominator of sTimebaseInfo
    if (sTimebaseInfo.denom == 0) {
        (void) mach_timebase_info(&sTimebaseInfo); // Get the timebase
        Init_SysTime(); // first time through we will init systime to be zero
    }
    // Now do the calculation to return system time in seconds with nanosecond precision
    return (double)elapsed*((double)sTimebaseInfo.numer/(double)sTimebaseInfo.denom)/1.0e9;
#else // Linux???
	double current_time;

	if (!time_initialized) {
		Init_SysTime();
		time_initialized = 1;
	}
	clock_gettime(CLOCK_MONOTONIC, &ts);	// Get the current machine time
	current_time = ts.tv_sec + 1e9*ts.tv_nsec;
	//return rt_get_time_ns()/1e9 - systime_offset;
	return ts.tv_sec - start_time.tv_sec + 1e-9*(ts.tv_nsec - start_time.tv_nsec);
}

#endif // End of MAC version
}

void Init_SysTime(void)
{
#if defined (_WIN32) || defined (_WIN64)
	LARGE_INTEGER time;
	QueryPerformanceCounter(&time);

	// Here is where we need to check to see if the shared mem has been set up, or if we are 'first to it'
    if (!isSysTimeSharedMemConnected) connectSysTimeSharedMemory();
	if (isSysTimeSharedMemConnected) {
		if (sysTimeInitSharedMem->isInitialized) {
			// Someone has already initialized systime
			init_time = sysTimeInitSharedMem->init_time;
		}
		else {
			// We need to initialize SysTime Shared mem
			init_time = time.QuadPart;
			sysTimeInitSharedMem->init_time = init_time;
			sysTimeInitSharedMem->isInitialized = 1;

		}
	}
	else {
		// We couldn't connect to shared mem, so lets just initialize our systime the old way
		init_time = time.QuadPart;
	}

#elif __APPLE__
	// clock_gettime(CLOCK_REALTIME, &ts);  // Apparently the clock_gettime function is not available in OS-X
	
    // The following line is used to attempt to refer system time to seconds since sunday midnight, however
    //  I've ignored this for quick & dirty testing...I'll leave this as a future Todo
    //seconds_since_midnight = fmod(ts.tv_sec + 1e9*ts.tv_nsec - 3600*utc_offset + 24*3600, 24*3600);

	if (!isSysTimeSharedMemConnected) connectSysTimeSharedMemory();
	if (isSysTimeSharedMemConnected) {
		if (sysTimeInitSharedMem->isInitialized) {
			// Someone has already initialized systime
			start_time_tics = sysTimeInitSharedMem->start_time_tics;
		}
		else {
			// We need to initialize SysTime Shared mem
			start_time_tics = mach_absolute_time();
			sysTimeInitSharedMem->start_time_tics = start_time_tics;
			sysTimeInitSharedMem->isInitialized = 1;

}
		}
	else {
		// We couldn't connect to shared mem, so lets just initialize our systime the old way
		start_time_tics = mach_absolute_time();
	}
    
        
        
	//systime_offset = rt_get_time_ns()/1e9 - seconds_since_midnight;
	return;
	// end of MAC version
#else 
	// NOTE: OTHER OS VERSION DOES NOT YET CONNECT TO SYSTIME SHARED MEM
	clock_gettime(CLOCK_MONOTONIC, &start_time);	// Get the current machine time to use as an offset

	printf("Initial time = %ld (s), %ld(ns)", start_time.tv_sec, start_time.tv_nsec);
	return;
#endif // end of other os (linux) version
}

void resetSysTime(void) {
	// This function will reset any sys time offset stored in shared mem
	// USE THIS FUNCTION CAREFULLY!!!!
	if (isSysTimeSharedMemConnected == 0) {
		// We haven't connected...let's try
		connectSysTimeSharedMemory();
	}
	// Here we need to branch for different OS's
#if defined (_WIN32) || defined (_WIN64)
	LARGE_INTEGER time;
	QueryPerformanceCounter(&time);
	init_time = time.QuadPart; // This will reset the internal variable
	if (isSysTimeSharedMemConnected == 1) {	// And if shared mem is connected we will overwrite the shared systime
		sysTimeInitSharedMem->init_time = init_time;
		sysTimeInitSharedMem->isInitialized = 1;
	}
#else
	start_time_tics = mach_absolute_time();
	if (isSysTimeSharedMemConnected == 1) {
		sysTimeInitSharedMem->start_time_tics = start_time_tics;
		sysTimeInitSharedMem->isInitialized = 1;
	}
#endif

}

// Connecting to the systime shared memory
#ifdef _WIN32
int connectSysTimeSharedMemory()
{
	hMapFileSysTime = CreateFileMapping(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD)
		BUF_SIZE_SYS_TIME,                // maximum object size (low-order DWORD)
		sysTimeMemName);                 // name of mapping object

	if (hMapFileSysTime == NULL)
	{
		printf("Could not create the SysTime shared mem file mapping object");
		return 1;
	}
	sysTimeInitSharedMem = (sysTimeInit_t *)MapViewOfFile(hMapFileSysTime,   // assign the map object to the shared data struct. Note: I get a truncation warning here. Is this a problem?
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,
		0,
		BUF_SIZE_SYS_TIME);

	if (sysTimeInitSharedMem == NULL)
	{
		printf("Could not map the SysTime shared mem view of file."),
			CloseHandle(hMapFileSysTime);

		return 1;
	}

	isSysTimeSharedMemConnected = 1;
	return 0;
}

int unlinkSysTimeSharedMemory() {
	printf("Unlinking SysTime shared memory");
	UnmapViewOfFile(sysTimeInitSharedMem);
	CloseHandle(hMapFileSysTime);
	isSysTimeSharedMemConnected = 0;
	return 0;
}
#else  // MACOS or Linux
int connectSysTimeSharedMemory(void) {
	hMapFileSysTime = shm_open(sysTimeMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set permissions to 666
	if (hMapFileSysTime < 0) {
		printf("Could not create the SysTime shared memory file descriptor via shm_open()");
		return 1;
	}
	ftruncate(hMapFileSysTime, BUF_SIZE_SYS_TIME);
	sysTimeInitSharedMem = (sysTimeInit_t *)mmap(NULL, BUF_SIZE_SYS_TIME, PROT_READ | PROT_WRITE, MAP_SHARED, hMapFileSysTime, 0);
	if (sysTimeInitSharedMem == MAP_FAILED) {
		printf("Could not map the SysTime shared memory via mmap.");
		return 1;
	}
	printf("Successfully Mapped SysTime Shared Memory");
	printf("SysTime Shared memory size is %ld bytes\n", BUF_SIZE_SYS_TIME);
	isSysTimeSharedMemConnected = 1;
	return 0;
}

int unlinkSysTimeSharedMemory(void) {
	//printf("Unlinking shared memory\n");
	munmap(sysTimeInitSharedMem, BUF_SIZE_SYS_TIME);
	close(hMapFileSysTime);
	isSysTimeSharedMemConnected = 0;
	return 0;
}
#endif
