//
//  serialToEthThreads.h
//  mavNrc
//
//  Created by Kris Ellis on 2020-07-30.
//  Copyright © 2020 Kris Ellis. All rights reserved.
//

#ifndef serialToEthThreads_h
#define serialToEthThreads_h

#include <stdio.h>


// NOTE: AS OF NOV 5 2021 FOR PEREGRINE TO WORK THE IP ADDRESS OF THE
//       ADAPTER ITS CONNECTED TO NEEDS TO BE 10.1.10.100

#define MAX_SER_BUFFER_CHARS 1024
#define SERIAL_TO_ETH_PORT 4001
#define MAVLINK_OVER_UDP_PORT 14601
#define PEREGRINE_IP "10.1.10.2"
#define SERIAL_TO_ETH_COM 1 // Not really needed, but the parse function lets you parse multiple chans at once

// Fun protos
long writeToSerialMavMsgCue(mavlink_message_t msg);
void sendSerialMavLinkThread(void *arg);
void serialToEthernetRcvThread(void *arg);
void sendPeregrineMissionCount(uint8_t missionType);  // Global fun needs to be called when full mission is received from GCS
#endif /* serialToEthThreads_h */
