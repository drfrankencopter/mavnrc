//
//  data_defs.c
//  mavNrc
//
//  Created by Kris Ellis on 2020-09-15.
//  Copyright © 2020 Kris Ellis. All rights reserved.
//

#include "data_defs.h"
#include "NrcDefaults.h"


void HG1700_Data_Def(buffer_t *buf)
{
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: HG1700 (%d Hz)\n", buf->def_hdr.mesg_minor, (int)HG1700_LOG_RATE);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"GpsTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"Status   (n/a)   : b2\n");
    pos += sprintf(&buf->data_def[pos],"Skips    (n/a)   : b1\n");
    pos += sprintf(&buf->data_def[pos],"Checksum (n/a)   : b1\n");
    pos += sprintf(&buf->data_def[pos],"Pitch    (deg)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Roll     (deg)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Heading  (deg)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Q        (deg/s) : f4\n");
    pos += sprintf(&buf->data_def[pos],"P        (deg/s) : f4\n");
    pos += sprintf(&buf->data_def[pos],"R        (deg/s) : f4\n");
    pos += sprintf(&buf->data_def[pos],"Ax       (ft/s^2): f4\n");
    pos += sprintf(&buf->data_def[pos],"Ay       (ft/s^2): f4\n");
    pos += sprintf(&buf->data_def[pos],"Az       (ft/s^2): f4\n");
    pos += sprintf(&buf->data_def[pos],"Vn       (ft/s)  : f4\n");
    pos += sprintf(&buf->data_def[pos],"Ve       (ft/s)  : f4\n");
    pos += sprintf(&buf->data_def[pos],"Vz       (ft/s)  : f4\n");
    pos += sprintf(&buf->data_def[pos],"Lat      (deg)   : f8\n");
    pos += sprintf(&buf->data_def[pos],"Long     (deg)   : f8\n");
    pos += sprintf(&buf->data_def[pos],"Alt      (ft)    : f4\n");
    pos += sprintf(&buf->data_def[pos],"LaserRaw (ft)    : f4\n");
    pos += sprintf(&buf->data_def[pos],"LaserAlt (ft)    : f4\n");
    pos += sprintf(&buf->data_def[pos],"MixedHgt (ft)    : f4\n");
    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void LandingTarget_Data_Def(buffer_t *buf)
{
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: Landing Target (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"GpsTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"TargetID (n/a)   : b1\n");
    pos += sprintf(&buf->data_def[pos],"Frame    (n/a)   : b1\n");
    pos += sprintf(&buf->data_def[pos],"X        (m)     : f4\n");
    pos += sprintf(&buf->data_def[pos],"Y        (m)     : f4\n");
    pos += sprintf(&buf->data_def[pos],"Z        (m)     : f4\n");
    pos += sprintf(&buf->data_def[pos],"Latitude (deg)   : f8\n");
    pos += sprintf(&buf->data_def[pos],"Longitud (deg)   : f8\n");
    pos += sprintf(&buf->data_def[pos],"Altitude (ft)    : f4\n");
    pos += sprintf(&buf->data_def[pos],"Q1       (n/a)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Q2       (n/a)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Q3       (n/a)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Q4       (n/a)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Pitch    (deg)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Roll     (deg)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Heading  (deg)   : f4\n");
    pos += sprintf(&buf->data_def[pos],"Type     (n/a)   : b1\n");
    pos += sprintf(&buf->data_def[pos],"PosValid (bool)  : b1\n");
    

    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}


void SupervisorLandingPoint_Data_Def(buffer_t *buf)
{
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: Supervisor Landing Point (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"LPNum  (N/A)     : b1\n");
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"GpsTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"Latitude (deg)   : f8\n");
    pos += sprintf(&buf->data_def[pos],"Longitud (deg)   : f8\n");
    pos += sprintf(&buf->data_def[pos],"Heading  (deg)   : f4\n");
    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void missionData_Data_Def(buffer_t *buf)
{ // For now this just holds the regular mission data, but we can extend it to include Fence and Rally points
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: MissionData (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"Frame            : b4\n");
    pos += sprintf(&buf->data_def[pos],"Command  (#)     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Current  (bool)  : b4\n");
    pos += sprintf(&buf->data_def[pos],"AutoContinue     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Param 1          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 2          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 3          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 4          : f4\n");
    pos += sprintf(&buf->data_def[pos],"X                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Y                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Z   (m)          : f4\n");

    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void missionGeofenceData_Data_Def(buffer_t *buf)
{ // For now this just holds the regular mission data, but we can extend it to include Fence and Rally points
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: GeoFenceData (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"Frame            : b4\n");
    pos += sprintf(&buf->data_def[pos],"Command  (#)     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Current  (bool)  : b4\n");
    pos += sprintf(&buf->data_def[pos],"AutoContinue     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Param 1          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 2          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 3          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 4          : f4\n");
    pos += sprintf(&buf->data_def[pos],"X                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Y                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Z   (m)          : f4\n");
    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void missionRallyPointData_Data_Def(buffer_t *buf)
{ // For now this just holds the regular mission data, but we can extend it to include Fence and Rally points
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: RallyPointData (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"Frame            : b4\n");
    pos += sprintf(&buf->data_def[pos],"Command  (#)     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Current  (bool)  : b4\n");
    pos += sprintf(&buf->data_def[pos],"AutoContinue     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Param 1          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 2          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 3          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 4          : f4\n");
    pos += sprintf(&buf->data_def[pos],"X                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Y                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Z   (m)          : f4\n");
    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void missionStatus_Data_Def(buffer_t *buf)
{ // For now this just holds the current mission item # we are tracking to
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: MissionStatus (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"Cur Mission Item : b4\n");
    pos += sprintf(&buf->data_def[pos],"Frame            : b4\n");
    pos += sprintf(&buf->data_def[pos],"Command  (#)     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Current  (bool)  : b4\n");
    pos += sprintf(&buf->data_def[pos],"AutoContinue     : b4\n");
    pos += sprintf(&buf->data_def[pos],"Param 1          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 2          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 3          : f4\n");
    pos += sprintf(&buf->data_def[pos],"Param 4          : f4\n");
    pos += sprintf(&buf->data_def[pos],"X                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Y                : i4\n");
    pos += sprintf(&buf->data_def[pos],"Z   (m)          : f4\n");

    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void LocalPosition_Data_Def(buffer_t *buf)
{
    int pad;
    int pos = 0;
    
    sprintf(buf->data_type, "Data type %d: LocalPosition (VarRate)\n", buf->def_hdr.mesg_minor);
    pos += sprintf(&buf->data_def[pos],"SysTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"GpsTime  (s)     : f8\n");
    pos += sprintf(&buf->data_def[pos],"LocalX   (m)     : f4\n");
    pos += sprintf(&buf->data_def[pos],"LocalY   (m)     : f4\n");
    pos += sprintf(&buf->data_def[pos],"LocalZ   (m)     : f4\n");
    
    // Calculate pad size to align on 32-bit boundaries
    pad = 4 - ((pos-1) % 4 + 1);
    
    // Add pad to header size
    buf->def_hdr.mesg_size = pos + pad;
    
    return;
}

void missionManager_Data_Def(buffer_t *buf) {
	// A place for the mission manager to dump its data to disk
	int pad;
	int pos = 0;

	sprintf(buf->data_type, "Data type %d: MissionMgr (10Hz)\n", buf->def_hdr.mesg_minor);
	pos += sprintf(&buf->data_def[pos], "SysTime  (s)     : f8\n");
	pos += sprintf(&buf->data_def[pos], "Item No.         : b4\n");
	pos += sprintf(&buf->data_def[pos], "Horz Dist (m)    : f4\n");
	pos += sprintf(&buf->data_def[pos], "Vert Dist (m)    : f4\n");
	pos += sprintf(&buf->data_def[pos], "GndSpeed (m/s)   : f4\n");
	// Calculate pad size to align on 32-bit boundaries
	pad = 4 - ((pos - 1) % 4 + 1);

	// Add pad to header size
	buf->def_hdr.mesg_size = pos + pad;

	return;
}


void bossMission_Data_Def(buffer_t *buf)
{// This is a small subset of the boss mission data
	int pad;
	int pos = 0;

	sprintf(buf->data_type, "Data type %d: BossMission (VarRate)\n", buf->def_hdr.mesg_minor);
	pos += sprintf(&buf->data_def[pos], "SysTime  (s)     : f8\n");
	pos += sprintf(&buf->data_def[pos], "Lp No.           : b4\n");
	pos += sprintf(&buf->data_def[pos], "Lp Lat   (deg)   : f8\n");
	pos += sprintf(&buf->data_def[pos], "Lp Lon   (deg)   : f8\n");
	pos += sprintf(&buf->data_def[pos], "Mission No.      : b4\n");
	pos += sprintf(&buf->data_def[pos], "Mission Item No. : b4\n");
	pos += sprintf(&buf->data_def[pos], "Is Msn Started   : b4\n");
	pos += sprintf(&buf->data_def[pos], "Is Landing Leg   : b4\n");
	pos += sprintf(&buf->data_def[pos], "M-Itm Lat   (deg): f8\n");
	pos += sprintf(&buf->data_def[pos], "M-Itm Lon   (deg): f8\n");
	pos += sprintf(&buf->data_def[pos], "M-Itm Alt (m)    : f4\n");
	pos += sprintf(&buf->data_def[pos], "M-Itm Yaw (deg)  : f4\n");
	pos += sprintf(&buf->data_def[pos], "M-Itm Speed (m/s): f4\n");
	pos += sprintf(&buf->data_def[pos], "HorzAcceptRad (m): f4\n");
	pos += sprintf(&buf->data_def[pos], "VertAcceptRad (m): f4\n");

	// Calculate pad size to align on 32-bit boundaries
	pad = 4 - ((pos - 1) % 4 + 1);

	// Add pad to header size
	buf->def_hdr.mesg_size = pos + pad;

	return;
}


void NrcAutFccStatusDataDef(buffer_t *buf)
{
	int pad;
	int pos = 0;

	sprintf(buf->data_type, "Data type %d: NrcAut Fcc Status (%d Hz)\n", buf->def_hdr.mesg_minor, 100); // KE thinks this is 100 Hz nominally
	pos += sprintf(&buf->data_def[pos], "SysTime  (s)     : f8\n");
	pos += sprintf(&buf->data_def[pos], "FccGps Time(s)   : f8\n");
	pos += sprintf(&buf->data_def[pos], "Fcc Status       : b4\n");
	pos += sprintf(&buf->data_def[pos], "Tru Hdg Cmd(deg) : f4\n");
	pos += sprintf(&buf->data_def[pos], "Msl Alt Cmd(ft)  : f4\n");
	pos += sprintf(&buf->data_def[pos], "MslAltCmdPrev(ft): f4\n"); // Prev MSL alt cmd
	pos += sprintf(&buf->data_def[pos], "GndSpeedCmd(ft/s): f4\n");
    pos += sprintf(&buf->data_def[pos], "GndSpeedCmdRate  : f4\n");
	pos += sprintf(&buf->data_def[pos], "GndSpeedCmdPrev  : f4\n");
	pos += sprintf(&buf->data_def[pos], "DirnCmd (deg)    : f4\n");
	pos += sprintf(&buf->data_def[pos], "N Vel Cmd (ft/s) : f4\n");
	pos += sprintf(&buf->data_def[pos], "E Vel Cmd (ft/s) : f4\n");
	pos += sprintf(&buf->data_def[pos], "VertVelCmd (ft/s): f4\n");
	pos += sprintf(&buf->data_def[pos], "LatCmdCur (deg)  : f8\n");
	pos += sprintf(&buf->data_def[pos], "LonCmdCur (deg)  : f8\n");
	pos += sprintf(&buf->data_def[pos], "LatCmdPrev (deg) : f8\n");
	pos += sprintf(&buf->data_def[pos], "LonCmdPrev (deg) : f8\n");
	pos += sprintf(&buf->data_def[pos], "Des Track (deg)  : f4\n");
	pos += sprintf(&buf->data_def[pos], "Pitch Cmd (deg)  : f4\n");
	pos += sprintf(&buf->data_def[pos], "Roll Cmd  (deg)  : f4\n");
	pos += sprintf(&buf->data_def[pos], "No. Orbits (N/A) : f4\n");
	pos += sprintf(&buf->data_def[pos], "ETA Horz (s)     : f4\n");
	// Calculate pad size to align on 32-bit boundaries
	pad = 4 - ((pos - 1) % 4 + 1);

	// Add pad to header size
	buf->def_hdr.mesg_size = pos + pad;

	return;
}
