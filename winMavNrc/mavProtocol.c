/*
    Mission Protocol Helper Functions
    
    File name    : missionProtocol.c
    Date        : 1/19/2022 2:06:49 PM
    Author        : Levesqueje
    Description : This file holds every function that is used to communicate the mavlink mission protocol.
                  It decodes the received information and sends back requests if needed.
*/

// Standard librairies includes
#include <assert.h>
#include <pthread.h>
#ifdef _WIN32
#include <ws2tcpip.h>
#else
#include <sys/time.h>
#endif
#include <stdlib.h>

// External installed librairies <>


// Project documents includes ""
#include "mavProtocol.h"
#include "mavParams.h"
#include "NrcDefaults.h"
#include "socket.h"
#include "globals.h"
#include "updateVehicleMavLinkStatusThread.h"
#include "supervisor.h"
#include "missionManagerToSupervisorComs.h"
#include "remotePermissions.h"
#include "log.h"
#include "serialToEthThreads.h"
#include "missionHelper.h"
#include "equivalenceCOREsharedMem.h"
#include "sleep.h"

/////////////////////
// Main content below

extern int numMavParams;
extern mavlink_param_value_t myMavParams[MAXPARAMS];
extern remotes_infos_t remotesInfo;

// Local functions
void identifyMavNrcConfig(int sockfd, const struct sockaddr_in* to);
void sendMissionToBoss(const missionType_t* missionSpecific, bossMissionData_t* bossMissionData, int missionNumber);
void recordMissionItems(missionData_t* mission_data, int mission_type);

/*
    handleMessageReceived
    Description : This function is the main entry point to treat any mavlink messages. It will appropriately
                  call the function that handles the specific type of message.
 
                  The function will also verify the permissions of the sender before handling the message.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - listenedMessage   : Pointer to the structure holding all the data relative to the listened message.
        - recvbuf : Buffer holding the message to be parsed
        - nBytes : Length in bytes of the message to handle

*/
void handleMessageReceived(listened_message_t* listenedMessage, const char* recvbuf, long nBytes) {
    int i;

    for (i = 0; i < nBytes; ++i) {
        // Only returns when the full msg is parsed
        if (mavlink_parse_char(MAVLINK_COMM_0, recvbuf[i], &listenedMessage->msg, &listenedMessage->status)) {
            
            // Check if the remote has the permissions
            if (hasPermission(listenedMessage)) {
                switch (listenedMessage->msg.msgid) {
                    case MAVLINK_MSG_ID_HEARTBEAT:
                        //LOG("Received a heartbeat from a remote\n");
                        handleHeartbeatMessage(listenedMessage);
                        if (isGcs(&listenedMessage->toAddr)) {
                            identifyMavNrcConfig(listenedMessage->sockfd, &listenedMessage->toAddr);
                        }
                        break;
                    case MAVLINK_MSG_ID_MISSION_COUNT:
                        LOG("Received a mission count item msg\n");
                        handleMissionCountMessage(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_ITEM_INT:
                        LOG("Received a mission item int msg\n");
                        handleMissionItemMessage(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_AUTOPILOT_VERSION:
                        LOG("Received a AUTOPILOT VERSION msg (deprecated)\n");
                        sendAutopilotVersionRemote(listenedMessage->sockfd, &listenedMessage->toAddr, listenedMessage->toAddrLen);
                        break;
                    case MAVLINK_MSG_ID_SET_MODE: // (deprecated...?)
                        LOG("Received a set mode msg\n");
                        handleSetModeMessage(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_REQUEST_LIST:
                        LOG("Received a DOWNLOAD request.\n");
                        handleMissionRequestListMessage(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_REQUEST_INT:
                        LOG("Received a ITEM DOWNLOAD request.\n");
                        handleMissionRequestIntMessage(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_COMMAND_LONG:
                        LOG("Received a COMMAND LONG msg\n");
                        handleCommandLong(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_COMMAND_INT:
                        LOG("Received a COMMAND INT msg\n");
                        handleCommandInt(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_ACK:
                        LOG("Received a Mission ACK.\n");
                        handleMissionAck(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
                        LOG("Received a PARAM REQUEST LIST\n");
                        handleParamRequestList(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
                        LOG("Received a PARAM REQUEST READ\n");
                        handleParamRequestRead(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_PARAM_SET:
                        LOG("Received a PARAM SET\n");
                        handleParamSet(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_SET_CURRENT:
                        LOG("Received a MISSION SET CURRENT\n");
                        handleMissionSetCurrent(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_CURRENT:
                        LOG("Received a MISSION CURRENT\n");
                        handleMissionCurrent(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MISSION_CLEAR_ALL:
                        LOG("Received a MISSION CLEAR ALL\n");
                        handleMissionClearAll(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_MANUAL_CONTROL:
                        LOG("Received a handle manual control");
                        handleManualControl(listenedMessage);
                        break;
                    case MAVLINK_MSG_ID_PING:
                        //LOG("Received a PING!\n");
                        handlePing(listenedMessage);
                        break;
                    default:
                        LOG("\nReceived an un-handled MavLink message. Details below:\n");
                        LOG("Message informations:\n");
                        LOG("sysid :     %u\n", listenedMessage->msg.sysid);
                        LOG("compid :    %u\n", listenedMessage->msg.compid);
                        LOG("compatf:    %u\n", listenedMessage->msg.compat_flags);
                        LOG("incompf:    %u\n", listenedMessage->msg.incompat_flags);
                        LOG("checksum:   %u\n", listenedMessage->msg.checksum);
                        LOG("len:        %u\n", listenedMessage->msg.len);
                        LOG("magic:      %u\n", listenedMessage->msg.magic);
                        LOG("msgid:      %u\n", listenedMessage->msg.msgid);
                        LOG("seq:        %u\n", listenedMessage->msg.seq);
                        break;
                }   // end of switch
            } // end of permission check
            else {
                LOG("PERMISSION FAULT : The remote (SysId : %d, CompId : %d) doesn't have permission to send message (MsgId : %d)\n", listenedMessage->msg.sysid, listenedMessage->msg.compid, listenedMessage->msg.msgid);
            }
        }
    }
}

/*
    handleHeartbeatMessage
    Description : This function handles a heartbeat message primarily coming from the remote instances of QGC.
                    

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleHeartbeatMessage(listened_message_t* lm) {
    extern int heartBeatsMissed;
    heartBeatsMissed = 0;
    
    mavlink_heartbeat_t heartbeat;
    mavlink_msg_heartbeat_decode(&lm->msg, &heartbeat);

    remoteInfo_t* remote = getRemote(lm->mission_data, &lm->toAddr);
    if (remote != NULL)
        remote->misses = 0;
}

/*
    handleMissionCountMessage
    Description : This function handles a mission count message received. The mission count info is saved in the shared_memory
                  and allocates enough space to receive the mission items into the shared_memory. If there are mission items to
                  download, it asks for the first mission item, otherwise (the count is 0) we acknowledge the mission received.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionCountMessage(listened_message_t* lm) {
    mavlink_mission_count_t missionCount;
    mavlink_msg_mission_count_decode(&lm->msg, &missionCount);
    
    missionType_t* missionSpecific = getSpecificMissionType(&lm->mission_data->myNewMission, missionCount.mission_type);
    
    // If has permission, set the items to download and set the mission count
    missionSpecific->numMissionItems = missionCount.count;
    missionSpecific->numMissionItemsToDownload = missionCount.count;
    missionSpecific->itemRequested = 0;
    
    LOG("\nMission Count Message Decoded : \n");
    LOG("count : %u\n", missionCount.count);
    LOG("mission_type : %u\n", missionCount.mission_type);
    LOG("target_component : %u\n", missionCount.target_component);
    LOG("target_system : %u\n\n", missionCount.target_system);

    if (missionCount.count > 0)
        askForMissionItems(lm, missionCount.mission_type);
    else
        sendMissionAckRemote(lm, missionCount.mission_type, MAV_MISSION_ACCEPTED);

}

/*
    handleMissionItemMessage
    Description : This function handles a mission item received. It will store it inside the
                  mission data shared memory and either ask for the next mission item if there's one
                  or ack the mission back to the sender.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionItemMessage(listened_message_t* lm) {
    mavlink_mission_item_int_t item;
    mavlink_msg_mission_item_int_decode(&lm->msg, &item);
    
    missionType_t* missionSpecific = getSpecificMissionType(&lm->mission_data->myNewMission, item.mission_type);
    
    if (item.seq != missionSpecific->itemRequested) {
        LOG("ERROR : Received a mission item that's not in the right sequence.\n");
        LOG("Requested seq number was %d, received seq %d\n", missionSpecific->itemRequested, item.seq);
        
        // Little sleep to avoid flooding things.
        usleep(10000);
        
        askForMissionItems(lm, item.mission_type);

    }
    else {
        // The mission item is the one we asked for
        
        // Copy the received mission item into the list in the shared_memory
        int itemNumberReceived = item.seq;
        memcpy(&missionSpecific->items[itemNumberReceived], &item, sizeof(item));
        --missionSpecific->numMissionItemsToDownload;
        
        LOG("\nReceived item %d. Description:\n", itemNumberReceived);
        LOG("Param1 : %f\n", missionSpecific->items[itemNumberReceived].param1);
        LOG("Param2 : %f\n", missionSpecific->items[itemNumberReceived].param2);
        LOG("Param3 : %f\n", missionSpecific->items[itemNumberReceived].param3);
        LOG("Param4 : %f\n", missionSpecific->items[itemNumberReceived].param4);
        LOG("x : %d\n", missionSpecific->items[itemNumberReceived].x);
        LOG("y : %d\n", missionSpecific->items[itemNumberReceived].y);
        LOG("z : %f\n\n", missionSpecific->items[itemNumberReceived].z);
    }

    if (missionSpecific->numMissionItemsToDownload > 0) {
        askForMissionItems(lm, item.mission_type);
    }
    else { // We have all th messages
        
        sendMissionAckRemote(lm, item.mission_type, MAV_MISSION_ACCEPTED);
    }

}

/*
    handleMissionRequestListMessage
    Description : This function receives a mission request list message and then answers back with the mission count
                  as detailed in the mission protocol.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionRequestListMessage(listened_message_t* lm) {
    mavlink_mission_request_list_t reqList;

    mavlink_msg_mission_request_list_decode(&lm->msg, &reqList);
    LOG("RECEIVED MISSION REQUEST LIST FOR MISSION TYPE %d\n", reqList.mission_type);
    
    sendMissionCount(lm->sockfd, lm->mission_data, &lm->toAddr, lm->toAddrLen, lm->msg.sysid, lm->msg.compid, reqList.mission_type);
}

/*
    handleMissionRequestIntMessage
    Description : This function receives a request for a particular mission item and answers back with the asked item.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionRequestIntMessage(listened_message_t* lm) {
    mavlink_mission_request_int_t reqInt;
    mavlink_msg_mission_request_int_decode(&lm->msg, &reqInt);
    sendMissionItem(lm->sockfd, lm->mission_data, &reqInt, &lm->toAddr, lm->toAddrLen);
}

/*
    handleMissionAck
    Description : This function handles a received mission acknowledge. The following checks are being made to answer to the ACK :
                    1. If the ACK was not successfull or not expected, ignore the entire message.
                    2. If the ACK was of type MISSION, send the mission count for the Fences
                    3. If the ACK was of type FENCES, send the mission count for the Rally Points
                    4. If the ACK was of type RALLY, set the expected flags to unexpected.

                  These checks are there to allow the transmission of each type of mission items between instances of the
                  mission helper. This way, to send all the mission items to a remote instance for example, all you have to do is
                  send the mission count for the basic mission items of type MISSION. The mission counts of other mission types
                  will then be sent here.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionAck(listened_message_t* lm) {
    mavlink_mission_ack_t ack;

    mavlink_msg_mission_ack_decode(&lm->msg, &ack);

    int ackWasExpected;
    remoteInfo_t* ri = getRemote(lm->mission_data, &lm->toAddr);
    
    if (isGcs(&lm->toAddr)) {
        // This is to by pass when we're using this function for the localhost
        ackWasExpected = ACK_EXPECTED;
    }
    else {
        ackWasExpected = (ri->expectedAck != ACK_UNEXPECTED);
    }
        


    if (ackWasExpected != ACK_UNEXPECTED && ack.type == MAV_MISSION_ACCEPTED) {
        // The Gcs will ask itself the subsequent mission counts.
        switch (ack.mission_type) {
            case MAV_MISSION_TYPE_MISSION:
                // Send the fence items
                sendMissionCount(lm->sockfd, lm->mission_data, &lm->toAddr, lm->toAddrLen, lm->msg.sysid, lm->msg.compid, MAV_MISSION_TYPE_FENCE);
                break;
            case MAV_MISSION_TYPE_FENCE:
                sendMissionCount(lm->sockfd, lm->mission_data, &lm->toAddr, lm->toAddrLen, lm->msg.sysid, lm->msg.compid, MAV_MISSION_TYPE_RALLY);
                break;
            case MAV_MISSION_TYPE_RALLY:
                // This condition is to by pass when we're using this function for the localhost
                if (ri != NULL)
                    ri->expectedAck = ACK_UNEXPECTED;
                break;
            default:
                break;
        }
    }
}

/*
    handleSetModeMessage
    Description : This function handles a received set mode message.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/

void handleSetModeMessage(listened_message_t* lm) {
    // For now we will just print out the contents of the state change requsest
    mavlink_set_mode_t modeMsg;
    mavlink_msg_set_mode_decode(&lm->msg, &modeMsg);
    
    
    printf("Mode Change Request Received:\n");
    printf("Target System (doc says this is the requester): %d\n", modeMsg.target_system);
    printf("Requested base mode = %d\n", modeMsg.base_mode);
    printf("Requested custom mode = %d\n", modeMsg.custom_mode);
    if (modeMsg.custom_mode == 67371008) {
        printf("GCS Requested to start mission...informing supervisor\n");
        // Let's force the current mission to be not started....
        extern missionData_t* mission_data;    // Global where we save all the mission information--from updateVehicleMavStatusThread
        mission_data->myCurrentMission.missionStarted = 0;
        signalGcsRequestedStartMission(); // needs missionManagerToSupervisorComs.h
    }

    printf("Changing Custom Mode to reflect request....\n");
    lm->mission_data->myMavCustomMode = modeMsg.custom_mode;
    lm->mission_data->myMavBaseMode = modeMsg.base_mode;
}


/*
    handleParamRequestList
    Description : This function receives a parameter request list and answers back by sending all the parameters to the
                  requester.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleParamRequestList(listened_message_t* lm) {
    mavlink_param_request_list_t paramReq;
    mavlink_msg_param_request_list_decode(&lm->msg, &paramReq);
    
    sendParameters(lm->sockfd, lm->mission_data, myMavParams, numMavParams, &lm->toAddr);
}


/*
    handleMissionSetCurrent
    Description : This function handles the MISSION_SET_CURRENT mavlink message. It will update the shared_memory and send back the
                  current mission item to the sender.

                  If we are the main, we should then send the mission current to all remotes and expect them to send back
                  their mission current.

    Author         : Jeremi Levesque
    Date         : 02/09/2022
    Return value: None
    Inputs        :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionSetCurrent(listened_message_t* lm) {
    // Decode the message
    mavlink_mission_set_current_t setCurrent;
    missionType_t* specificMission = getSpecificMissionType(&lm->mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);
    
    mavlink_msg_mission_set_current_decode(&lm->msg, &setCurrent);
    myMissionStruct_t* currentMission = &lm->mission_data->myCurrentMission;
    
    LOG("Received a direct to waypoint %d command from GCS\n", setCurrent.seq);
    
    float commandedSpeed = speedAtWaypoint(currentMission, setCurrent.seq);
    double latNext[WPT_PREVIEW_LENGTH] = { 0.0 };
    double lonNext[WPT_PREVIEW_LENGTH] = { 0.0 };
    int numPreviewItems = buildBossWptPreviewFromMissionItem(currentMission, setCurrent.seq, latNext, lonNext);
    
    extern bossMissionData_t bossMissionDataGlobal;
    bossMissionData_t currentPosMissionItem;
    
    memcpy(&currentPosMissionItem, &bossMissionDataGlobal, sizeof(bossMissionDataGlobal));
    
    currentPosMissionItem.lat = shared_memory->hg1700.lat;
    currentPosMissionItem.lon = shared_memory->hg1700.lng;
    currentPosMissionItem.alt = shared_memory->hg1700.alt * FEET_TO_M;
    currentPosMissionItem.speed = commandedSpeed;
    
    // Now copy the new Wp preview
    // Start by priming the first lat/lon slot with the lat/lon of the mission item we are going direct to
    currentPosMissionItem.latNext[0] = specificMission->items[setCurrent.seq].x / (1.0E7);
    currentPosMissionItem.lonNext[0] = specificMission->items[setCurrent.seq].y / (1.0E7);
    // Now copy the rest of the array
    memcpy(&currentPosMissionItem.latNext[1], &latNext[0], sizeof(double) * 2);
    memcpy(&currentPosMissionItem.lonNext[1], &lonNext[0], sizeof(double) * 2);
    if (numPreviewItems < (WPT_PREVIEW_LENGTH-1)) { // Handle cases where the preview is shorter than we want
        currentPosMissionItem.previewLength = numPreviewItems + 1; // Recall that we stuffed at least one valid item in there
    } else {
        currentPosMissionItem.previewLength = WPT_PREVIEW_LENGTH;
    }
    currentPosMissionItem.missionItemNo = DUMMY_MISSION_ITEM_NO; // To signal that this is just a placeholder to assign a 'direct to'
    
    // Send to boss...
    // Copy to the global mission item first (so we can use the updateBossMissionItem function)
    memcpy(&bossMissionDataGlobal, &currentPosMissionItem, sizeof(bossMissionDataGlobal));
    sendMissionItemToBoss(bossMissionDataGlobal); // This will hopefully update the current mission item lat/lon data using the current position
    // Now lets issue the command to the FCC via the supervisor
    signalDirectToReposition(specificMission->items[setCurrent.seq], commandedSpeed);

    // Now we need to sleep for a couple of 100 Hz samples to ensure that the boss helper has received it (and so has boss)
    usleep(20000);
    // Now build the newMissionItemData to be sent to Boss
    bossMissionData_t directToMissionItem;
    directToMissionItem = updateBossMissionDataForItem(setCurrent.seq, bossMissionDataGlobal);
    // We still need to manually populate the wpt preview as update bossMissionData for item doesn't seem to do it by itself
    memcpy(&directToMissionItem.latNext[0], &latNext[0], sizeof(double)*WPT_PREVIEW_LENGTH);
    memcpy(&directToMissionItem.lonNext[0], &lonNext[0], sizeof(double)*WPT_PREVIEW_LENGTH);
    directToMissionItem.previewLength = numPreviewItems; // The value our function returned
    //Now we can copy this mess into the global variable and fire off to Boss (cross your fingers)
    memcpy(&bossMissionDataGlobal, &directToMissionItem, sizeof(bossMissionData_t));
    sendMissionItemToBoss(bossMissionDataGlobal);
    // Finally, inform all systems that we are now on a new mission item number
    lm->mission_data->myCurrentMission.currentMissionItem = setCurrent.seq; // Set the current mission item equal to the requested sequence
    broadcastCurrentMissionItem(lm->mission_data->myCurrentMission.currentMissionItem);

    // TODO : Delete the following if never used.
#ifdef ALL_THE_SHIT_ABOVE_DIDNT_WORK
    // Get Prepared to Shit can everything from here down......

    // In order to comply with this we will need to loop through all the mission items in sequence to determine what our speed commands should be
    // since we will be going direct to, the previous waypoint position will need to be our current position
    bossMissionData_t currentPositionMissionItem;
    memcpy(&currentPositionMissionItem, &bossMissionDataGlobal, sizeof(bossMissionDataGlobal)); // Copy the current mission item
    currentPositionMissionItem.lat = shared_memory->hg1700.lat;
    currentPositionMissionItem.lon = shared_memory->hg1700.lng;
    currentPositionMissionItem.alt = shared_memory->hg1700.alt * FEET_TO_M;
    currentPosMissionItem.missionItemNo = -1; // To signal that this is just a placeholder to assign a 'direct to'

    bossMissionData_t scannedMissionItem;
    memcpy(&scannedMissionItem, &currentPositionMissionItem, sizeof(bossMissionData_t)); // Copy the current mission item to the scanned one so we can keep track of changes
    for (int i = lm->mission_data->myCurrentMission.currentMissionItem; i <= setCurrent.seq; ++i) {
        // Loop through mission items
        scannedMissionItem = updateBossMissionDataForItem(i, scannedMissionItem); // This should keep track of speed changes
    }
    // Now we can see if there was a speed change requested over the mission items
    if (currentPositionMissionItem.speed != scannedMissionItem.speed) {
        printf("There is a speed change associated with the direct to item...need to tell the FCC\n");
    }
    // Now we can figure out the direction to the next waypoint/item
    lm->mission_data->myCurrentMission.currentMissionItem = setCurrent.seq; // Now we override the mission item
    // And update the global boss mission data
    memcpy(&bossMissionDataGlobal, &scannedMissionItem, sizeof(bossMissionData_t));
    // Send to boss...
    sendMissionItemToBoss(currentPositionMissionItem); // This will hopefully update the current mission item lat/lon data using the current position
    updateBossMissionData(); //See if this works Jan 5 2021
    //sendMissionItemToBoss(scannedMissionItem);
    lm->mission_data->myCurrentMission.currentMissionItem = setCurrent.seq; // Set the current mission item equal to the requested sequence
    broadcastCurrentMissionItem(lm->mission_data->myCurrentMission.currentMissionItem);
#endif // ALL_THE_SHIT_ABOVE_DIDNT_WORK
}

/*
    handleMissionCurrent
    Description : This function handles the MISSION_CURRENT mavlink message. We only receive this message if
                  we sent the MISSION_SET_CURRENT message to another instance. We want to check if the current
                  we received is the same as the one currently in the shared_memory. The function retries to send
                  the mission current if it isn't the same as the one in the shared_memory.

    Author         : Jeremi Levesque
    Date         : 02/09/2022
    Return value: None
    Inputs        :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionCurrent(listened_message_t* lm) {

    mavlink_mission_current_t rcvedCurrent;
    mavlink_msg_mission_current_decode(&lm->msg, &rcvedCurrent);

    if (lm->mission_data->myCurrentMission.currentMissionItem != rcvedCurrent.seq) {
        // ERROR : An error has occured. Retry to send it.
        sendMissionSetCurrent(lm->sockfd,
                              lm->mission_data->myCurrentMission.currentMissionItem,
                              lm->msg.sysid,
                              lm->msg.compid,
                              &lm->toAddr);
    }
}

/*
    handleParamRequestRead
    Description : This function handles the request to read a parameter. The function will find the requested
                  parameter in the list of parameters and will send it back to the one requesting it.

    Author          : Jeremi Levesque
    Date            : 02/23/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleParamRequestRead(listened_message_t* lm) {
    mavlink_param_request_read_t paramRequest;
    mavlink_msg_param_request_read_decode(&lm->msg, &paramRequest);
    mavlink_param_value_t* param;
    int index = findParamByName(paramRequest.param_id);
    
    if (index >= 0) {
        param = &myMavParams[index];
    }
    else {
        param = &myMavParams[paramRequest.param_index];
        index = paramRequest.param_index;
        printf("Sending Parameter %d to GCS\n", paramRequest.param_index);
    }
    sendParameter(lm->sockfd, lm->mission_data, param, index, &lm->toAddr);
}

/*
    handleParamSet
    Description : This function will handle a mavlink message to set a specified parameter to a value. If the parameter
                  that needs to be set is already in our list, we will change it. The new parameter will then be broadcasted
                  to all instances of GCS (local if it came from a remote and all remote instances).

    Author          : Jeremi Levesque
    Date            : 02/23/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleParamSet(listened_message_t* lm) {
    mavlink_param_set_t paramSet;
    mavlink_msg_param_set_decode(&lm->msg, &paramSet);

    int index = findParamByName(paramSet.param_id);

    if (index == -1) {
        // The parameter doesn't exist
        // Append it to the end of the list
        index = numMavParams;
        ++numMavParams;

        // Update the count in each parameter
        for (int i = 0; i < numMavParams; ++i) {
            ++myMavParams->param_count;
        }
    }

    mavlink_param_value_t newParam;
    newParam.param_count = numMavParams;
    newParam.param_index = index;
    newParam.param_type = paramSet.param_type;
    newParam.param_value = paramSet.param_value;
    memcpy(&newParam.param_id, &paramSet.param_id, sizeof(paramSet.param_id));

    // Copy the new parameter into our list of parameters
    memcpy(&myMavParams[index], &newParam, sizeof(newParam));


    // Broadcast the updated parameter to update everywhere and ack the parameter back
    // This is untested!
    sendParameter(lm->sockfd, lm->mission_data, &myMavParams[index], index, &remotesInfo.gcsAddr);
    sendParameter(lm->sockfd, lm->mission_data, &myMavParams[index], index, &remotesInfo.broadcastAddr);
}

/*
    handleMissionClearAll
    Description : This function will handle the MISSION_CLEAR_ALL mavlink message. It will clear the specified mission
                  type to be cleared. Once what's requested has been cleared in the mission data shared_memory, we ack
                  the message and then populate the change to all instances of GCS.

                  Boss will also be changed regarding to the cleared mission if needed.

    Author          : Jeremi Levesque
    Date            : 02/23/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleMissionClearAll(listened_message_t* lm) {
    // This function clears both the newMission (being downloaded), as well as the currentMission
    mavlink_mission_clear_all_t clearAll;
    mavlink_msg_mission_clear_all_decode(&lm->msg, &clearAll);
    if (clearAll.mission_type == MAV_MISSION_TYPE_ALL) {
        LOG("Clearing all missions...\n");
        memset(&lm->mission_data->myCurrentMission, 0, sizeof(lm->mission_data->myCurrentMission));
        
        lm->mission_data->myNewMission.standard.itemRequested = -1;
        lm->mission_data->myNewMission.fences.itemRequested = -1;
        lm->mission_data->myNewMission.rallyPoints.itemRequested = -1;
        
        lm->mission_data->myCurrentMission.standard.itemRequested = -1;
        lm->mission_data->myCurrentMission.fences.itemRequested = -1;
        lm->mission_data->myCurrentMission.rallyPoints.itemRequested = -1;
    }
    else {
        LOG("Clearing mission of type %d\n", clearAll.mission_type);
        missionType_t* newMission = getSpecificMissionType(&lm->mission_data->myNewMission, clearAll.mission_type);
        missionType_t* currentMission = getSpecificMissionType(&lm->mission_data->myCurrentMission, clearAll.mission_type);
        
        newMission->itemRequested = -1;
        currentMission->itemRequested = -1;
        
        memset(newMission, 0, sizeof(*newMission));
        memset(currentMission, 0, sizeof(*currentMission));
    }
    
    sendMissionAckRemote(lm, clearAll.mission_type, MAV_MISSION_ACCEPTED);
    
    if (clearAll.mission_type == MAV_MISSION_TYPE_ALL || clearAll.mission_type == MAV_MISSION_TYPE_MISSION) {
        extern bossMissionData_t bossMissionDataGlobal; // defined in bossComms.c
        
        clearBossMissionData();
        // send Boss the cleared mission item so the display reflects the clear
        sendMissionItemToBoss(bossMissionDataGlobal);
    }
    
    if (isGcs(&lm->toAddr)) {
        // Mission update coming from GCS. Set flag to send to all remotes
        lm->mission_data->newMissionFromGcs = 1;
    }
    else {
        // Mission update from a remote. Set flag to update GCS and all remotes
        lm->mission_data->newMissionFromRemote = 1;
    }
    //KE Made a change here to force the missionNumber to negative when the mission is cleared. Upon receipt of a new mission it should be converted back to +ve and incremented
    // Jeremi's legacy line left here
    //++lm->mission_data->missionNumber;  //KE Oct 28 2022...not sure why the missionNumber is incremented on a clear operation
    lm->mission_data->missionNumber = -lm->mission_data->missionNumber; // Make the current number negative. This signals a change to any process looking at the mission shared mem area
}

/*
    handleManualControl
    Description : This function will handle the MANUAL_CONTROL message. The joystick data
                  will be updated in regard of the received message.

    Author          : Jeremi Levesque
    Date            : 02/23/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleManualControl(listened_message_t* lm) {
    extern joystickData_t myJoystickData;
    mavlink_manual_control_t mavJoyData;
    mavlink_msg_manual_control_decode(&lm->msg, &mavJoyData);
    myJoystickData.x = mavJoyData.x;
    myJoystickData.y = mavJoyData.y;
    myJoystickData.z = mavJoyData.z;
    myJoystickData.r = mavJoyData.r;
    myJoystickData.buttons = mavJoyData.buttons;
}

/*
    handlePing
    Description : This function handles a received PING mavlink message. This is used whenever we want to
                  check the latency with a system. If the ping message is addressed to us, we compute the
                  latency time and display it on the screen. If the ping message is a ping request (sys &
                  comp ids are set to 0, we echo back the message to the sender targeting its system/comp
                  ids).

                  This was implemented following the mavlink ping protocol : https://mavlink.io/en/services/ping.html

    Author          : Jeremi Levesque
    Date            : 02/23/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handlePing(listened_message_t* lm) {
    mavlink_ping_t ping;
    mavlink_msg_ping_decode(&lm->msg, &ping);
    
    if (ping.target_system == MY_MAV_SYS_ID && ping.target_component == MY_MAV_COMP_ID) {
        // We received our response
        struct timeval tv;
        gettimeofday(&tv, NULL);
        uint64_t currentTime = 1000000 * tv.tv_sec + tv.tv_usec;
        uint64_t latency = currentTime - ping.time_usec;
        
        // Get information about the sender
        int port = ntohs(lm->toAddr.sin_port);
        char sender[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &lm->toAddr.sin_addr, sender, INET_ADDRSTRLEN);
        
        LOG("LATENCY with %s:%d : %lld usec\n", sender, port, latency);
    }
    else {
        // Someone wants pinged us
        // We have to return the ping to where it came from
        // And target the sender's sys/comp ids
        mavlink_message_t msg;
        mavlink_msg_ping_pack(MY_MAV_SYS_ID,
                              MY_MAV_COMP_ID,
                              &msg,
                              ping.time_usec,
                              ping.seq,
                              lm->msg.sysid,
                              lm->msg.compid);

        sendMavlinkMessage(lm->sockfd, &msg, &lm->toAddr, lm->toAddrLen);
    }
}

/*
    askForMissionItems
    Description : This function sends a MISSION_REQUEST_INT message to ask for the next mission item related to the provided specific mission type.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - lm                : Pointer to the struct holding information relative to the mavlink message
        - missionType       : Type of the mission to ask the item for

*/
void askForMissionItems(listened_message_t* lm, int mission_type) {
    mavlink_message_t msg;
    missionType_t* specificMission = getSpecificMissionType(&lm->mission_data->myNewMission, mission_type);
    int itemNumberToDownload = specificMission->numMissionItems - specificMission->numMissionItemsToDownload;
    
    specificMission->itemRequested = itemNumberToDownload;
    
    mavlink_msg_mission_request_int_pack(MY_MAV_SYS_ID,
                                         MY_MAV_COMP_ID,
                                         &msg,
                                         lm->msg.sysid,
                                         lm->msg.compid,
                                         itemNumberToDownload,
                                         mission_type);

    if (sendMavlinkMessage(lm->sockfd, &msg, &lm->toAddr, lm->toAddrLen))
        LOG("Listening Thread : Item (%d) requested\n", itemNumberToDownload);
}

/*
    sendMissionAck
    Description : This function sends a mission acknowledge to a specified address regarding a specific
                  mission type.

                  It also manages to set the appropriate flags to send update the mission
                  to all remote instances if it's the main instance. If the mission is completely downloaded
                  and we received it from a GCS as a remote instance, we push the new mission information to
                  the main instance of the mission helper. After pushing all the mission to the main instance,
                  the main will redistribute the mission information to all other remotes.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - lm                : Pointer to the struct holding information relative to the mavlink message
        - mission_type      : Mavlink mission type to ack
        - type              : Type of the mission ack


*/
void sendMissionAckRemote(listened_message_t* lm, int mission_type, uint8_t type) {
    mavlink_message_t msg;

    mavlink_msg_mission_ack_pack(MY_MAV_SYS_ID,
                                 MY_MAV_COMP_ID,
                                 &msg,
                                 lm->msg.sysid,
                                 lm->msg.compid,
                                 type,
                                 mission_type);

    if (sendMavlinkMessage(lm->sockfd, &msg, &lm->toAddr, lm->toAddrLen)) {
        LOG("Listening Thread : ACK sent to GCS with status MAV_MISSION_ACCEPTED\n");

        // If we are sending a mission ack of type RALLY POINT, the mission is fully uploaded
        // We want to send the flag to send the mission to all remotes, and if it didn't came from GCS,
        // send it to GCS as well.
        if (mission_type == MAV_MISSION_TYPE_RALLY) {
            
            // Copy the new mission into the current mission
            memcpy(&lm->mission_data->myCurrentMission, &lm->mission_data->myNewMission, sizeof(lm->mission_data->myNewMission));
            
            // Reset asked items to IDLE (-1)
            missionType_t* newMissionSpec = getSpecificMissionType(&lm->mission_data->myNewMission, mission_type);
            missionType_t* curMissionSpec = getSpecificMissionType(&lm->mission_data->myCurrentMission, mission_type);
            missionType_t* missionSpecific = getSpecificMissionType(&lm->mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);

            newMissionSpec->itemRequested = -1;
            curMissionSpec->itemRequested = -1;
            
            // Tell the mission manager that there is a new mission and it's not yet started
            lm->mission_data->myCurrentMission.missionStarted = 0;
            lm->mission_data->myCurrentMission.currentMissionItem = getFirstWaypoint(missionSpecific);

            // Send to peregrine
            sendPeregrineMissionCount(MAV_MISSION_TYPE_MISSION);

            

            // Record all the mission items
            recordMissionItems(lm->mission_data, MAV_MISSION_TYPE_MISSION);

            // Disable 'armed' flag
            disarmVehicle(lm->mission_data);

            if (isGcs(&lm->toAddr)) {
                lm->mission_data->newMissionFromGcs = 1;
            }
            else {
                lm->mission_data->newMissionFromRemote = 1;
            }
            // Now that we have the full suite of missions, lets update our mission number
            // recall that the mission number can be negative if the mission was cleared, so increment an absolute value
            lm->mission_data->missionNumber = abs(lm->mission_data->missionNumber) + 1;
            
            // Legacy line from Jeremi here
            //++lm->mission_data->missionNumber; // Ugly code practice...but this increments missionNumber (was missionCount in older builds)
            
            // KE moved the send to boss here so it can use our global mission number
            // Send to BOSS
            extern bossMissionData_t bossMissionDataGlobal;
            sendMissionToBoss(missionSpecific, &bossMissionDataGlobal, lm->mission_data->missionNumber);
        }
    }
}

/*
    sendMissionCount
    Description : This function sends the mission count from a provided mission type.
                  It will set the appropriate flags to expect a acknowledge. (Should be done in the sendMissionItem function, try to remove it from here)

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - sockfd            : File descriptor of the socket to send the message from
        - missionData       : Pointer to the shared_memory struct
        - to                : Destination address to send the message to
        - toLen             : Destination address byte size
        - target_sys        : Mavlink Id of the system to target
        - target_comp       : Mavlink Id of the component to target
        - mission_type      : Mission type of the count to send

*/
int sendMissionCount(int sockfd, missionData_t* missionData, const struct sockaddr_in* to, int toLen, int target_sys, int target_comp, int mission_type) {

    mavlink_message_t msg;
    missionType_t* missionSpecific = getSpecificMissionType(&missionData->myCurrentMission, mission_type);
    
    mavlink_msg_mission_count_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, target_sys, target_comp, missionSpecific->numMissionItems, mission_type);
    
    if (sendMavlinkMessage(sockfd, &msg, to, toLen)) {
        LOG("Mission count (%d) send to a remote.\n", missionSpecific->numMissionItems);
        
        // If no mission items will be sent, we will expect a mission ack
        remoteInfo_t *remote = getRemote(missionData, to);
        if (missionSpecific->numMissionItems == 0 && remote != NULL) {
            remote->expectedAck = getExpectedMissionAck(mission_type);
        }
        return 1;
    }
    
    return 0;
}

/*
    sendMissionItem
    Description : This function sends the mission item corresponding to the sequence that was asked to the specified address.
                  If we send the last item, we set the flags to expect a ack after that.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - sockfd            : File descriptor of the socket to send the message from
        - mission_data      : Pointer to the mission data shared memory
        - reqInt            : Pointer to the decoded MISSION_REQUEST_INT message
        - to                : Destination address to send the message to
        - toLen             : Destination address byte size

*/
void sendMissionItem(int sockfd, missionData_t* mission_data, const mavlink_mission_request_int_t* reqInt, const struct sockaddr_in* to, int toLen) {
    missionType_t* missionSpecific = getSpecificMissionType(&mission_data->myCurrentMission, reqInt->mission_type);
    mavlink_message_t msg;

    mavlink_msg_mission_item_int_pack(
        MY_MAV_SYS_ID,
        MY_MAV_COMP_ID,
        &msg,
        reqInt->target_system,
        reqInt->target_component,
        reqInt->seq,
        missionSpecific->items[reqInt->seq].frame,
        missionSpecific->items[reqInt->seq].command,
        missionSpecific->items[reqInt->seq].current,
        missionSpecific->items[reqInt->seq].autocontinue,
        missionSpecific->items[reqInt->seq].param1,
        missionSpecific->items[reqInt->seq].param2,
        missionSpecific->items[reqInt->seq].param3,
        missionSpecific->items[reqInt->seq].param4,
        missionSpecific->items[reqInt->seq].x,
        missionSpecific->items[reqInt->seq].y,
        missionSpecific->items[reqInt->seq].z,
        missionSpecific->items[reqInt->seq].mission_type
        );

    if (sendMavlinkMessage(sockfd, &msg, to, toLen)) {
        LOG("Item no %d sent to GCS.\n", reqInt->seq);
        if (!isGcs(to)) {
            eACK expectedAck = getExpectedMissionAck(reqInt->mission_type);
            getRemote(mission_data, to)->expectedAck = expectedAck;
        }
    }
}

/*
    sendMavlinkMessage
    Description : This function sends a generic mavlink message to a provided address.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: 0 on failure, 1 if message was sent successfully.
    Inputs        :
        - sockfd            : File descriptor of the socket to send the message from
        - msg               : The packed mavlink message to send
        - to                : Destination address to send the message to
        - toLen             : Destination address byte size

*/
int sendMavlinkMessage(int sockfd, const mavlink_message_t *msg, const struct sockaddr_in* to, int toLen) {
    long res, nBytes;
    uint8_t buf[PACKET_MAX_LEN];

    nBytes = mavlink_msg_to_send_buffer(buf, msg);

    char adr[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &to->sin_addr, adr, INET_ADDRSTRLEN);
    int port = ntohs(to->sin_port);
    LOG("Trying to send to : %s:%d (toLen : %d). Info : %s (%ld Bytes)\n", adr, port, toLen, buf, nBytes);

    res = sendto(sockfd, buf, nBytes, 0, (const struct sockaddr*)to, toLen);
    if (res == -1) {
        SERROR("Problem when sending the message\n");
        return 0;
    }

    return 1;
}

void sendHeartbeat(int sockfd, const missionData_t* mission_data, const struct sockaddr_in* addr, unsigned char mavState) {
    long res;
    mavlink_message_t msg;
    uint8_t buf[MAVLINK_MAX_PACKET_LEN];

    mavlink_msg_heartbeat_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, MAV_TYPE_HELICOPTER, MAV_AUTOPILOT_PX4, mission_data->myMavBaseMode, mission_data->myMavCustomMode, mavState);
    uint16_t nBytes = mavlink_msg_to_send_buffer(buf, &msg);
    res = sendto(sockfd, &buf, nBytes, 0, (const struct sockaddr*)addr, sizeof(*addr));
    if (res == -1) {
        SERROR("HeartbeatThread : Problem sending the Heartbeat message\n");
    }
    else {
        //LOG("Heartbeat sent (%ld bytes)\n", res);
    }

}

/*
    sendParameters
    Description : This function sends all the parameters from the shared memory to a provided
                  address over the network. A small pause is allowed between each shipment as
                  recommended in the parameter protocol documentation : https://mavlink.io/en/services/parameter.html

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None.
    Inputs          :
        - sockfd            : File descriptor of the socket to send the message from
        - mission_data      : Pointer to the mission data shared memory
        - params            : Pointer to the list of loaded params
        - numParams         : Number of parameters to send
        - to                : Destination address to send the message to

*/
void sendParameters(int sockfd, missionData_t* mission_data, const mavlink_param_value_t* params, int numParams, const struct sockaddr_in* to) {
    // This was copied from MavNRC and adapted to fit the shared_memory here. The copied code was encapsulated
    // into a separate thread (explained below). The commented code is the original code :
    mavlink_message_t msg;
    int i;
    for (i = 0; i < numParams; i++) {
        // Loop through all our parameters in the array
        mavlink_msg_param_value_pack(
            MY_MAV_SYS_ID,
            MY_MAV_COMP_ID,
            &msg,
            params[i].param_id,
            params[i].param_value,
            params[i].param_type,
            (uint16_t)numParams,
            (uint16_t)i);
        printf("PARAM : ");
        sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
    }

//     Jeremi : I prefered launching a thread. The mavlink documentation suggests to allow a pause between each sending of the parameters.
//     Pausing here would mean that the whole communication process between remote-main instances will be paused. This said, allowing
//     to launch a separate thread that cause pause effectively, that will not block the entire communication process.

//    sendParametersArgs_t* args = malloc(sizeof(sendParametersArgs_t)); // Have to malloc. Otherwise object gets deleted before the thread even starts
//                                                                       // free() called on the object at the end of the thread.
//
//    args->sockfd = sockfd;              // Providing the file descriptor to send the parameters from
//    args->numParams = numParams;
//    args->params = params;
//    memcpy(&args->to, to, sizeof(*to)); // Providing the address to send the parameters to
//
//    pthread_t pid;
//    pthread_create(&pid, NULL, &sendParametersThread, (void *)args);
}

/*
    sendParameter
    Description : This function will send only one provided parameter to a specified address.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None.
    Inputs          :
        - sockfd            : File descriptor of the socket to send the message from
        - mission_data      : Pointer to the mission data shared memory
        - param             : Pointer to the parameter to send
        - index             : Index of the parameter in the list of parameters
        - to                : Destination address to send the message to

*/
void sendParameter(int sockfd, missionData_t* mission_data, const mavlink_param_value_t* param, int index, const struct sockaddr_in* to) {
    
    
    mavlink_message_t msg;
    
    mavlink_msg_param_value_pack(MY_MAV_SYS_ID,
                                 MY_MAV_COMP_ID,
                                 &msg,
                                 param->param_id,
                                 param->param_value,
                                 param->param_type,
                                 numMavParams,
                                 index);
    
    sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
}

/*
    sendMissionCurrent
    Description : This function sends the mission current item from the shared_memory to a specified address.
                  It is mostly used as an answer to a MISSION_SET_CURRENT mavlink message.
    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: 1 (TRUE) on success, 0 (FALSE) on failure
    Inputs        :
        - sockfd            : File descriptor of the socket to send the message from
        - current           : Current mission item's seq number
        - to                : Destination address to send the message to

*/
int sendMissionCurrent(int sockfd, int current, const struct sockaddr_in* to) {
    mavlink_message_t msg;
    mavlink_msg_mission_current_pack(
        MY_MAV_SYS_ID,
        MY_MAV_COMP_ID,
        &msg,
        current
    );

    int res = sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
    if (res)
        LOG("Mission current message sent\n");

    return res;
}

/*
    sendMissionSetCurrent
    Description : This function sends a MISSION_SET_CURRENT message to the specified address.
                  It's mostly used to set the mission current of the remote instances if needed.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: 1 (TRUE) on success, 0 (FALSE) on failure
    Inputs        :
        - sockfd            : File descriptor of the socket to send the message from
        - current           : Current mission item's seq number
        - targ_sys          : Target system id
        - targ_comp         : Target component id
        - to                : Destination address to send the message to

*/
int sendMissionSetCurrent(int sockfd, int current, int targ_sys, int targ_comp, const struct sockaddr_in* to) {
    mavlink_message_t msg;
    mavlink_msg_mission_set_current_pack(
        MY_MAV_SYS_ID,
        MY_MAV_COMP_ID,
        &msg,
        targ_sys,
        targ_comp,
        current
    );

    int res = sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
    if (res)
        LOG("Mission set current message sent\n");

    return res;
}

/*
    sendSimpleText
    Description : This function sends a specified remote a status text to the specified
                  destination as a provided severity flag.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - sockfd        : file descriptor of the socket to send the message from
        - text          : the text to send
        - severity      : Mavlink severity check for the message
        - to            : Destination address
*/
void sendSimpleText(int sockfd, const char* text, uint8_t severity, const struct sockaddr_in* to) {
    mavlink_message_t msg;
    unsigned long textLen = strlen(text);
    char limitedText[MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN];
    
    // The maximum length for a status text field is defined by MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN.
    // If it exceeds the max len for the status message, we currently just truncate the message
    // Instead of truncation, we could send the status message in sseparate parts which would be
    // better, but not needed as of Feb 16.
    if (textLen > MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN) {
        memcpy(limitedText, text, MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN);
    }
    else {
        memcpy(limitedText, text, textLen);
    }
    
    mavlink_msg_statustext_pack(MY_MAV_SYS_ID,
                                MY_MAV_COMP_ID,
                                &msg,
                                severity,
                                text,
                                0,
                                0);
    
    // Send the message to the address
    sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
}

/*
    identifyMavNrcConfig
    Description : This function will identify the configuration of MavNRC
                  on the first heartbeat handled.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - sockfd        : file descriptor of the socket to send the message from
        - to            : Destination address
*/
void identifyMavNrcConfig(int sockfd, const struct sockaddr_in* to) {
    static int messageSent = 0;
    static int supervisorDetailsSent = 0;
    char mavNrcVersionInfo[128] = { 0 };
    char perceptionTestMsg[28] = { 0 };

    if ((messageSent == 1) && (supervisorDetailsSent == 0)) {
        // Send the supervisor details, but lets just send the to the log file
        sprintf(mavNrcVersionInfo, "SupR RepoTimR:%1.1f, OrbTimR:%1.1f, LpAccTimR: %1.1f", REPO_TIMER, ORBIT_TIMER, LP_ACCEPT_TIMER);
        sendSimpleText(sockfd, mavNrcVersionInfo, MAV_SEVERITY_INFO, to);
        sprintf(mavNrcVersionInfo, "Supr OrbRad: %1.0f, OrbVel: %1.1f, HvrAlt: %1.1f", DEFAULT_ORBIT_RADIUS, DEFAULT_ORBIT_VELOCITY, DEFAULT_HOVER_ALTITUDE_AGL);
        sendSimpleText(sockfd, mavNrcVersionInfo, MAV_SEVERITY_INFO, to);
        sprintf(mavNrcVersionInfo, "Supr LndCrit Hdist:%1.1f, Vdis:%1.1f, Vel:%1.1f, Hdg: %1.1f, Time: %1.1f", DEFAULT_LAND_CRITERIA_HOR_DIST, DEFAULT_LAND_CRITERIA_VERT_DIST, DEFAULT_LAND_CRITERIA_VEL, DEFAULT_LAND_CRITERIA_HDG, DEFAULT_LAND_CRITERIA_TIME);
        sendSimpleText(sockfd, mavNrcVersionInfo, MAV_SEVERITY_INFO, to);
        supervisorDetailsSent = 1;
    }

    if (messageSent == 0) {
        
        if (SIMULATE_PEREGRINE_TEST_CASE > 0) {
            sprintf(perceptionTestMsg, ", PERCEPT TEST: %d", SIMULATE_PEREGRINE_TEST_CASE);
        }
        sprintf(mavNrcVersionInfo, "MavNrc V: %s, BossIp: %s%s", MAV_NRC_VERSION, BOSS_DESTINATION_IP,perceptionTestMsg);
        sendSimpleText(sockfd, mavNrcVersionInfo, MAV_SEVERITY_INFO, to);
#ifndef FLIGHT_TEST
        sendSimpleText(sockfd, "This code not configured for flight test", MAV_SEVERITY_ALERT, to);
#endif
        messageSent = 1;
    }
}

/*
    sendMissionToBoss
    Description : This function will send a mission to Boss. Still using external functions that are using global
                  variables.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - missionSpecific        : Specific mission pointer to send mission items from
        - bossMissionData        : Pointer to the struct holding the Boss mission data
*/
void sendMissionToBoss(const missionType_t* missionSpecific, bossMissionData_t* bossMissionData ,int missionNumber) {
    //commented out the local version of missionNumber in favour of one passed from the fun call
    //static int missionNumber = 0;
    //++missionNumber;
    
    bossMissionData->missionNo = missionNumber;
    char myString[128];
    sprintf(myString, "MX %d loaded", missionNumber);
    sendSimpleTextToGcs(myString, MAV_SEVERITY_ALERT);
    // This will become 0 when it gets to boss...hopefully all non started missions will follow this
    bossMissionData->missionItemNo = -1;
    
    // Let's just grab the 1st non zero x/y coords and use them as the lat/lon fields for boss
    for (int i = 0; i < missionSpecific->numMissionItems; i++) {
        if ((missionSpecific->items[i].x != 0.0) || (missionSpecific->items[0].y != 0)) {
            bossMissionData->lat = missionSpecific->items[i].x / (1.0E7);
            bossMissionData->lon = missionSpecific->items[i].y / (1.0E7);
            break;
        }
    }
    
    // We also want to set the description to "IDLE" since we are not in mission mode
    sprintf(bossMissionData->description, "IDLE");
    sendMissionItemToBoss(*bossMissionData); // Tell boss we have a new mission
}

/*
    recordMissionItems
    Description : This function allows to record the mission data into a specific log buffer
                  for its respective mission type.

                  The function is still using global variables. It should be adjusted to remove
                  the global variables.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - mission_data           : Pointer to the struct holding the mission shared data
        - mission_type           : Mission type of the mission to record
*/
void recordMissionItems(missionData_t* mission_data, int mission_type) {
    missionType_t* currentMissionSpecific = getSpecificMissionType(&mission_data->myCurrentMission, mission_type);
    extern buffer_t missionData_logbuf;
    extern buffer_t missionGeoFence_logbuf;
    extern buffer_t missionRallyPoints_logbuf;
    buffer_t* logbuf;
    
    switch (mission_type) {
        case MAV_MISSION_TYPE_MISSION:
            logbuf = &missionData_logbuf;
            break;
        case MAV_MISSION_TYPE_FENCE:
            logbuf = &missionGeoFence_logbuf;
            break;
        case MAV_MISSION_TYPE_RALLY:
            logbuf = &missionRallyPoints_logbuf;
            break;
        default:
            logbuf = NULL;
            break;
    }
    
    // Record all the mission items (loop)
    for (int i = 0; i < currentMissionSpecific->numMissionItems; ++i) {
        missionRecording_t missionData;
        missionData.frame = currentMissionSpecific->items[i].frame;
        missionData.command = currentMissionSpecific->items[i].command;
        missionData.autocontinue = currentMissionSpecific->items[i].autocontinue;
        missionData.param1 = currentMissionSpecific->items[i].param1;
        missionData.param2 = currentMissionSpecific->items[i].param2;
        missionData.param3 = currentMissionSpecific->items[i].param3;
        missionData.param4 = currentMissionSpecific->items[i].param4;
        missionData.x = currentMissionSpecific->items[i].x;
        missionData.y = currentMissionSpecific->items[i].y;
        missionData.z = currentMissionSpecific->items[i].z;
        
        // Add a check for the final mission item to see if the precision landing flag was set
        if (mission_type == MAV_MISSION_TYPE_MISSION) {
            if (i == currentMissionSpecific->numMissionItems - 1) {
                if (missionData.param2 != 2.0)
                    sendSimpleTextToGcs("Last wpt does not specify precision land", MAV_SEVERITY_ALERT);
            }
            // Add a check for no flight speed specified for 1st item
            if (i == 0) {
                if (currentMissionSpecific->items[0].command != MAV_CMD_DO_CHANGE_SPEED) {
                    sendSimpleTextToGcs("Mission doesn't specify speed to the TakeOff point", MAV_SEVERITY_ALERT);
                }
            }
        }
        
        Write_To_Buffer(&missionData, sizeof(missionRecording_t), logbuf);
    }
}

/*
    sendPingRequest
    Description : This function sends a mavlink PING message to a specified address holding the
                  current time of the system.
                  
                  The expected behavior is for the remote to echo back this ping to calculate the
                  latency. (Happening in handlePing)


    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - sockfd                 : File descriptor from which to send the ping request from 
        - seq                    : Seq number of ping sent
        - to                     : Address to send the request to
*/
void sendPingRequest(int sockfd, int seq, const struct sockaddr_in* to) {
    mavlink_message_t msg;
    
    struct timeval tv;
    gettimeofday(&tv, NULL);
    
    uint64_t pingTime = 1000000 * tv.tv_sec + tv.tv_usec;
    mavlink_msg_ping_pack(MY_MAV_SYS_ID,
                          MY_MAV_COMP_ID,
                          &msg,
                          pingTime,
                          seq,
                          0,
                          0);
    
    sendMavlinkMessage(sockfd, &msg, to, sizeof(*to));
//    int port = ntohs(to->sin_port);
//    char adr[INET_ADDRSTRLEN];
//    inet_ntop(AF_INET, &to->sin_addr, adr, INET_ADDRSTRLEN);
//
//    if (sendMavlinkMessage(sockfd, &msg, to, sizeof(*to)))
//        LOG("Sent ping request to %s:%d\n", adr, port);
}

/*
    isGcs
    Description : This function is a helper function that checks if a provided address in the
                  sockaddr_in format is the address of the GCS.

                  e.g. Checks if the address is localhost & port 14550

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: 1 if it's the GCS' address, 0 if not.
    Inputs        :
        - to : The address to compare

*/
int isGcs(const struct sockaddr_in* to) {
    int port = ntohs(to->sin_port);
    char adr[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &to->sin_addr, adr, INET_ADDRSTRLEN);

    return (port == MAVLINK_SEND_PORT) && !strcmp(adr, LOCALHOST_ADDR);
}

void disarmVehicle(missionData_t* mission_data) {
    // Deactivate the bit flag that says the vehicle is armed
    // Using a XOR (^) and a AND operator to turn off the flag
    long mask = (MAV_MODE_FLAG_SAFETY_ARMED ^ -1) && MAV_MODE_FLAG_SAFETY_ARMED;
    mission_data->myMavBaseMode &= mask;
    mission_data->myMavCustomMode = 1;
}

/*
    handleCommandLong
    Description : Main function that decodes and processes LONG commands from mavlink protocol. The appropriate call
                  to the function supporting the command will be made correctly. If the command is not supported,
                  its informations will be logged.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
void handleCommandLong(listened_message_t* lm) {
    mavlink_command_long_t cmd;

    mavlink_msg_command_long_decode(&lm->msg, &cmd);

    switch (cmd.command)
    {
        case MAV_CMD_NAV_TAKEOFF:
            LOG("Rakeoff Requested...\n");
            handleTakeoffRequest(lm, &cmd);
            break;
        case MAV_CMD_DO_REPOSITION:
            LOG("CMD DO REPOSITION\n");
            handleRepositionCmdLong(lm, &cmd);
            break;
        case MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN:
            // For now let's just ack it
            sendCommandAckRemote(&cmd, MAV_RESULT_ACCEPTED, 1, lm->sockfd, &lm->toAddr, lm->toAddrLen);
            break;
        case MAV_CMD_REQUEST_PROTOCOL_VERSION:
            // Deprecated
            LOG("Received a DEPRECATED protocol version request.\n");
            LOG("Sending Protocol Information\n");
            sendCommandAckRemote(&cmd, MAV_RESULT_ACCEPTED, 0, lm->sockfd, &lm->toAddr, lm->toAddrLen);
            sendProtocolVersion(); // TODO : STILL USING GLOBALS IN HERE
            sendCommandAckRemote(&cmd, MAV_RESULT_ACCEPTED, 1, lm->sockfd, &lm->toAddr, lm->toAddrLen);
            break;
        case MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES:
            // Deprecated
            LOG("Received a DEPRECATED autopilot capabilities request\n");
            sendCommandAckRemote(&cmd, MAV_RESULT_ACCEPTED, 1, lm->sockfd, &lm->toAddr, lm->toAddrLen);
            sendAutopilotVersionRemote(lm->sockfd, &lm->toAddr, lm->toAddrLen);
            break;
        case MAV_CMD_COMPONENT_ARM_DISARM:
            //sendCommandAckRemote(&cmd, MAV_RESULT_IN_PROGRESS, 0, lm->sockfd, &lm->toAddr, lm->toAddrLen);
            handleArmDisarmRequest(lm, &cmd);
            break;
        case MAV_CMD_REQUEST_MESSAGE:
            LOG("Received a REQUEST MESSAGE\n");
            handleRequestMessageRemote(lm, &cmd);  // Currently only supports autopilot capabilities
            break;
        default:
            LOG("COMMAND : %d\n", cmd.command);
            LOG("CONFIRMATION : %d\n", cmd.confirmation);
            LOG("T_SYS : %d\n", cmd.target_system);
            LOG("T_COM : %d\n", cmd.target_component);
            LOG("PARAM1 : %f\n", cmd.param1);
            LOG("PARAM2 : %f\n", cmd.param2);
            LOG("PARAM3 : %f\n", cmd.param3);
            LOG("PARAM4 : %f\n", cmd.param4);
            LOG("PARAM5 : %f\n", cmd.param5);
            LOG("PARAM6 : %f\n", cmd.param6);
            LOG("PARAM7 : %f\n", cmd.param7);
            sendCommandAckRemote(&cmd, MAV_RESULT_UNSUPPORTED, 0, lm->sockfd, &lm->toAddr, lm->toAddrLen);
            break;
    }
}

/*
    sendCommandAckRemote
    Description : This function is used to ack a received command. It will send a command ack detailing the result and the command to ack
                  to the specified address.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - command   : The unpacked long command to ack
        - result    : Result associated with the received command
        - progress  : The command progress
        - sockfd    : File descriptor of the socket to send the message from
        - to        : Destination address to send the message to
        - toLen     : Destination address byte length

*/
void sendCommandAckRemote(const mavlink_command_long_t* command, uint8_t result, uint8_t progress, int sockfd, const struct sockaddr_in* to, int toLen) {

    mavlink_message_t msg;
    mavlink_msg_command_ack_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, command->command, result, progress, 0, command->target_system, command->target_component);
    if (sendMavlinkMessage(sockfd, &msg, to, toLen))
        LOG("ACK COMMAND SENT\n");

}

/*
    handleRequestMessageRemote
    Description : This function handles a COMMAND_REQUEST_MESSAGE command. It will check the id of the message requested
                  and redirect to the appropriate function to answer back. Before answering, the function will have to
                  ACK the command to respect the mavlink protocol.

                  On a unsupported command, it will send back a command ack with a MAV_RESULT_UNSUPPORTED result.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - lm                : A pointer to the structure holding all the data relative to the listened message.
        - command           : The unpacked long command to ack

*/
void handleRequestMessageRemote(const listened_message_t* lm, const mavlink_command_long_t* command) {
    int messageRequested = (int) command->param1;

    LOG("Received a REQUEST MESSAGE for msg %d\n", messageRequested);

    switch (messageRequested)
    {
    case MAVLINK_MSG_ID_AUTOPILOT_VERSION:
        sendCommandAckRemote(command, MAV_RESULT_ACCEPTED, 0, lm->sockfd, &lm->toAddr, lm->toAddrLen);
        sendAutopilotVersionRemote(lm->sockfd, &lm->toAddr, lm->toAddrLen);
        break;

    default:
        sendCommandAckRemote(command, MAV_RESULT_UNSUPPORTED, 0, lm->sockfd, &lm->toAddr, lm->toAddrLen);
        break;
    }

}

/*
    sendAutopilotVersion
    Description : This function sends the Autopilot version to GCS whenever it receives the request. This is function
                  is needed mainly because we want the GCS to know the capabilities supported by our missionhelper. This
                  allows for example the use/transmission of GeoFences and Rally Points.

                  This function was copied directly from MavNRC and adapted only to fit the environnement of the remote handler thread.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: None
    Inputs        :
        - sockfd    : File descriptor of the socket to send the message from
        - to        : Destination address to send the message to
        - toLen     : Destination address byte length

*/
void sendAutopilotVersionRemote(int sockfd, const struct sockaddr_in* to, int toLen) {
    // This is our response to the request for autopilot capabilities
    LOG("Sending Autopilot Version Info...\n");
    mavlink_message_t msg;
    // Basically the only field we are currently going to worry about is the supported feature bitmap
    uint64_t autoCapabilities = 0;
    // Now built the bitmask from those here: https://mavlink.io/en/messages/common.html#MAV_PROTOCOL_CAPABILITY

    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_PARAM_FLOAT;
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MISSION_INT;                    // Not sure if this is desired format or not
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_COMMAND_INT;
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_SET_ATTITUDE_TARGET;
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_SET_POSITION_TARGET_GLOBAL_INT; // Use global scaled integers for pos cmds
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MAVLINK2;
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MISSION_FENCE;
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MISSION_RALLY;
    autoCapabilities |= MAV_PROTOCOL_CAPABILITY_FLIGHT_INFORMATION;

    // Other data for the AP version message
#define FIRMWARE_TYPE_DEV 1
    int firmware_type = FIRMWARE_TYPE_DEV;
    char version[3] = { 1, 9, 1 };

    uint32_t flight_sw_ver = 0;
    flight_sw_ver = ((uint8_t)version[0] << 8 * 3) |
        ((uint8_t)version[1] << 8 * 2) |
        ((uint8_t)version[2] << 8 * 1) | firmware_type;

    const uint32_t middleware_sw_ver = 1;
    const uint32_t os_sw_ver = 1;
    const uint8_t board_ver = 1;
    const uint8_t flight_custom_ver[8] = { 0 };
    const uint8_t middleware_custom_ver[8] = { 0 };
    const uint8_t os_custom_ver[8] = { 0 };
    const uint16_t vendor_id = 0;
    const uint16_t product_id = 0;
    const uint64_t uid = 0;
    const uint8_t uid2[18] = { 0 };
    mavlink_msg_autopilot_version_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, autoCapabilities, flight_sw_ver, middleware_sw_ver,
        os_sw_ver, board_ver, flight_custom_ver, middleware_custom_ver, os_custom_ver, vendor_id,
        product_id, uid, uid2);


    sendMavlinkMessage(sockfd, &msg, to, toLen);
}

/*
    handleTakeoffRequest
    Description : This function currently is unimplemented and only prints the information from the
                  takeoff request.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - lm               : A pointer to the structure holding all the data relative to the listened message.
        - cmd              : The unpacked long command

*/
void handleTakeoffRequest(listened_message_t* lm, mavlink_command_long_t* cmd) {
    // Here is a placeholder for processing takeoff requests
    printf("P1 = Pitch Target = %1.1f --> Ignored for ASRA\n", cmd->param1);
    printf("P4 = Heading Target =%1.1f -- If Nan, use current heading\n", cmd->param4);
    printf("P5 = Latitude %1.6f --> Use Current if NaN\n", cmd->param5);
    printf("P6 = Longitude %1.6f --> Use Current if NaN\n", cmd->param6);
    printf("P7 = Altitude %1.1f m MSL (I think) or %1.1f ft MSL\n", cmd->param7, cmd->param7*METERS_TO_FT);

    // Here is where you should actually signal the autopilot to takeoff
    
}

/*
    handleRepositionCmdLong
    Description : This function currently is unimplemented and only prints the information from the
                  reposition cmd.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - lm               : A pointer to the structure holding all the data relative to the listened message.
        - cmd              : The unpacked long command

*/
void handleRepositionCmdLong(listened_message_t* lm, mavlink_command_long_t* cmd) {
    printf("Received a Long reposition command (192) ===== THIS DOESN'T DO ANYTHING YET\n");
    if (cmd->param1 < 0)
        printf("-Default speed\n");
    else
        printf("Speed: %d m/s\n", (int)cmd->param1);
    
    printf("Reposition Flags: %d\n", (int)cmd->param2);
    
    if (isnan(cmd->param4))
        printf("-Default yaw mode\n");
    else
        printf("-Yaw deg: %1.2f\n", cmd->param4);
    if (isnan(cmd->param5) && isnan(cmd->param6) && isnan(cmd->param7)) {
        printf("We have received a PAUSE command...slow down to hover and then PH while following path\n");
    } else if (isnan(cmd->param5) && isnan(cmd->param6)) {
        printf("We have received a change altitude command\n");
        printf("New altitude requested is: %1.2f m, or %1.2f ft\n", cmd->param7, cmd->param7*METERS_TO_FT);
    }  else { // IT's a reposition command with specifics
        printf("X: %1.7f\n", cmd->param5);
        printf("Y: %1.7f\n", cmd->param6);
        printf("Z: %1.7f\n", cmd->param7);
    }
}

/*
    handleArmDisarmRequest
    Description : This function only changes the base mode flag in the mission data shared_memory
                  based on the arm/disarm request.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - lm               : A pointer to the structure holding all the data relative to the listened message.
        - cmd              : The unpacked long command

*/
void handleArmDisarmRequest(listened_message_t* lm, mavlink_command_long_t* cmd) {
    if (cmd->param1 == 1) {
        printf("Vehicle Arm Requested\n");
        lm->mission_data->myMavBaseMode |= MAV_MODE_FLAG_SAFETY_ARMED;
    } else {
        printf("Vehicle DisArm Requested\n");
        
        disarmVehicle(lm->mission_data);
        // Trying to let know boss the mission is paused
        extern bossMissionData_t bossMissionDataGlobal;
        bossMissionDataGlobal.isMissionStarted = 0;
        lm->mission_data->myCurrentMission.missionStarted = 0;
        // KE modded the fun call below to add the global missionNumber as an arg
        sendMissionToBoss(getSpecificMissionType(&lm->mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION), &bossMissionDataGlobal, lm->mission_data->missionNumber);
    }

    mavlink_message_t msg;
    mavlink_msg_command_ack_pack(MY_MAV_SYS_ID,
                                 MY_MAV_COMP_ID,
                                 &msg,
                                 MAV_CMD_COMPONENT_ARM_DISARM,
                                 MAV_RESULT_ACCEPTED,
                                 0,             // If the result is ACCEPTED, should be zero
                                 10000,         // If the result is ACCEPTED, should be the time in seconds this authorization is valid. Arbitrarily picked 10 000
                                 lm->msg.sysid,
                                 lm->msg.compid);
    sendMavlinkMessage(lm->sockfd, &msg, &lm->toAddr, lm->toAddrLen);

    // Send Heartbeat to let know immediately that the system has been armed and it accepted the arm/disarm request.
    uint8_t mavState = determineMavState();
    extern int qGcsSockFd;
    sendHeartbeat(qGcsSockFd, lm->mission_data, &remotesInfo.gcsAddr, mavState);
    sendHeartbeat(remotesInfo.sockfdRemotes, lm->mission_data, &remotesInfo.broadcastAddr, mavState);
}

/*
    handleCommandInt
    Description : This function handles all the supported command int messages. The appropriate function
                  handling the specific type of int command is called from here.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - lm               : A pointer to the structure holding all the data relative to the listened message.

*/
void handleCommandInt(listened_message_t* lm) {
    mavlink_command_int_t command;
    mavlink_msg_command_int_decode(&lm->msg, &command);
    switch (command.command) {
        //Place all known commands here
    case MAV_CMD_DO_REPOSITION: // MEssage 192 (Go-to-here from GCS mouse input)
        sendCommandAck(MAV_CMD_DO_REPOSITION, MAV_RESULT_ACCEPTED, 1, 0, lm->msg.sysid, lm->msg.compid);
        handleRepositionCmdInt(&command);
        break;
    default:
        printf("\nReceived an un-handled Gcs Cmd int message. Details below:\n");
        printf("Command Int Msg Contents:\n");
        printf("-Frame: %d\n", command.frame);
        printf("-Cmd Id: %d\n", command.command);
        printf("-Current (T/f): %d\n", command.current);
        printf("-AutoContinue: %d\n", command.autocontinue);
        printf("-Param1: %f\n", command.param1);
        printf("-Param2: %f\n", command.param2);
        printf("-Param3: %f\n", command.param3);
        printf("-Param4: %f\n", command.param4);
        switch (command.frame) {
        case MAV_FRAME_GLOBAL:
            printf("Global Frame\n");
            printf("-X: %d, Lat =%1.7f\n", command.x, (float)command.x/1E7); // Depends on whether global or local coords
            printf("-Y: %d, Lon =%1.7f\n", command.y, (float)command.y/1E7);
            printf("-Z: %f, Alt = %1.2f m %1.2f ft\n", command.z, command.z, command.z*METERS_TO_FT);
            break;
        default:
            printf("Unknown Frame...here's the raw x,y,z\n");
            printf("-X: %d\n", command.x); // Depends on whether global or local coords
            printf("-Y: %d\n", command.y);
            printf("-Z: %f\n", command.z);
            break;
        }
        
        
        break;
    }
}

/*
    handleRepositionCmdInt
    Description : This function is currently unimplemented and only prints the reposition command
                  related information.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - cmd              : The unpacked int command

*/
void handleRepositionCmdInt(mavlink_command_int_t* command) {
    printf("Received an Int reposition command (192) ===== THIS DOESN'T DO ANYTHING YET\n");
    printf("-Current (T/f): %d\n", command->current);
    printf("-AutoContinue: %d\n", command->autocontinue);
    if (command->param1 < 0)
        printf("-Default speed\n");
    else
        printf("Speed: %d m/s\n", (int)command->param1);
    
    printf("Reposition Flags: %d\n", (int)command->param2);
    
    if (isnan(command->param4))
        printf("-Default yaw mode\n");
    else
        printf("-Yaw deg: %1.2f\n", command->param4);
    
    // Depends on whether global or local coords
    printf("-X: %d, Lat =%1.7f\n", command->x, (float)command->x / 1E7);
    printf("-Y: %d, Lon =%1.7f\n", command->y, (float)command->y / 1E7);
    printf("-Z: %f, Alt = %1.2f m %1.2f ft\n", command->z, command->z, command->z*METERS_TO_FT);
}
