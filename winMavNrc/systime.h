//
//  systime.h
//  iNS Test
//
//  Created by Kris Ellis on 11-11-08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <stdint.h> // if rewd for uint types
#if defined (_WIN32) || defined (_WIN64)
#include <Windows.h>
#endif

//A struct for saving systime initial value to shared mem
typedef struct {
	uint8_t isInitialized;
#if defined (_WIN32) || defined (_WIN64)
	LONGLONG init_time; // Probably would be best to use the same var is in other os's but KE is feeling lazy
#else
	uint64_t start_time_tics;
#endif
} sysTimeInit_t;

// Function prototypes for systime.c

double SysTime(void);
void Init_SysTime(void);    // Used by SysTime to initialize the timer offset
int connectSysTimeSharedMemory(void);
void resetSysTime(void); // Resets systime offset so systime starts at 0 again....USE CAREFULLY
