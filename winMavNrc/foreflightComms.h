#pragma once
#include <stdint.h>

#define FOREFLIGHT_PORT 49002 // Per https://support.foreflight.com/hc/en-us/articles/204115005-Flight-Simulator-GPS-Integration-UDP-Protocol-
#define FOREFLIGHT_DESTINATION_IP "132.246.193.255" //"192.168.0.255" // "132.246.193.255"  // Set as broadcast on 412 for now

typedef struct { // Holds data to be sent to foreflight GPS reporting
	float latDeg;	// floats are precise enough for foreflight
	float lonDeg;
	float altFt; //will need to be converted to meters
	float trueHdgDeg;
	float trueTrackDeg; // Lets shove heading in there below some speed, and track otherwise
	float groundspeedKts; // Will need to be converted to m/s
} foreflightGpsMsgData_t;

typedef struct { // Holds data to be sent to foreflight attitude reporting
	float trueHeadingDeg;
	float pitchDeg;
	float rollDeg;
} foreflightAttMsgData_t;

typedef struct { //Holds data to be sent to foreflight traffic
	uint32_t icaoAddress;
	float latDeg;
	float lonDeg;
	float altFeet; //MSL altitude (geometric...gps)
	float vertVelFpm; // Vert velocity in feet per minunte
	uint8_t airborneFlag; //1 = airborne, 0 = surface
	float trueHeadingDeg; 
	float velocityKts; // velocity in knots
	char  callsign[8];
} foreflightTrafficMsgData_t;

void sendForeflightThread(void *arg);
void sendAttDataToForeflight(void);
void sendGpsDataToForeflight(void);
