#pragma once
//  log.h
//  iNS Test
//
//  Created by Kris Ellis on 11-11-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <stdio.h> // For FILE identifier
#define LOG_PRIO			27
// FRL File Format
#define HEADERWORD					0x5555
#define TRAILERWORD					0xAAAA

#define MAX_LOGFILES				2
#define MAX_BUF_SIZE				500000   // These are original values from DRP code was 50000
#define MAX_BUFFERS					20      // Note the memory consumption here:
#define MAX_DATA_DEF_SIZE			50000   //      Logfile size is>Max Buffers*(sizeof(long)*Max Buf Size+MAX Data Def)
											//      For default values from DRP code base this is approx 3.5 MB


//******************************************************************* Log File Types Below ****************************

typedef struct {
	unsigned short header_word;
	unsigned short mesg_size;
	unsigned char mesg_major;
	unsigned char mesg_minor;
	unsigned short trailer_word;
} msg_header_t;

typedef struct {
	msg_header_t def_hdr;
	msg_header_t data_hdr;
	char data_type[80];
	char data_def[MAX_DATA_DEF_SIZE];
	unsigned char array[MAX_BUF_SIZE];
	int hdr_reqd;
	int write_p;
	int read_p;
} buffer_t;

typedef struct {
	msg_header_t info_hdr;
	char filename[256]; // was 80 char long before. Includes the path and prefix
	char extension[20];
	char call_sign[5];
	char filestring[256];   // This variable stores the full path to the filename including filenuber and extension
	int hdr_reqd;
	int max_files;
	FILE *f;
	char number[10];
	int num_buffers;
	int num_events;
	char signature[10];
	char flight[80];
	char date[80];
	buffer_t *buf[MAX_BUFFERS];
} logfile_t;

// Function prototypes
void *Log_Thread(void *arg);
void Increment_Filenum(char *filename);
void Write_Filenum(char *);
void Log_Data(int, logfile_t *);
int Count_Files(logfile_t []);
void Open_Files(logfile_t [], int);
void Close_Files(logfile_t []);
void Delete_File(char *);
void Init_FileInfo_Header(logfile_t *);
void Create_Filename(logfile_t *, int);
void Add_Buffer(logfile_t *, buffer_t *);
void Add_Event(logfile_t *, buffer_t *);
void Add_File(logfile_t *, char *, char *, char *, int, int);
void Write_To_Buffer(void *data, int n_bytes, buffer_t *buf);
void Set_Max_Storage_Size(int mb);
