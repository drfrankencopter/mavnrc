#include <stdio.h>
#include "threads.h"
#include "socket.h"
#include "log.h" // for logbuf types
#include "sendSockThread.h" // For sending clear mission to BOSS
#include "CVLADtypes.h"
#include "endian.h"
#include "NrcDefaults.h"  // For Fligth test macro to know whether to grab FCC status data from shared mem, or packets directly
#include "sleep.h"
#include "FCCstatusRECV.h"


#define NRCAUT_WDT_TIMEOUT	500000		// Sets a 1/2 second timeout now 5 seconds
#define NRCAUT_FCC_STATUS_PORT 4061

extern int endGlobal; // winPthreadConsole.c (main)

socket_t NRCAUT_fccstatus_recv_sock = {NRCAUT_FCC_STATUS_PORT};

NRCAUT_fccstatus_t NRCAUT_fccstatus_data;

#ifdef USE_SHMEM_FOR_FCC_GUIDANCE
extern NRCAUT_fccstatus_t *sharedFccGuidanceData; // Here's where we grab data from shared mem (if needed)
#endif

extern buffer_t nrcAutFccStatus_logbuf; // Declared in main winPthreadConsole.c

int NRCAUT_fccstatus_ethernet_coms_error;

// Private function prototype
void populateFccStatusFromSharedMem(void);

// Recieve the NRCAUT fcc status packet
void NRCAUT_fccstatus_Packet_Recv(void *arg)
{
	NRCAUT_fccstatus_t NRCAUT_fccstatus_data_temp;
	long rval;

#ifdef _WIN32
    pthread_setname_np(pthread_self(), "NRCAUT_fccstatus_RECv");
#else
    pthread_setname_np("NRCAUT_fccstatus_RECv");
#endif
    
#ifndef USE_SHMEM_FOR_FCC_GUIDANCE

	Open_Recv_Socket_Timeout(&NRCAUT_fccstatus_recv_sock, NRCAUT_WDT_TIMEOUT);
	NRCAUT_fccstatus_ethernet_coms_error = 1;

	while(!endGlobal) {
		rval = recv(NRCAUT_fccstatus_recv_sock.s, &NRCAUT_fccstatus_data_temp, sizeof(NRCAUT_fccstatus_data), 0);
		
		if( rval > 0 ) {						
			Struct_ntohl((void *)(&NRCAUT_fccstatus_data_temp), sizeof(NRCAUT_fccstatus_data_temp));// swap endian to network byte order
			// now handle doubles...
			Swap_Double(&NRCAUT_fccstatus_data_temp.fcc_gps_time);
			Swap_Double(&NRCAUT_fccstatus_data_temp.cur_latitude_cmd);
			Swap_Double(&NRCAUT_fccstatus_data_temp.cur_longitude_cmd);
			Swap_Double(&NRCAUT_fccstatus_data_temp.prev_latitude_cmd);
			Swap_Double(&NRCAUT_fccstatus_data_temp.prev_longitude_cmd);
			memcpy(&NRCAUT_fccstatus_data, &NRCAUT_fccstatus_data_temp, sizeof(NRCAUT_fccstatus_data));
			NRCAUT_fccstatus_ethernet_coms_error = 0;
			Write_To_Buffer((void *)(&NRCAUT_fccstatus_data), sizeof(NRCAUT_fccstatus_data), &nrcAutFccStatus_logbuf);
		} else {
			NRCAUT_fccstatus_ethernet_coms_error = 1;
		}
	}

#ifdef _WIN32
	closesocket(NRCAUT_fccstatus_recv_sock.s); // windows version of close...
#else
	close(NRCAUT_fccstatus_recv_sock.s);
#endif
#else // Not flight test code
	while (!endGlobal) {
		populateFccStatusFromSharedMem();
		Write_To_Buffer((void *)(&NRCAUT_fccstatus_data), sizeof(NRCAUT_fccstatus_data), &nrcAutFccStatus_logbuf);
	}
#endif

}


#ifdef USE_SHMEM_FOR_FCC_GUIDANCE   // We don't receive from shared mem when running on eng WS (note multi QGC will need to handle this differently
void populateFccStatusFromSharedMem(void) {
	memcpy(&NRCAUT_fccstatus_data, sharedFccGuidanceData, sizeof(NRCAUT_fccstatus_t));
	usleep(100000); // About 10 Hz
}
#endif
