#pragma once
#include <math.h>
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#include <standard/mavlink.h>
#pragma clang diagnostic pop
#include "conversions.h"
#include "sendSockThread.h" // for definition of bossMission_t

#include "NrcDefaults.h"
#include "globals.h"
#include "remotes.h"

#define CYCLE_RATE 100 // 100 Hz nominal rate




// NOTE!!! The MEssage Send Rates MUST BE integer divisible by the cycle rate
// Aug 18
#define HEARTBEAT_MSG_RATE_HZ 1.0
#define GPS_POS_MSG_RATE_HZ 10.0
#define ATTITUDE_MSG_RATE 10.0
#define SYSTEM_STATUS_RATE_HZ 1.0
#define HOME_POS_RATE_HZ 0.2
#define VFR_HUD_RATE_HZ 4.0
#define MISSION_PROGRESS_RATE 10.0 // Modified Feb 26 to match Supervisor rate
#define LOCAL_POS_NED_RATE 10.0
#define PEREGRINE_HOME_POS_AND_ORIGIN_RATE 0.01 //Every 100 seconds
#define PING_MSG_RATE 0.01
// Flags for controlling sim peregrine timer
#define SIM_PEREGRINE_TIMER_RESET 0
#define SIM_PEREGRINE_TIMER_RUN 1

typedef struct {
	double lat; // WGS 84 Coords
	double lon;
	float  x; // Local coords
	float  y;
	float  z; // Alt in meters (note that send out in mm)
	quaternion_t q;
	float approach_x; // End of appraoch vector
	float approach_y; 
	float approach_z;
} home_pos_t;

typedef struct {
    uint32_t frame;
    uint32_t command;
    uint32_t current;
    uint32_t autocontinue;
    float param1;
    float param2;
    float param3;
    float param4;
    uint32_t x;
    uint32_t y;
    float z;
} missionRecording_t;

typedef struct {
    uint32_t currentItemNo;
    uint32_t frame;
    uint32_t command;
    uint32_t current;
    uint32_t autocontinue;
    float param1;
    float param2;
    float param3;
    float param4;
    uint32_t x;
    uint32_t y;
    float z;
} missionStatusRecording_t;

typedef struct {
	uint16_t x; // -1000 to 1000 INT16_MAX if not valid
	uint16_t y;
	uint16_t z;
	uint16_t r;
	uint16_t buttons;
} joystickData_t;

extern joystickData_t myJoystickData; // For globalb access

void updateVehicleMavLinkStatusThread(void *arg);

void setMissionMonitorStatusTo(int status);

void sendHeartBeatMsg(uint8_t mavState);
void sendGpsPosMsg(void);
void sendSystemStatusMsg(void);
void sendExtendedSystemStatus(void);
void sendAttitudeMsg(void);
void sendAttitudeQuaternionMsg(void);
void sendCommandAck(uint16_t cmd, uint8_t result, uint8_t progress, int32_t resultParam2, uint8_t targetSys, uint8_t targetComp);
void sendProtocolVersion(void);
void sendAutopilotVersion(int target);
void processTakeoffRequest(mavlink_command_long_t command);
void sendSimpleTextToGcs(char *myText, uint8_t severityLevel); // Send a single line to GCS
void writeTextMsgToLogFile(char *myText, uint8_t severityLevel); // Write the simple text into a log buffer

bool initializeLocalFrame(void);
void setCurrentPositionAsHome(void);
void sendHomePos(void);
void sendVfrHud(void);
void sendGpsRaw(void);
void resetAllMissionItems(void);
void sendGpsGlobalOrigin(double lat, double lon, float alt);
void sendLocalPosNed(void);
void broadcastCurrentMissionItem(int itemNo);
void broadcastMissionItemReached(void);
void monitorMissionProgress(void);
void sendHomePosPeregrine(void);
void sendGpsGlobalOriginPeregrine(void);
void simulatePeregrineFindingLp(int resetRunFlag);
uint8_t determineMavState(void);
void verifyRemotesConnection(remotes_infos_t* remotesInfo);
void broadcastLandingPoint(double lat, double lon, float heading);

#ifdef _WIN32
int gettimeofday(struct timeval* tp, struct timezone* tzp);
#endif