//
//  data_defs.h
//  iNS Test
//
//  Created by Kris Ellis on 11-11-08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#pragma once
#include "log.h"

// Function prototypes here
void HG1700_Data_Def(buffer_t *);
void LandingTarget_Data_Def(buffer_t *buf);
void LocalPosition_Data_Def(buffer_t *buf);
void missionData_Data_Def(buffer_t *buf);
void missionStatus_Data_Def(buffer_t *buf);
void missionRallyPointData_Data_Def(buffer_t *buf);
void missionGeofenceData_Data_Def(buffer_t *buf);
void SupervisorLandingPoint_Data_Def(buffer_t *buf);
void NrcAutFccStatusDataDef(buffer_t *buf);

void bossMission_Data_Def(buffer_t *buf);
void missionManager_Data_Def(buffer_t *buf); //for internals to the mission mgr
