#pragma once
#include <stdio.h>
// This header is intended to house the function prototypes allowing the perception system to communicate with the supervisor
// In early testing we will use global variables, or passed arguments in functions, but eventually we will need to move to
// a fully UDP packet based protocol

//Jan 27/2021 The current thinking is that in the initial implementation that the functions here will call supervisor functionsdirectly,
// but that in later implmentations they will send coms signals to the supervisor that will require teh supervisor to parse/recognize,
// and act upon

void signalLpFound(double lat, double lon, float hdg);
void signalPerceptionSystemStatusChange(int status);
