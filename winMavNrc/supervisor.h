#include <standard/mavlink.h> // Needed for understanding mission items
#include "CVLADtypes.h"
//Version 1.3 Flown on Monday March 22

#define NRCAUT_SUPERVISOR_PORT 4060
#define REPO_TIMER 60.0 //sec
#define ORBIT_TIMER 120.0 //sec
#define LP_ACCEPT_TIMER 120.0 //sec

#define DEFAULT_ORBIT_RADIUS 30.0 //m
#define DEFAULT_ORBIT_VELOCITY 2.0 //kmots
#define DEFAULT_HOVER_ALTITUDE_AGL 15.0 //ft
#define DEFAULT_ORBIT_YAW_BEHAVIOUR ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TANGENT_TO_CIRCLE

#define LP_HOR_ACCEPT_TOLERANCE_DISTANCE 5.0 //meters, hor acceptance criteria to do repo

// Apr 26 2021 KE REmoved a ton of double defines here....PLZ STOP doing this!
// Also, let's keepo all the like criteria together...e.g. all the land cirt are below
#define DEFAULT_LAND_CRITERIA_TIME 3.0 //sec
#define DEFAULT_LAND_CRITERIA_HOR_DIST 10.0 //in feet
#define DEFAULT_LAND_CRITERIA_VERT_DIST 5.0  // 3 feet as per DDD but imcreased to 5 as requested prior to flihjt test
#define DEFAULT_LAND_CRITERIA_VEL 3.0 //knots
#define DEFAULT_LAND_CRITERIA_HDG 15.0 //5 degrees in DDD but requested to increase to 15 prior to the flight 
#define DEFAULT_INITIAL_DISTANCE_CRIT 6.0 // Feet to get within LP pos before running the rest of the criteria
#define LP_TIME_ACCEPT_TOLERANCE 20.0 //seconds


#define MAX_REPO_VEL 5.0 //knots -- This matches the FCC value
#define MIN_REPO_VEL  1.0 //knots
#define REPO_TRANSIT_TIME  10.0 //secs, cannot begreater than repo timer; desirably twice less than repo timer


struct hoverCriteria{
    double desiredLat;		// Decimal degrees
    double desiredLon;		// Decimal degrees
    float desiredAltMSL;	// Ft
	float desiredHdgDeg;	// 0360 True hdg
    double horDistTolFt;		// Ft
    double initialHorzDistTolFt; // allow for specifying a tighter initial position tolerance...so we can get to the center of the LP before counting the other criteria
	double vertDistTolFt;		// Ft
	double velTolKts;			// Kts
	double timeTol;			// Seconds
	double timeCritFirstMet;
	double hoverCompleted;
	double hdgToleranceDeg;
    int initialDistanceCritMet; // Set to 1 if it's met (or not required). Zero otherwise
	int manCtrlRequiredAfterCritMet; // Allows supervisor to flag display system that MAN CTRL needed

};
typedef struct {
    
    int LPnum;
    double GpsTime;
    double Lat;
    double Lon;
    float hdg;

} landingPtData_t;

void *supervisorMain(void *arg);
int sendSupervisorPacketToFcc(void);
void supervisorMissionItemPacketStuffer(mavlink_mission_item_int_t    newMissionItem);
void fccStatusDataSpoofer(void);
void sendStartMission(void);
void handleMissedWaypoint(void);
void handleRequestStartMission(void);
void handleLpFound(double lat, double lon, float hdg);
void handlePerceptionSystemStatusChange(int status);
void handleLastWaypointAchieved(mavlink_mission_item_int_t    newMissionItem);
void handleNewCurrentMissionItem(mavlink_mission_item_int_t	newMissionItem);
void handleLpRepoTimerExpired(void);
void handleLzScanTimerExpired(void);
void handleLpReposition(mavlink_mission_item_int_t    newMissionItem);
void handleDirectToReposition(mavlink_mission_item_int_t    newMissionItem, float final_velocity);
void Land(void);
void HoverAtLocation(mavlink_mission_item_int_t    newMissionItem);
void StartOrbit(mavlink_mission_item_int_t    newMissionItem);
void handleLpAcceptTimerExpired(void);
void handleReachedEndOfMission(void);
int CheckHoverCriteria(void);
void SetHoverCriteria(double lat, double lon, float alt, float hdg, float hDist, float vDist, float vel, float hdgTol, float time);
void setHoverCritInitialDistanceTol(float myDistTolFt);
int CheckLandedCriteria(void);
void setSupervisorStatusBits (NRCAUT_control_mode_t new_mode);
void clearSupervisorStatusBits (void);
void startLPAcceptTimer(void);
void startLpRefineTimer(void);
void startLPRepoTimer(void);
void startOrbitTimer(void);
void setHoverCritManCtrlRequired(void);
void startMissionTimer(void);
void writeLpDataToDisk(int num, double lat, double lon, float hdg);





