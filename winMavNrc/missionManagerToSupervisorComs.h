#pragma once
#include <stdio.h>
#include <standard/mavlink.h> // Needed for understanding mission items

extern void startMission(void); // Intended to be called by supervisor to signal the start of the mission. This fn is located in mavLinkRcv.c
void signalGcsRequestedStartMission(void);
void signalNewMissionItem(mavlink_mission_item_int_t newMissionItem);
void signalLastWaypointAchieved(mavlink_mission_item_int_t lastMissionItem);
void signalDirectToReposition(mavlink_mission_item_int_t    myDtMissionItem, float final_velocity);
int isCurrentMissionStarted(void);
