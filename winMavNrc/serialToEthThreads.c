//
//  serialToEthThreads.c
//  mavNrc
//
//  Created by Kris Ellis on 2020-07-30.
//  Copyright © 2020 Kris Ellis. All rights reserved.
//

// This file is intended to hold the threads necessary for communicating with the serial to Ethernet converter box
// I'm not sure how well it will work on windows (if at all)

#include <stdio.h>
#include "threads.h"
#include "sleep.h"
#include <stdlib.h>
#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#define socklen_t int  // seriosly, winsock doesn't define this....ugh
#pragma comment (lib, "ws2_32.lib")
#define WIN32_LEAN_AND_MEAN  // Prevent windows.h from including winsock...put this in the project defines
#include <windows.h>
#else 
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation" // prevents doxygen warnings from overpowering other errors...
#include <common/mavlink.h> // For mav parse
#include <standard/mavlink.h>
#include "serialToEthThreads.h" // for sending to peregrine
#include "mavLinkRcv.h" // For access to processing functions
#pragma clang diagnostic pop
#include "sendSockThread.h"
#include "updateVehicleMavLinkStatusThread.h"
#include <semaphore.h>  // We will use regular semaphores to replace the RT ones
#include <errno.h>
#include "equivalenceCOREsharedMem.h" //access to shared mem and standard varaible macro definitions
#include "geo.h"
#include "log.h"
#include "socket.h"
#include "perceptionToSupervisorComs.h"
#include "systime.h"

// Currently the serialToEthRcvThread is responsible for signalling landing points from the Peregrine. It will need to talk to the Supervisor module


// Extern'd globals
extern int endGlobal; // winPthreadConsole.c (main)
extern missionData_t* mission_data; //current mission is the one we are executing, defined in updateVehicleMavLinkStatus

//Local-global variables
static int debug = 0; // Set to 0 to bypass debug output
static int serialSockFd; // File descriptor for the socket
static int peregrineHeartBeatsMissed = 0;

static struct sockaddr_in serialToEthServerAddr; // This is the server side name...where we receive data
static struct sockaddr_in serialToEthClientAddr; // This is where we will be sedinng data to
static socklen_t clientAddrSize = sizeof(struct sockaddr_in); // Holds the senders address for

static mavlink_status_t lastStatus; // Previous status value

#define MAV_SERIAL_MSG_BUFFER_LENGTH 64 // Max # of messages to buffer
static mavMsgData_t mavSerialMsgBuffer[MAV_SERIAL_MSG_BUFFER_LENGTH]; // Our array of mav messages going for serial conversion
static int bufferInputIndex=0, bufferOutputIndex=0;

#ifdef _WIN32
static sem_t sendSerialMavMsgSem;  // Don't know why but these can't be pointers in windows
#else
static sem_t *sendSerialMavMsgSem;
#endif
static int connectedToSerial = 0; // Set to 1 for valid connection
static int semaphoreValid = 0; // Prevent signaling invalid semaphores

#define SEND_SERIAL_MAV_MUTEX_NAME "/sendSerialMavMutex"  // No idea if this will work on Windows...but MacOs doesn't support unnamed semaphores
#define HEARTBEAT_LOST_THRESHOLD 8 // sucessive missing heartbeats before declaring a problem
//Fn protos
static int initializeSerToEthSocket(void);
static void processPeregrineMavlinkMsg(mavlink_message_t msg);

static void sendPeregrineMissionItemIntForRequest(mavlink_mission_request_int_t missionRequest);
static void sendPeregrineMissionItemForRequest(mavlink_mission_request_t missionRequest); //THis response to mission request (not int)
static void processPeregrineMissionRequestList(mavlink_message_t msg);
static void processPeregrineMissionRequestInt(mavlink_message_t msg);
static void processPeregrineMissionRequest(mavlink_message_t msg);
static void processPeregrineMissionAck(mavlink_message_t msg);
static void rebootPeregrine(void);
static void shutdownPeregrine(void);


void processPeregrineCommandLong(mavlink_message_t msg);
void processSendPeregrineHomePos(mavlink_message_t msg);
void processLandingTargetMessage(mavlink_message_t msg);
int globallocalconverter_toglobal(double x, double y, double z,  double *lat, double *lon, float *alt);
void processRepositionCmdLong(mavlink_command_long_t command);
void processHeartbeatMsg(mavlink_message_t msg);

//Static private function
int initializeSerToEthSocket(void) {
    // This code does the socket initilization required for the serToEth converter
    // Creating socket file descriptor

#ifdef _WIN32
	initialize_windows_sockets();
#endif

    // Big changes here Nov 5 2021 As Peregrine Now Can send UDP direct

    printf("Initializing Serial To Ethernet Socket\n");
    if ( (serialSockFd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("SerToEthernet socket creation failed:");
        return -1; //Signal failure
    }
    //Now configure the socket option for a recv timeout of 1 second
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    if (setsockopt(serialSockFd, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof(tv)) < 0) {
        // Socket option error
        perror("SerToEthernet unable to set Socket Rcv Timeout:");
        printf("Continuing, but heartbeat monitor will not function\n");
    }
    
    // Now complete the server (self) information
    memset(&serialToEthServerAddr, 0, sizeof(serialToEthServerAddr));  // Zero out initially
    memset(&serialToEthClientAddr, 0, sizeof(serialToEthClientAddr));
    
    // Filling server information
    serialToEthServerAddr.sin_family    = AF_INET; // IPv4
    serialToEthServerAddr.sin_addr.s_addr = INADDR_ANY;
    serialToEthServerAddr.sin_port = htons(MAVLINK_OVER_UDP_PORT);
    

    
    // Bind the socket with the server address
    if ( bind(serialSockFd, (const struct sockaddr *)&serialToEthServerAddr,
              sizeof(serialToEthServerAddr)) < 0 )
    {
        perror("SerToEthernet bind failed");
        return -1; //Signal failure
    }
    
    //Fill in the client side here
    memset(&serialToEthClientAddr, 0, sizeof(serialToEthClientAddr));
    serialToEthClientAddr.sin_family = AF_INET;
    serialToEthClientAddr.sin_addr.s_addr = inet_addr(PEREGRINE_IP);
    serialToEthClientAddr.sin_port = htons(MAVLINK_OVER_UDP_PORT);
    
    if (connect(serialSockFd,(struct sockaddr *) &serialToEthClientAddr, sizeof(struct sockaddr)) < 0 ) {
        perror("Connect Peregrine Error: ");
        return -1;
    }
    return 0; // For success...
}



void serialToEthernetRcvThread(void *arg) {
    // The receive thread will act as the server thread, setting up the socket
    
    mavlink_message_t myNewMavMessage; // Here's where we will read the msg intp
    mavlink_status_t myNewMavStatus; // Status of the message?
    static unsigned char charRcvBuffer[MAX_SER_BUFFER_CHARS]; // This is a buffer that holds the input from the serial to Eth converter itself...cleared after every packet is read and processed

    //struct sockaddr senderAddr;
    socklen_t senderAddrSize = sizeof(struct sockaddr_in); // Holds the senders address for

    printf("Serial to Ethernet thread spawned\n");
    if (initializeSerToEthSocket()!=0) {
        printf("Error: Can't open serial to Ethernet Socket\n");
        // Not sure how to handle this...let's hope it doesn't happen
        // Perhaps we just put the program into a while loop that keeps re-trying?
    } else {
        connectedToSerial = 1; // Now a valid connection for serial
    }
    
    while(!endGlobal) {
        // Main thread loop
		//Check for the peregrine shutdown switch being up
		//if (shared_memory->ffs_output.ffs_switches & BIT(27)) { // Commented out Sept 20 2021 as FFS can occas gen random DIO outs (and reboots/shutdowns)
        if (shared_memory->cif_output.function_switches & BIT(11)) {
			//Shutdown the Peregrine
			shutdownPeregrine();
		}
        /* // Peregrine reboot commented out Sep 20 2021 due to FFS ananlog random fail issue
        else if (shared_memory->ffs_output.ffs_switches & BIT(26)) {
			//Soft restart Peregrine
			rebootPeregrine();
		}
         */
        // Start by clearing the RcvBuffer
        memset(charRcvBuffer, 0, MAX_SER_BUFFER_CHARS); // Clear to zeros
        int bytesReceived = recvfrom(serialSockFd, &charRcvBuffer, MAX_SER_BUFFER_CHARS, 0, (struct sockaddr *) &serialToEthClientAddr, &senderAddrSize);
        if (bytesReceived>0) {
            // We got data from the port
            // Now do something with it
            // We will use the convenience mavlink parse function to help out
            // Loop through each byte received
            //printf("Got serial data...\n");
            uint8_t messageReceived = false;
            for (int i=0; i<bytesReceived; i++) {
                messageReceived = mavlink_parse_char(SERIAL_TO_ETH_COM, (uint8_t)charRcvBuffer[i], &myNewMavMessage, &myNewMavStatus);
                // Check for dropped packets
                if ((lastStatus.packet_rx_drop_count != myNewMavStatus.packet_rx_drop_count) && debug) {
                    printf("SERIAL RCV ERROR: DROPPED %d PACKETS\n", myNewMavStatus.packet_rx_drop_count);
                    fprintf(stderr,"%02x ", charRcvBuffer[i]);
                }
                lastStatus = myNewMavStatus; // Update prev status field
                // Now check for a completed message
                if (messageReceived) {
                    // Send the message out for handling
                    // Debug verbose output
                    if (debug) {
                        DEBUG_PRINT(("Received message from serial with ID #%d (sys:%d|comp:%d):\n", myNewMavMessage.msgid, myNewMavMessage.sysid, myNewMavMessage.compid));
                        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
                        // check message is write length
                        unsigned int messageLength = mavlink_msg_to_send_buffer(buffer, &myNewMavMessage);
                        if (messageLength > MAVLINK_MAX_PACKET_LEN){
                            fprintf(stderr, "\nFATAL ERROR: MESSAGE LENGTH IS LARGER THAN BUFFER SIZE\n");
                        } else {
                            // Just print the message to the console
                            for (int j=0; j<messageLength; j++) {
                                unsigned char v=buffer[i];
                                fprintf(stderr,"%02x ", v);
                            }
                            fprintf(stderr,"\n");
                        }
                    } // End of debug stuff
                    processPeregrineMavlinkMsg(myNewMavMessage);
                    //Now clear the message for re-use
                    memset(&myNewMavMessage, 0, MAVLINK_MAX_PACKET_LEN); // Hope this zeros the msg without error
                }
                // Hopefully the loop just continues even if we complete a mav message
            }

        } else {
            // We didn't get data
            // Handle errors here....
            if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                // We timed out, increment the heartbeats missed
                peregrineHeartBeatsMissed++;
                if (peregrineHeartBeatsMissed == HEARTBEAT_LOST_THRESHOLD) { // This should enter only once upon exceeding the lost heartbeat threshold (8)
                    //Send a message when we pass 3 missing heartbeats
                    sendSimpleTextToGcs("Not receiving peregrine heartbeats", MAV_SEVERITY_INFO);
                    signalPerceptionSystemStatusChange(0);
                }
            }
            
        }
        
    }
}

void processPeregrineMavlinkMsg(mavlink_message_t msg) {
    // This is a function that we call once a complete message has been assembled
    uint8_t buf[512]; // a local buffer
    
    // We will start by dissecting the message into its parts...for printing/verification
    if (debug) {
        printf("-------------------------------------------------\n");
        printf("|Mavlink message completed...here are the contents\n");
        printf("|Packet Start = %02X -- Should be 0xFD\n", msg.magic);
        printf("|Payload length = %d bytes\n", msg.len);
        printf("|Incompat flags = %d\n", msg.incompat_flags);
        printf("|Compat flags = %d\n", msg.compat_flags);
        printf("|Packet Seq# = %d\n",msg.seq);
        printf("|System Id = %d\n", msg.sysid);
        printf("|Component Id = %d\n", msg.compid);
        printf("|Message Id = %d\n", msg.msgid);
        printf("-------------------------------------------------\n");
    }
    
    // Cue this up for sending on an internal port
    // Since traffic is expected to be low on this port I'll just send out here rather than
    // buffering in a separate thread as is done in sendSockThread.c
    
    switch (msg.msgid) {
            // We will deal with msg receipt in a manner similar to that in the GCS thread in mavLinkRcv.c
        static  int prevMavlink240Status = -1; // Default to un initialized
        static  int prevMavlink196Status = -1;
        case MAVLINK_MSG_ID_HEARTBEAT: // msg 0
            //Commented out debug print here since Heartbeats are so common
            //DEBUG_PRINT(("Serial port Received a Heartbeat msg\n"));
            processHeartbeatMsg(msg);
            if (peregrineHeartBeatsMissed >= HEARTBEAT_LOST_THRESHOLD) {
                sendSimpleTextToGcs("Receiving Peregrine Heartbeats", MAV_SEVERITY_INFO);
                // Let's re-initialize the different mavlink component statuses so they can be reported below
                prevMavlink240Status = -1;
                prevMavlink196Status = -1;
            }
            peregrineHeartBeatsMissed = 0; // Reset the missed heartbeat count
            // Here is where we do some more detailed analysis of the Peregrine HEartbeat
            mavlink_heartbeat_t myHeartbeat;
            mavlink_msg_heartbeat_decode(&msg, &myHeartbeat);
            // Now look inside;

            if (msg.compid == PEREGRINE_COMP_ID) {
                if (myHeartbeat.system_status != prevMavlink240Status) {
					signalPerceptionSystemStatusChange(myHeartbeat.system_status); // Tell supervisor about status change
                    switch (myHeartbeat.system_status) {
                        case MAV_STATE_BOOT:
                            sendSimpleTextToGcs("Peregrine 240 is Booting", MAV_SEVERITY_INFO);
                            printf("Peregrine is waking up...don't move\n");
							
                            break;
                        case MAV_STATE_CALIBRATING:
                            sendSimpleTextToGcs("Peregrine 240 is Calibrating (acquiring GPS)", MAV_SEVERITY_INFO);
                            printf("Peregrine is still acquiring GPS...don't move\n");
                            break;
                        case MAV_STATE_CRITICAL:
                            sendSimpleTextToGcs("Peregrine 240 has lost GPS solution", MAV_SEVERITY_INFO);
                            printf("Peregrine has lost GPS\n");
                            break;
                        case MAV_STATE_EMERGENCY:
                            sendSimpleTextToGcs("Peregrine 240 has lost GPS solution", MAV_SEVERITY_INFO);
                            printf("Peregrine has lost GPS\n");
                            break;
                        case MAV_STATE_ACTIVE:
                            sendSimpleTextToGcs("Peregrine 240 is Active and ready", MAV_SEVERITY_INFO);
                            printf("Peregrine has GPS and thinks it knows where it is\n");
                            break;
                        case MAV_STATE_STANDBY:
                            sendSimpleTextToGcs("Peregrine 240 is Standby 4 msn mode", MAV_SEVERITY_INFO);
                            printf("Peregrine is in standby, don't know what this means\n");
                            break;
                        default:
                            sendSimpleTextToGcs("Peregrine 240 is in an unknown state", MAV_SEVERITY_INFO);
                            printf("Peregrine is in an unknown state, mode is %d\n", myHeartbeat.system_status);
                            break;
                    }
                }
                prevMavlink240Status = myHeartbeat.system_status;
            } else if (msg.compid == 196) {
#ifndef IGNORE_PEREGRINE_196_ROS_MODULE
                if (myHeartbeat.system_status != prevMavlink196Status) {
                    signalPerceptionSystemStatusChange(myHeartbeat.system_status); // Tell supervisor about status change
                    switch (myHeartbeat.system_status) {
                        case MAV_STATE_BOOT:
                            sendSimpleTextToGcs("Peregrine is Booting", MAV_SEVERITY_INFO);
                            printf("Peregrine 196 is waking up...don't move\n");
                            break;
                        case MAV_STATE_CALIBRATING:
                            sendSimpleTextToGcs("Peregrine 196 is Calibrating (acquiring GPS)", MAV_SEVERITY_INFO);
                            printf("Peregrine is still acquiring GPS...don't move\n");
                            break;
                        case MAV_STATE_CRITICAL:
                            sendSimpleTextToGcs("Peregrine 196 has lost GPS solution", MAV_SEVERITY_INFO);
                            printf("Peregrine has lost GPS\n");
                            break;
                        case MAV_STATE_EMERGENCY:
                            sendSimpleTextToGcs("Peregrine 196 has lost GPS solution", MAV_SEVERITY_INFO);
                            printf("Peregrine has lost GPS\n");
                            break;
                        case MAV_STATE_ACTIVE:
                            sendSimpleTextToGcs("Peregrine 196 is Active and ready", MAV_SEVERITY_INFO);
                            printf("Peregrine has GPS and thinks it knows where it is\n");
                            break;
                        case MAV_STATE_STANDBY:
                            sendSimpleTextToGcs("Peregrine 196 is in Standby mode", MAV_SEVERITY_INFO);
                            printf("Peregrine is in standby, don't know what this means\n");
                            break;
                        default:
                            //sendSimpleTextToGcs("Peregrine is in an unknown state", MAV_SEVERITY_ALERT);
                            printf("Peregrine is in an unknown state, mode is %d\n", myHeartbeat.system_status);
                            break;
                    }
                }
                prevMavlink196Status = myHeartbeat.system_status;
#endif // IGNORE PEREGRINE ROS MODULE
            }
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST_LIST: // Msg 43
            processPeregrineMissionRequestList(msg);
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST_INT: // MSg 51
            processPeregrineMissionRequestInt(msg);
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST: // Msg 40 (deprecated, but used by Peregrine)
            processPeregrineMissionRequest(msg);
            break;
        case MAVLINK_MSG_ID_MISSION_ACK: // Msg 47
            processPeregrineMissionAck(msg);
            break;
        case MAVLINK_MSG_ID_COMMAND_LONG: // Msg 76
            DEBUG_PRINT(("Received a Command Long Msg\n"));
            processPeregrineCommandLong(msg);
            break;
        case MAVLINK_MSG_ID_SYS_STATUS:
            DEBUG_PRINT(("Serial port Received a system status msg\n"));
            break;
        case MAVLINK_MSG_ID_GPS_RAW_INT: // msg 24
            DEBUG_PRINT(("Serial port Received an Gps Raw msg\n"));
            break;
        case MAVLINK_MSG_ID_ATTITUDE: // msg 30
            DEBUG_PRINT(("Serial port Received an attitude msg\n"));
            break;
        case MAVLINK_MSG_ID_ATTITUDE_QUATERNION: // msg 31
            DEBUG_PRINT(("Serial port Received an attitude quaternion msg\n"));
            break;
        case MAVLINK_MSG_ID_LOCAL_POSITION_NED: // msg 32
            DEBUG_PRINT(("Serial port Received a global position NED coords msg\n"));
            break;
        case MAVLINK_MSG_ID_GLOBAL_POSITION_INT: //msg 33
            DEBUG_PRINT(("Serial port Received a global position internal coords msg\n"));
            break;
        case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW: // msg 36
            DEBUG_PRINT(("Serial port Received a servo output raw msg\n"));
            break;
        case MAVLINK_MSG_ID_VFR_HUD:
            DEBUG_PRINT(("Serial port Received a VFR HuD msg\n"));
            break;
        case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED: // msg 85
            DEBUG_PRINT(("Serial port Received a position target local NED coords msg\n"));
            break;
        case MAVLINK_MSG_ID_HIGHRES_IMU: // Msg 105
            DEBUG_PRINT(("Serial port Received a HighRes IMU msg\n"));
            break;
        case MAVLINK_MSG_ID_TIMESYNC: // msg 111
            DEBUG_PRINT(("Received a TimeSync msg\n"));
            break;
        case MAVLINK_MSG_ID_ACTUATOR_CONTROL_TARGET: // msg 140
            DEBUG_PRINT(("Received an Actuator control target msg\n"));
            break;
        case MAVLINK_MSG_ID_ALTITUDE: // msg 141
            DEBUG_PRINT(("Received an Altitude msg\n"));
            break;
        case MAVLINK_MSG_ID_LANDING_TARGET:
            printf("Received a Landing Target Message!!\n");
            processLandingTargetMessage(msg);
            break;
        case MAVLINK_MSG_ID_EXTENDED_SYS_STATE: // msg 245
            DEBUG_PRINT(("Received an extended system state msg\n"));
            break;
        default:
            printf("\nReceived an un-handled MavLink message. Details below:\n");
            printMavLinkMsg(sizeof(msg), msg);
    }
    
    
}

// Note that as of Sep 20 2021 this thread is bypassed when DONT_BUFFER_MAVLINK_PEREGRINE_MESSAGES is defined
void sendSerialMavLinkThread(void *arg) {
    // This thread does the actual work on the socket
    // and ultimately empties the FIFO
    uint8_t buf[512]; // a local buffer



    //struct sockaddr senderAddr;

    
    static int addressAvailable = 1;
    
    //Now initialize the semaphore

#ifdef _WIN32
	int retVal = sem_init(&sendSerialMavMsgSem, 0, 0); // Initialize sem for cross threads, and 0 starting value
	if (retVal < 0) {
		printf("Couldn't open mavlink peregrine serial message cue semaphore, errno = %d\n", errno);
		perror("MavLink Semaphore: ");
}
	else {
		printf("Opened mavlink message cue semaphore\n");
		semaphoreValid = 1;
	}
#else
	sendSerialMavMsgSem = sem_open(SEND_SERIAL_MAV_MUTEX_NAME, O_CREAT, 0660, 1);
	if (sendSerialMavMsgSem == SEM_FAILED) {
		printf("Couldn't open serial semaphore\n");
		perror("MavLink Semaphore: ");
	}
	else {
		printf("Opened serial semaphore\n");
		semaphoreValid = 1;
	}
#endif
	
	
	
    
	while (!endGlobal && semaphoreValid) {
#ifdef _WIN32
		sem_wait(&sendSerialMavMsgSem); // Wait to decrement the semaphore from the write to msg cue function
#else
		sem_wait(sendSerialMavMsgSem); // Wait to decrement the semaphore from the write to msg cue function
#endif
		
		bufferOutputIndex++;
		if (bufferOutputIndex >= MAV_SERIAL_MSG_BUFFER_LENGTH) bufferOutputIndex = 0; // Roll around)
		int bytesToSend = mavlink_msg_to_send_buffer(buf, &mavSerialMsgBuffer[bufferOutputIndex].mavMsg);
		long bytesSent = sendto(serialSockFd, buf, bytesToSend, 0, (const struct sockaddr *) &serialToEthClientAddr, clientAddrSize); //Note that for multiple destinations we should use send to
		if (bytesSent < 0) {
			if (errno == 49) { // No address available (we haven't received any data from the serial port yet, so we don't know who to send to
				if (addressAvailable) { // We thought there was an address available
					addressAvailable = 0;
					printf("Don't have and address for the serial to ethernet converter yet...need to receive packets first\n");
				}
			}
			else {
				// It was an error other than 49 (no address)
				DEBUG_PRINT("There was an error sending  MavLink UDP data to serial converter\n");
#ifdef _WIN32
				DEBUG_PRINT("Send failed with errorcode: %d\n", WSAGetLastError());
#else
				perror("MavLink SendError: "); // Untested
				printf("Error #: %d\n", errno);
#endif
			}


		}
		else {
			if (addressAvailable == 0) printf("Received an IP address for the serial to Eth converter...successfully sending packets\n");
			addressAvailable = 1; // We sent bytes and it worked
		}
		mavSerialMsgBuffer[bufferOutputIndex].msgPendingStatus = 0; // message sent
		
    }

	if (semaphoreValid) {
#ifdef _WIN32
		sem_destroy(&sendSerialMavMsgSem);
#else
		sem_destroy(sendSerialMavMsgSem);
#endif
		
	} else {
		printf("Serial to Ethernet thread terminated...no valid semaphore\n");
	}
}

long writeToSerialMavMsgCue(mavlink_message_t msg) {
    // This function adds the latest message to the message cue
    // Returns 0 on success, and -1 if the buffer is full
#ifdef    DONT_BUFFER_MAVLINK_PEREGRINE_MESSAGES
    // Here we just want to send stuff directly to the socket...probably cuz buffering isn't working
    if (connectedToSerial == 1) {
        uint8_t buf[512]; // a local buffer
        int bytesToSend = mavlink_msg_to_send_buffer(buf, &msg);
        // KE Commented out the sed version in favoru of sendTo below
        //int bytesSent = send(serialSockFd, buf, bytesToSend, 0); // send the bytes to the GCS
        //int bytesSent = sendto(serialSockFd, buf, bytesToSend, 0, (const struct sockaddr *) &serialToEthClientAddr, clientAddrSize); //Note that for multiple destinations we
        long bytesSent = send(serialSockFd, buf, bytesToSend, 0); //Note that for multiple destinations we

        if (bytesSent < 0) {
            perror("mavNrc UDP sendto err:");
            return -1;
        } else {
            return bytesSent;
        }
    } else {
        return -1;
    }
#else
	if (semaphoreValid) {
		bufferInputIndex++;
		if (bufferInputIndex >= MAV_SERIAL_MSG_BUFFER_LENGTH) {
			bufferInputIndex = 0; // Roll around
		}
		if (mavSerialMsgBuffer[bufferInputIndex].msgPendingStatus == 1) {
			printf("ERROR: Could not write the mav message to the serial cue...it's full!\n");
			return -1; // Could not add the message to the message cue
		}
		else {
			int mySemValue; // Note: appears unused
			// Add the message to the cue
			// Clear the data in the message cue
			memset(&mavSerialMsgBuffer[bufferInputIndex].mavMsg, 0, sizeof(mavlink_message_t));
			memcpy(&mavSerialMsgBuffer[bufferInputIndex].mavMsg, &msg, sizeof(msg));
			mavSerialMsgBuffer[bufferInputIndex].msgPendingStatus = 1;
#ifdef _WIN32
			sem_post(&sendSerialMavMsgSem); // Signal the sendMavThread that new data is waiting to be sent
#else
			sem_post(sendSerialMavMsgSem); // Signal the sendMavThread that new data is waiting to be sent
#endif
		
			return 0;
		}
	} else  {
		return -1; // Non valid semaphore
	}
#endif //Buffer wthernet data
}


void sendPeregrineMissionCount(uint8_t missionType) {
    // Read the current mission and send the # of points in this type
    printf("------Sending Mission Count To Peregrine -------\n");
    mavlink_message_t msg;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, missionType);
    mavlink_msg_mission_count_pack(MY_MAV_COMP_ID,
                                   MY_MAV_COMP_ID,
                                   &msg,
                                   PEREGRINE_SYS_ID,
                                   PEREGRINE_COMP_ID,
                                   specificMission->numMissionItems,
                                   missionType);
    writeToSerialMavMsgCue(msg);
    
}

static void sendPeregrineMissionItemIntForRequest(mavlink_mission_request_int_t missionRequest) {
    mavlink_message_t msg;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, missionRequest.mission_type);
    
    if (missionRequest.seq < specificMission->numMissionItems) {
        // Useful pointer to the item we want to send
        mavlink_mission_item_int_t* item = &specificMission->items[missionRequest.seq];
        
        
        mavlink_msg_mission_item_int_pack(MY_MAV_SYS_ID,
                                          MY_MAV_COMP_ID,
                                          &msg,
                                          PEREGRINE_SYS_ID,
                                          PEREGRINE_COMP_ID,
                                          missionRequest.seq,
                                          item->frame,
                                          item->command,
                                          item->current,
                                          item->autocontinue,
                                          item->param1,
                                          item->param2,
                                          item->param3,
                                          item->param4,
                                          item->x,
                                          item->y,
                                          item->z,
                                          missionRequest.mission_type);
    
        writeToSerialMavMsgCue(msg);
    }
}

static void sendPeregrineMissionItemForRequest(mavlink_mission_request_t missionRequest) {
    // This function is a DUPLICATE of the function sendPeregrineMissionItemIntForRequest. Should we redirect or delete? (Jeremi)
    // NO theyare not duplicates....int sends as integer lat/lons and regular item sends as floats...big differences
    // This function sends peregrine the mission item data in float format
    mavlink_message_t msg;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, missionRequest.mission_type);
    if (missionRequest.seq < specificMission->numMissionItems) {
        // Useful pointer to the item we want to send
        mavlink_mission_item_int_t* item = &specificMission->items[missionRequest.seq];
        
        
        mavlink_msg_mission_item_pack(MY_MAV_SYS_ID,
                                          MY_MAV_COMP_ID,
                                          &msg,
                                          PEREGRINE_SYS_ID,
                                          PEREGRINE_COMP_ID,
                                          missionRequest.seq,
                                          item->frame,
                                          item->command,
                                          item->current,
                                          item->autocontinue,
                                          item->param1,
                                          item->param2,
                                          item->param3,
                                          item->param4,
                                          item->x/1.0E7, // Conversions to float Lat/Lon
                                          item->y/1.0E7,
                                          item->z,
                                          missionRequest.mission_type);
    
        writeToSerialMavMsgCue(msg);
    }
}

void sendPeregrineMissionAck(uint8_t targetSys, uint8_t targetComp, uint8_t missionType) {
    mavlink_message_t msg;
    mavlink_msg_mission_ack_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, targetSys, targetComp, MAV_MISSION_ACCEPTED, missionType);
    writeToSerialMavMsgCue(msg);
}

void rebootPeregrine(void) {
    static double timeOfLastRebootRequest = 0;
    
    double timeOfCurrentRebootRequest = SysTime();
    if (timeOfCurrentRebootRequest-timeOfLastRebootRequest > PEREGRINE_REBOOT_DELAY) {
	mavlink_message_t msg;
	//mavlink_msg_command_int_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, PEREGRINE_COMP_ID, 0, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN,
	//	1, 0, 0.0, 1.0, 0.0, 0.0, 0, 0, 0.0); // Set param 2 to 1.0 to reboot peregrine
    //static inline uint16_t mavlink_msg_command_int_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
    //uint8_t target_system, uint8_t target_component, uint8_t frame, uint16_t command, uint8_t current, uint8_t autocontinue, float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z)
    //mavlink_msg_command_int_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, PEREGRINE_COMP_ID, 0, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0, 1.0, 1.0, 1.0, 1.0, 0, 0, 1.0);
    mavlink_msg_command_long_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, PEREGRINE_COMP_ID, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	writeToSerialMavMsgCue(msg);
    //mavlink_msg_command_int_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, 196, 0, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0, 1.0, 1.0, 1.0, 1.0, 0, 0, 1.0);
    mavlink_msg_command_long_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, PEREGRINE_COMP_ID, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    writeToSerialMavMsgCue(msg);
    sendSimpleTextToGcs("Peregrine reboot requested", MAV_SEVERITY_ALERT);
        timeOfLastRebootRequest = timeOfCurrentRebootRequest; // Update the reboot request
    }
}

void shutdownPeregrine(void) {
    static double timeOfLastShutDownRequest = 0;
    
    double timeOfCurrentShutDownRequest = SysTime();
    if (timeOfCurrentShutDownRequest-timeOfLastShutDownRequest > PEREGRINE_REBOOT_DELAY) {
	mavlink_message_t msg;
    // Changed from 240 (PEREGRINE_COMP_ID) to 196
	//mavlink_msg_command_int_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, 196, 0, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0, 2.0, 2.0, 2.0, 2.0, 0, 0, 2.0); // Set param 2 to 2.0 to shut down peregrine
    mavlink_msg_command_long_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, 196, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	writeToSerialMavMsgCue(msg);
    
    // Try as a command long pack
    //mavlink_msg_command_int_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, PEREGRINE_COMP_ID, 0, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0, 2.0, 2.0, 2.0, 2.0, 0, 0, 2.0); // Set param 2 to 2.0 to shut down peregrine
    mavlink_msg_command_long_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, PEREGRINE_SYS_ID, PEREGRINE_COMP_ID, MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    writeToSerialMavMsgCue(msg);
    sendSimpleTextToGcs("Peregrine Shutdown requested", MAV_SEVERITY_ALERT);
        timeOfLastShutDownRequest = timeOfCurrentShutDownRequest; // Update the time
    }
}

static void processPeregrineMissionRequestList(mavlink_message_t msg) {
    // This just simply responds with the # of items in the list
    printf("Received Mission request list from Peregrine...\n");
    mavlink_mission_request_list_t missionRequest;
    mavlink_msg_mission_request_list_decode(&msg, &missionRequest);
    sendPeregrineMissionCount(missionRequest.mission_type); // local function
}


static void processPeregrineMissionRequestInt(mavlink_message_t msg) {
    // Here the GCS (or other component) is requesting a mission item
    mavlink_mission_request_int_t missionRequest;
    mavlink_msg_mission_request_int_decode(&msg, &missionRequest);
    sendPeregrineMissionItemIntForRequest(missionRequest);
}

static void processPeregrineMissionRequest(mavlink_message_t msg) {
    // Here the GCS (or other component) is requesting a mission item
    mavlink_mission_request_t missionRequest;
    mavlink_msg_mission_request_decode(&msg, &missionRequest);
    printf("Sending Peregrine mission item# %d\n",missionRequest.seq);
    sendPeregrineMissionItemForRequest(missionRequest);
}

static void processPeregrineMissionAck(mavlink_message_t msg) {
    mavlink_mission_ack_t ackMsg;
    mavlink_msg_mission_ack_decode(&msg, &ackMsg);
    printf("Received Mission Ack type %d (0 = success) from Peregrine component %d for mission type %d\n",ackMsg.type, msg.compid,ackMsg.mission_type);
    char myMessage[50];
    if (ackMsg.type == 0) {
        snprintf(myMessage,50,"Peregrine received mission with %d items",mission_data->myCurrentMission.standard.numMissionItems);
        sendSimpleTextToGcs(myMessage, MAV_SEVERITY_ALERT);
    } else {
        snprintf(myMessage,50,"Peregrine reported mission result error %d",ackMsg.type);
        sendSimpleTextToGcs(myMessage, MAV_SEVERITY_ALERT);
    }
}

void processPeregrineCommandLong(mavlink_message_t msg) {
    mavlink_command_long_t command;
    mavlink_msg_command_long_decode(&msg, &command);
    // Now lets print the contents and find out what's inside
    
    // Handle the command here
    switch (command.command) {
        case MAV_CMD_NAV_TAKEOFF: // Msg 22
            printf("Takeoff Requested...\n");
            sendCommandAck(MAV_CMD_NAV_TAKEOFF, MAV_RESULT_ACCEPTED, 1, 0, msg.sysid, msg.compid);
            // Probably we should be sending an "in progress" message here
            // Here is where we would hook in to the flight controls to do the takeoff.
            processTakeoffRequest(command); // -- in unpdateVehcileMavLinkStatusThread
            break;
        case MAV_CMD_DO_REPOSITION:
            sendCommandAck(MAV_CMD_DO_REPOSITION, MAV_RESULT_ACCEPTED, 1, 0, msg.sysid, msg.compid); // ACK
            processRepositionCmdLong(command);
            break;
        case MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN: //MSG 256
            // For now let's just ack it
            sendCommandAck(MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, MAV_RESULT_ACCEPTED, 1, 0, msg.sysid, msg.compid); // ACK
            break;
        case MAV_CMD_REQUEST_PROTOCOL_VERSION: // Msg 519 ...deprecated, but sent by QGcs anyways
            //We are supposed to ack, and send the protocol message
            printf("Sending Peregrine Protocol Information\n");
            sendCommandAck(MAV_CMD_REQUEST_PROTOCOL_VERSION, MAV_RESULT_IN_PROGRESS, 0, 0, msg.sysid, msg.compid);
            sendProtocolVersion();
            sendCommandAck(MAV_CMD_REQUEST_PROTOCOL_VERSION, MAV_RESULT_ACCEPTED, 0, 0, msg.sysid, msg.compid);
            break;
        case MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES: // msg 520
            printf("Sending Peregrine Autopilot Capabilities\n");
            sendCommandAck(MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES, MAV_RESULT_ACCEPTED, 0, 0, msg.sysid, msg.compid); // Ack
            sendAutopilotVersion(PEREGRINE_COMP_ID);
            break;
        case MAV_CMD_GET_HOME_POSITION: // Msg 410
            processSendPeregrineHomePos(msg);
            break;
        default:
            printf("\nReceived an un-handled Peregrine Cmd Long message. Details below:\n");
            printf("Command Long Msg Contents:\n");
            printf("-Cmd Id: %d\n", command.command);
            printf("-Confirmation #: %d\n", command.confirmation);
            printf("-Param1: %f\n", command.param1);
            printf("-Param2: %f\n", command.param2);
            printf("-Param3: %f\n", command.param3);
            printf("-Param4: %f\n", command.param4);
            printf("-Param5: %f\n", command.param5);
            printf("-Param6: %f\n", command.param6);
            printf("-Param7: %f\n", command.param7);
            //Ack as unsupported
            sendCommandAck(command.command, MAV_RESULT_UNSUPPORTED, 0, 0, msg.sysid, msg.compid); // Ack
    }
}


void processSendPeregrineHomePos(mavlink_message_t msg) {
    extern home_pos_t myHomePos; // from updateVehicle Status
    

    
    if (myHomePos.lat == 0.0) {
        // We don't yet have a defined home pos
        sendSimpleTextToGcs("Png requested home but no valid GPS yet", MAV_SEVERITY_ALERT);
        //printf("Home position not yet defined...waiting for GPS\n");
        sendCommandAck(MAV_CMD_GET_HOME_POSITION, MAV_RESULT_TEMPORARILY_REJECTED, 0, 0, msg.sysid, msg.compid);
    } else {
        sendCommandAck(MAV_CMD_GET_HOME_POSITION, MAV_RESULT_ACCEPTED, 0, 0, msg.sysid, msg.compid);
        sendHomePos(); // In updateVehicleStatusThread
    }
}


void processLandingTargetMessage(mavlink_message_t msg) {
    //This is where we handle lz's received from Peregrine
    mavlink_landing_target_t myLz;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);
    extern buffer_t landingTarget_logbuf; // WinPthreadConsole
    
    // Force byte alignment packed
#pragma pack(push,1)
    typedef struct {
        double gpsTime;
        uint8_t id;
        uint8_t frame;
        float x;
        float y;
        float z;
        double lat;
        double lon;
        float alt;
        float q1;
        float q2;
        float q3;
        float q4;
        float pitch;
        float roll;
        float heading;
        uint8_t posValid;
    } frlLzData_t;
#pragma pack(pop)
    
    frlLzData_t myFrlLzData;
    
    mavlink_msg_landing_target_decode(&msg, &myLz); // Decode the LZ into the
    
    // Now populate the struct
    myFrlLzData.gpsTime = shared_memory->hg1700.time;
    myFrlLzData.id = myLz.target_num;
    myFrlLzData.frame = myLz.frame;
    myFrlLzData.x = myLz.x;
    myFrlLzData.y = myLz.y;
    myFrlLzData.z = myLz.z;
    myFrlLzData.q1 = myLz.q[0];
    myFrlLzData.q2 = myLz.q[1];
    myFrlLzData.q3 = myLz.q[2];
    myFrlLzData.q4 = myLz.q[3];
    myFrlLzData.posValid = myLz.position_valid;
    
    
    // Now do the conversions
    quaternion_t myQuaternion;
    Euler_t myEulerAngles;
    myQuaternion.x = myLz.q[0];
    myQuaternion.y = myLz.q[1];
    myQuaternion.z = myLz.q[2];
    myQuaternion.w = myLz.q[3];
    QuaternionToEuler(myQuaternion, &myEulerAngles);
    
    double myLat,myLon;
    float myAlt;
    // TODO KE CHECK GLOBAL LOCAL WITH KNOWN VALUES....THIS DOESN'T SEEM TO WORK!
    globallocalconverter_toglobal(myLz.x, myLz.y, myLz.z, &myLat, &myLon, &myAlt);
    myFrlLzData.pitch = myEulerAngles.pitch;
    myFrlLzData.roll = myEulerAngles.roll;
    myFrlLzData.heading = myEulerAngles.heading;
    myFrlLzData.lat = myLat;
    myFrlLzData.lon = myLon;
    myFrlLzData.alt = myAlt * METERS_TO_FT;

	//This line below is fine if the perception syustem can identify heading -- but for now let's assume that it can't
	//signalLpFound(myLat, myLon, myEulerAngles.heading); // I'm not sure if we should send the sensed heading, or the mission item heading.
	//This line below is supplies the mission item heading as the landing heading
	if (!isnan(specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4)) {
		//Param4 is heading...assign it as the LP's heading
		signalLpFound(myLat, myLon, specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4);
	} else { // If there is no heading associated with the Land mission item
		//Let's use whatever was previously calculated and stored in the BossMission data global struct
		extern bossMissionData_t bossMissionDataGlobal; //Declared in bossComms.c
		signalLpFound(myLat, myLon, bossMissionDataGlobal.yaw);
	}

    Write_To_Buffer(&myFrlLzData, sizeof(frlLzData_t), &landingTarget_logbuf);
    
}

void processRepositionCmdLong(mavlink_command_long_t command) {
    printf("Received a Long reposition command (192) ===== THIS DOESN'T DO ANYTHING YET\n");
    if (command.param1 < 0) printf("-Default speed\n");
    else printf("Speed: %d m/s\n", (int)command.param1);
    printf("Reposition Flags: %d\n", (int)command.param2);
    if (isnan(command.param4)) printf("-Default yaw mode\n");
    else printf("-Yaw deg: %1.2f\n", command.param4);
    if (isnan(command.param5) && isnan(command.param6) && isnan(command.param7)) {
        printf("We have received a PAUSE command...slow down to hover and then PH while following path\n");
    }
    else if (isnan(command.param5) && isnan(command.param6)) {
        printf("We have received a change altitude command\n");
        printf("New altitude requested is: %1.2f m, or %1.2f ft\n", command.param7, command.param7 * METERS_TO_FT);
    }
    else { // IT's a reposition command with specifics
        printf("X: %1.7f\n", command.param5);
        printf("Y: %1.7f\n", command.param6);
        printf("Z: %1.7f\n", command.param7);
    }
}

void processHeartbeatMsg(mavlink_message_t msg) {
    mavlink_heartbeat_t heartbeat;
    mavlink_msg_heartbeat_decode(&msg, &heartbeat);
#ifdef PRINT_DECODED
    // Print out the contents
    printf("HeartBeat Msg Contents:\n");
    printf("-Mav_Type: %d\n", heartbeat.type);
    printf("-AutoPilot Type: %d\n", heartbeat.autopilot);
    printf("-Base Mode: %d\n", heartbeat.base_mode);
    printf("-Custom Mode: %d\n", heartbeat.custom_mode);
    printf("-System Status: %d\n", heartbeat.system_status);
    printf("-MavLink Ver: %d\n", heartbeat.mavlink_version);
#endif
}
