//
//  FCCstatusRECV.h
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-21.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#ifndef FCCstatusRECV_h
#define FCCstatusRECV_h

void NRCAUT_fccstatus_Packet_Recv(void *arg);

#endif /* FCCstatusRECV_h */
