//A bunch of includes for socket stuff
#include <stdio.h>
#include "threads.h"
#include "sleep.h" //this is a comment
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#define socklen_t int  // seriosly, winsock doesn't define this....ugh
#pragma comment (lib, "ws2_32.lib")
#define WIN32_LEAN_AND_MEAN  // Prevent windows.h from including winsock...put this in the project defines
#include <windows.h>
#else 
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "foreflightComms.h"
#include "conversions.h"
#include "equivalenceCOREsharedMem.h" //access to shared mem and standard varaible macro definitions


extern int endGlobal; // decalred in winConsoleMain

int foreflightSockFd; // Global socket file descriptor for coms with Boss for mission data
struct sockaddr_in locAddrForeflight; // Holds the local address
struct sockaddr_in remoteAddrForeflight; // Address where the foreflight packets go to

// Socket for writing mission info to BOSS
void sendForeflightThread(void *arg) {
	// This thread does the actual work on the socket
	// and ultimately empties the FIFO

	// Add in the wsa startup in case this gets called before any other socket routines on windoze
#ifdef _WIN32
// Initialize windows sockets
	initialize_windows_sockets();
#endif

	//Start by opening the  socket (recall that its shared between send/rcv)
	foreflightSockFd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP); // Global socket file descriptor for coms with qgc
	memset(&locAddrForeflight, 0, sizeof(locAddrForeflight));
	locAddrForeflight.sin_family = AF_INET;
	locAddrForeflight.sin_addr.s_addr = INADDR_ANY;
	locAddrForeflight.sin_port = htons(FOREFLIGHT_PORT + 1); // Choose a convenient port (hope its free)...probably could assign zero here for any local port. Worth a try
	//Now configure the socket option for a recv timeout of 1 second
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	if (setsockopt(foreflightSockFd, SOL_SOCKET, SO_BROADCAST, (const char*)&tv, sizeof(tv)) < 0) {
		// Socket option error
		perror("Foreflight Socket unable to set Broadcast:");
		printf("Continuing, but odds are Foreflight won't function\n");
	}
	if (-1 == bind(foreflightSockFd, (struct sockaddr *)&locAddrForeflight, sizeof(struct sockaddr))) {
		perror("Error binding ForeFlight Socket: ");
#ifdef _WIN32
		closesocket(foreflightSockFd);
#else
		close(foreflightSockFd);
#endif

	}
	memset(&remoteAddrForeflight, 0, sizeof(remoteAddrForeflight));
	remoteAddrForeflight.sin_family = AF_INET;
	remoteAddrForeflight.sin_addr.s_addr = inet_addr(FOREFLIGHT_DESTINATION_IP);
	remoteAddrForeflight.sin_port = htons(FOREFLIGHT_PORT);
	//connect the socket
	connect(foreflightSockFd, (struct sockaddr *) &remoteAddrForeflight, sizeof(struct sockaddr));

	//Now initialize the semaphore
	// Legacy code for unnamed semaphores. Unsupported on MacOs
	//int retVal = sem_init(&sendMavMsgSem, 0, 0); // Initialize sem for cross threads, and 0 starting value

	printf("Foreflight Packet Send successfully started....now sleeping until packets need to be sent\n");
	while (!endGlobal) {
		usleep(100000); // Just keep sleeping
	}
}


void sendGpsDataToForeflight(void) {
	//The simulator will need to send GPS info packets on UDP port 49002 once per second in the form of a string message like this:
	//XGPSMy Sim, -80.11, 34.55, 1200.1, 359.05, 55.6
	//The "words" are separated by a comma(no word may contain a comma).The required words are :
	//XGPS followed by a name / ID of the simulator type sending the data(that might be "My Sim" without quotes)
	//� Longitude
	//� Latitude
	//� Altitude in meters MSL
	//� Track - along - ground from true north
	//� Groundspeed in meters / sec

	//Start by converting the variables we need to convert
	float trackToForeflight;
	char gpsStringBuffer[256] = ""; // No idea what the max size is, but we should only send up to the null char
	int bytesWrittenToStringBuffer;
	foreflightGpsMsgData_t gpsData;

	//Populate foreflight struct here
	gpsData.latDeg = shared_memory->hg1700.lat;
	gpsData.lonDeg = shared_memory->hg1700.lng;
	gpsData.altFt = shared_memory->hg1700.alt;
	gpsData.trueHdgDeg = shared_memory->hg1700.hdg;
	if (gpsData.trueHdgDeg < 0) gpsData.trueHdgDeg += 360.0; // Make heading 0-360
	gpsData.trueTrackDeg = shared_memory->fcc_project.track_angle;
	if (gpsData.trueTrackDeg < 0) gpsData.trueTrackDeg += 360.0;
	gpsData.groundspeedKts = shared_memory->fcc_project.ground_speed; // I'm assuming this is knots

	if (gpsData.groundspeedKts < 30.0) {
		trackToForeflight = gpsData.trueHdgDeg; // Use heading when going slow
	} else { // Otherwise use track
		trackToForeflight = gpsData.trueTrackDeg;
	}
	float altToForeflight = gpsData.altFt * FEET_TO_M; // Foreflight wants alt in m
	float speedToForeflight = gpsData.groundspeedKts * KTS_TO_MPS; // Foreflight wants speed in m/s
	// now assemble the string
	bytesWrittenToStringBuffer = sprintf(gpsStringBuffer, "XGPSAsra, %1.4f, %1.4f, %1.1f, %1.1f, %1.1f",
		gpsData.lonDeg, gpsData.latDeg, altToForeflight, trackToForeflight, speedToForeflight);

	// Now fire it out on the socket
	long bytesSent = send(foreflightSockFd, gpsStringBuffer, bytesWrittenToStringBuffer, 0);
	if (bytesSent < 0) {
		printf("There was an error sending GPS UDP data to foreflight socket\n");
#ifdef _WIN32
		printf("Send to GPS foreflight failed with errorcode: %d\n", WSAGetLastError());
#else
		perror("GPS Foreflight SendError: "); // Untested
#endif
	}

}

void sendAttDataToForeflight(void) {
//	For attitude data, the simulator will need to send packets in the form of a string message like this (at 4 - 10Hz rate, evenly spaced in time) :
//		XATTMy Sim, 180.2, 0.1, 0.2
//		The "words" are separated by a comma(no word may contain a comma).The required words are :
//	� XATT followed by a name / ID of the simulator type sending the data(that might be "My Sim" without quotes)
//		� True Heading - float
//		� Pitch - degrees, float, up is positive
//		� Roll - degrees, float, right is positive

	// Following the example above we just build a string and fire it out
	char attStringBuffer[256] = ""; // No idea what the max size is, but we should only send up to the null char
	int bytesWrittenToStringBuffer;
	foreflightAttMsgData_t attData;

	//Populate the att struct
	attData.pitchDeg = shared_memory->hg1700.pitch;
	attData.trueHeadingDeg = shared_memory->hg1700.hdg;
	if (attData.trueHeadingDeg < 0) attData.trueHeadingDeg += 360.0;
	attData.rollDeg = shared_memory->hg1700.roll;
	   
	bytesWrittenToStringBuffer = sprintf(attStringBuffer, "XATTAsra, %1.1f, %1.1f, %1.1f", attData.trueHeadingDeg, attData.pitchDeg, attData.rollDeg);
	long bytesSent = send(foreflightSockFd, attStringBuffer, bytesWrittenToStringBuffer, 0);
	if (bytesSent < 0) {
		printf("There was an error sending Attitude UDP data to foreflight socket\n");
#ifdef _WIN32
		printf("Send to attitude foreflight failed with errorcode: %d\n", WSAGetLastError());
#else
		perror("Attitude Foreflight SendError: "); // Untested
#endif
	}
}

void sendTrafficDataToForeflight(foreflightTrafficMsgData_t trafficData) {
//	For traffic data, the simulator will need to send packets in the form of a string message like this:
//	XTRAFFICMy Sim, 168, 33.85397339, -118.32486725, 3749.9, -213.0, 1, 68.2, 126.0, KS6
//		The "words" are separated by a comma(no word may contain a comma).The required words are :
//	� XTRAFFIC followed by a name / ID of the simulator type sending the data(that might be "My Sim" without quotes)
//		� ICAO address, an integer ID
//		� Traffic latitude - float
//		� Traffic longitude - float
//		� Traffic geometric altitude - float(feet)
//		� Traffic vertical speed - float(ft / min)
//		� Airborne boolean flag - 1 or 0: 1 = airborne; 0 = surface
//		� Heading - float, degrees true
//		� Velocity knots - float
//		� Callsign - string

}
