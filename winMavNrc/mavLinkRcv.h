#pragma once
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#include <standard/mavlink.h>
#pragma clang diagnostic pop
#define MAVLINK_RECEIVE_UAV_PORT 14540 //It might be 14580...not sure who is receiver and sender...this is a high bandwidth connectino
#define MAVLINK_RECEIVE_UAV_LOW_BW_PORT 14030 //Might be 14280

#define MAVLINK_RECEIVE_GCS_PORT 64807 // Is 14428 //according to https://mavlink.io/en/mavgen_c/example_c_udp.html ...was 18570. but not sure

#include "NrcDefaults.h" 
// Beffer examnple stolen from https://github.com/mavlink/mavlink/blob/master/examples/linux/mavlink_udp.c
#define MAVRCV_BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)

// Public functions go here
void rcvGcsMavLinkThread(void *arg);
void printMavLinkMsg(long bytesReceived, mavlink_message_t msg);

