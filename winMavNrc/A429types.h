#pragma once // Prevent redundant declaration of types
// place Arinc 429 data definition here fore the 429 to Ethernet converter box..
// Heres the struct definition for G500 data ... sourced from Kris' iOS implementation in rcv_a429.h
// Gubbs was here!
#pragma pack(push,4) // pack ethernet data to fit on 4 byte boundaries...
typedef struct {
    float selected_course;
    float selected_heading;
    float horizontal_command;
    float altitude;
    float altitude_baro_cor;
    float indicated_airspeed;
    float true_airspeed;
    float total_air_temp;
    float static_air_temp;
    float vertical_speed;
    float groundspeed;
    float track_angle;
    float true_heading;
    float mag_heading;
    float pitch_angle;
    float roll_angle;
    float pitch_rate;
    float roll_rate;
    float yaw_rate;
    float lat_accel;
    float normal_accel;
} g500_data_t;

typedef struct {
	double arinc_time;
    float selected_course;
    float total_air_temp;
    float static_air_temp;
} g500_5HZ_data_t;

typedef struct {
	double arinc_time;
    float selected_heading;
    float altitude;
    float altitude_baro_cor;
    float vertical_speed;
    float groundspeed;
    float track_angle;
    float mag_heading;
    float pitch_angle;
    float roll_angle;
    float pitch_rate;
    float roll_rate;
    float yaw_rate;
    float lat_accel;
    float normal_accel;
} g500_20HZ_data_t;

typedef struct {
	double arinc_time;
    float horizontal_command;
    float indicated_airspeed;
    float true_airspeed;
    float true_heading;
} g500_10HZ_data_t;

// First we need to define an rxp type. See the AltaView Software Users Guide page 152 (A429 RX Data)
typedef struct {
    uint32_t status_word;   // The details of the status word are below
    uint32_t time_high; // Time_high and time_low are 20 nsec resolution time stamps
    uint32_t time_low;  //
    uint32_t data_word; // Here is where the arinc data lies
} rxp_t;

// Status word details:
//  Bits 0-15: API Info RXP number. These are set through the user/API to signify the RXP number in the RX and MC RXP table...I believe they are unused for APMP packets
//  Bits 16-23: API Infor RXP number: These 8 bits are set with an incrementing value to provide a sequence (rolling serial number), and can be used to see if words have been dropped
//  Bits 24-27: Rx Channel Number: These 4 bits contain the channel number the Arinc message was received on
//  Bit 28: Reserved
//  Bit 29: Trigger out W: This bit is set to one by the user/API to direct the ENET box to generate an external trigger (likely not used for APMP operation)
//  Bit 30: Interrupt W: This bit is set to one by the user/API to direct the ENET box to generate an interrup (likely not used)
//  Bit 31: Decode error: THis bit is set to one by the ENET box to tell the user that the label/word had a decode error

typedef struct {
    uint32_t    modeWord;           // Bit 0 should be set to 1, all other bits reserved
    uint32_t    sequenceNumber;
    uint32_t    serviceStatusWord;  // Bits 0-15 should always be zero. 1's indicate a failure of some sort. Bit 16 = 1553 data, Bit 17  = ARINC 429 data, Bits 19-31 are reserved
    uint32_t    altaASCII;      // Should read 0x414C5441 ("ALTA" in ASCII)
    uint32_t    reserved[3];
    uint32_t    payloadSize;    // The number of bytes in the payload portion of the packet
} a429_header_t;

typedef struct {
    a429_header_t header;
    uint32_t timing_bytes[4];   // All zero unless IRIG has been enabled
    rxp_t rxp[85];              // Maximum of 85 rxp's for Arinc 429 data
} a429_rcv_t;

typedef struct {
    double  systime; 		
    float   pressure_alt;
    float 	density_alt;       
    float   true_airspeed;      
    float   tat;                
    float   oat;                
    float   true_hdg;           
    float   mag_hdg;            
    float   theta;              
    float   phi;               
    float   q;                  
    float   p;                  
    float   r;  
    float	lateral_deviation;
    float  	glideslope_deviation;
    float 	DME_distance;  
    float 	altitude_rate;
    float 	VOR_omni_bearing;
    float 	track_angle_true;
    float 	wind_speed;
    float 	wind_direction;
	float	ground_speed;
    float   cbox_oil_press;     
    float   cbox_oil_temp;      
	float 	trans_oil_press;
    float   trans_oil_temp;     
    float   eng1_t5_temp;      
    float   eng2_t5_temp;       
    float   eng1_fuel_press;    
    float   eng2_fuel_press;    
    float   eng1_ng;            
    float   eng2_ng;            
    float   eng1_oil_press;     
    float   eng2_oil_press;     
    float   eng1_oil_temp;      
    float   eng2_oil_temp;      
    float   eng1_np;            
    float   eng2_np;            
    float   eng1_q;             
    float   eng2_q;             
    float   generator1_dc_current;  
	float   generator2_dc_current;  
    float   generator1_dc_voltage; 
	float   generator2_dc_voltage;   
    float   inverter1_ac_voltage; 
    float   inverter2_ac_voltage;   
    float   sys1_hydraulic_press;   
    float   sys2_hydraulic_press;   
    float   sys1_hydraulic_temp;    
    float   sys2_hydraulic_temp;    
    float   mast_torque_right; 
	float   mast_torque_left;       
    float   rotor_speed;        
    float   radalt_hi_res;
	float   radalt_low_res;            
    float   left_fuel_quan;    
    float   right_fuel_quan; 
    float 	total_fuel;   
    float 	total_fuel_flow;
    float	eng1_fuel_flow;
    float 	eng2_fuel_flow;
    uint32_t 	dau1_discretes1;    
    uint32_t 	dau1_discretes2;   
    uint32_t 	dau2_discretes1;    
    uint32_t 	dau2_discretes2;    
    uint32_t 	discretes_adc1;     
    uint32_t 	discretes_adc2; 
    uint32_t 	faultandmaintenance1;
    uint32_t 	faultandmaintenance2;  
} asra_arinc_data_t;
#pragma pack(pop)  // end packing

