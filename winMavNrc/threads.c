#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include "threads.h"

// Thread includes
#include "foreflightComms.h"
#include "recvFromRemoteThread.h"
#include "log.h"
#include "mavLinkRcv.h"
#include "FCCstatusRECV.h"
#include "supervisor.h"
#include "updateVehicleMavLinkStatusThread.h"
#include "serialToEthThreads.h"


#ifndef MAX_THREADS
#define MAX_THREADS 20	// Note, that this is sometimes also defined in frl_file_types.h
#endif

pthread_t thread[MAX_THREADS];  //an array or thread IDs
pthread_attr_t attr;

void LaunchThreads(void) {

	int i;
	int returnVal;
	int threadError;
    void *thread_ptr[MAX_THREADS] = {
        //rcvUavMavLinkThread,              // This one receives from the UAV...not used for now
        sendForeflightThread,               // Sends attitude and gps data to foreflight
        Log_Thread,
        rcvGcsMavLinkThread,
        NRCAUT_fccstatus_Packet_Recv,       // Recv autonomy status packets from FCC from FCCstatusRECV.c
        supervisorMain,                     // This is the main loop for the supervisor as found in supervisor.c
        //updateVehicleMavLinkStatusThread,   // Populates regularly sent messasges and passes to the send thread
        serialToEthernetRcvThread,          // SendReceive data from serial converter
    #ifdef PEREGRINE_INSTALLED
    #ifndef DONT_BUFFER_MAVLINK_PEREGRINE_MESSAGES  // Don't run the buffer if this is defined
        sendSerialMavLinkThread,            // FIFO for sending packets to the serial converter
    #endif
    #endif
        sendBossMissionThread,              // For sending mission data to BOSS
        //supervisorTestThread,             // Test supervisor thread
        sendMavLinkThread,                  // This thread holds the message cue for sending data out to GCS on the wire
        recvFromRemoteThread
    };
    
	// Create necessary threads
	for (i = 0; i < MAX_THREADS; ++i)
		if (thread_ptr[i] != NULL) { // Look to see if there are more threads to start
			returnVal = pthread_attr_init(&attr);
			assert(!returnVal);
			returnVal = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);    // Note that threads here have been   
																						// set up as detached whereas in 
																						// the DRP base they are joined
																						// with main
			assert(!returnVal);

			threadError = pthread_create(&thread[i], &attr, thread_ptr[i], NULL);
			returnVal = pthread_attr_destroy(&attr); // Clear the thread attributes variable
			assert(!returnVal);

			if (threadError != 0) {
				// We need to report an error
				printf("Error starting thread # %d\n", i);
			}
			
		}
}
