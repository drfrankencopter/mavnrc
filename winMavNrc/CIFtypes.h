#pragma once //Prevent redeclaration
#include "node_configuration.h" // Needed for ANALOG NUM CHS
#pragma pack(push,4)
typedef struct { // contains the data collected by the CIF node
	double cif_system_time;
	uint32_t function_switches;
	uint32_t function_switches2;
	float sp_control_position_lateral;
	float sp_control_position_long;
	float sp_control_position_pedal;
	float sp_control_position_collective;
	float console_pot1;
	float console_pot2;
	float console_pot3;
	float console_pot4;
	float console_pot5;
	float console_pot6;
	float console_pot7;
	float console_pot8;
	float true_airspeed;
	float barometric_altimeter;
	float total_air_temp;
	float side_slip;
	float angle_of_attack;
	float collective_thumb_wheel_pot;
} cif_output_t; // called the same in Bell 205 ICD

typedef struct {
	float ps;
	float pd;
	float ps_corrected;
	float pd_corrected;	
} airdata_t;

#pragma pack(pop)

typedef struct {
	#ifdef DBL_TIME
		#ifdef SPLIT_DBL
			float time;
			float time_dec;
		#else
			double time;
		#endif
	#else
		float time;
	#endif
	float ch[ANALOG_NUM_CHS];
} analog_t;


