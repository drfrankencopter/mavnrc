#pragma once

#include <standard/mavlink.h>
#include "bossComms.h"


int writeToMavMsgCue(mavlink_message_t msg);
void *sendMavLinkThread(void *arg);


typedef struct {
    mavlink_message_t mavMsg;
    int  msgPendingStatus; // 1 = pending to be sent, 0 = sent
} mavMsgData_t;
