#pragma once
#include "globals.h"

int getFirstWaypoint(const missionType_t* specificMission);

// Add units selector
float speedAtWaypoint(myMissionStruct_t *myMission, int missionItemNo);

float timeBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end);
float timeToWaypoint(double lat1, double lat2, int waypoint_end, float currentSpeed, myMissionStruct_t* mission);
float computeDirection(double lat1, double lon1, double lat2, double lon2);
float directionBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end);

float timeRemainingForMission(myMissionStruct_t* mission, double lat_now, double lon_now, float currentSpeed);
float timeForAllMission(myMissionStruct_t* mission);
float percentageOfTimeCompleted(myMissionStruct_t* mission, double lat_now, double lon_now, float currentSpeed);

float percentageOfDistanceCompleted(myMissionStruct_t* mission, double lat_now, double lon_now);
// Add units selector
float distanceForAllMission(myMissionStruct_t* mission);
// Add units selector
float distanceRemainingForMission(myMissionStruct_t* mission, double lat_now, double lon_now);

// Add units selector
float accelerationBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end);
float climbRateBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end);
float fuelForAllMission(myMissionStruct_t* mission, double consumption_rate);
float fuelForRemainingMission(myMissionStruct_t* mission, double lat_now, double lon_now, double currentSpeed, double consumption_in_lb_per_hour);
// Note that this function below expects pointers to double arrays of length WPT_PREVIEW_LENGTH and returns the number of lat/lons successfully parsed from the mission 
int buildBossWptPreviewFromMissionItem(myMissionStruct_t *myMission, int missionItemNo, double *wptPreviewLat, double *wptPreviewLon);
