#pragma once
#include <stdint.h>  //FFS people, you need to include this in order to reference uint32_t and other types....don't remove it from this h file!
#pragma pack(push,4)

typedef struct { // CVLAD steering commands from boeing laptop
	double system_time;
	uint32_t boeing_status;
	float long_accel_cmd;
	float lat_accel_cmd;
	float climb_rate_cmd;
	float turn_rate_cmd;
	float spare[8];	
} cvlad_steering_t; 

typedef struct { // fcc status back to boeing laptop
	double system_time;
	uint32_t fcc_status;
	float spare[8];	
} cvlad_fccstatus_t;


// NRC Autonomy types below here....

typedef struct { // NRC autonomy commands from supervisor
	double supervisor_gps_time;
	uint32_t supervisor_status;
	uint16_t mav_command;
	float param1;
	float param2;
	float param3;
	float param4;
	int32_t latitude;
	int32_t longitude;
	float altitude_msl;
} NRCAUT_supervisor_t;

typedef struct { // fcc status back to autonomy system
	double fcc_gps_time;
	uint32_t fcc_status;
	float true_heading_cmd;
	float msl_altitude_cmd;	// ft
	float prev_msl_altitude_cmd;
	float ground_speed_cmd;	// ft/s
	float ground_speed_cmd_rate; // ft/s
	float prev_ground_speed_cmd;
	float direction_cmd; // Deg 360
	float north_vel_cmd;	// ft/s
	float east_vel_cmd;
	float vertical_vel_cmd;	// fps
	double cur_latitude_cmd;
	double cur_longitude_cmd;
	double prev_latitude_cmd;
	double prev_longitude_cmd;
	float desired_track;
	float pitch_att_cmd;
	float roll_att_cmd;
	float number_of_orbits;
	float ETA_horz; // Estimated time of arrival at WP lat/lon in seconds
} NRCAUT_fccstatus_t;

typedef struct { // NRC autonomy tradjectory commands 
	double tradjectory_gps_time;
	uint32_t tradjectory_status;
	float ground_speed_cmd;
	float roll_attitude_cmd;
	float vertical_rate_cmd;
	float true_heading_cmd;
} NRCAUT_tradjectory_t;


typedef struct { // NRC autonomy DAA commands 
	double DAA_gps_time;
	uint32_t DAA_status;
	float track_cmd;
	float msl_altitude_cmd;
	float ground_speed_cmd;
} NRCAUT_DAA_t;

typedef struct { // NRC autonomy terrain database request 
	double terrainRQ_gps_time;
	double latitude;
	double longitude;
} terrain_request_t;

typedef struct { // NRC autonomy terrain database response
	double terrainRS_gps_time;
	double latitude;
	double longitude;
	float msl_terrain_height;
} terrain_response_t;

typedef enum
{
	LANDING_REQUESTED,
	TAKEOFF_REQUESTED,
	TRADJECTORY_CONTROL,
	DAA_CONTROL,
	MAV_COMMAND,
	NO_MODE_SET
} NRCAUT_control_mode_t;

#pragma pack(pop)

 

