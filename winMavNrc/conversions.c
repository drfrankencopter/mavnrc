#include "conversions.h"

void QuaternionToEuler(quaternion_t quaternion, Euler_t *phpr)
{
	double local_w = quaternion.w;
	double local_x = quaternion.x;
	double local_y = quaternion.y;
	double local_z = quaternion.z;

	double sq_w = local_w * local_w;
	double sq_x = local_x * local_x;
	double sq_y = local_y * local_y;
	double sq_z = local_z * local_z;

	phpr->heading = atan2(2.0 * (local_x * local_y + local_z * local_w), (sq_x - sq_y - sq_z + sq_w)) * RADS2DEG;
	phpr->pitch = asin(-2.0 * (local_x * local_z - local_y * local_w)) * RADS2DEG;
	phpr->roll = atan2(2.0 * (local_y * local_z + local_x * local_w), (-sq_x - sq_y + sq_z + sq_w)) * RADS2DEG;
}


void EulerToQuaternion(Euler_t hpr, quaternion_t *pquaternion)
{
	double local_Heading = hpr.heading * DEG2RADS;
	double local_Pitch = hpr.pitch * DEG2RADS;
	double local_Roll = hpr.roll * DEG2RADS;

	double Cosine1 = cos(local_Roll / 2.0);
	double Cosine2 = cos(local_Pitch / 2.0);
	double Cosine3 = cos(local_Heading / 2.0);
	double Sine1 = sin(local_Roll / 2.0);
	double Sine2 = sin(local_Pitch / 2.0);
	double Sine3 = sin(local_Heading / 2.0);

	pquaternion->w = Cosine1 * Cosine2 * Cosine3 + Sine1 * Sine2 * Sine3;
	pquaternion->x = Sine1 * Cosine2 * Cosine3 - Cosine1 * Sine2 * Sine3;
	pquaternion->y = Cosine1 * Sine2 * Cosine3 + Sine1 * Cosine2 * Sine3;
	pquaternion->z = Cosine1 * Cosine2 * Sine3 - Sine1 * Sine2 * Cosine3;
}