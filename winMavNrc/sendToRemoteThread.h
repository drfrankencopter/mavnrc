//
//  sendToRemoteThread.h
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-11.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#ifndef sendToRemoteThread_h
#define sendToRemoteThread_h

#include <stdio.h>
#include "globals.h"
#include "mavProtocol.h"
#include "remotes.h"

// Main thread

void sendToRemoteThread(void* arg);

// Helper functions

int sendMessageToAllRemotes(int sockfd, const mavlink_message_t* msg, const remotes_infos_t* remotes);
//void verifyRemotesConnection(remotes_infos_t* remotesInfo);
#endif /* sendToRemoteThread_h */
