#include <stdio.h>
#include "threads.h"
#include "mavLinkRcv.h"
#include "sleep.h"
#include "socket.h"
#include "updateVehicleMavLinkStatusThread.h"  // For access to functions to respond to GCS messages
#include "mavParams.h"
#include "serialToEthThreads.h"				// for sending to peregrine
#include "log.h" // for logbuf types
#include "sendSockThread.h" // For sending clear mission to BOSS
#include "NrcDefaults.h"
#include "equivalenceCOREsharedMem.h" //access to shared mem and standard varaible macro definitions
#include "missionManagerToSupervisorComs.h"
#include "bossComms.h" // For functions to send data to BOSS
#include "missionHelper.h" // Needed for finding speed at mission items
#include "mavProtocol.h"

#include "supervisor.h" // For communicating supervisor config to GCS/message log


// Definitions for our socket type go here:
socket_t mavLinkRcvSock = { MAVLINK_RECEIVE_UAV_PORT }; // Here's where we receive from the UAV side
socket_t mavLinkGcsRcvSock = { MAVLINK_RECEIVE_GCS_PORT }; // Here's where we receive from the GCS side
#define MAVLINK_ETHERNET_TIMEOUT 500000		// Defines a 500 milisecond timeout
#define MAVLINK_GCS_TIMEOUT 2000000			

extern int endGlobal; // winPthreadConsole.c (main)
extern struct sockaddr_in gcAddr;
extern int qGcsSockFd; // Global socket file descriptor for coms with qgc..defined in sendSockThread.c
extern missionData_t* mission_data;
extern remotes_infos_t remotesInfo;

int connectionToGcsEstablished = 0; // Set to 1 on first successful transmission to qgc...needed by the sendSockThread only try to send data once connected....don't even write to buffer otherwise
int heartBeatsMissed = 0; //To supress missing heartbeat msgs at startup



// macros for debug print
//#define PRINT_DECODED   // Flag to print decoded messages


// This thread listens for mavlink data on the UAV side....using its port
void *rcvUavMavLinkThread(void *arg) {
	//This thread listens for UDP packets in Mavlink format
	
	uint8_t rcvBuffer[MAVRCV_BUFFER_LENGTH]; // Here's where we temp store our messages
	struct sockaddr_in senderAddr;
	unsigned int senderAddrSize = sizeof(senderAddr); // Holds the senders address for recvFrom
		
	printf("Mav Link Uav Receive Thread has been spawned\n");
	// Start by doing some setup on the socket
	Open_Recv_Socket_Timeout(&mavLinkRcvSock, MAVLINK_ETHERNET_TIMEOUT);

	while (!endGlobal) {
		// Main Receive Loop Here
		// Clear out the buffer before attempting to receive into it
		memset(rcvBuffer, 0, MAVRCV_BUFFER_LENGTH);
		long bytesReceived = recvfrom(mavLinkRcvSock.s, &rcvBuffer, MAVRCV_BUFFER_LENGTH, 0, (struct sockaddr*)&senderAddr, &senderAddrSize);
		if (bytesReceived > 0) { // We got data on the port
			mavlink_message_t msg;
			mavlink_status_t status;
			const int showPacketBytes = 0; // Set to 1 to do detailed printing for debugging

			for (int i = 0; i < bytesReceived; ++i) { // Loop thru the byte array building the message structure
				if (showPacketBytes) {
					unsigned int temp = rcvBuffer[i];
					printf("%02x ", (unsigned char)temp);
				}
				// Now we parse the message
				if (mavlink_parse_char(MAVLINK_COMM_0, rcvBuffer[i], &msg, &status)) { // This only returns if a complete message is parsed
					// Packet received
					DEBUG_PRINT(("UAV Message Received\n"));
					switch (msg.msgid) {
					case MAVLINK_MSG_ID_HEARTBEAT: // msg 0
						DEBUG_PRINT(("Received an Heartbeat msg\n"));
						break;
					case MAVLINK_MSG_ID_SYS_STATUS:
						DEBUG_PRINT(("Received a system status msg\n"));
						break;
					case MAVLINK_MSG_ID_GPS_RAW_INT: // msg 24
						DEBUG_PRINT(("Received an Gps Raw msg\n"));
						break;
					case MAVLINK_MSG_ID_ATTITUDE: // msg 30
						DEBUG_PRINT(("Received an attitude msg\n"));
						break;
					case MAVLINK_MSG_ID_ATTITUDE_QUATERNION: // msg 31
						DEBUG_PRINT(("Received an attitude quaternion msg\n"));
						break;
					case MAVLINK_MSG_ID_LOCAL_POSITION_NED: // msg 32
						DEBUG_PRINT(("Received a global position NED coords msg\n"));
						break;
					case MAVLINK_MSG_ID_GLOBAL_POSITION_INT: //msg 33
						DEBUG_PRINT(("Received a global position internal coords msg\n"));
						break;
					case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW: // msg 36
						DEBUG_PRINT(("Received a servo output raw msg\n"));
						break;
					case MAVLINK_MSG_ID_VFR_HUD:
						DEBUG_PRINT(("Received a VFR HuD msg\n"));
						break;
					case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED: // msg 85
						DEBUG_PRINT(("Received a position target local NED coords msg\n"));
						break;
					case MAVLINK_MSG_ID_HIGHRES_IMU: // Msg 105
						DEBUG_PRINT(("Received a HighRes IMU msg\n"));
						break;
					case MAVLINK_MSG_ID_TIMESYNC: // msg 111
						DEBUG_PRINT(("Received a TimeSync msg\n"));
						break;
					case MAVLINK_MSG_ID_ACTUATOR_CONTROL_TARGET: // msg 140
						DEBUG_PRINT(("Received an Actuator control target msg\n"));
						break;
					case MAVLINK_MSG_ID_ALTITUDE: // msg 141
						DEBUG_PRINT(("Received an Altitude msg\n"));
						break;
					case MAVLINK_MSG_ID_EXTENDED_SYS_STATE: // msg 245
						DEBUG_PRINT(("Received an extended system state msg\n"));
						break;
					default:
						printf("\nReceived an un-handled MavLink message. Details below:\n");
						printMavLinkMsg(bytesReceived, msg);
					}
				}
			}
		} else { // Error
			printf("Didn't get any Uav UDP data....or got an error, or timed out\n");
			processRcvFail("MavLnkUavRcv", &mavLinkRcvSock, MAVLINK_ETHERNET_TIMEOUT);
		}
	}

	CLOSESOCKET(mavLinkRcvSock.s);
	
#ifdef _WIN32
	WSACleanup();
#endif 

	return 0; // I'm done!
} // end sampleThread

// This thread listens for mavlink data on the UAV side....using its port
void rcvGcsMavLinkThread(void *arg) {
	//This thread listens for UDP packets in Mavlink format
	char rcvBuffer[MAVRCV_BUFFER_LENGTH];
    listened_message_t listenedMessage;
	printf("Mav Link Gcs Receive Thread has been spawned\n");
    
    listenedMessage.mission_data = mission_data;
    memset(&listenedMessage.toAddr, 0, sizeof(listenedMessage.toAddr));
    listenedMessage.toAddrLen = sizeof(listenedMessage.toAddr);
    
#ifdef _WIN32
	initialize_windows_sockets();
#endif
    
    // Pinging the GCS w/Heartbeats, and waiting for a response before allowing the rest of the program execution to go
    while(!connectionToGcsEstablished) {
        listenedMessage.sockfd = qGcsSockFd;
        printf("Trying to connect to Gcs...\n");
        sendHeartbeat(listenedMessage.sockfd, listenedMessage.mission_data, &gcAddr, 0);
        //IB added receive
        memset(rcvBuffer, 0, MAVRCV_BUFFER_LENGTH);

        long bytesReceived = recvfrom(listenedMessage.sockfd, &rcvBuffer, MAVRCV_BUFFER_LENGTH, 0, (struct sockaddr *)&listenedMessage.toAddr, &listenedMessage.toAddrLen);
        if (bytesReceived > 0) { // We got data on the port
            connectionToGcsEstablished=1;
			memcpy(&remotesInfo.gcsAddr, &listenedMessage.toAddr, sizeof(listenedMessage.toAddr));
        }
        usleep(1000000);
    }
    

	while (!endGlobal) {
		// Main Receive Loop Here
		// Clear out the buffer before attempting to receive into it
		memset(rcvBuffer, 0, MAVRCV_BUFFER_LENGTH);

		long bytesReceived = recvfrom(listenedMessage.sockfd, rcvBuffer, MAVRCV_BUFFER_LENGTH, 0, (struct sockaddr*)&listenedMessage.toAddr, &listenedMessage.toAddrLen);

		if (bytesReceived > 0) { // We got data on the port
            handleMessageReceived(&listenedMessage, rcvBuffer, bytesReceived);
		}
		else { // Error
			processRcvFail("MavLnkGcsRcv", &mavLinkGcsRcvSock, MAVLINK_ETHERNET_TIMEOUT);
			heartBeatsMissed += 1;
			if (heartBeatsMissed > 6)
				printf("Not receiving GCS Heartbeats!\n");
			
		}
	}

    CLOSESOCKET(mavLinkRcvSock.s); // No closesocket  defined on other OS's

#ifdef _WIN32
	WSACleanup();
#endif
} // end sampleThread

void startMission(void) {
	// This function added Jan 27/2021 so that the supervisor can signal to the mission manager when to start the mission
	// Basically this allows the supervisor to ensure that the whole system is ready to start the mission before declaring it started
    extern missionData_t* mission_data;
	extern bossMissionData_t bossMissionDataGlobal; // Declared in bossComms.c
	printf("Requested to start mission...\n");

	// Added some sanity checks on mission start here. At present nothing is enforced other than warning the user
	if (mission_data->myCurrentMission.standard.numMissionItems == 0) {
		sendSimpleTextToGcs("Current mission has No Items. Restart QGC", MAV_SEVERITY_ALERT); // Can happen if QGC doesn't send a CMD request for the autopilot capabilities
	}


	bossMissionDataGlobal.speed = DEFAULT_SPEED; // Set speed to the default value since 1st waypoint is takeoff....
	printf("Setting default flight speed to %1.1f m/s\n", bossMissionDataGlobal.speed);
	// Now we need to start the mission
	//Broadcast the current mission item
	broadcastCurrentMissionItem(0); //start at 0
	// Here we should populate the Boss current mission item data, and send the packet to BOSS so we know the mission has started
	// Just to signal the change in state let's temporarily override the boss mission item with -666

	// First thing passed to BOSS should just be to take current position to the takeoff item position
	// Here we should make a temp mission item, and also set the mission number briefly to -1 so BOSS knows we are starting a new mission
	bossMissionData_t tempMissionData; // temp data for letting boss know we have a new mission to start
	tempMissionData.missionNo = -1; // Not a valid mission (yet)
	tempMissionData.alt = FEET_TO_M * shared_memory->hg1700.alt; // Take current alt
	tempMissionData.lat = shared_memory->hg1700.lat; // Convert to lat decimal degrees
	tempMissionData.lon = shared_memory->hg1700.lng; // Convert to lat decimal degrees
	tempMissionData.isLandingLeg = 0; // Default to not landing leg, and evaluate below
	tempMissionData.yaw = 0; // shouldn't matter
	// Now send the mission data to BOSS....
	sendMissionItemToBoss(tempMissionData); // send the mission data to Boss...this does nto use the updateMission data sequence, as it's to get you from current pos to TO pos
	usleep(5000); // Just a quick sleep on start of mission to ensure that the receive end is processed before the mission gets updated to the Takeoff waypoint

	mission_data->myCurrentMission.missionStarted = 1; // Start the mission
	mission_data->myCurrentMission.currentMissionItem = 0; // Start at the 0th mission item --- added Jan 5 2021, to ensure that mission start works.
	// Need to update global boss data that the mission has started
	bossMissionDataGlobal.isMissionStarted = 1; // Set mission start to true
	sprintf(bossMissionDataGlobal.description, "MSN"); // Set description to mission mode
	updateBossMissionData(); // recall that this fn sends the global mission data to BOSS
	signalNewMissionItem(mission_data->myCurrentMission.standard.items[mission_data->myCurrentMission.currentMissionItem]); // Send the current item back to the supervisor
}



// HELPER FUNCTIONS
void printMavLinkMsg(long bytesReceived, mavlink_message_t msg) {
	printf("Bytes Received: %ld\nDatagram: ", bytesReceived);
	printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
	printf("\n"); // For pretty formatting
}
