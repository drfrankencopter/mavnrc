
#pragma once
#include "node_configuration.h" // Needed for FCC NUM CHS
#pragma pack(push,4) // pack ethernet data to fit on 4 byte boundaries...
typedef struct { //output packet from the fcc mainly for monitoring by the HMU
	double fcc_system_time; 
	float final_drive_lateral;
	float final_drive_longitudinal;
	float final_drive_pedal;
	float final_drive_collective;
	uint32_t fccok1;	// KE added these Dec 5 2013 to allof HMU to read OK state
	uint32_t fccok2;
	float dlat_net;	// Added by KE Jan 2014 to allow clients/cdu to see when controls are in deadband
	float dlon_net;
	float dped_net;
	float dcol_net;
} fcc_output_t; // called the same in ICD

typedef struct { // provides configuration information mainly for display on the CDU
	double fcc_system_time;
	uint32_t fbw_engaged;
	uint32_t engagement_number;
	uint32_t control_index;
	float	cp1_value;	// pot engineering units from 0-10 as assigned by fcc_switches in fcc code
	float	cp2_value;
	float	cp3_value;
	float	cp4_value;
	float	cp5_value;
	float	cp6_value;
	float	cp7_value;
	float	cp8_value;
	char lateral_inceptor[12];
	char longitudinal_inceptor[12];
	char direction_inceptor[12];
	char heave_inceptor[12];
	char lateral_control_mode[16];
	char longitudinal_control_mode[16];
	char pedal_control_mode[16];
	char collective_control_mode[16];
	char cp1_function[12];	// Current assigned function of pots
	char cp2_function[12];
	char cp3_function[12];
	char cp4_function[12];
	char cp5_function[12];
	char cp6_function[12];
	char cp7_function[12];
	char cp8_function[12];
	char filename[12];	// I hope 12 is enough
} fcc_configuration_t; // called the same in Bell 205 ICD


// packet broadcast to the data logger
typedef struct {    // Contains project specific data for recording on the data logger.
    double fcc_systime;
    uint32_t fcc_proj1_bits;  // used for control system mode identifications and other stuff...
    float   p_notch;   // Notch filtered rates
    float   q_notch;
    float   r_notch;
    float   ax_notch;  // Notch filtered accels
    float   ay_notch;
    float   az_notch;
    float   p_mix;     // Mixed rates
    float   q_mix;
    float   r_mix;
    float   u_gnd;     // Body axis velocities
    float   v_gnd;
    float   w_gnd;
    float   u_horz;    // Horizontal axis velocities
    float   v_horz;
    float   w_horz;
    float   u_gnd_dot; // Body axis rate of change of velocities
    float   v_gnd_dot;
    float   w_gnd_dot;
    float   u_horz_dot;// Horizontal axis rate of change of velocities
    float   v_horz_dot;
	float 	w_horz_dot;
	float	ground_speed; 
    float	track_angle;
	float 	wind_speed;
	float 	wind_direction;
    float   phi_com;   // Commanded attitudes
    float   theta_com;
    float   psi_com;
    float   p_com;     // Commanded rates
    float   q_com;
    float   r_com; 
    float   u_com;     // Commanded velocities
    float   v_com;
    float   w_com;     // For vert vel com
    double  latitude_com;
    double  longitude_com;
    float   h_com;     // Commanded height above ground
    float   proj1;	   // project specific variables	
    float   proj2;
    float   proj3;
    float   proj4;
    float   proj5;
    float   proj6;
    float   proj7;
    float   proj8;
    float   proj9;
    float   proj10;
    float   proj11;
    float   proj12;
    float	proj13;
	float	proj14;
	float	proj15;
	float	proj16;
	float	proj17;
	float	proj18;
	float	proj19;
	float	proj20;
} fcc_project_t; // Intended for data logger purposes only
#pragma pack(pop)  // end packing

typedef struct {
	#ifdef DBL_TIME
		#ifdef SPLIT_DBL
			float time;
			float time_dec;
		#else
			double time;
		#endif
	#else
		float time;
	#endif
	float ch[FCC205_NUM_CHS];
} fcc205_t;  

