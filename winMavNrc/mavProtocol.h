/*
    Mission Protocol Helper Functions
    
    File name    : missionProtocol.h
    Date        : 1/19/2022 2:06:49 PM
    Author        : Levesqueje
    Description : This file holds every function that is used to communicate the mavlink mission protocol.
                  It decodes the received information and sends back requests if needed.
    TODOS : Not noted.
*/

#ifndef MAVPROTOCOL
#define MAVPROTOCOL
// Standard librairies includes
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <netinet/in.h>
#endif

// External installed librairies <>


// Project documents includes ""
#include "globals.h"
#include "bossComms.h"

/////////////////////
// Main content below

////
// listened_message_t
// This struct is used to store all the information relative to a message.
// This is passed between all the mavlink handling functions and limits
// the number of parameters because each function uses all this information.
//
#ifndef LISTENED_MSG_STRUCT
#define LISTENED_MSG_STRUCT
typedef struct {
    int sockfd;
    mavlink_message_t msg;
    mavlink_status_t status;
    missionData_t* mission_data;    // BE VERY CAREFUL HERE as this points to our shared memory global struct (done in recvFromRemoteThread)
    struct sockaddr_in toAddr;
    unsigned int toAddrLen;
} listened_message_t;
#endif

// Main message handling function

void handleMessageReceived(listened_message_t* listenedMessage, const char* recvbuf, long nBytes);

// Sub functions to handle specific mavlink messages

void handleHeartbeatMessage(listened_message_t* lm);
void handleMissionCountMessage(listened_message_t* lm);
void handleMissionItemMessage(listened_message_t* lm);
void handleMissionRequestListMessage(listened_message_t* lm);
void handleMissionRequestIntMessage(listened_message_t* lm);
void handleMissionAck(listened_message_t* lm);
void handleSetModeMessage(listened_message_t* lm);
void handleParamRequestList(listened_message_t* lm);
void handleMissionSetCurrent(listened_message_t* lm);
void handleMissionCurrent(listened_message_t* lm);
void handleParamRequestRead(listened_message_t* lm);
void handleParamSet(listened_message_t* lm);
void handleMissionClearAll(listened_message_t* lm);
void handleManualControl(listened_message_t* lm);
void handlePing(listened_message_t* lm);

// Sub functions to send specific mavlink messages

void askForMissionItems(listened_message_t* lm, int mission_type);
void sendMissionAckRemote(listened_message_t* lm, int mission_type, uint8_t type);
int sendMissionCount(int sockfd, missionData_t* mission_data, const struct sockaddr_in* to, int toLen, int target_sys, int target_comp, int mission_type);
void sendMissionItem(int sockfd, missionData_t* mission_data, const mavlink_mission_request_int_t* reqInt, const struct sockaddr_in* to, int toLen);
int sendMavlinkMessage(int sockfd, const mavlink_message_t *msg, const struct sockaddr_in* to, int toLen);
void sendHeartbeat(int sockfd, const missionData_t* mission_data, const struct sockaddr_in* addr, unsigned char mavState);
void sendParameters(int sockfd, missionData_t* mission_data, const mavlink_param_value_t* params, int numParams, const struct sockaddr_in* to);
void sendParameter(int sockfd, missionData_t* mission_data, const mavlink_param_value_t* param, int index, const struct sockaddr_in* to);
int sendMissionCurrent(int sockfd, int current, const struct sockaddr_in* to);
int sendMissionSetCurrent(int sockfd, int current, int targ_sys, int targ_comp, const struct sockaddr_in* to);
void sendSimpleText(int sockfd, const char* text, uint8_t severity, const struct sockaddr_in* to);
void sendPingRequest(int sockfd, int seq, const struct sockaddr_in* to);

// Helper functions

int isGcs(const struct sockaddr_in* to);
void disarmVehicle(missionData_t* mission_data);

// CMD LONG

void handleCommandLong(listened_message_t* msg);
void sendCommandAckRemote(const mavlink_command_long_t* command, uint8_t result, uint8_t progress, int sockfd, const struct sockaddr_in* to, int toLen);
void handleRequestMessageRemote(const listened_message_t* lm, const mavlink_command_long_t* command);
void sendAutopilotVersionRemote(int sockfd, const struct sockaddr_in* to, int toLen);
void handleTakeoffRequest(listened_message_t* lm, mavlink_command_long_t* cmd);
void handleRepositionCmdLong(listened_message_t* lm, mavlink_command_long_t* cmd);
void handleArmDisarmRequest(listened_message_t* lm, mavlink_command_long_t* cmd);

// CMD INT

void handleCommandInt(listened_message_t* lm);
void handleRepositionCmdInt(mavlink_command_int_t* command);

#endif /* mavProtocol.h */
