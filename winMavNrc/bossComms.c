#include <pthread.h>

#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#pragma comment (lib, "ws2_32.lib")
#define WIN32_LEAN_AND_MEAN  // Prevent windows.h from including winsock...put this in the project defines
#include <windows.h>
#else 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#include "sleep.h"
#include <errno.h>
#include "socket.h"
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>  // We will use regular semaphores to replace the RT ones
#include "NrcDefaults.h"
#include "updateVehicleMavLinkStatusThread.h" // for access to mission data...
#include "bossComms.h"
#include "log.h" // for buffer_t definition

bossMissionData_t bossMissionDataGlobal; // A global that needs to be reset when the mission is cleared.

extern int endGlobal; // program control from main


int bossMissionSockFd; // Global socket file descriptor for coms with Boss for mission data
struct sockaddr_in locAddrBossMission; // Holds the local address
struct sockaddr_in remoteAddrBossMission; // Address where the Boss mission packets go to

// Socket for writing mission info to BOSS
void *sendBossMissionThread(void *arg) {
	// This thread does the actual work on the socket
	// and ultimately empties the FIFO
	//uint8_t buf[512]; // a local buffer

	// Add in the wsa startup in case this gets called before any other socket routines on windoze
#ifdef _WIN32
// Initialize windows sockets
	initialize_windows_sockets();
#endif

	//Start by opening the  socket (recall that its shared between send/rcv)
	bossMissionSockFd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP); // Global socket file descriptor for coms with qgc
	memset(&locAddrBossMission, 0, sizeof(locAddrBossMission));
	locAddrBossMission.sin_family = AF_INET;
	locAddrBossMission.sin_addr.s_addr = INADDR_ANY;
	locAddrBossMission.sin_port = htons(0); //htons(BOSS_MISSION_SEND_PORT + 1); // Choose a convenient port (hope its free)...probably could assign zero here for any local port. Worth a try
	//Now configure the socket option for a recv timeout of 1 second
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	if (setsockopt(bossMissionSockFd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv)) < 0) {
		// Socket option error
		perror("Boss Mission Socket unable to set Socket Rcv Timeout:");
		printf("Continuing, but Boss Mission Coms Ack will not function\n");
	}
	if (setsockopt(bossMissionSockFd, SOL_SOCKET, SO_BROADCAST, (const char*)&tv, sizeof(tv)) < 0) {
		// Socket option error
		perror("Boss Mission Socket unable to set Socket Rcv Timeout:");
		printf("Continuing, but Boss Mission Coms Ack will not function\n");
	}
	if (-1 == bind(bossMissionSockFd, (struct sockaddr *)&locAddrBossMission, sizeof(struct sockaddr))) {
		perror("Error binding Boss Mission Socket: ");
#ifdef _WIN32
		closesocket(bossMissionSockFd);
#else
		close(bossMissionSockFd);
#endif

	}
	memset(&remoteAddrBossMission, 0, sizeof(remoteAddrBossMission));
	remoteAddrBossMission.sin_family = AF_INET;
	remoteAddrBossMission.sin_addr.s_addr = inet_addr(BOSS_DESTINATION_IP);
	remoteAddrBossMission.sin_port = htons(BOSS_MISSION_SEND_PORT);
	//connect the socket
	connect(bossMissionSockFd, (struct sockaddr *) &remoteAddrBossMission, sizeof(struct sockaddr));

	//Now initialize the semaphore
	// Legacy code for unnamed semaphores. Unsupported on MacOs
	//int retVal = sem_init(&sendMavMsgSem, 0, 0); // Initialize sem for cross threads, and 0 starting value

	printf("Boss Mission Packet Send successfully started....now sleeping until packets need to be sent\n");
	while (!endGlobal) {
		usleep(100000); // Just keep sleeping
	}
	return 0; // Don't know why but XCode is forcing a return on a void* function
}


int sendMissionItemToBoss(bossMissionData_t myMissionData) {

	myMissionData.missionItemNo++; // Add one to the mission item number for waypoint display in boss (Mavlink starts mission items at 0, humans don't )
	// Populate the mission startefd field
    extern missionData_t* mission_data;
	myMissionData.isMissionStarted = mission_data->myCurrentMission.missionStarted; // reflect the current status of the mission
	long bytesSent = send(bossMissionSockFd, &myMissionData, sizeof(bossMissionData_t), 0);
	if (bytesSent < 0) {
		printf("There was an error sending  UDP data to Boss Mission socket\n");
#ifdef _WIN32
		printf("Send failed with errorcode: %d\n", WSAGetLastError());
#else
		perror("Boss Mission SendError: "); // Untested
#endif
	}
	writeBossMissionDataToDisk(myMissionData); // If we are sending to Boss, lets write to disk too!

	return 0; // For success
}

void updateBossLpData(double lat, double lon) {
	// This function writes to the lp data sections of the boss mission struct
	bossMissionDataGlobal.lpNo++; // Increment the LP number
	bossMissionDataGlobal.lpLat = lat;
	bossMissionDataGlobal.lpLon = lon;
	sendMissionItemToBoss(bossMissionDataGlobal);
}


void updateBossMissionData(void) {
    extern missionData_t* mission_data; //current mission is the one we are executing, defined in updateVehicleMavLinkStatus
	// It seems best to use a function to implement this functionality as its needed in a few places
#define ITEM_YAW_NOT_VALID 666
	float missionItemYaw = ITEM_YAW_NOT_VALID; // Default to something impossible for error checking
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);

	if (specificMission->numMissionItems >= (mission_data->myCurrentMission.currentMissionItem + 1)) {

		// Since we are not on the last wayppoint we can calculate the direction to the next waypoint
		// We will need to search for the next waypoint. Let's consider anything that has a 0.0 value for x as not being valid
		for (int i = mission_data->myCurrentMission.currentMissionItem + 1; i < specificMission->numMissionItems; i++) {
			// Loop thru until we find a mission item with a non-zero x coordinate.
			if (specificMission->items[i].x != 0) { // we have a waypoint with a position. -- Note that pram x is an integer, so don't put a decimal here
			//Calculate bearing to it.
				double bearingRads;
				// This code doesn't work for takeoff WP...Needs to be fixed
				get_bearing_to_next_waypointKE(specificMission->items[mission_data->myCurrentMission.currentMissionItem].x / (1.0E7), specificMission->items[mission_data->myCurrentMission.currentMissionItem].y / (1.0E7), specificMission->items[i].x / (1.0E7), specificMission->items[i].y / (1.0E7), &bearingRads);
				missionItemYaw = (float)(180.0 / M_PI)*bearingRads; // should be 0-360
				break; // out of the for loop...we found our item yaw
			}


		}

	}

	// Send next mission item to Boss
	// Now we want to build the packet that goes off to BOSS Mission
	bossMissionData_t newMisionItemData;
	// now we want to populate the newMissionItem with all the old info, and then will overwrite cerain parts
	memcpy(&newMisionItemData, &bossMissionDataGlobal, sizeof(bossMissionData_t));
	int itemNo = mission_data->myCurrentMission.currentMissionItem; // Recall that this is controlled by broadcast mission item reached
	// But this is not yet shoved into the BOSS mission data so lets do it here
	newMisionItemData.missionItemNo = itemNo;
	newMisionItemData.isMissionStarted = mission_data->myCurrentMission.missionStarted;
	// New we need to overwrite the 
	newMisionItemData.lat = specificMission->items[itemNo].x / (1.0E7); // Convert to lat decimal degrees
	newMisionItemData.lon = specificMission->items[itemNo].y / (1.0E7); // Convert to lat decimal degrees
	newMisionItemData.alt = specificMission->items[itemNo].z;   // Hopefull MSL alt if we specified the mission correctly
	newMisionItemData.isLandingLeg = 0; // Default to not landing leg, and evaluate below

	//Mod made July 14 to account for weird DH headings in boss
	if ((specificMission->items[mission_data->myCurrentMission.currentMissionItem].command == MAV_CMD_NAV_TAKEOFF) || (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command == MAV_CMD_NAV_LAND)) {
		if (!isnan(specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4)) {
			// We have a valid parameter 4...so pass that to the mission
			missionItemYaw = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4;
		}
	}
	if (missionItemYaw != ITEM_YAW_NOT_VALID) {
		newMisionItemData.yaw = missionItemYaw; // Populate with yaw
	}
	else {
		newMisionItemData.yaw = bossMissionDataGlobal.yaw; // ReUse previous heading...hopefully this handles speed changes
	}

	if (itemNo == specificMission->numMissionItems - 1) { // Check if its the last waypoint
		if (specificMission->items[itemNo].param2 == 2.0) newMisionItemData.isLandingLeg = 1; // And if a precision landing is specified
		// Now try and grab the yaw angle from the mission item.
		// If it's nan, then just use yaw from the previous item
		if (!isnan(specificMission->items[itemNo].param4)) { // Check if yaw is valid
			newMisionItemData.yaw = specificMission->items[itemNo].param4;
		}
	}

	//Lets only use this function when we are in mission mode
	memset(newMisionItemData.description, 0, sizeof(newMisionItemData.description)); // zero out the string...
	sprintf(newMisionItemData.description, "MSN");

	// Now populate the description
	// Here we will check for speed changes, item descriptions, etc
	switch (specificMission->items[itemNo].command) {
	case MAV_CMD_NAV_TAKEOFF: // 22
		strcat(newMisionItemData.description, " T/O");
		newMisionItemData.horzAcceptRadiusM = DEFAULT_ACCEPTANCE_RADIUS_HORZ; // Go to defaultfor takeoff
		break;
	case MAV_CMD_NAV_LAND: // 21
		strcat(newMisionItemData.description, " PLP");
		newMisionItemData.horzAcceptRadiusM = DEFAULT_LAND_ZONE_RADIUS;
		break;
	case MAV_CMD_NAV_WAYPOINT: // 16
		// No need to describe aregular waypoint
		newMisionItemData.horzAcceptRadiusM = specificMission->items[itemNo].param2;
		break;
	case MAV_CMD_CONDITION_CHANGE_ALT: // 113
		strcat(newMisionItemData.description, " ALT");
		break;
	case MAV_CMD_DO_CHANGE_SPEED: // 178
		// For speed changes we need to use the previously stored waypoint info since since there is zero position data associated with speed changes
		newMisionItemData.lat = bossMissionDataGlobal.lat;
		newMisionItemData.lon = bossMissionDataGlobal.lon;
		newMisionItemData.alt = bossMissionDataGlobal.alt;
		strcat(newMisionItemData.description, " SPD");
		newMisionItemData.speed = specificMission->items[itemNo].param2;
		break;
	default:
		// If we don't recognize the command just put the number in the field
		sprintf(newMisionItemData.description, "CMD%d", specificMission->items[itemNo].command);
	}

	//Ideally we would just send the mission to BOSS, but that will be slow to implement, so instead I send a preview
	// Now we need to loop through the mission items to build the preview of the the next legs in the path.
	int numPreviewItemsCompleted = 0; // We want to build a preview up to WPT_PREVIEW_LENGTH
	for (int i = mission_data->myCurrentMission.currentMissionItem + 1; i < specificMission->numMissionItems; i++) {
		// Note that there is a similar loop earlier, and this code could benefit from some clean-up
		int breakOutOfForLoop = 0;
		switch (specificMission->items[i].command) {
		case MAV_CMD_NAV_TAKEOFF: // 22
			printf("This mission may have more than one takeoff...this can happen if speed change was 1st item\n");
			if (i >= 1) { // Look at the previous item
				if (specificMission->items[i - 1].command == MAV_CMD_DO_CHANGE_SPEED) {
					newMisionItemData.latNext[numPreviewItemsCompleted] = specificMission->items[i].x / (1.0E7); // Convert to lat decimal degrees
					newMisionItemData.lonNext[numPreviewItemsCompleted] = specificMission->items[i].y / (1.0E7); // Convert to lat decimal degrees
					numPreviewItemsCompleted++;
				}
			}
			break;
		case MAV_CMD_NAV_LAND: // 21
			newMisionItemData.latNext[numPreviewItemsCompleted] = specificMission->items[i].x / (1.0E7); // Convert to lat decimal degrees
			newMisionItemData.lonNext[numPreviewItemsCompleted] = specificMission->items[i].y / (1.0E7); // Convert to lat decimal degrees
			numPreviewItemsCompleted++;
			breakOutOfForLoop = 1; // A land is the last item
			break;
		case MAV_CMD_NAV_WAYPOINT: // 16
			newMisionItemData.latNext[numPreviewItemsCompleted] = specificMission->items[i].x / (1.0E7); // Convert to lat decimal degrees
			newMisionItemData.lonNext[numPreviewItemsCompleted] = specificMission->items[i].y / (1.0E7); // Convert to lat decimal degrees
			numPreviewItemsCompleted++;
			break;

		}
		if (numPreviewItemsCompleted == WPT_PREVIEW_LENGTH) breakOutOfForLoop = 1; // Break if we found all the points in the preview
		if (breakOutOfForLoop == 1) break;
	}
	newMisionItemData.previewLength = numPreviewItemsCompleted; // These include only the wpt and landing points 
	sendMissionItemToBoss(newMisionItemData);
	// Now copy the newMissionItemData to the global
	memcpy(&bossMissionDataGlobal, &newMisionItemData, sizeof(bossMissionData_t));

}

// This function liberally taken from the updateBossMission function above in updateVehicelStatusThread.c
// Notable differences include:
//   -- Direction to waypoint is 'direct to'
//   -- Does not issue the 'send to boss' command...it's up to the user to do that after all items 
//		between the current item and the selected item are processed to
//		determine if speed changes are required
//	 -- Yaw to waypoint is not calculated becuase it's direct to, not the mission track
// NOTE: There probably should only be one function that does this...big chance for unanticipated behaviour if we support more mission item types
bossMissionData_t updateBossMissionDataForItem(int itemNo, bossMissionData_t prevScannedMissionData) {
	// It seems best to use a function to implement this functionality as its needed in a few places
    extern missionData_t* mission_data;// current mission is the one we are executing, and is decalred in updateVehicelMavLinkStatusThread.c
	// Now we want to build the packet that goes off to BOSS Mission
	bossMissionData_t newMisionItemData;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);
	newMisionItemData.missionItemNo = itemNo;
	newMisionItemData.lat = specificMission->items[itemNo].x / (1.0E7); // Convert to lat decimal degrees
	newMisionItemData.lon = specificMission->items[itemNo].y / (1.0E7); // Convert to lat decimal degrees
	newMisionItemData.alt = specificMission->items[itemNo].z;   // Hopefull MSL alt if we specified the mission correctly
	newMisionItemData.isLandingLeg = 0; // Default to not landing leg, and evaluate below
#define ITEM_YAW_NOT_VALID 666
	newMisionItemData.yaw = ITEM_YAW_NOT_VALID; // Default to something impossible for error checking

	if (itemNo == specificMission->numMissionItems - 1) { // Check if its the last waypoint
		if (specificMission->items[itemNo].param2 == 2.0) newMisionItemData.isLandingLeg = 1; // And if a precision landing is specified
		// Now try and grab the yaw angle from the mission item.
		// If it's nan, then just use yaw from the previous item
		if (!isnan(specificMission->items[itemNo].param4)) { // Check if yaw is valid
			newMisionItemData.yaw = specificMission->items[itemNo].param4;
		}
	}

	// Now populate the description
	// Pre populate new mission data with old speed data
	newMisionItemData.speed = bossMissionDataGlobal.speed; // If a speed command is issued we will overwrite it here
	switch (specificMission->items[itemNo].command) {
	case MAV_CMD_NAV_TAKEOFF: // 22
		sprintf(newMisionItemData.description, " T/O");
		newMisionItemData.horzAcceptRadiusM = DEFAULT_ACCEPTANCE_RADIUS_HORZ; // Go to defaultfor takeoff
		break;
	case MAV_CMD_NAV_LAND: // 21
		sprintf(newMisionItemData.description, " LP");
		newMisionItemData.horzAcceptRadiusM = DEFAULT_LAND_ZONE_RADIUS;
		break;
	case MAV_CMD_NAV_WAYPOINT: // 16
		sprintf(newMisionItemData.description, "");// Waypoints are already in the boss text no need to add
		newMisionItemData.horzAcceptRadiusM = specificMission->items[itemNo].param2;
		break;
	case MAV_CMD_CONDITION_CHANGE_ALT: // 113
		sprintf(newMisionItemData.description, " ALT");
		break;
	case MAV_CMD_DO_CHANGE_SPEED: // 178
		// For speed changes we need to use the previously stored waypoint info since since there is zero position data associated with speed changes
		newMisionItemData.lat = prevScannedMissionData.lat;
		newMisionItemData.lon = prevScannedMissionData.lon;
		newMisionItemData.alt = prevScannedMissionData.alt;
		sprintf(newMisionItemData.description, " SPD");
		newMisionItemData.speed = specificMission->items[itemNo].param2;
		break;
	default:
		// If we don't recognize the command just put the number in the field
		sprintf(newMisionItemData.description, " CMD%d", specificMission->items[itemNo].command);
	}

	return newMisionItemData;

}

void clearBossMissionData(void) {
	bossMissionData_t clearMissionData;
	memset(&clearMissionData, 0, sizeof(bossMissionData_t));
	clearMissionData.speed = DEFAULT_SPEED; // Populate with default speed
	clearMissionData.missionItemNo = 9999999; // A ridiculous number so boss knows we have a new mission
	memcpy(&bossMissionDataGlobal, &clearMissionData, sizeof(bossMissionData_t)); // Clear the global variable with mission data
}


void writeBossMissionDataToDisk(bossMissionData_t myBossData) {
	// writes the first bit of boss mission data (up to the WP preview) to disk
	extern buffer_t bossMissionData_logbuf; // declared in 'main'
	bossMissionDataToDisk_t myDataToWrite;

	memcpy(&myDataToWrite, &myBossData, sizeof(bossMissionDataToDisk_t)); //Copy the first chunk of data up to the preview
	Write_To_Buffer(&myDataToWrite, sizeof(bossMissionDataToDisk_t), &bossMissionData_logbuf);
}
