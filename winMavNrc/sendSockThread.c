#include <pthread.h>

#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#pragma comment (lib, "ws2_32.lib")
#define WIN32_LEAN_AND_MEAN  // Prevent windows.h from including winsock...put this in the project defines
#include <windows.h>
#else 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "sleep.h"
#include <errno.h>
#include "socket.h"
#include "sendSockThread.h"
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>  // We will use regular semaphores to replace the RT ones
#include "NrcDefaults.h"
#include "updateVehicleMavLinkStatusThread.h" // for access to mission data...maybe socket handling isn't the best place to hide the stuffing of mission started
#include "globals.h"

// My thinking on this is that there will be some kind of a FIFO
// buffer that accumulates the packets that need to be sent
// Writing to the buffer should trigger a semaphore that data needs to be emptied
// which in turn shoud put the thread to work to try and empty the buffer

static int semaphoreValid = 0;

int qGcsSockFd; // Global socket file descriptor for coms with qgc

struct sockaddr_in gcAddr; // Holds ths GCS address
struct sockaddr_in locAddr; // Holds the local address

#define MAV_MSG_BUFFER_LENGTH 1024 // Max # of messages to buffer
static mavMsgData_t mavMsgBuffer[MAV_MSG_BUFFER_LENGTH]; // Our array of mav messages
static int bufferInputIndex=0, bufferOutputIndex=0;

#ifdef _WIN32
static sem_t sendMavMsgSem;  // Grasping atstraws here....
#else
static sem_t *sendMavMsgSem;
#endif

#define SEND_MAV_MUTEX_NAME "/sendMavMutex"  // No idea if this will work on Windows...but MacOs doesn't support unnamed semaphores

extern int endGlobal; // program control from main
extern int connectionToGcsEstablished; //from mavRcv.c

void *sendMavLinkThread(void *arg) {
	// This thread does the actual work on the socket
	// and ultimately empties the FIFO
	uint8_t buf[512]; // a local buffer

#ifdef _WIN32
// Initialize windows sockets
	initialize_windows_sockets();
#endif


	//Start by opening the socket (recall that its shared between send/rcv)
    qGcsSockFd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP); // Global socket file descriptor for coms with qgc
	if (qGcsSockFd < 0)
		SERROR("Gcs Socket Creation Failed...:");
    
    memset(&locAddr, 0, sizeof(locAddr));
    locAddr.sin_family = AF_INET;
    locAddr.sin_addr.s_addr = INADDR_ANY;
    locAddr.sin_port = htons(0); // Bind on a free port
    
    //Now configure the socket option for a recv timeout of 1 second
#ifdef _WIN32
	DWORD tv = 1000;
#else
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
#endif
	if (setsockopt(qGcsSockFd, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof(tv)) < 0) {
        // Socket option error
        perror("Gcs Socket unable to set Socket Rcv Timeout:");
        printf("Continuing, but heartbeat monitor will not function\n");
    }
    
    if (bind(qGcsSockFd,(struct sockaddr *)&locAddr, sizeof(struct sockaddr)) < 0) {
        SERROR("Error binding QGCs Socket: ");
        CLOSESOCKET(qGcsSockFd);
    }
    
    memset(&gcAddr, 0, sizeof(gcAddr));
    gcAddr.sin_family = AF_INET;
    gcAddr.sin_addr.s_addr = inet_addr(MAV_DESTINATION_IP);
    gcAddr.sin_port = htons(MAVLINK_SEND_PORT);
    //connect the socket to gcs
    //connect(qGcsSockFd,(struct sockaddr *) &gcAddr, sizeof(struct sockaddr));

	//Now initialize the semaphore
#ifdef _WIN32
	int retVal = sem_init(&sendMavMsgSem, 0, 0); // Initialize sem for cross threads, and 0 starting value
	if (retVal<0) {
		printf("Couldn't open mavlink message cue semaphore, errno = %d\n", errno);
		perror("MavLink Semaphore: ");
	}
	else {
		printf("Opened mavlink message cue semaphore\n");
		semaphoreValid = 1;
	}
#else
	sendMavMsgSem = sem_open(SEND_MAV_MUTEX_NAME, O_CREAT, 0777, 1);
	if (sendMavMsgSem == SEM_FAILED) {
		printf("Couldn't open mavlink message cue semaphore, errno = %d\n", errno);
		perror("MavLink Semaphore: ");

	}
	else {
		printf("Opened mavlink message cue semaphore\n");
		semaphoreValid = 1;
	}
#endif


	while ((!endGlobal && (semaphoreValid != 0) && (connectionToGcsEstablished == 1))) {
#ifdef _WIN32
		sem_wait(&sendMavMsgSem); // Wait to decrement the semaphore from the write to msg cue function
#else
		sem_wait(sendMavMsgSem); // Wait to decrement the semaphore from the write to msg cue function
#endif
        int stillDataToSend = 1;

        while(stillDataToSend) {
            bufferOutputIndex++;
            if (bufferOutputIndex >= MAV_MSG_BUFFER_LENGTH) bufferOutputIndex = 0; // Roll around)
            int bytesToSend = mavlink_msg_to_send_buffer(buf, &mavMsgBuffer[bufferOutputIndex].mavMsg);
            //int bytesSent = send(mavLinkSendSock.s, buf, bytesToSend, 0); //Note that for multiple destinations we should use send to
            //int bytesSent = sendto(qGcsSockFd, buf, bytesToSend, 0, (struct sockaddr *) &gcAddr, sizeof(struct sockaddr_in));
            if (connectionToGcsEstablished) { // only try to send if there's somebody listening
                long bytesSent = sendto(qGcsSockFd, buf, bytesToSend, 0, (const struct sockaddr*)&gcAddr, sizeof(gcAddr));
                if (bytesSent < 0) {
                    SERROR("There was an error sending  UDP data to MavLink\n");
                    stillDataToSend = 0;
                } else {
                    mavMsgBuffer[bufferOutputIndex].msgPendingStatus = 0; // message sent...
                    int theNextIndex = bufferOutputIndex + 1;
                    if (theNextIndex>= MAV_MSG_BUFFER_LENGTH) theNextIndex = 0;
                    if (mavMsgBuffer[theNextIndex].msgPendingStatus == 0) {
                        // look at the next item, it its pending is zero we are done
                        stillDataToSend = 0;
                    }
                }
            }
        }
		
	}
	if (semaphoreValid) {
#ifdef _WIN32
		sem_destroy(&sendMavMsgSem);
#else
		sem_destroy(sendMavMsgSem);
#endif
		
	}
	
    return 0; // Don't know why but XCode is forcing a return on a void* function
}

int writeToMavMsgCue(mavlink_message_t msg) {
	// This function adds the latest message to the message cue
	// Returns 0 on success, and -1 if the buffer is full
#ifdef	DONT_BUFFER_MAVLINK_LOCALHOST_MESSAGES
	// Here we just want to send stuff directly to the socket...probably cuz buffering isn't working
	if (connectionToGcsEstablished) {
		uint8_t buf[512]; // a local buffer
		int bytesToSend = mavlink_msg_to_send_buffer(buf, &msg);
        
        
		long bytesSent = sendto(qGcsSockFd, buf, bytesToSend, 0, (const struct sockaddr*)&gcAddr, sizeof(gcAddr)); // send the bytes to the GCS
		if (bytesSent < 0) {
			return -1;
		} else {
			return (int)bytesSent;
		}
	} else {
		return -1;
	}
#else

	if ((semaphoreValid) && (connectionToGcsEstablished)) {
		bufferInputIndex++;
		if (bufferInputIndex >= MAV_MSG_BUFFER_LENGTH) {
			bufferInputIndex = 0; // Roll around
		}
		if (mavMsgBuffer[bufferInputIndex].msgPendingStatus == 1) {
			printf("ERROR: Could not write the mav message to the cue...it's full!\n");
			return -1; // Could not add the message to the message cue
		}
		else {
			int mySemValue; // Note: appears unused
			// Add the message to the cue
			// Clear the data in the message cue
			memset(&mavMsgBuffer[bufferInputIndex].mavMsg, 0, sizeof(mavlink_message_t));
			memcpy(&mavMsgBuffer[bufferInputIndex].mavMsg, &msg, sizeof(msg));
			mavMsgBuffer[bufferInputIndex].msgPendingStatus = 1;
#ifdef _WIN32
			sem_post(&sendMavMsgSem); // Signal the sendMavThread that new data is waiting to be sent
#else
			sem_post(sendMavMsgSem); // Signal the sendMavThread that new data is waiting to be sent
#endif
		
			return 0;
		}
	} else {
		return -1;
	}
#endif
}





