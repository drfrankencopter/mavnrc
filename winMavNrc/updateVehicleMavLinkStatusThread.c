#include <stdio.h>
#include <stdlib.h>
#include "equivalenceCOREsharedMem.h" //access to shared mem and standard varaible macro definitions
#include "sleep.h" // for sleepy time
#include "updateVehicleMavLinkStatusThread.h"
#include "sendSockThread.h" // for sending mav data out on the wire, and to BOSS
#include <stdint.h>
#include <time.h>
#include <math.h>
#include "geo.h"
#include "systime.h"
#include "mavParams.h"
#include "serialToEthThreads.h"  // For sending out messages to the Peregrine
#include "log.h"
#include "NrcDefaults.h"
#include "missionManagerToSupervisorComs.h"
#include "bossComms.h" //For functions to send data to BOSS
#include "perceptionToSupervisorComs.h" // For simulating Peregrine output
#include "foreflightComms.h" // for sending data to foreflight 
#include "supervisor.h"
#include "mavProtocol.h"
#include "remotes.h"


#ifndef _WIN32
#include <sys/time.h>
#include <arpa/inet.h>
#endif

extern int endGlobal; // from main
// extern functions (from c++) -- These are in geo.h, but for some reason XCode doesn't see them
extern float get_distance_to_next_waypoint(double lat_now, double lon_now, double lat_next, double lon_next);
extern float get_distance_to_point_global_wgs84(double lat_now, double lon_now, float alt_now,
                                                    double lat_next, double lon_next, float alt_next,
                                                    float *dist_xy, float *dist_z);
extern int globallocalconverter_tolocal(double lat, double lon, double alt, float *x, float *y, float *z);
extern int globallocalconverter_init(double lat_0, double lon_0, float alt_0, uint64_t timestamp);

extern int connectionToGcsEstablished; //from mavRcv.c
extern int qGcsSockFd;
home_pos_t myHomePos;
// Some globals for the gps origin
static double originLat=0, originLon=0;
static float originAlt=0;
static int simulatedLpCounter = 0; // Used for simulating LP's being found

// Globals to send to remote instances
// These are necessary because all the functions called in this
// thread are using other global variables.
// Eventually this should be removed because globals = not cool
struct sockaddr_in* broadcastAddr;
struct sockaddr_in* gcsAddr;

void resetSimulatedLpCounter(void);

extern remotes_infos_t remotesInfo;
extern missionData_t* mission_data;                                     // Shared data for mission information
                                                                        // Global declared in main.c and defined in globals.c

joystickData_t myJoystickData;

//Private functions
int numSamplesForMsgRate(float msgRateHz);
void writeMissionMgrDataToDisk(float horzDist, float vertDist);


uint64_t microsSinceEpoch(void);
uint8_t determineEngageStatus(void);


// MSVC defines this in winsock2.h!?
//#ifdef _WIN32
//// KE not sure if we still need this on the windows side
//typedef struct timeval {
//	long tv_sec;
//	long tv_usec;
//} timeval;
//#endif

typedef 	struct {
	uint32_t itemNo;
	float horzDist; // m
	float vertDist; // m
	float gndSpeed; // m/s
} missionMgrLogData_t;

void updateVehicleMavLinkStatusThread(void *arg) {
	// This thread runs at 100 Hz, populates the mav link messages to send to the GCS at the desired rate
	static int cycleCounter = 0;
    static int pingCounter = 0;
	static uint8_t engaged = (uint8_t)666;          // Define as a high number for unknown state.
	static uint8_t mavState = MAV_STATE_UNINIT;     // Mav state as per https://mavlink.io/en/messages/common.html
	static bool localFrameInitialized = false;
    

    
#ifdef _WIN32
    initialize_windows_sockets();
#endif

	gcsAddr = &remotesInfo.gcsAddr;
	broadcastAddr = &remotesInfo.broadcastAddr;

    if (!connectionToGcsEstablished) { // sleep on sending regular updates until we have a connection w/gcs established
        usleep(100000); // Sleep 0.1 second
    }
    
	resetAllMissionItems(); // Zero out all mission data on launch

	while (!endGlobal) {
		usleep(10000); // 100 Hz operation

		if (engaged != determineEngageStatus()) { // Engage status has changed
            char* engagedText;
            engaged = determineEngageStatus();
            
			if (engaged == 1)
                engagedText = "FBW Engaged";
            else
                engagedText = "FBW Disengaged";
            
            sendSimpleText(remotesInfo.sockfdRemotes, engagedText, MAV_SEVERITY_INFO, broadcastAddr); // Send to all remotes
            sendSimpleTextToGcs(engagedText, MAV_SEVERITY_INFO);
		}
		mavState = determineMavState();
		if (localFrameInitialized == false) {
			localFrameInitialized = initializeLocalFrame(); // will return false if fails
		} else {
            if (cycleCounter % numSamplesForMsgRate(HOME_POS_RATE_HZ) == 0) {
                sendHomePos();
            }
			if (cycleCounter % numSamplesForMsgRate(LOCAL_POS_NED_RATE) == 0)
                sendLocalPosNed();
		}
		// Conditions to send data at different rates
		if (cycleCounter++ > 1000)
            cycleCounter = 0; // This measn our slowest rate message can be 0.1 Hz
		// Handle the regular messages here
		if (cycleCounter % numSamplesForMsgRate(HEARTBEAT_MSG_RATE_HZ) == 0) { // This loop at 1 Hz
            incrementMissesForAllRemotes(&remotesInfo);
            verifyRemotesConnection(&remotesInfo);
            
            sendHeartbeat(remotesInfo.sockfdRemotes, mission_data, broadcastAddr, mavState);
			sendHeartBeatMsg(mavState);  // Not sure what this does now
			sendGpsDataToForeflight();
		}
		if (cycleCounter % numSamplesForMsgRate(GPS_POS_MSG_RATE_HZ) == 0)
            sendGpsPosMsg();
		if (cycleCounter % numSamplesForMsgRate(VFR_HUD_RATE_HZ) == 0)
            sendVfrHud();
		if (cycleCounter % numSamplesForMsgRate(SYSTEM_STATUS_RATE_HZ) == 0) {
			sendSystemStatusMsg();
			sendExtendedSystemStatus();
			sendGpsRaw();
		}
		if (cycleCounter % numSamplesForMsgRate(ATTITUDE_MSG_RATE) == 0) {
			sendAttitudeMsg();				// Note that this message doesn't seem to update the GCS
			sendAttitudeQuaternionMsg();	// This message does seem to update the GCS
			sendAttDataToForeflight();		// Foreflight attitude data at 10 Hz
		}
        if (cycleCounter % numSamplesForMsgRate(PING_MSG_RATE) == 0) {
            // Send a ping request to everyone
            sendPingRequest(qGcsSockFd, pingCounter, gcsAddr);
            sendPingRequest(remotesInfo.sockfdRemotes, pingCounter, broadcastAddr);
            ++pingCounter;
        }
        if (cycleCounter % numSamplesForMsgRate(PEREGRINE_HOME_POS_AND_ORIGIN_RATE) == 0) {
            if (originLat != 0.0) {  // Don't send origin if we haven't initialized it yet
                printf("Sending Home Position to Peregrine\n");
                sendHomePosPeregrine();
                sendGpsGlobalOriginPeregrine();
            }
        }
		if ((mission_data->myCurrentMission.missionStarted) && (cycleCounter % numSamplesForMsgRate(MISSION_PROGRESS_RATE) == 0)) {
			if (mission_data->myCurrentMission.missionMonitoringRequired)
                monitorMissionProgress(); // Only monitor progress if supervisor says so
		}
        
        if (remotesInfo.numRemotes > 0) {
            
            if (mission_data->newMissionFromGcs) {
                // If there's a new mission, send the new mission count to the remote instances of GCS
                printf("SENDING NEW MISSION COUNT TO ALL REMOTES\n");
                sendMissionCount(remotesInfo.sockfdRemotes, mission_data, broadcastAddr, sizeof(*broadcastAddr), 0, 0, MAV_MISSION_TYPE_MISSION);
                mission_data->newMissionFromGcs = 0;
            }
            else if (mission_data->newMissionFromRemote) {
                // If there's a new mission coming from a remote instance, send to everyone plus to the local GCS
                int resRemote = sendMissionCount(remotesInfo.sockfdRemotes, mission_data, broadcastAddr, sizeof(*broadcastAddr), 0, 0, MAV_MISSION_TYPE_MISSION);
                int resGcs = sendMissionCount(qGcsSockFd, mission_data, gcsAddr, sizeof(*gcsAddr), 0, 0, MAV_MISSION_TYPE_MISSION);
                if (resGcs && resRemote)
                    mission_data->newMissionFromRemote = 0;
            }
            
        }
        else if (mission_data->newMissionFromGcs || mission_data->newMissionFromRemote) {
            // No remotes, but there's a new mission. Don't send it.
            mission_data->newMissionFromGcs = 0;
            mission_data->newMissionFromRemote = 0;
        }
	}
	// End of thread signalled
}

void resetAllMissionItems(void) {
    memset(&mission_data->myCurrentMission.standard, 0, sizeof(mission_data->myCurrentMission.standard));
    memset(&mission_data->myCurrentMission.fences, 0, sizeof(mission_data->myCurrentMission.fences));
    memset(&mission_data->myCurrentMission.rallyPoints, 0, sizeof(mission_data->myCurrentMission.rallyPoints));
    
    memset(&mission_data->myNewMission.standard, 0, sizeof(mission_data->myNewMission.standard));
    memset(&mission_data->myNewMission.fences, 0, sizeof(mission_data->myNewMission.fences));
    memset(&mission_data->myNewMission.rallyPoints, 0, sizeof(mission_data->myNewMission.rallyPoints));
    
	clearBossMissionData(); // zero out the mission
	extern bossMissionData_t bossMissionDataGlobal; // Declared in bossComms.c
	sendMissionItemToBoss(bossMissionDataGlobal);
}

uint8_t determineMavState(void) {
	// Let's use the INS status to determine the Mav state
	uint8_t mavState = MAV_STATE_UNINIT;
	uint32_t insStatus = shared_memory->hg1700.status;
	uint32_t hmuStatus = shared_memory->hmu_safety.safety_status;

	if ((insStatus & BIT(0)) == 0) {
		mavState = MAV_STATE_BOOT; // Ins is reading 0, so lets say it's booting
	} else {
		mavState = MAV_STATE_CALIBRATING;
		if (insStatus && BIT(1)) {
			mavState = MAV_STATE_STANDBY;
			if ((hmuStatus & BIT(6) == 0) && (hmuStatus & BIT(7)) == 0)
                mavState = MAV_STATE_ACTIVE; // Here there are no alerts from the HMU
		}
	}
	return mavState;
}

bool initializeLocalFrame(void) {
	bool initialized = false;

	// Here, we check and see if the value of lat/lon is non zero and if so try to init the coord system
	if ((LAT_INS != 0.0) && (LON_INS != 0.0)) {
		// Start the init sequence
		// For now let's also make this the home positoin
		globallocalconverter_init(LAT_INS, LON_INS, ALT_INS * FEET_TO_M, 0); // Initialize with a 0 timestamp;
		sendGpsGlobalOrigin(LAT_INS, LON_INS, ALT_INS);
		setCurrentPositionAsHome();

		initialized = true;
	}
	return initialized;
}

void sendGpsGlobalOriginPeregrine(void) {
	// Suggest that if periodic re-sending of GPS-Global Origin is required that the home position lat/lon alt be sent
	// as we are using initial home position as gps origin

	int32_t latIntScaled, lonIntScaled, altIntMm;
	mavlink_message_t msg;

	latIntScaled = originLat * 1E7;
	lonIntScaled = originLon * 1E7;
	altIntMm = originAlt * FEET_TO_MM;

	mavlink_msg_gps_global_origin_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, latIntScaled, lonIntScaled, altIntMm, microsSinceEpoch());
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to Peregrine

}

void sendGpsGlobalOrigin(double lat, double lon, float alt) {
    // Suggest that if periodic re-sending of GPS-Global Origin is required that the home position lat/lon alt be sent
    // as we are using initial home position as gps origin
    
    // Save current position as origin
    originLat = lat;
    originLon = lon;
    originAlt = alt;
    
    int32_t latIntScaled, lonIntScaled, altIntMm;
    mavlink_message_t msg;
    
    latIntScaled = lat * 1E7;
    lonIntScaled = lon * 1E7;
    altIntMm = alt * FEET_TO_MM;
    
    mavlink_msg_gps_global_origin_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, latIntScaled, lonIntScaled, altIntMm, microsSinceEpoch());
    
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
    
    writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to Peregrine
    
}

void setCurrentPositionAsHome(void) {
	// Ad functionality here
			// Now populate the home position struct
	myHomePos.lat = LAT_INS;
	myHomePos.lon = LON_INS;
	globallocalconverter_tolocal(myHomePos.lat, myHomePos.lon, ALT_INS * FEET_TO_M, &myHomePos.x, &myHomePos.y, &myHomePos.z);
	Euler_t attitude;
	attitude.heading = PSI_INS;
	attitude.pitch = THETA_INS;
	attitude.roll = PHI_INS;
	EulerToQuaternion(attitude, &myHomePos.q); // Grab the quaternion
	// Let's set the appraoch point as a vertical approach
	myHomePos.approach_x = myHomePos.x; 
	myHomePos.approach_y = myHomePos.y;
	myHomePos.approach_z = myHomePos.z + 10 * FEET_TO_M; // Let's say  10 feet above the home position, then vert land
	sendHomePos();
}

void sendHomePos(void) {
	mavlink_message_t msg;
	float q[4];
	q[0] = myHomePos.q.x;
	q[1] = myHomePos.q.y;
	q[2] = myHomePos.q.z;
	q[3] = myHomePos.q.w;
	mavlink_msg_home_position_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, (int32_t)(myHomePos.lat*1E7), (int32_t)(myHomePos.lon*1E7), myHomePos.z * 1000, myHomePos.x, myHomePos.y, myHomePos.z, q, myHomePos.approach_x, myHomePos.approach_y, myHomePos.approach_z, microsSinceEpoch());
    
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	
    writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to peregrine
}

void sendHomePosPeregrine(void) {
    //For repeatedly sending home to Peregrine
    mavlink_message_t msg;
    float q[4];
    q[0] = myHomePos.q.x;
    q[1] = myHomePos.q.y;
    q[2] = myHomePos.q.z;
    q[3] = myHomePos.q.w;
    mavlink_msg_home_position_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, (int32_t)(myHomePos.lat*1E7), (int32_t)(myHomePos.lon*1E7), myHomePos.z * 1000, myHomePos.x, myHomePos.y, myHomePos.z, q, myHomePos.approach_x, myHomePos.approach_y, myHomePos.approach_z, microsSinceEpoch());
    writeToSerialMavMsgCue(msg); // Also send to peregrine
}

void sendGpsRaw(void) {
	mavlink_message_t msg;
	//Raw GPS data...not all of which is within INS data
	uint8_t gpsMode = GPS_FIX_TYPE_NO_GPS; // Default

/*
	
Field Name	Type	Units	Values	Description
time_usec	uint64_t	us		Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
fix_type	uint8_t		GPS_FIX_TYPE	GPS fix type.
lat	int32_t	degE7		Latitude (WGS84, EGM96 ellipsoid)
lon	int32_t	degE7		Longitude (WGS84, EGM96 ellipsoid)
alt	int32_t	mm		Altitude (MSL). Positive for up. Note that virtually all GPS modules provide the MSL altitude in addition to the WGS84 altitude.
eph	uint16_t			GPS HDOP horizontal dilution of position (unitless). If unknown, set to: UINT16_MAX
epv	uint16_t			GPS VDOP vertical dilution of position (unitless). If unknown, set to: UINT16_MAX
vel	uint16_t	cm/s		GPS ground speed. If unknown, set to: UINT16_MAX
cog	uint16_t	cdeg		Course over ground (NOT heading, but direction of movement) in degrees * 100, 0.0..359.99 degrees. If unknown, set to: UINT16_MAX
satellites_visible	uint8_t			Number of satellites visible. If unknown, set to 255
alt_ellipsoid **	int32_t	mm		Altitude (above WGS84, EGM96 ellipsoid). Positive for up.
h_acc **	uint32_t	mm		Position uncertainty.
v_acc **	uint32_t	mm		Altitude uncertainty.
vel_acc **	uint32_t	mm		Speed uncertainty.
hdg_acc **	uint32_t	degE5		Heading / track uncertainty
yaw **	uint16_t	cdeg		Yaw in earth frame from north. Use 0 if this GPS does not provide yaw. Use 65535 if this GPS is configured to provide yaw and is currently unable to provide it. Use 36000 for north.
*/

	if (INS_STATUS & BIT(0)) {
		gpsMode = GPS_FIX_TYPE_NO_FIX;
		if ((INS_STATUS & BIT(2))==0) { //GPS bit
			gpsMode = GPS_FIX_TYPE_3D_FIX;
		}
		if ((INS_STATUS & BIT(3)) == 0) { // DGPS bit
			gpsMode = GPS_FIX_TYPE_RTK_FLOAT;
		}
	}
	uint8_t numSats = (uint8_t)shared_memory->gps_data.number_of_satelites;
	float  trackAngleTemp = (RADS2DEG)*atan2(VEL_E_INS, VEL_N_INS);
	if (trackAngleTemp < 0) trackAngleTemp += 360;
	uint16_t courseOverGndCdeg = (uint16_t) trackAngleTemp*100; // In centideg
	uint16_t  groundspeed = (uint16_t) (sqrt(VEL_E_INS*VEL_E_INS + VEL_N_INS * VEL_N_INS)*FEET_TO_M*100); // Cm/s
	uint32_t hAcc = ((shared_memory->gps_data.north_sigma + shared_memory->gps_data.east_sigma) / 2.0 * 1000); // sigma in mm
	uint32_t vAcc = shared_memory->gps_data.vertical_sigma*1000; // sigma in mm
    uint32_t diffAge = (uint32_t)shared_memory->gps_data.differential_age;
	uint32_t altElipsoid = ALT_INS * FEET_TO_MM; // THIS SHOULD REALLY BE WGS84 ALTITUDE
    

    //KE commented this out OCt 22 2021 as latest mavlink has more arguments
	//mavlink_msg_gps2_raw_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, microsSinceEpoch(), gpsMode, (int32_t)(LAT_INS*1E7), (int32_t)(LON_INS*1E7), ALT_INS*FEET_TO_MM, UINT16_MAX, UINT16_MAX, groundspeed, courseOverGndCdeg, numSats, 0, diffAge, 0); // I shoved in 0 for yaw here..wasn't sure what to put
    mavlink_msg_gps2_raw_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, microsSinceEpoch(), gpsMode, (int32_t) (LAT_INS*1E7), (int32_t) (LON_INS*1E7), ALT_INS*FEET_TO_MM, UINT16_MAX, UINT16_MAX, groundspeed, courseOverGndCdeg, numSats, 0, diffAge, 0, altElipsoid, hAcc , vAcc, 0, 0);
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
	mavlink_msg_gps_raw_int_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, microsSinceEpoch(), gpsMode, (int32_t)(LAT_INS*1E7), (int32_t)(LON_INS*1E7), ALT_INS*FEET_TO_MM, UINT16_MAX, UINT16_MAX, groundspeed, courseOverGndCdeg, numSats, altElipsoid, hAcc, vAcc, 0, 0, 0); // I used zero for a bunch of these in the end
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
	
}

void sendVfrHud(void) {
	// This message does metrics for display in the GCS...message 74
	mavlink_message_t msg;
	float airspeed = TAS_CIF * KTS_TO_MPS;
	float groundspeed = sqrt(VEL_E_INS*VEL_E_INS + VEL_N_INS * VEL_N_INS)*FEET_TO_M;
	uint16_t heading;
	if (PSI_INS < 0) {
		heading = PSI_INS + 360;
	} else {
		heading = PSI_INS;
	}
	uint16_t throttle = 100.0 * COL_ACT_POS / 10.7; // Map colelctive as throttle
	float altitudeMsl = ALT_INS * FEET_TO_M; // Might want this to be pressure altitude above a certain height above ground
	float climbRate = -VEL_Z_INS * FEET_TO_M;
	// Populate the message
	mavlink_msg_vfr_hud_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, airspeed, groundspeed, heading, throttle, altitudeMsl, climbRate);
    
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
    
	writeToMavMsgCue(msg);
    
}

void processTakeoffRequest(mavlink_command_long_t command) {

	// Here is a placeholder for processing takeoff requests
	printf("P1 = Pitch Target = %1.1f --> Ignored for ASRA\n", command.param1);
	printf("P4 = Heading Target =%1.1f -- If Nan, use current heading\n", command.param4);
	printf("P5 = Latitude %1.6f --> Use Current if NaN\n", command.param5);
	printf("P6 = Longitude %1.6f --> Use Current if NaN\n", command.param6);
	printf("P7 = Altitude %1.1f m MSL (I think) or %1.1f ft MSL\n", command.param7, command.param7*METERS_TO_FT);

	// Here is where you should actually signal the autopilot to takeoff
	
}



void sendSimpleTextToGcs(char *myText, uint8_t severityLevel) {
	//Sends a single text string up to 50 chars to the gcs. Severity level defined in https://mavlink.io/en/messages/common.html#MAV_SEVERITY
	mavlink_message_t msg;
	char myLimitedText[MAX_TXT_MSG_LENGTH];
#ifdef _WIN32
	strncpy_s(myLimitedText, MAX_TXT_MSG_LENGTH, myText, MAX_TXT_MSG_LENGTH); // Max 255  bytes for a single message...note that this function won't work in Linux/MacOs
#else
    strncpy(myLimitedText,myText, MAX_TXT_MSG_LENGTH);
#endif
	mavlink_msg_statustext_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, severityLevel, myLimitedText, 0, 0);
	writeToMavMsgCue(msg);
	// Now we want to build up a new time tagged string and record that to our message buffer
	writeTextMsgToLogFile(myText, severityLevel);
}

void writeTextMsgToLogFile(char *myText, uint8_t severityLevel) {
	extern buffer_t mavNrcMessage_logbuf; // decalred main winPthreadConsole
	static int i = 0; // Keep a running log of message #'s
	i++;

	char myLimitedText[MAX_TXT_MSG_LENGTH + 128] = { 0 }; // I added some buffer here so we can add time tags,etc...defined to all null to start
	char myEngageTxt[20];
	if (shared_memory->fcc_configuration.fbw_engaged) {
		sprintf(myEngageTxt, "%d", shared_memory->fcc_configuration.engagement_number);
	} else {
		sprintf(myEngageTxt, "N/A"); // Not engaged
	}
	// I put the 126 here so we only allow up to 126 chars prior to writing the message itself. Then we leave room for a CR and a null char at the end of the string
	snprintf(myLimitedText, 126, "%d: GpsT: %1.2f(s), SysT: %1.2f(s), Eng: %s, Sev: %d, Msg: ", i, shared_memory->hg1700.time, SysTime(), myEngageTxt, severityLevel);
	strcat(myLimitedText, myText);
	strcat(myLimitedText, "\n"); // Add the trailing carriage return
	// Now let's write the data to the log buffer
	Write_To_Buffer((void *)(&myLimitedText), (int)strlen(myLimitedText), &mavNrcMessage_logbuf);
}

void sendExtendedSystemStatus(void) {
	// here we will send the takeoff/landing state of the heli
	mavlink_message_t msg;
	uint8_t landedState = 0;
	// This is really dumb code right now and should use flags in the autopilot code
	if (shared_memory->hg1700.mixedhgt > 7) {
		landedState = MAV_LANDED_STATE_IN_AIR;
	} else {
		landedState = MAV_LANDED_STATE_ON_GROUND;
	}
	
	// NOTE: DEC 4 2020 --- landed state has been hard coded to ON_GROUND so we can start a mission mid flight
	//				    ---- this should probably inclide a check for a switch setting to prevent accidental mission start in flight
	//						 or unanticipated effects of declaring the vehicle is on ground
	// A test to see if we can just override the landed state to be able to start missions in the air -- it works
	//landedState = MAV_LANDED_STATE_ON_GROUND;
	// NOTE: FEB 16 2021 --- landed state is now set to be overriden by a switch on eng workstation
	if (shared_memory->ffs_output.ffs_switches & BIT(25)) {
		// We want to enable in flight restart
		landedState = MAV_LANDED_STATE_ON_GROUND;  // so declare the aircraft as 'landed' even though it is in the air
	}
	   	 
	mavlink_msg_extended_sys_state_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, 0, landedState);
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
}

void sendAutopilotVersion(int target) {
	// This is our response to the request for autopilot capabilities
	printf("Sending Autopilot Version Info...\n");
	mavlink_message_t msg;
	// Basically the only field we are currently going to worry about is the supported feature bitmap
	uint64_t autoCapabilities = 0;
	// Now built the bitmask from those here: https://mavlink.io/en/messages/common.html#MAV_PROTOCOL_CAPABILITY
	
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_PARAM_FLOAT;
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MISSION_INT; // Not sure if this is desired format or not
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_COMMAND_INT;
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_SET_ATTITUDE_TARGET;
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_SET_POSITION_TARGET_GLOBAL_INT; // Use global scaled integers for pos cmds
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MAVLINK2;
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MISSION_FENCE;
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_MISSION_RALLY;
	autoCapabilities |= MAV_PROTOCOL_CAPABILITY_FLIGHT_INFORMATION; 
	//autoCapabilities |= MAV_PROTOCOL_CAPABILITY_FLIGHT_INFORMATION; // Not sure on this one yet
	// Other data for the AP version message
#define FIRMWARE_TYPE_DEV 1
	int firmware_type = FIRMWARE_TYPE_DEV;
	char version[3] = { 1, 9, 1 };

	uint32_t flight_sw_ver = 0;
	flight_sw_ver = ((uint8_t)version[0] << 8 * 3) |
		((uint8_t)version[1] << 8 * 2) |
		((uint8_t)version[2] << 8 * 1) | firmware_type;
	
	const uint32_t middleware_sw_ver = 1;
	const uint32_t os_sw_ver = 1;
	const uint8_t board_ver = 1;
	const uint8_t flight_custom_ver[8] = { 0 };
	const uint8_t middleware_custom_ver[8] = { 0 };
	const uint8_t os_custom_ver[8] = { 0 };
	const uint16_t vendor_id = 0;
	const uint16_t product_id = 0;
	const uint64_t uid = 0;
	const uint8_t uid2[18] = { 0 };
	mavlink_msg_autopilot_version_pack(MY_MAV_SYS_ID, MY_MAV_SYS_ID, &msg, autoCapabilities, flight_sw_ver, middleware_sw_ver,
		os_sw_ver, board_ver, flight_custom_ver, middleware_custom_ver, os_custom_ver, vendor_id,
		product_id, uid, uid2);
    switch(target) {
        case PEREGRINE_COMP_ID:
            writeToSerialMavMsgCue(msg);
            break;
        case GCS_ID:
            writeToMavMsgCue(msg);
            break;
        default:
            writeToMavMsgCue(msg);
    }
	

}

void sendCommandAck(uint16_t cmd, uint8_t result, uint8_t progress, int32_t resultParam2, uint8_t targetSys, uint8_t targetComp ) {
	// Send an ack to the GCS
	mavlink_message_t msg;
	mavlink_msg_command_ack_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, cmd, result, progress, resultParam2, targetSys, targetComp);
	writeToMavMsgCue(msg);
}

void sendProtocolVersion(void) {
	// Send the protocol version message
	mavlink_message_t msg;
	const uint8_t srcHash = 0; // No idea here
	const uint8_t libHash = 0;
#define MAVLINK_VER 200
	mavlink_msg_protocol_version_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg,
		MAVLINK_VER,
		MAVLINK_VER,
		MAVLINK_VER,
		&srcHash,
		&libHash);
	writeToMavMsgCue(msg);
}

void sendHeartBeatMsg(uint8_t mavState) {
	mavlink_message_t msg;
	// KE Is trying here to let the mav state reflect the FBW status: https://mavlink.io/en/messages/common.html#MAV_MODE_FLAG
	// This is a bit field so multiple simultaneous options are present
	// 	MAV_MODE_FLAG_AUTO_ENABLED - autonomous mode enabled, system finds its own goal positions. Guided flag can be set or not, depends on the actual implementation.
	//  MAV_MODE_FLAG_GUIDED_ENABLED - guided mode enabled, system flies waypoints / mission items. (Autopilot is coupled)
	//  MAV_MODE_FLAG_STABILIZE_ENABLED - system stabilizes electronically its attitude (and optionally position). It needs however further control inputs to move around.
	//  MAV_MODE_FLAG_HIL_ENABLED - hardware in the loop simulation. All motors / actuators are blocked, but internal software is full operational.
	//  MAV_MODE_FLAG_MANUAL_INPUT_ENABLED -  remote control input is enabled.
	//  MAV_MODE_FLAG_SAFETY_ARMED - safety set to armed. Motors are enabled / running / can start. Ready to fly. Additional note: this flag is to be ignore when sent in the command MAV_CMD_DO_SET_MODE and MAV_CMD_COMPONENT_ARM_DISARM shall be used instead. The flag can still be used to report the armed state.
	uint8_t autopilotMode = 0; // 221; // 221 is the nominal Px4 mode
	
	// Let's always say that we are ready to fly/armed
	//autopilotMode |= MAV_MODE_FLAG_SAFETY_ARMED;
	autopilotMode |= MAV_MODE_FLAG_SAFETY_ARMED;

	if ((HMU_SAFETY_STATUS & BIT(0)) && (HMU_SAFETY_STATUS & BIT(1))) {
		// Here we would do some checks based on what control mode is active
		// If we are in a fully autonomous mode (collision/obstacle avoid, or other): MAV_MODE_FLAG_AUTO_ENABLED 
		// If we are coupled to some guidance: MAV_MODE_FLAG_GUIDED_ENABLED 
		// If our control index is >= 2, then we are above ACAH and should flag MAV_MODE_FLAG_STABILIZE_ENABLED
		 
	}
	else {

	}
	// For generic Autopilot
	//mavlink_msg_heartbeat_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, MAV_TYPE_HELICOPTER, MAV_AUTOPILOT_GENERIC, autopilotMode, myMavCustomMode, mavState);
	//
    // For peregrine testing just echo back the requesrted flight mode myBaseMode and myCustomMode
	// For some reason it seems that QGroundControl will only display the UI elements for the PX4 autopilot.
	mavlink_msg_heartbeat_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, MAV_TYPE_HELICOPTER, MAV_AUTOPILOT_PX4, mission_data->myMavBaseMode, mission_data->myMavCustomMode, mavState);
	//mavlink_msg_heartbeat_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, 2, 12, 29, 50593792,3 ); // A verbatim copy from the sitl sim
	writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to peregrine
}

void sendGpsPosMsg(void) {
	mavlink_message_t msg;
	float psi360 = PSI_INS;
	if (psi360 < 0) psi360 += 360;

	mavlink_msg_global_position_int_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, (uint32_t)microsSinceEpoch(), (int32_t)(LAT_INS*1E7), (int32_t)(LON_INS*1E7), (int32_t)(ALT_INS*FEET_TO_MM),
		(int32_t)HEIGHT_AGL*FEET_TO_MM, (int16_t)VEL_N_INS*FEET_TO_CM, (int16_t)VEL_E_INS*FEET_TO_CM, (int16_t)VEL_Z_INS*FEET_TO_CM, (uint16_t)(psi360*100));
    
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
    
	writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to Peregrine
}

void sendGpsRawMsg(void) {
	// Do some intermediate calculations here
    
}

void sendLocalPosNed(void) {
	// Grab INS position, convert to local, and send to GCS
    extern buffer_t localPosition_logbuf;
	mavlink_message_t msg;
	float localX, localY, localZ;
	float myAltIns = ALT_INS;
	float myAltInsM = ALT_INS * FEET_TO_M;
	//printf("AltIns = %f, ft, and %f m\n", myAltIns, myAltInsM);

    
    // Make a struct for data recording
    typedef struct {
        double gpsTime;
        float x;
        float y;
        float z;
    } localPosition_t;

    localPosition_t  myLocalPositionForRecording;
    
	globallocalconverter_tolocal(LAT_INS, LON_INS, myAltInsM, &localX, &localY, &localZ);
	mavlink_msg_local_position_ned_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, (uint32_t)microsSinceEpoch(), localX, localY, localZ, VEL_N_INS*FEET_TO_M, VEL_E_INS*FEET_TO_M, VEL_Z_INS*FEET_TO_M);
    
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
    
	writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to Peregrine
    
    // Now get ready to send to log buffer
    // Start by populating struct
    myLocalPositionForRecording.gpsTime = shared_memory->hg1700.time;
    myLocalPositionForRecording.x = localX;
    myLocalPositionForRecording.y = localY;
    myLocalPositionForRecording.z = localZ;
    Write_To_Buffer(&myLocalPositionForRecording, sizeof(localPosition_t), &localPosition_logbuf);
    
}

void sendSystemStatusMsg(void) {
	mavlink_message_t msg;
	// Placeholder data here for now until I better understand the satus word
	mavlink_msg_sys_status_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, 0, 0, 0, 500, 11000, -1, -1, 0, 0, 0, 0, 0, 0);
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
}

void sendAttitudeMsg(void) {
	mavlink_message_t msg;
	float psi360 = PSI_INS;
	if (psi360 < 0) psi360 += 360;
	mavlink_msg_attitude_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, microsSinceEpoch(), PHI_INS*DEG2RADS, THETA_INS*DEG2RADS, psi360*DEG2RADS, P_INS*DEG2RADS, Q_INS*DEG2RADS, R_INS*DEG2RADS);
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
}

void sendAttitudeQuaternionMsg(void) {
	mavlink_message_t msg;
	float psi360 = PSI_INS;
	if (psi360 < 0) psi360 += 360;
	Euler_t attitudeEuler;
	quaternion_t attitudeQuaternion;
	attitudeEuler.roll = PHI_INS;
	attitudeEuler.pitch = THETA_INS;
	attitudeEuler.heading = psi360;
	EulerToQuaternion(attitudeEuler, &attitudeQuaternion);
	const float offsetRot[4] = { 1, 0, 0, 0 };
	mavlink_msg_attitude_quaternion_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, microsSinceEpoch(), attitudeQuaternion.w, attitudeQuaternion.x, attitudeQuaternion.y, attitudeQuaternion.z, P_INS*DEG2RADS, Q_INS*DEG2RADS, R_INS*DEG2RADS,offsetRot);
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
	writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg); // Also send to Peregrine
}

uint8_t determineEngageStatus(void) {
	uint8_t engaged;
	if ((HMU_SAFETY_STATUS & BIT(0)) && (HMU_SAFETY_STATUS & BIT(1))) {
		// We are engaged
		engaged = 1;
	}
	else {
		//We are not engaged
		engaged = 0;
	}
	return engaged;
}

#ifdef _WIN32 //MacOs has a gettimeofday function
int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
	// Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
	// This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
	// until 00:00:00 January 1, 1970 
	static const uint64_t EPOCH = ((uint64_t)116444736000000000ULL);

	SYSTEMTIME  system_time;
	FILETIME    file_time;
	uint64_t    time;

	GetSystemTime(&system_time);
	SystemTimeToFileTime(&system_time, &file_time);
	time = ((uint64_t)file_time.dwLowDateTime);
	time += ((uint64_t)file_time.dwHighDateTime) << 32;

	tp->tv_sec = (long)((time - EPOCH) / 10000000L);
	tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
	return 0;
}
#endif

// HELPER FUNCTIONS
int numSamplesForMsgRate(float msgRateHz) {
	int retVal = (int)round(CYCLE_RATE / msgRateHz);
	return retVal;
}


// Helper timer functinon
uint64_t microsSinceEpoch()
{

	struct timeval tv; // Note in windows this was timeval tv, not struct timeval

	uint64_t micros = 0;

	gettimeofday(&tv, NULL);
	micros = ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;

	return micros;
}

void broadcastCurrentMissionItem(int itemNo) {

    mavlink_message_t msg;
    mavlink_msg_mission_current_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, itemNo);
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
    writeToMavMsgCue(msg);
    // Now build the Peregrine item
    writeToSerialMavMsgCue(msg);

    // Populate mission data
    DEBUG_PRINT("Broadcasting Mission Item Data to GCS and Peregrine\n");
}

void broadcastMissionItemReached(void) {
    //Signals that the current mission item has been reached, and cues up the next one
    mavlink_message_t msg;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);
	//PAck the message that the current item was reached
    mavlink_msg_mission_item_reached_pack(MY_MAV_SYS_ID, MY_MAV_COMP_ID, &msg, mission_data->myCurrentMission.currentMissionItem);
	//Send it to the GCS and the PErception system (peregrine)
    
	if (remotesInfo.numRemotes > 0)
		// Send to all remotes
		sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr));
    writeToMavMsgCue(msg);
    writeToSerialMavMsgCue(msg);
	// Now figure out what the next item is
    extern buffer_t missionStatus_logbuf;
    missionStatusRecording_t missionStatusData = {0}; // zero it initially
#define ITEM_YAW_NOT_VALID 666
    float missionItemYaw = ITEM_YAW_NOT_VALID; // Default to something impossible for error checking
    
    
    if (specificMission->numMissionItems > (mission_data->myCurrentMission.currentMissionItem + 1) ) {
        // There are still more mission items to go through
        //Set the current item to the next item
        mission_data->myCurrentMission.currentMissionItem = mission_data->myCurrentMission.currentMissionItem + 1;
		// If the takeoff point doesn't have a heading let's assign it to the direction of the next WP
		if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command == MAV_CMD_NAV_TAKEOFF) {
			if (isnan(specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4)) { // Param 4 is T/O heading...if not specified then it's nan
				double dirToNextWp;
				// We need to find the lat/lon of the next Wp...but it can't be speed change
				int nextWpFound = 0; // false
				int nextWpIndex = 1;

				for (int j = 1; mission_data->myCurrentMission.currentMissionItem + j < specificMission->numMissionItems; j++) {

					if ((specificMission->items[mission_data->myCurrentMission.currentMissionItem + j].command == MAV_CMD_NAV_WAYPOINT) || (specificMission->items[mission_data->myCurrentMission.currentMissionItem + j].command == MAV_CMD_NAV_LAND)) {
						// We'll let the 2nd WP be  a WP or a land
						nextWpFound = 1;
						nextWpIndex = mission_data->myCurrentMission.currentMissionItem + j;
						break;
					}
				}

				// WE want to determing the direction from the TO Wp to the next WP to see if we're on the right heading
				get_bearing_to_next_waypointKE(specificMission->items[mission_data->myCurrentMission.currentMissionItem].x / (1.0E7), specificMission->items[mission_data->myCurrentMission.currentMissionItem].y / (1.0E7), specificMission->items[nextWpIndex].x / (1.0E7), specificMission->items[nextWpIndex].y / (1.0E7), &dirToNextWp);
				dirToNextWp = dirToNextWp * RAD2DEG;
				// Now let's modify the mission to have include the heading
                specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4 = dirToNextWp;
			}
		}
        broadcastCurrentMissionItem(mission_data->myCurrentMission.currentMissionItem);
		//now tell the supervisor what the current mission item is
		signalNewMissionItem(specificMission->items[mission_data->myCurrentMission.currentMissionItem]); // Send the current item
        //Popuate mission status struct
        missionStatusData.currentItemNo = mission_data->myCurrentMission.currentMissionItem;
        missionStatusData.frame = specificMission->items[mission_data->myCurrentMission.currentMissionItem].frame;
        missionStatusData.autocontinue = specificMission->items[mission_data->myCurrentMission.currentMissionItem].autocontinue;
        missionStatusData.command = specificMission->items[mission_data->myCurrentMission.currentMissionItem].command;
        missionStatusData.current = specificMission->items[mission_data->myCurrentMission.currentMissionItem].current;
        missionStatusData.param1  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param1;
        missionStatusData.param2  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param2;
        missionStatusData.param3  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param3;
        missionStatusData.param4  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4;
        missionStatusData.x  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].x;
        missionStatusData.y  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].y;
        missionStatusData.z  = specificMission->items[mission_data->myCurrentMission.currentMissionItem].z;
		updateBossMissionData();
       
    } else {
        // We got to the end of the mission
        sendSimpleTextToGcs("Mission completed", MAV_SEVERITY_ALERT);
        sendSimpleText(remotesInfo.sockfdRemotes, "Mission completed", MAV_SEVERITY_ALERT, broadcastAddr);
    }
    Write_To_Buffer(&missionStatusData, sizeof(missionStatusRecording_t), &missionStatus_logbuf);
    

}

void setMissionMonitorStatusTo(int status) {
	// Set to 1 to monitor mission progress (mission manager function)
	// Set to 0 to bypass regular mission management
	mission_data->myCurrentMission.missionMonitoringRequired = status;
}

// Mission Management done here
void monitorMissionProgress(void) {
    // For Peregrine we will just compare our position against the current waypoint, and see if we are in
    // some kind of acceptance radius
	static int lastWpAchieved = 0;
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);
	static int clrTakeoffNotificationArmed = 1;
	static int liftOffNotificationArmed = 1; // For announcing lift off via speech

//#define TIMED_SEQUENCE_TEST
#ifdef TIMED_SEQUENCE_TEST
    //For now just a cheesy timer based thing
    static double lastEventTime = 0;
    if (lastEventTime==0) {
        lastEventTime = SysTime();
    }
    double currentTime = SysTime();
    if (currentTime - lastEventTime > 10.0) { // 10 seconds per waypoint
        // Switch to next waypoint
        if (mission_data->myCurrentMission.numMissionItems >= (mission_data->myCurrentMission.currentMissionItem + 1) ) {
            // There are still more mission items to go through
            printf("Reached Mission Item #%d\n",mission_data->myCurrentMission.currentMissionItem);
            broadcastMissionItemReached();
            lastEventTime = currentTime;
        } else {
            // We got to the end of the mission
        }
    }
#else
    static float horzDist; // Made these static so they keep their values even if we have speed mission items
    static float vertDist;

	//Determine if we can calculate distance
	if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command != MAV_CMD_DO_CHANGE_SPEED) {
		get_distance_to_point_global_wgs84(shared_memory->hg1700.lat, shared_memory->hg1700.lng, shared_memory->hg1700.alt*FEET_TO_M,
			(double)specificMission->items[mission_data->myCurrentMission.currentMissionItem].x / 1.0E7,
			(double)specificMission->items[mission_data->myCurrentMission.currentMissionItem].y / 1.0E7,
            specificMission->items[mission_data->myCurrentMission.currentMissionItem].z, &horzDist, &vertDist);
	}
    
    //If statement for printouts
    static int i=0;
    if (i++>500) { // Reducing the printfs....
        i = 0;
        // Now trying another function
        printf("Horz Distance to next Waypoint = %1.1f m, Vert Dist = %1.1f m\n",horzDist,vertDist );
    }
    // Aug 19 2020, it doesn't seem like the vertical reference calculation is working well
    //if ((horzDist < DEFAULT_ACCEPTANCE_RADIUS_HORZ) && (vertDist < DEFAULT_ACCEPTANCE_RADIUS_VERT))
    float horzAcceptDist = DEFAULT_ACCEPTANCE_RADIUS_HORZ;
	static int needToSetHoverCriteria = 1; // 1 for true.
    switch (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command) {
        case MAV_CMD_NAV_WAYPOINT:
        case MAV_CMD_NAV_TAKEOFF: // Technically, Takeoffs have headings already from broadcastMissionItem reached
            if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].param2>0.0) {
                horzAcceptDist = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param2;
            }
			if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command == MAV_CMD_NAV_WAYPOINT) {
				liftOffNotificationArmed = 1; // Re-Arm for liftoff notification
			}
			else {
				// For the takeoff we want to check against the heading
				if ((liftOffNotificationArmed == 1) && (shared_memory->hg1700.mixedhgt < 5.0)) { // If we are low and have a takeoff, annouce lifting off
					sendSimpleTextToGcs("Lifting off", MAV_SEVERITY_ALERT);
					liftOffNotificationArmed = 0;
				}
			}
			// If it's a take off point we also want to ensure that the heading points close to the dir to the next Wp
			

            if ((horzDist < horzAcceptDist)) {
                // We met the acceptance threshold, move on to next waypoint...assuming we're not done
                if (specificMission->numMissionItems >= (mission_data->myCurrentMission.currentMissionItem + 1) ) {
                    if (mission_data->myCurrentMission.currentMissionItem == specificMission->numMissionItems - 1) {
                        // We are on the last waypoint
                        if (shared_memory->hg1700.mixedhgt < -5.0) { // This needs to be changed back to +5 ft
                            // Consider being below 5 feet as mission item reached
                            // Here we probably want to check for weight on skids, and some timer to have elapsed.
                            printf("Reached Mission Item #%d\n",mission_data->myCurrentMission.currentMissionItem);
                            printf("Vertical Distance is %1.1fm, InsAlt is %1.1f, Waypoint Alt is %1.1f\n",vertDist,ALT_INS*FEET_TO_M,specificMission->items[mission_data->myCurrentMission.currentMissionItem].z);
                            broadcastMissionItemReached();
							clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
                        }
                    } else {
                        // There are still more mission items to go through
						// But prevent Takeoff from being achieved unless we are within X degrees of the desired track to the next wp
						if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command==MAV_CMD_NAV_TAKEOFF) {


							float dirToNextWp = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4;
							
							float headingError;
							headingError = dirToNextWp - shared_memory->hg1700.hdg;
							if (headingError > 180) headingError -= 360;
							if (headingError < -180) headingError += 360;
							// Now check that we have achieved the hover height and heading
							if ((fabs(headingError) < 10.0) && (fabs(shared_memory->hg1700.alt - specificMission->items[mission_data->myCurrentMission.currentMissionItem].z*METERS_TO_FT)<DEFAULT_HOVER_HEIGHT_ACCEPT_FT)) { // lets say we a re good to go if heading is within 10, or we can't find a valid next WP
								#ifdef PILOT_CONSENT_FOR_TAKEOFF_CLEARANCE 

								if (shared_memory->ffs_output.ffs_switches & BIT(12)) {
									printf("Reached Mission Item #%d with Pilot Consent cleared to takeoff\n", mission_data->myCurrentMission.currentMissionItem);
									printf("Vertical Distance is %1.1fm, InsAlt is %1.1f, Waypoint Alt is %1.1f\n", vertDist, ALT_INS*FEET_TO_M, specificMission->items[mission_data->myCurrentMission.currentMissionItem].z);
									broadcastMissionItemReached();
									clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
								} else if (clrTakeoffNotificationArmed) {
										sendSimpleTextToGcs("Holding for clearance", MAV_SEVERITY_ALERT);
										clrTakeoffNotificationArmed = 0;
								}
								
								#else // Don't need pilot switch set to proceed to rest of mission after T/O item achieved
								printf("Reached Mission Item #%d\n", mission_data->myCurrentMission.currentMissionItem);
								printf("Vertical Distance is %1.1fm, InsAlt is %1.1f, Waypoint Alt is %1.1f\n", vertDist, ALT_INS*FEET_TO_M, mission_data->myCurrentMission.standard.items[mission_data->myCurrentMission.currentMissionItem].z);
								broadcastMissionItemReached();
								#endif
								
							}
						}
						else {
							printf("Reached Mission Item #%d\n", mission_data->myCurrentMission.currentMissionItem);
							printf("Vertical Distance is %1.1fm, InsAlt is %1.1f, Waypoint Alt is %1.1f\n", vertDist, ALT_INS*FEET_TO_M, specificMission->items[mission_data->myCurrentMission.currentMissionItem].z);
							broadcastMissionItemReached();
							clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
						}
                        
                    }
                }
            }
			lastWpAchieved = 0; // Ensure that the last wp flag is cleared on regular waypoints
			needToSetHoverCriteria = 1; // Anytime we aren't on the land command we need to flag hover criteria to be set on first entry
			if (SIMULATE_PEREGRINE_TEST_CASE > 0) {
				// This next line only needed for simulated peregrine LP's
				resetSimulatedLpCounter();	//Sets LP counter index to 0
                simulatePeregrineFindingLp(SIM_PEREGRINE_TIMER_RESET);
			}
            break;
        case MAV_CMD_NAV_LAND:
			liftOffNotificationArmed = 1;
			// We will be using param 3 for acceptance radius of the LZ
			if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].param3 > 0.0) {
				horzAcceptDist = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param3;
			}
			if (needToSetHoverCriteria) {
				SetHoverCriteria(specificMission->items[mission_data->myCurrentMission.currentMissionItem].x / (1E7),
                                 specificMission->items[mission_data->myCurrentMission.currentMissionItem].y / (1E7),
                                 specificMission->items[mission_data->myCurrentMission.currentMissionItem].z,
                                 NAN,                           // Heading goes here...but since we aren't checking it, it doesn't matter
                                 horzAcceptDist*METERS_TO_FT,   // Use PLP accept radius, but in feet
                                 NAN,                           //don't check altitude
                                 5.0,			                // Under 5 knots...this is the speed we will scan the LZ at
                                 NAN,                           // Don't check heading
                                 5.0);			                // 5 seconds
                                 needToSetHoverCriteria = 0;    // It's been set
			}
			if (lastWpAchieved == 0) {
				if (CheckHoverCriteria() == 1) {
					lastWpAchieved = 1;
					signalLastWaypointAchieved(specificMission->items[mission_data->myCurrentMission.currentMissionItem]); // Let supervisor know we hit the last waypoint criteria
					clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
				}
			}
		
            if (horzDist < 1.2*horzAcceptDist) { // consider starting the timer even when we are 20% away from the accept criteria
				if ((SIMULATE_PEREGRINE_TEST_CASE == 3) || (SIMULATE_PEREGRINE_TEST_CASE == 4)) {
					// Cases 3 & 4 
					simulatePeregrineFindingLp(SIM_PEREGRINE_TIMER_RUN);	//This handles case of LP found before PLP achieved...here we get inside the accept radius, but don't need to hover
					// Note that this will only generate the 1st LP, as mission manager function is suspended after an LP is found. Handling the multiple LP condition must be done in supervisor.c
				}
                //Make sure we are low enough to consider landing accomplished....should use WOG switch
				// NOTE THE CODE BELOW SHOULD BE DELETED AFTER REVIEW AND VERIFICATION OF SUPERVISOR FUNCTION
                if (shared_memory->hg1700.mixedhgt < -5.0) { // This needs to be changed back to +5 ft
                    // Consider being below 5 feet as mission item reached
                    printf("Reached Mission Item #%d\n",mission_data->myCurrentMission.currentMissionItem);
                    printf("Vertical Distance is %1.1fm, InsAlt is %1.1f, Waypoint Alt is %1.1f\n", vertDist, ALT_INS*FEET_TO_M, specificMission->items[mission_data->myCurrentMission.currentMissionItem].z);
                    broadcastMissionItemReached();
					clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
                }
                
            }
            break;
        case MAV_CMD_DO_CHANGE_SPEED:
            //Just say we reached a speed change
            printf("Reached a speed change waypoint...considering as reached\n");
            broadcastMissionItemReached();
			clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
			liftOffNotificationArmed = 1;
			break; // KE added missing break statement Mar 10/22
            
        default: // Here we mostly just assume that the waypoints have beend reached. Might need special cases for describing in BOSS
            if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].x == 0.0) {
                // Waypoints with no position should be skipped
                // But we will prob need a way to flag speed changes somehow
                if (specificMission->numMissionItems >= (mission_data->myCurrentMission.currentMissionItem + 1) ) {
                    printf("Reached Mission Item #%d\n",mission_data->myCurrentMission.currentMissionItem);
                    broadcastMissionItemReached(); // Onto the next one
					clrTakeoffNotificationArmed = 1; //Arm for any further T/O points
					liftOffNotificationArmed = 1;
                }
            }
            break;
    }
    
#endif //Timed sequence test
    //Broadcast our current mission item
    broadcastCurrentMissionItem(mission_data->myCurrentMission.currentMissionItem);
	writeMissionMgrDataToDisk(horzDist, vertDist);
    
}

void writeMissionMgrDataToDisk(float horzDist, float vertDist) {
	extern buffer_t missionMgrData_logbuf;
	
	missionMgrLogData_t missionMgrLogData;

	missionMgrLogData.horzDist = horzDist;
	missionMgrLogData.vertDist = vertDist;
	missionMgrLogData.itemNo = mission_data->myCurrentMission.currentMissionItem + 1; // Add 1 so it lines up with QGC indication 1 index instead of 0 index
	float gndSpeedMps = sqrt(pow(shared_memory->hg1700.ve, 2.0) + pow(shared_memory->hg1700.vn, 2.0))*FEET_TO_M;
	missionMgrLogData.gndSpeed = gndSpeedMps;

	Write_To_Buffer(&missionMgrLogData, sizeof(missionMgrLogData_t), &missionMgrData_logbuf);
}


void resetSimulatedLpCounter(void) {
	simulatedLpCounter = 0;
}

void simulatePeregrineFindingLp(int resetRunFlag) {
    // resetRunFlag arg controls whether the timer for generating Lps should be reset (SIM_PEREGRINE_TIMER_RESET)
    // or if we should run (SIM_PEREGRINE_TIMER_RUN)
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, MAV_MISSION_TYPE_MISSION);
    
	// Feb 26 2021 -> Additions to make LP simulation deterministic, not random
	float latOffsetArray[4] = { 5.0, -3.0, 10.0, 6.0 };
	float lonOffsetArray[4] = { -10.0, 3.0, 5.0, 0.0 };

	static int numFakeLpsFound = 0;

    //Note: since we are comparing the elpsed loopConter against 30 seconds,the time before getting an LP will be 30-25= 5 seconds in the example below
	static int loopCounter = 25 * MISSION_PROGRESS_RATE; // This sets us up for being inside the cyan LZ for 5 sec prior to getting a new LP

    if (resetRunFlag == SIM_PEREGRINE_TIMER_RESET) {
        loopCounter = 25 * MISSION_PROGRESS_RATE; // This sets us up for being inside the cyan LZ for 5 sec prior to getting a new LP
		numFakeLpsFound = 0;
    } else {
        
        loopCounter++;
        if ((loopCounter > MISSION_PROGRESS_RATE * pow(2,numFakeLpsFound))) { //&& (LZNo<3)) { // Here we have 30 for a new LP every 30 seconds
#define DEG_LON_IN_M 78158.03  // Meters per degree longitude around 45.5 deg lat
#define DEG_LAT_IN_M 111141.52 // Meters per degree latitude around 45.5 deg lat
			numFakeLpsFound++; // Increment
            //Code below is for random LPs's
            double latOffset = (((double)rand() / (double)RAND_MAX) * 2 - 1)*10.0 / DEG_LAT_IN_M;
            double lonOffset = (((double)rand() / (double)RAND_MAX) * 2 - 1)*10.0 / DEG_LON_IN_M;
            
            //Code below is for deterministic LP's
            //simulatedLpCounter++;
            //if (simulatedLpCounter > 3) simulatedLpCounter = 0; // Wrap around
            //double latOffset = latOffsetArray[simulatedLpCounter] / DEG_LAT_IN_M;
            //double lonOffset = lonOffsetArray[simulatedLpCounter] / DEG_LON_IN_M;
            
            
            // Now we need to send updated LZ info to BOSS
            // For now we will just overwrite the global in the LP fields
            // Note: it would be a good idea to test the updateLpFunction
            
            //Grab the current mission item lat/lon as a basis
			extern bossMissionData_t bossMissionDataGlobal; //Declared in bossComms.c
            double plpLat = specificMission->items[mission_data->myCurrentMission.currentMissionItem].x / (1.0e7);
            double plpLon = specificMission->items[mission_data->myCurrentMission.currentMissionItem].y / (1.0e7);
			float heading = isnan(specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4) ? bossMissionDataGlobal.yaw : specificMission->items[mission_data->myCurrentMission.currentMissionItem].param4;
			
			signalLpFound(plpLat + latOffset, plpLon + lonOffset, heading);

            // Code below commented out as it is the supervisor's function to send the LP data to BOSS
            //extern bossMissionData_t bossMissionDataGlobal; // Declared in bossComms.c
            //updateBossLpData(bossMissionDataGlobal.lat + latOffset, bossMissionDataGlobal.lon + lonOffset);
            //printf("Simulated Peregrine sent New LP to BOSS...\n"); //
            loopCounter = 0; // Reset the loop counter
            
        }
    }

}

/*
    verifyRemotesConnection
    Description : This function is usually called periodically to verify that there's a response
                  (heartbeat) from each remote connected. If the number of expected heartbeats exceeds
                  a certain limit, MavNRC deletes the unresponsive remote from its list of remotes.
 
                  This allows to stop sending information to a remote instance that's not listening.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - remotesInfo : Struct holding the list of remotes and the number of remotes
*/
void verifyRemotesConnection(remotes_infos_t* remotesInfo) {
    int i;

    for (i = 0; i < remotesInfo->numRemotes; ++i) {
        if (remotesInfo->remotes[i].misses > 8) {
            // After 8 misses, disconnect it
            LOG("DELETING REMOTE\n");

            // If we are at the end, just set it to zero and decrement the num
            if (i == remotesInfo->numRemotes-1) {
                memset(&remotesInfo->remotes[i], 0, sizeof(remotesInfo->remotes[i]));
            }
            else {
                // Otherwise, shift all thoses on the right to the left by 1
                int j;
                for (j = i; j < remotesInfo->numRemotes-1; ++j) {
                    remotesInfo->remotes[j] = remotesInfo->remotes[j+1];
                }
            }
            --remotesInfo->numRemotes;
        }
    }
}

void broadcastLandingPoint(double lat, double lon, float heading) {
	const float q[4] = { 1.0, 0, 0, 0 };
	mavlink_message_t msg;

	mavlink_msg_landing_target_pack(MY_MAV_SYS_ID,
		MY_MAV_COMP_ID,
		&msg,
		microsSinceEpoch(),
		1,
		MAV_FRAME_GLOBAL,
		0,	// Mav1 unused
		0,	// Mav1 unused
		0,	// Mav1 unused
		0,	// Mav1 unused
		0,	// Mav1 unused
		lat,
		lon,
		heading,
		q,
		LANDING_TARGET_TYPE_VISION_OTHER,
		1);

	if (sendMavlinkMessage(qGcsSockFd, &msg, gcsAddr, sizeof(*gcsAddr)))
		LOG("LANDING POINT SENT TO QGC");

	if (sendMavlinkMessage(remotesInfo.sockfdRemotes, &msg, broadcastAddr, sizeof(*broadcastAddr)))
		LOG("LANDING POINT SENT TO REMOTES");
	// Also send to all remotes
}
