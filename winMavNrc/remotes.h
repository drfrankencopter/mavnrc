#pragma once
/*
	Replace me by your file title
	
	File name	: remotes.h
	Date		: 3/10/2022 2:45:41 PM
	Author		: Levesqueje
	Description : 

	TODOs : Not noted.
*/

// Standard librairies includes
#ifdef _WIN32
#include <winsock2.h>
#else
#include <netinet/in.h>
#endif

// External installed librairies <>


// Project documents includes ""
#include "globals.h"


#define MAX_REMOTES 25
/////////////////////
// Main content below

typedef enum {
    //
    // Levels of permission accorded to a specific remote
    // There are 3 levels of permissions :
    // 1. Full privileges
    //      - Can push a mission to the MavNRC
    //      - Can view mission data
    //      - Can send commands to the MavNRC
    // 2. Semi
    //      - Can send commands to the MavNRC
    //      - Can view mission data
    // 3. Read-only
    //      - Can only read and ask for mission data
    //
    eREMOTE_PERMISSION_ADMIN,
    eREMOTE_PERMISSION_SEMI,
    eREMOTE_PERMISSION_READONLY,
    eREMOTE_PERMISSION_UNKNOWN,
    eREMOTE_PERMISSION_NO_PERMISSION
} eREMOTE_PERMISSION_LEVEL;

typedef enum {
    //
    // Types for expected ACK
    //
    ACK_UNEXPECTED,
    ACK_EXPECTED,
    ACK_MISSION,
    ACK_FENCE,
    ACK_RALLY
} eACK;

typedef struct {
    struct sockaddr_in addr;
    int addrLen;
    int sysid;
    int compid;
    eACK expectedAck;
    int misses;
    eREMOTE_PERMISSION_LEVEL permissions;
} remoteInfo_t;

typedef struct {
    int sockfdRemotes;
    struct sockaddr_in broadcastAddr;   // Set in the updateVehicleMavLinkStatusThread
    struct sockaddr_in gcsAddr;         // Set in the mavlinkrcv.c on first message from gcs

    int numRemotes;
    remoteInfo_t remotes[MAX_REMOTES];
} remotes_infos_t;

remoteInfo_t* getRemote(missionData_t* mission_data, const struct sockaddr_in* to);
const remoteInfo_t* getRemoteWithSysId(const remotes_infos_t* remotes, int sysid);
eACK getExpectedMissionAck(MAV_MISSION_TYPE mission_type);
eREMOTE_PERMISSION_LEVEL getPermissionsForId(int sysid);
void incrementMissesForAllRemotes(remotes_infos_t* remotesInfo);
