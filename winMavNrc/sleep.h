#pragma once
// This header lets windows use unix style usleep
#if defined(_WIN32) || defined(_WIN64) 
#include <windows.h>
#include <synchapi.h>
// The next line uses the windows Sleep function to substitute for Linux's usleep
#define usleep(sec) Sleep(sec/1000.0)
#else
#include <unistd.h>
#endif