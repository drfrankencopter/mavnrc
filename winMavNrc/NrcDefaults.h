//
//  NrcDefaults.h
//  mavNrc
//
//  Created by Kris Ellis on 2020-11-20.
//  Copyright © 2020 Kris Ellis. All rights reserved.
//

#ifndef NrcDefaults_h
#define NrcDefaults_h

#define MAV_NRC_VERSION "2.1.0"   // NOTE: This is a string so we can use X.X.X notation ==== Keep this flag up to date please!!!!

#define PILOT_CONSENT_FOR_TAKEOFF_CLEARANCE   // Define this to require EPCOLSW3 to be up to have acheived the T/O Wp

// Important configuratation flags
//#define FLIGHT_TEST    //DEFINE THIS for flying on the aircraft
#define DONT_BUFFER_MAVLINK_LOCALHOST_MESSAGES // Use this to send directly instead of buffering messages
#define DONT_BUFFER_MAVLINK_PEREGRINE_MESSAGES
#define USE_SHMEM_FOR_FCC_GUIDANCE   // Define to use shmem, and not listen for packets (typical operation)
#define IGNORE_PEREGRINE_196_ROS_MODULE // Ignore peregrine saying code 6 emerg on ros module

#ifdef FLIGHT_TEST
#define MAV_DESTINATION_IP "127.0.0.1"  // Destination IP addy here in Ipv4
#define BOSS_DESTINATION_IP "132.246.193.255" // Broadcast on aircraft network
#define BROADCAST_ADDRESS "132.246.193.255"
#define PEREGRINE_INSTALLED
#else
#define MAV_DESTINATION_IP "127.0.0.1"  // Destination IP addy here in Ipv4
#define BOSS_DESTINATION_IP "127.0.0.1" //"132.246.193.255" //"127.0.0.1" //"132.246.193.222"
#define BROADCAST_ADDRESS       "127.0.0.1" //"132.246.193.255"
#endif



// Peregrine simulation flags
#define SIMULATE_PEREGRINE_TEST_CASE 2  // 1 = finding one LP after reaching PLP

// 0 = Do not simulate peregrine output (can be used for all 'No LP found' cases, or for flight test)
// 1 = finding one LP after reaching PLP (i.e. during scan)
// 2 = finding multiple LP after reaching PLP (i.e. during scan)
// 3 = finding one LP prior to reaching PLP
// 4 = finding multiple LP prior to reaching PLP

// Defaults is params are not defined in mission parameterss
#define DEFAULT_SPEED  5.0   //ms/ default speed if not defined in the mission
#define DEFAULT_ACCEPTANCE_RADIUS_HORZ 30.0  // Defined in meters --- Changed to 10m for Peregrine ground testing
#define DEFAULT_ACCEPTANCE_RADIUS_VERT 10.0  // Meters
#define DEFAULT_HOVER_HEIGHT_ACCEPT_FT 5.0 //Changed to feet July 14/21 to make things easier to set/use
#define DEFAULT_LAND_ZONE_RADIUS 30.0 // meters...defined in peregrine software


#define REGISTRATION    "PGV"  // for adding into filename

// Mavlink ID's
#define MY_MAV_SYS_ID 1 //Nominally the UAV
#define MY_MAV_COMP_ID 1 // Component ID for this code (is 1)
#define PEREGRINE_SYS_ID 1
#define PEREGRINE_COMP_ID 240 //196 // Ke unsure, but this seems most promising
#define GCS_ID 255 // Strandard ID for the GCS


// Network stuff
#define MAVLINK_SEND_PORT 14550 //14580 doesn't seem to work
#define MAVLINK_RECEIVE_UAV_PORT 14540 //It might be 14580...not sure who is receiver and sender...this is a high bandwidth connectino
#define MAVLINK_RECEIVE_UAV_LOW_BW_PORT 14030 //Might be 14280

#define MAVLINK_RECEIVE_GCS_PORT 64807 // Is 14428 //according to https://mavlink.io/en/mavgen_c/example_c_udp.html ...was 18570. but not sure


#define BOSS_MISSION_SEND_PORT 13333 // Randomly chosen for BOSS

// For printouts to console
// #define DEBUG_VERBOSE  // Uncomment this for verbose debug printing
#ifdef DEBUG_VERBOSE
# define DEBUG_PRINT(x) printf x
#else // Make DEBUG_PRINT a do nothing
# define DEBUG_PRINT(x) do {} while (0)
#endif

#define MAX_TXT_MSG_LENGTH 128  // Characters...this can be as high as 255 and still be contained in a single mavlink packet. IF we need hiogher than 255 then we will need to write code to break messages into sequences.

//Some lefotver stuff needed in other files
#define HG1700_LOG_RATE 20.0    //stated log rate in Hz...for logging position information. Should have a decimal to force floating point conversion in any math ops

#define PEREGRINE_REBOOT_DELAY 4.0 // Number of seconds to wait in between reboot requests

#define DUMMY_MISSION_ITEM_NO (-1) //Used in Direct to cases (and maybe elsewhere)

// Defines for the Multi-GCS
#define QGC_BROADCAST_ADDR "132.246.193.255"	// This is the address on which the information for other GCS will be sent to
#define QGC_BROADCAST_PORT 12346			// This is the port of the broadcast address for the Multi-GCS
#define LOCALHOST_ADDR "127.0.0.1"		// Localhost addr (do know if it's the local GCS)
#endif /* NrcDefaults_h */
