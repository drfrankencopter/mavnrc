#pragma once // Prevent redundant declaration of types
#pragma pack(push,4)
typedef struct { // provides data only read by the HMU to other systems on the network
	double hmu_system_time;
	float fbw_actuator_position_lateral;
	float fbw_actuator_position_long;
	float fbw_actuator_position_pedal;
	float fbw_actuator_position_collective;
	float main_rotor_torque;
	float main_rotor_rpm;
	float radar_altimeter;
} hmu_output_t; // called the same in Bell 205 ICD


typedef struct { // Allows safety calculations on HMU to be recorded on FCC
	double hmu_system_time;
	uint32_t safety_status;
	float project_hydraulic_pressure;
	float project_hydraulic_temperature;
	float flap_angle;
	float lateral_command_rate;
	float longitudinal_command_rate;
	float pedal_command_rate;
	float collective_command_rate;
	float lateral_override;
	float longitudinal_override;
	float pedal_override;
	float collective_override;
	float cva_phi_pred;	// Predicted roll angle
	float cva_theta_pred;	// Predicted pitch angle
	float cva_alt_pred;		// predicted altitude
	float cva_tas;			// true airspeed used in cva calc
} hmu_safety_t; // called the same in Bell 205 ICD
#pragma pack(pop)
