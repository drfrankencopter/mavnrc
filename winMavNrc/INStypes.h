#pragma once
#ifdef DBL_TIME
	typedef double gps_time_t;
#else
	typedef float gps_time_t;
#endif

#pragma pack(push,4) // pack ethernet data to fit on 4 byte boundaries...
typedef struct {
	#ifdef DBL_TIME
		#ifdef SPLIT_DBL
			float time;
			float time_dec;
		#else
			double time;
		#endif
	#else
		float time;
	#endif
	unsigned int status;
	float pitch;
	float roll;
	float hdg;
	float q;
	float p;
	float r;
	float ax;
	float ay;
	float az;
	float vn;
	float ve;
	float vz;
	#ifdef SPLIT_DBL
		float lat_deg;
		float lat_dec;
		float lng_deg;
		float lng_dec;
	#else
		double lat;
		double lng;
	#endif
	float alt;
	#ifdef LASER_RAW
		float laser_raw;
	#endif
	#ifdef LASER_ALT
		float laser_alt;
		float mixedhgt;
	#endif
	#ifdef HDG_SIGMA
		float hdg_sigma;
	#endif
	#ifdef DVI_TIME
		float dvi_time;
	#endif
	// alpha and beta added Aprl 30 2009 KE 
	#ifdef ALPHA_BETA
		float alpha;
		float beta;
	#endif
} hg1700_t;   /* Called INS output in the Bell 205 ICD */


typedef struct { // usually called by the HMU to provide magnetic heading for fast align of the INS
	float heading;
	int mode;
} hdg_t;  // Called INS Align in the Bell 205 ICD

// DO NOT USE THIS STRUCT
typedef struct {
	double time;
	double lat;
	double lng;
	float alt;
	float vel[3];
	float sigma[3];
	unsigned long num_obs;
	unsigned long pos_type;
	float diff_age;
} gps_data_t;
#pragma pack(pop)  // end packing

