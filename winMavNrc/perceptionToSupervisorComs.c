#include <stdio.h>
#include "CVLADtypes.h"
#include "perceptionToSupervisorComs.h"
#include "supervisor.h" // Note that this is included so we can call supervisor functions directly. Eventually we need to remove this dependance


// Here is where we will implement our functions
void signalLpFound(double lat, double lon, float hdg) {
	//Signal to the supervisor that the perception system has found a new LP
	handleLpFound(lat, lon, hdg);
}

void signalPerceptionSystemStatusChange(int status) {
	//status 4 == ACTIVE, and ready, everything else should be considered as a failure of the perception system
	handlePerceptionSystemStatusChange(status);
}
