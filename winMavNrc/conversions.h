#pragma once
#include <math.h>
#define FEET_TO_M 0.3048	// Ft to meters/Users/ellisk/Code-Development/mavnrc/winMavNrc/perceptionToSupervisorComs.c
#define FEET_TO_MM 304.8	// Feet to milimeters
#define FEET_TO_CM 30.48	// Feet/s to cm/s
#define METERS_TO_FT 3.281  // Meters to feet
#define DEG2RADS  (M_PI/180.0)
#define RADS2DEG  (180.0/M_PI)
#define KTS_TO_MPS 0.514444


#define BIT_0       (1 << 0)    // 0000 0000 0000 0000 0000 0000 0000 0001
#define BIT_1       (1 << 1)    // 0000 0000 0000 0000 0000 0000 0000 0010
#define BIT_2       (1 << 2)    // 0000 0000 0000 0000 0000 0000 0000 0100
#define BIT_3       (1 << 3)    // 0000 0000 0000 0000 0000 0000 0000 1000
#define BIT_4       (1 << 4)    // 0000 0000 0000 0000 0000 0000 0001 0000
#define BIT_5       (1 << 5)    // 0000 0000 0000 0000 0000 0000 0010 0000
#define BIT_6       (1 << 6)    // 0000 0000 0000 0000 0000 0000 0100 0000
#define BIT_7       (1 << 7)    // 0000 0000 0000 0000 0000 0000 1000 0000
#define BIT_8       (1 << 8)    // 0000 0000 0000 0000 0000 0001 0000 0000
#define BIT_9       (1 << 9)    // 0000 0000 0000 0000 0000 0010 0000 0000
#define BIT_10      (1 << 10)   // 0000 0000 0000 0000 0000 0100 0000 0000
#define BIT_11      (1 << 11)   // 0000 0000 0000 0000 0000 1000 0000 0000
#define BIT_12      (1 << 12)   // 0000 0000 0000 0000 0001 0000 0000 0000
#define BIT_13      (1 << 13)   // 0000 0000 0000 0000 0010 0000 0000 0000
#define BIT_14      (1 << 14)   // 0000 0000 0000 0000 0100 0000 0000 0000
#define BIT_15      (1 << 15)   // 0000 0000 0000 0000 1000 0000 0000 0000
#define BIT_16      (1 << 16)   // 0000 0000 0000 0001 0000 0000 0000 0000
#define BIT_17      (1 << 17)   // 0000 0000 0000 0010 0000 0000 0000 0000
#define BIT_18      (1 << 18)   // 0000 0000 0000 0100 0000 0000 0000 0000
#define BIT_19      (1 << 19)   // 0000 0000 0000 1000 0000 0000 0000 0000
#define BIT_20      (1 << 20)   // 0000 0000 0001 0000 0000 0000 0000 0000
#define BIT_21      (1 << 21)   // 0000 0000 0010 0000 0000 0000 0000 0000
#define BIT_22      (1 << 22)   // 0000 0000 0100 0000 0000 0000 0000 0000
#define BIT_23      (1 << 23)   // 0000 0000 1000 0000 0000 0000 0000 0000
#define BIT_24      (1 << 24)   // 0000 0001 0000 0000 0000 0000 0000 0000
#define BIT_25      (1 << 25)   // 0000 0010 0000 0000 0000 0000 0000 0000
#define BIT_26      (1 << 26)   // 0000 0100 0000 0000 0000 0000 0000 0000
#define BIT_27      (1 << 27)   // 0000 1000 0000 0000 0000 0000 0000 0000
#define BIT_28      (1 << 28)   // 0001 0000 0000 0000 0000 0000 0000 0000
#define BIT_29      (1 << 29)   // 0010 0000 0000 0000 0000 0000 0000 0000
#define BIT_30      (1 << 30)   // 0100 0000 0000 0000 0000 0000 0000 0000
#define BIT_31      (1 << 31)   // 1000 0000 0000 0000 0000 0000 0000 0000


#define BIT(n)                  ( 1<<(n) )
#define BIT_MASK(len)           ( BIT(len)-1 ) // - See more at: http://www.coranac.com/documents/working-with-bits-and-bitfields/#sthash.xV40LJzV.dpuf

typedef struct
{	float w;
	float x;
	float y;
	float z;
} quaternion_t;

typedef struct
{
	float heading;
	float pitch;
	float roll;
} Euler_t;

void QuaternionToEuler(quaternion_t quaternion, Euler_t *phpr);
void EulerToQuaternion(Euler_t hpr, quaternion_t *pquaternion);
