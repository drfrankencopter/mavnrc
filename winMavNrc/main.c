// main.c : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <time.h>       // For creating date stamped directories
#include <sys/stat.h> 
#include "sleep.h"
#include "sharedMemType.h"
#include "updateVehicleMavLinkStatusThread.h" // For sending msgs to GCS
#include <standard/mavlink.h>
#include "mavParams.h"
#include "log.h"
#include "log_globals.h" // for the log information struct (Nov 30 2020)
#include "data_defs.h"
#include "NrcDefaults.h"
#include "CVLADtypes.h"
#include "supervisor.h"
#include "debug.h"
#include "systime.h"
#include "globals.h"
#include "remotes.h"

#ifdef _WIN32
//Some hacks here for windows shared memory access
#include <windows.h>
#include <conio.h>
#include <direct.h> // For _getcwd

HANDLE hMapFile, hMapFileLog, hMapFileFccGuidance, hMapFileMission;	// Global for saving the handle to the shared memory
TCHAR sharedMemName[] = TEXT("asraSharedMem");	// As also defined in the python code
TCHAR logfileSharedMemName[] = TEXT("logfileSharedMem");	// As also defined in the python code
TCHAR fccGuidanceSharedMemName[] = TEXT("fccGuidanceMem");
TCHAR missionDataSharedMemName[] = TEXT("missionDataMem");

#else
#include <fcntl.h>  // Needed for read/write definitions
#include <unistd.h> // needed for close()
#include <sys/mman.h>   // For POSIX shared memory via shm_open
const char *sharedMemName = "/asraSharedMem";
const char *logfileSharedMemName = "/logfileSharedMem";
const char *fccGuidanceSharedMemName = "/fccGuidanceMem";
const char *missionDataSharedMemName = "missionDataMem";
int hMapFile, hMapFileLog, hMapFileFccGuidance, hMapFileMission; // Regular file descriptor for Linux/Mac
// Now map windows Sleep to usleep
void Sleep(int miliseconds) {
    usleep(1000*miliseconds);
}
#endif // End MacOs specif stuff

#ifndef S_ISDIR
#define S_ISDIR(mode)  (((mode) & S_IFMT) == S_IFDIR)
#endif


// Fn Protos
void printSharedMemSizes(void);
int connectSharedMemory(void);
int connectMissionData(void);
int connectLogfileSharedMemory(void); //For shared datalogger
int connectFccGuidanceSharedMemory(void);
void XPLMDebugString(const char *inString);
void fakeInsData(void);
// Nov 30 2020Added for new style directory finding/naming
void createDirectoryAndFilenumber(void);
void addFilesBuffersAndDataDefs(void);
void LaunchThreads(void);
int unlinkFccGuidanceSharedMemory(void);
int unlinkSharedMemory(void);
int unlinkLogfileSharedMemory(void);

// Some globals for recording
// Note, Nov 30 2020 the individual variables were migrated to a struct


//LogFile Buffers
buffer_t hg1700_logbuf;
buffer_t landingTarget_logbuf;
buffer_t localPosition_logbuf;
buffer_t missionData_logbuf;
buffer_t missionGeoFence_logbuf;
buffer_t missionRallyPoints_logbuf;
buffer_t missionStatus_logbuf;
buffer_t supervisorLP_logbuf;
buffer_t mavNrcMessage_logbuf;
buffer_t nrcAutFccStatus_logbuf;
buffer_t bossMissionData_logbuf;
buffer_t missionMgrData_logbuf;


int endGlobal = 0;
int missionComplete=0;
shared_data_t *shared_memory;	// Global where we save all the data in machine native format
missionData_t *mission_data;    // Global where we save all the mission information
shared_logger_directory_t *shared_logger_directory;

NRCAUT_fccstatus_t *sharedFccGuidanceData; // We will share the FCC guidance data


#define MAX_FILE_SIZE 200   // Max size of file to record in MB
#define MAX_NUM_FILES 2		// Maximum simultaneous files recorded
#define BUF_SIZE sizeof(shared_data_t)	//A convenience define, big enough to hold our data
#define BUF_SIZE_LOG sizeof(shared_logger_directory_t)
#define BUF_SIZE_FCC_GUIDANCE sizeof(NRCAUT_fccstatus_t)

int	isConnected = 0; // Shared mem connection state
int    isLogMemConnected = 0; // Shared mem for file logging
static int  isFccGuidanceConnected = 0;
static int   foundSharedDirectoryName = 0;

extern remotes_infos_t remotesInfo;

//IMPORTANT: For this to work on windows you need the right pthread.dll to be installed in your system
//			 x64 -> pthreadVC3-w64.dll
//			 x86 -> pthreadVC3-w32.dll
// The best way to get these installed is to use an environment variable to point at the pthred-win directory
// see: https://www.java.com/en/download/help/path.xml for setting the path
// On my local machine here is the entry: C:\Users\ellisk\Documents\frlxplaneplugin\pthread-win\
// You will need to re-boot after adding it to your path.
// Alternatively, you can copy the required dll's to the same directory an your executable. Kinda lame, but it works


// The sample main application shows how to launch threads, and contains a couple sample threads that are launched and write to the console
// The threads themselves are in mySampleThread.c/h and they are called by threads.c
int main() {
    
	printf("***************************************************\n");
	printf("* MavLink Connection   (for desktop simulation)   \n");
	printf("* Developed by Kris Ellis                         \n");
	printf("* Compiled: %s, %s", __DATE__, __TIME__);
	printf("* Version: %s\n", MAV_NRC_VERSION);
	printf("***************************************************\n");
	printf("Connecting to shared memory\n");
	connectSharedMemory();
    connectMissionData();
	printSharedMemSizes();

#ifdef USE_SHMEM_FOR_FCC_GUIDANCE  
	connectFccGuidanceSharedMemory(); 
#endif
    SysTime(); // Prime system time....before starting main loop
	printf("***************************************************\n");
    //printf("FAKING INS DATA ===== REMOVE BEFORE FLIGHT\n");
    fakeInsData();
	Sleep(1000);
	printf("Reading Parameters from Disk\n");
	readParamsFromDisk();
	Sleep(1000);
    //Setting up Log File Data
    printf("Setting up Log File data\n");
	createDirectoryAndFilenumber();
	addFilesBuffersAndDataDefs();
 
    printf("Launching Threads!\n");
	LaunchThreads();
	Sleep(1000); // Sleep for a second to allow threads to launch

    
	sendSimpleTextToGcs("ASRA Mav Interface Launched", MAV_SEVERITY_INFO);

#ifdef TEST_ALL_MSG_SEVERITY_LEVELS
	// A little test of what gets displayed in QGC
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 7 - Debug", MAV_SEVERITY_DEBUG);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 6 - Info", MAV_SEVERITY_INFO);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 5 - Notice", MAV_SEVERITY_NOTICE);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 4 - Warning", MAV_SEVERITY_WARNING);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 3 - Error", MAV_SEVERITY_ERROR);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 2 - Critical", MAV_SEVERITY_CRITICAL);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 1 - ALERT", MAV_SEVERITY_ALERT);
	Sleep(1000); // Sleep for a second to allow threads to close
	sendSimpleTextToGcs("Severity 0 - EMERGENCY", MAV_SEVERITY_EMERGENCY);
#endif // test all mavlink msg severity levels
    
	while (1) { // Need to find a way to trigger exit condition (maybe from a shared mem flag)
		Sleep((int) ((1.0/HG1700_LOG_RATE)*1000.0)); // 20 Hz operation
        Write_To_Buffer(&shared_memory->hg1700, sizeof(hg1700_t), &hg1700_logbuf);
#ifndef FLIGHT_TEST
		//pauseSimAndDebug(); // Allows app to be debugged via FSW15...but not the flying code
        //IB commented this line on May 26
#endif
	}
	
	//endGlobal = 1;
	//Sleep(1000); // Sleep for a second to allow threads to close
	return 0;
}

#ifdef _WIN32
int connectSharedMemory()
{
	hMapFile = CreateFileMapping(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD)
		BUF_SIZE,                // maximum object size (low-order DWORD)
		sharedMemName);                 // name of mapping object

	if (hMapFile == NULL)
	{
		XPLMDebugString("NRC-412 Could not create file mapping object");
		return 0;
	}
	shared_memory = (shared_data_t *)MapViewOfFile(hMapFile,   // assign the map object to the shared data struct. Note: I get a truncation warning here. Is this a problem?
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,
		0,
		BUF_SIZE);

	if (shared_memory == NULL)
	{
		XPLMDebugString("NRC-412 Could not map view of file."),
			CloseHandle(hMapFile);

		return 0;
	}

	isConnected = 1;
	return 1;
}

int connectMissionData() {
    hMapFileMission = CreateFileMapping(
        INVALID_HANDLE_VALUE,
        NULL,
        PAGE_READWRITE,
        0,
        sizeof(missionData_t),
        missionDataSharedMemName);

    if (hMapFileMission == NULL) {
        XPLMDebugString("NRC-412 Could not create the shared memory file descriptor via shm_open()");
        return 0;
    }
    printf("Sizeof missionData_t : %ld\n", sizeof(missionData_t));
    
    mission_data = (missionData_t*)MapViewOfFile(hMapFileMission,
        FILE_MAP_ALL_ACCESS,
        0,
        0,
        sizeof(missionData_t));
    
    if (mission_data == NULL) {
        XPLMDebugString("NRC-412: Could not map the memory via mmap.");
        CloseHandle(hMapFileMission);
        return 0;
    }
    XPLMDebugString("NRC-412: Mapped Shared Memory");

    memset(mission_data, 0, sizeof(*mission_data)); // Always reset the mission data on launch
    memset(&remotesInfo, 0, sizeof(remotesInfo));
    return 1;
}

int unlinkSharedMemory() {
	XPLMDebugString("Unlinking NRC-412 shared memory");
	UnmapViewOfFile(shared_memory);
	CloseHandle(hMapFile);
	isConnected = 0;
	return 0;
}
#else  // MACOS or Linux
int connectSharedMemory() {
	hMapFile = shm_open(sharedMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set permissions to 666
	if (hMapFile < 0) {
		XPLMDebugString("NRC-412 Could not create the shared memory file descriptor via shm_open()");
		return 1;
	}
	ftruncate(hMapFile, BUF_SIZE);
	shared_memory = (shared_data_t *)mmap(NULL, BUF_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, hMapFile, 0);
	if (shared_memory == MAP_FAILED) {
		XPLMDebugString("NRC-412: Could not map the memory via mmap.");
		return 1;
	}
	XPLMDebugString("NRC-412: Mapped Shared Memory");
	isConnected = 1;

	return 0;
}

int connectMissionData() {
    hMapFileMission = shm_open(missionDataSharedMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set permissions to 666
    if (hMapFileMission < 0) {
        XPLMDebugString("NRC-412 Could not create the shared memory file descriptor via shm_open()");
        return 0;
    }
    printf("Sizeof missionData_t : %ld\n", sizeof(missionData_t));
    ftruncate(hMapFileMission, sizeof(missionData_t));
    mission_data = (missionData_t *)mmap(NULL, sizeof(missionData_t), PROT_READ | PROT_WRITE, MAP_SHARED, hMapFileMission, 0);
    if (mission_data == MAP_FAILED) {
        perror("Error.\n");
        XPLMDebugString("NRC-412: Could not map the memory via mmap.");
        return 0;
    }
    XPLMDebugString("NRC-412: Mapped Shared Memory");
    
    
    memset(mission_data, 0, sizeof(*mission_data)); // Always reset the mission data on launch
    memset(&remotesInfo, 0, sizeof(remotesInfo));
    return 1;
}

int unlinkSharedMemory() {
	//printf("Unlinking shared memory\n");
	munmap(shared_memory, BUF_SIZE);
	close(hMapFile);
	isConnected = 0;
	return 0;
}
#endif

#ifdef _WIN32
int connectLogfileSharedMemory()
//NOTE Here for Kris to add windows compat for this....
{
    hMapFileLog = CreateFileMapping(
                                    INVALID_HANDLE_VALUE,    // use paging file
                                    NULL,                    // default security
                                    PAGE_READWRITE,          // read/write access
                                    0,                       // maximum object size (high-order DWORD)
                                    BUF_SIZE_LOG,                // maximum object size (low-order DWORD)
                                    logfileSharedMemName);                 // name of mapping object
    
    if (hMapFileLog == NULL)
    {
        XPLMDebugString("Could not create log file memory mapping object");
        return 1;
    }
    shared_logger_directory = (shared_logger_directory_t *)MapViewOfFile(hMapFileLog,   // assign the map object to the shared data struct. Note: I get a truncation warning here. Is this a problem?
                                                                         FILE_MAP_ALL_ACCESS, // read/write permission
                                                                         0,
                                                                         0,
                                                                         BUF_SIZE_LOG);
    
    if (shared_logger_directory == NULL)
    {
        XPLMDebugString("Could not map the log file view of file."),
        CloseHandle(hMapFileLog);
        
        return 1;
    }
    
    isLogMemConnected = 1;
    return 0;
}

int unlinkLogfileSharedMemory() {
    XPLMDebugString("Unlinking Logfile shared memory");
    UnmapViewOfFile(shared_logger_directory);
    CloseHandle(hMapFileLog);
    isLogMemConnected = 0;
    return 0;
}
#else  // MACOS or Linux
int connectLogfileSharedMemory() {
    hMapFileLog = shm_open(logfileSharedMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set permissions to 666
    if (hMapFileLog < 0) {
        XPLMDebugString("Could not create the log file shared memory file descriptor via shm_open()");
        return 1;
    }
    ftruncate(hMapFileLog, BUF_SIZE_LOG);
    shared_logger_directory = (shared_logger_directory_t *)mmap(NULL, BUF_SIZE_LOG, PROT_READ | PROT_WRITE, MAP_SHARED, hMapFileLog, 0);
    if (shared_logger_directory == MAP_FAILED) {
        XPLMDebugString("Could not map the log file shared memory via mmap.");
        return 1;
    }
    XPLMDebugString("Successfully Mapped the log file Shared Memory");
    printf("Log file Shared memory size is %ld bytes\n",BUF_SIZE_LOG);
    //strcpy(shared_logger_directory->logfileDirectory, filepath_string_global); // put the log file directory into shared mem
    isLogMemConnected = 1;
    return 0;
}

int unlinkLogfileSharedMemory() {
    //printf("Unlinking shared memory\n");
    munmap(shared_logger_directory, BUF_SIZE_LOG);
    close(hMapFileLog);
    isLogMemConnected = 0;
    return 0;
}
#endif

void XPLMDebugString(const char *inString) {
	// You can wrap this function as you see fit (send to console, web server, file, etc)...one call to rule them all
	printf("dektopFcc: %s\n", inString);
}


void printSharedMemSizes(void) {
	int cumTot = 0;
	printf("Shared mem size = %ld bytes:\n", sizeof(shared_data_t));
	printf("1. HG1700 = %ld bytes, subtot: %d bytes\n", sizeof(hg1700_t), cumTot += sizeof(hg1700_t));
	printf("2. GPS = %ld bytes, subtot %d bytes\n", sizeof(ins_gps_performance_t), cumTot += sizeof(ins_gps_performance_t));
	printf("3. FFS Output = %ld bytes, subtot %d bytes\n", sizeof(ffs_output_t), cumTot += sizeof(ffs_output_t));
	printf("4. FFS Config = %ld bytes, subtot %d bytes\n", sizeof(ffs_configuration_t), cumTot += sizeof(ffs_configuration_t));
	printf("5. FCC Output = %ld bytes, subtot %d bytes\n", sizeof(fcc_output_t), cumTot += sizeof(fcc_output_t));
	printf("6. FCC Project = %ld bytes, subtot %d bytes\n", sizeof(fcc_project_t), cumTot += sizeof(fcc_project_t));
	printf("7. FCC Config = %ld bytes, subtot %d bytes\n", sizeof(fcc_configuration_t), cumTot += sizeof(fcc_configuration_t));
	printf("8. HMU Output = %ld bytes, subtot %d bytes\n", sizeof(hmu_output_t), cumTot += sizeof(hmu_output_t));
	printf("9. HMU Safety = %ld bytes, subtot %d bytes\n", sizeof(hmu_safety_t), cumTot += sizeof(hmu_safety_t));
	printf("10. CIF Output = %ld bytes, subtot %d bytes\n", sizeof(cif_output_t), cumTot += sizeof(cif_output_t));
	printf("11. Air Data = %ld bytes, subtot %d bytes\n", sizeof(airdata_t), cumTot += sizeof(airdata_t));
	printf("12. ARINC Data = %ld bytes, subtot %d bytes\n", sizeof(asra_arinc_data_t), cumTot += sizeof(asra_arinc_data_t));
	printf("13. VPot Data = %ld bytes, subtot %d bytes\n", sizeof(fcc_vpot_t), cumTot += sizeof(fcc_vpot_t));
	printf("14. Sim Config = %ld bytes, subtot %d bytes\n", sizeof(simulatorConfig_t), cumTot += sizeof(simulatorConfig_t));
    printf("15. Remotes information = %ld bytes, subtot %d bytes\n", sizeof(remotes_infos_t), cumTot += sizeof(remotes_infos_t));
}

void fakeInsData(void) {
    // Just a helper function to throw some data into shared memory
    // For now we will use Kris' house...
    shared_memory->hg1700.lat = 45.852287;
    shared_memory->hg1700.lng = -72.390960;
    shared_memory->hg1700.alt = 80*3.048; // 80 m in feet
    
}

void createDirectoryAndFilenumber(void)
{
    char directory_path[1024];
    char fileNumPath[256]; // Temporarily holds the path to the location of the filenumber.txt file
    char newDataDir[128]; // Holds the string for making directory

    // First we try to connect to logger shared memory and use that...if it fails, then we save locally
    connectLogfileSharedMemory();
    //Now figure out if we are connected, and if there is data of value in the logger shared mem
    if (isLogMemConnected) {
        //Examine the first character of the logfile directory and see if it's null...
        if(shared_logger_directory->logfileDirectory[0] != 0) foundSharedDirectoryName = 1; // Look and see if 1st character is not null.
    }
    
    if (foundSharedDirectoryName) {
        // Here is where we configure our file writing directory
        strcpy(logControl_global.filepath_string,shared_logger_directory->logfileDirectory);
        printf("The current directory is: %s\n", logControl_global.filepath_string);
        strcat(logControl_global.filepath_string,"/MavNrcData"); // Put the Event data into the filep[ath string global directory
        // NOTE, I think the line below needs modification for Windows
        // Fixed incompatibility between platforms
        
    #ifndef _WIN32
        mkdir(logControl_global.filepath_string,0777); // Make directory with privileges for all!
    #else
        _mkdir(logControl_global.filepath_string,0777); // Make directory with privileges for all!
    #endif
        // Now we need to figure out the filenumber...Apr 27, 2021 KE decided to read filenum.txt from the directory where the mavNrc exe is located
        // Note: Need windows compat version here
        strcpy(fileNumPath, logControl_global.filepath_string); // We will make a private MavNrcFilenum for each restart of the program
        strcat(fileNumPath, "/mavNrcFilenum.txt"); // Look for filenum.txt in the FlightData directory
        Increment_Filenum(fileNumPath); // Reads the filenum, and adds 1 to it if found. Otherwise, starts at 1
        
    } else { // Log mem didn't connect
        
        printf("Couldn't find shared location for logfiles...\n Finding Working Directory\n");
#ifndef _WIN32
        if (getcwd(directory_path, sizeof(directory_path)) != NULL) {   // Note that getcwd may be the user's home folder, even if you put the application in a different directory
#else
            if (_getcwd(directory_path, sizeof(directory_path)) != NULL) {   // Note that getcwd may be the user's home folder, even if you put the application in a different directory
#endif // !_WIN32
                printf("The current directory is: %s\n", directory_path);
                strcpy(logControl_global.filepath_string, directory_path);
                // Now check for the FlightData directory
                // Start by making the dir name by copying the current dir and adding /FlighData
                strcpy(logControl_global.flightDataRootDir, logControl_global.filepath_string);
                strcat(logControl_global.flightDataRootDir, "/FlightData");
                struct stat sb; // Returns directory info
                if (stat(logControl_global.flightDataRootDir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
                    //Directory exists
                    printf("The FlightData directory was found at: %s\n", logControl_global.flightDataRootDir);
                }
                else { //Directory doesn't exist...let's make it
                    printf("The FlightData directory doesn't exist in this location...creating it\n");
#ifndef _WIN32
                    mkdir(logControl_global.flightDataRootDir, 0777); //Make FlightData directory with RWX priv for all
#else
                    _mkdir(logControl_global.flightDataRootDir);
#endif
                } // End of Checking/Making FlightData Directory
                strcpy(fileNumPath, logControl_global.flightDataRootDir); // Start by grabbing the flight data dir
                strcat(fileNumPath, "/filenum.txt"); // Look for filenum.txt in the FlightData directory
                Increment_Filenum(fileNumPath); // Reads the filenum, and adds 1 to it if found. Otherwise, starts at 1
                // Now build the date/time string for the file folder
                time_t  T = time(NULL);
                struct tm tm = *localtime(&T);
                sprintf(newDataDir, "FlightData/%d-%02d-%02d-%d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, logControl_global.filenum);
                printf("Creating sub directory: %s\n", newDataDir);
#ifndef _WIN32
                mkdir(newDataDir, 0777); //Make new data dir relative to CWD
#else
                _mkdir(newDataDir);
#endif
                strcat(logControl_global.filepath_string, "/"); // Need to add a /
                strcat(logControl_global.filepath_string, newDataDir); // Save the data directory location (will need to add it to a shared mem area)
                
            }
            else { // The current working directory was not found
                printf("ERROR: There was a problem getting the current directory path. You won't get any recorded data!\n");
            }
        }
    }

void addFilesBuffersAndDataDefs(void) {
    char outputFileName[64] = {0}, messageFilename[64] = {0}; // We will build these
    
	Set_Max_Storage_Size(MAX_FILE_SIZE);
    // Now add the file we are recording to
    if(foundSharedDirectoryName) {
        sprintf(outputFileName,"mavNrc-%d-",shared_logger_directory->filenum);
    } else {
        sprintf(outputFileName,"MavNrc-X-"); // Put an X if we don't know the main output file it belongs with
    }
    
    Add_File(&file[0],outputFileName,"frl",REGISTRATION,1,MAX_NUM_FILES);
	//Add date recording buffers here
	Add_Buffer(&file[0], &hg1700_logbuf);
	HG1700_Data_Def(&hg1700_logbuf);
	Add_Buffer(&file[0], &nrcAutFccStatus_logbuf); // For logging FCC status messages
	NrcAutFccStatusDataDef(&nrcAutFccStatus_logbuf);
	Add_Buffer(&file[0], &landingTarget_logbuf);
	LandingTarget_Data_Def(&landingTarget_logbuf); // Populate data def. Actual data will be pushed to buffer on receipt of LZ proto messages
	Add_Buffer(&file[0], &localPosition_logbuf);
	LocalPosition_Data_Def(&localPosition_logbuf);
	Add_Buffer(&file[0], &missionData_logbuf); // For recording mission data
	missionData_Data_Def(&missionData_logbuf);
	Add_Buffer(&file[0], &missionGeoFence_logbuf);
	missionGeofenceData_Data_Def(&missionGeoFence_logbuf); // For saving geofence information
	Add_Buffer(&file[0], &missionRallyPoints_logbuf);
	missionRallyPointData_Data_Def(&missionRallyPoints_logbuf); // For saving rally points in mission
	Add_Buffer(&file[0], &missionMgrData_logbuf); // For internal data to mission mgr
	missionManager_Data_Def(&missionMgrData_logbuf);
	Add_Buffer(&file[0], &missionStatus_logbuf); // For progress towards waypoints
	missionStatus_Data_Def(&missionStatus_logbuf);
	Add_Buffer(&file[0], &bossMissionData_logbuf); // Logs the first bit of data sent to boss (before the wp preview)
	bossMission_Data_Def(&bossMissionData_logbuf);
    Add_Buffer(&file[0], &supervisorLP_logbuf); // For progress towards waypoints
    SupervisorLandingPoint_Data_Def(&supervisorLP_logbuf);


	// Now let's add another log buff, but for string messages
    if(foundSharedDirectoryName) {
        sprintf(messageFilename,"mavNrcMsg-%d-",shared_logger_directory->filenum);
    } else {
        sprintf(messageFilename,"mavNrcMsg-X-"); // Put an X if we don't know the main output file it belongs with
    }
	Add_File(&file[1], messageFilename, "txt", REGISTRATION, 0, MAX_NUM_FILES); // Note the 0 here means that no FRL signature block is required....Note that we will write to this file in the send text to GCS fn
	Add_Buffer(&file[1], &mavNrcMessage_logbuf);

	//Signal to start recording
	logControl_global.start_recording = 1;
}


// KE Put the FCC Status Autonomy PAcket shared memory here May 6 2021
#ifdef WIN32
int connectFccGuidanceSharedMemory()
{
	hMapFile = CreateFileMapping(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD)
		BUF_SIZE_FCC_GUIDANCE,                // maximum object size (low-order DWORD)
		fccGuidanceSharedMemName);                 // name of mapping object

	if (hMapFile == NULL)
	{
		printf("FCC Guidance Shared Mem Could not create file mapping object\n");
		return 1;
	}
	sharedFccGuidanceData = (NRCAUT_fccstatus_t *)MapViewOfFile(hMapFile,   // assign the map object to the shared data struct. Note: I get a truncation warning here. Is this a problem?
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,
		0,
		BUF_SIZE_FCC_GUIDANCE);

	if (sharedFccGuidanceData == NULL)
	{
		printf("FCC Guidance Shared Mem Could not map view of file.\n"),
			CloseHandle(hMapFile);

		return 1;
	}

	isFccGuidanceConnected = 1;
	printf("Successfully connected to FCC Guidance shared memory named: /fccGuidanceMem \n");
	return 0;
}

int unlinkFccGuidanceSharedMemory() {
	printf("Unlinking  FCC Guidance shared memory\n");
	UnmapViewOfFile(sharedFccGuidanceData);
	CloseHandle(hMapFile);
	isFccGuidanceConnected = 0;
	return 0;
}
#else  // MACOS or Linux
int connectFccGuidanceSharedMemory() {
	hMapFile = shm_open(fccGuidanceSharedMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set permissions to 666
	if (hMapFile < 0) {
		XPLMDebugString("Fcc Guidance shared mem Could not create the shared memory file descriptor via shm_open()");
		return 1;
	}
	ftruncate(hMapFile, BUF_SIZE_FCC_GUIDANCE);
	sharedFccGuidanceData = (NRCAUT_fccstatus_t*)mmap(NULL, BUF_SIZE_FCC_GUIDANCE, PROT_READ | PROT_WRITE, MAP_SHARED, hMapFile, 0);
	if (sharedFccGuidanceData == MAP_FAILED) {
		XPLMDebugString("FCC Guidance Shared Mem Could not map view of file via mmap.");
		return 1;
	}
	XPLMDebugString("Mapped FCC Guidance Shared Memory");
	isFccGuidanceConnected = 1;
	return 0;
}

int unlinkFccGuidanceSharedMemory() {
	//printf("Unlinking shared memory\n");
	munmap(sharedFccGuidanceData, BUF_SIZE_FCC_GUIDANCE);
	close(hMapFile);
	isFccGuidanceConnected = 0;
	return 0;
}
#endif
