// This is intended to be a holding tank for mission related helper functions

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <standard/mavlink.h>

#include "missionHelper.h"
#include "NrcDefaults.h"  // For access to NRC defaults
#include "geo.h"
#include "bossComms.h" // For accessing waypoint preview definitions

#define EPSILON 0.001

// Import C++ Functions from geo.h/cpp

extern float get_distance_to_next_waypoint(double lat_now, double lon_now, double lat_next, double lon_next);
extern float get_bearing_to_next_waypointKE(double lat_now, double lon_now, double lat_next, double lon_next, double* bearing);

// Private functions

int getNextWaypoint(const missionType_t* specificMission, int referenceWaypoint);
bool getWaypointLatLons(const missionType_t* specificMission, int waypoint, double* lat, double* lon);
bool getWaypointAltitude(const missionType_t* specificMission, int waypoint, double* altitude);

int getFirstWaypoint(const missionType_t* specificMission) {
	return getNextWaypoint(specificMission, -1);
}

/*
	getWaypointLatLons
	Description : This function decodes the mission item's (waypoint only) latitude and longitude from
				 int32_t into double precision floating point numbers.
				 
				 Mavlink encodes lat/lon into ints to ease the transmission of this information, but we
				 need to use the DD.DDDD version.

	Author			: Jeremi Levesque
	Date			: 02/09/2022
	Return value	: TRUE (1) if the decoding was successful, FALSE (0) if an error happened.
	Inputs			:
		- specificMission		: Pointer to the specific mission structure to search the waypoint at.
		- waypoint				: Waypoint index to decode
		- lat					: Pointer to a variable in which to store the latitude information
		- lon					: Pointer to a variable in which to store the longitude information

*/
bool getWaypointLatLons(const missionType_t* specificMission, int waypoint, double* lat, double* lon) {
	
	if ((waypoint > specificMission->numMissionItems - 1) || (waypoint < 0)) {
		LOG("Can't compute Lats/Lons for Waypoint %d : It's out of bounds.\n", waypoint);
		return 0;
	}

	if (specificMission->items[waypoint].command != MAV_CMD_NAV_WAYPOINT && specificMission->items[waypoint].command != MAV_CMD_NAV_TAKEOFF) {
		LOG("Can't compute Lat/Lons for Waypoint %d : It's not a waypoint.\n", waypoint);
		return 0;
	}

	const mavlink_mission_item_int_t* start_item = &specificMission->items[waypoint];

	int32_t x = start_item->x;
	int32_t y = start_item->y;
	*lat = x * 1E-7;
	*lon = y * 1E-7;

	return 1;
}

bool getWaypointAltitude(const missionType_t* specificMission, int waypoint, double* altitude) {
	if ((waypoint > specificMission->numMissionItems - 1) || (waypoint < 0)) {
		LOG("Can't compute Lats/Lons for Waypoint %d : It's out of bounds.\n", waypoint);
		return 0;
	}

	if (specificMission->items[waypoint].command != MAV_CMD_NAV_WAYPOINT && specificMission->items[waypoint].command != MAV_CMD_NAV_TAKEOFF) {
		LOG("Can't compute Lat/Lons for Waypoint %d : It's not a waypoint.\n", waypoint);
		return 0;
	}

	const mavlink_mission_item_int_t* start_item = &specificMission->items[waypoint];

	*altitude = start_item->z;
	return 1;
}

/*
	getNextWaypoint
	Description : This function searches for the index of the next waypoint inside the mission shared data list of items.
				  If there are no other waypoints, a negative value will be returned (-1).

	Author			: Jeremi Levesque
	Date			: 02/09/2022
	Return value	: Index of the next waypoint item in the list of mission items.
	Inputs			:
		- specificMission		: Pointer to the specific mission structure to search the waypoint at.
		- referenceWaypoint		: Waypoint index from which to begin the search of the next.

*/
int getNextWaypoint(const missionType_t* specificMission, int referenceWaypoint) {
	int i;
	const mavlink_mission_item_int_t* next = NULL;

	for (i = referenceWaypoint+1; i < specificMission->numMissionItems; ++i) {
		if (specificMission->items[i].command == MAV_CMD_NAV_WAYPOINT || specificMission->items[i].command == MAV_CMD_NAV_TAKEOFF) {
			next = &specificMission->items[i];
			break;
		}
	}
	
	if (next != NULL) {
		assert(i == next->seq);
		assert((MAV_CMD_NAV_WAYPOINT == next->command) || (MAV_CMD_NAV_TAKEOFF == next->command));
		return next->seq;
	}

	return -1; // There are no other waypoints
}

/*
	speedAtWaypoint
	Description : This function determines the speed at a waypoint or at takeoff. The index of the waypoint in the shared
				  mission data list is passed (which should be number displayed in QGC - 1).

				  We iterate through the list of mission items to find the last speed update before arriving to the waypoint.
				  The default speed (5.0 m/s) is outputed if there were no speed update items found in the list.

	Author			: Jeremi Levesque
	Date			: 02/09/2022
	Return value	: Speed at waypoint in m/s. If waypoint is invalid or an error occured, -1.0 is returned instead.
	Inputs			:
		- myMission		: Pointer to the shared mission data structure to find the waypoint at
		- waypointNo	: Waypoint index in the list inside the shared mission data items list

*/
float speedAtWaypoint(myMissionStruct_t *myMission, int waypointNo) {
	int i;
	missionType_t* specificMission = getSpecificMissionType(myMission, MAV_MISSION_TYPE_MISSION);

	if ((waypointNo < 0) || (waypointNo > specificMission->numMissionItems - 1)) {
		printf("speedAtMissionItemNo received request for mission item outside of mission bounds\n");
		return -1.0;
	}

	// Verify it's a waypoint (not a speed change, or altitude change, etc.)
	if (specificMission->items[waypointNo].command != MAV_CMD_NAV_WAYPOINT && specificMission->items[waypointNo].command != MAV_CMD_NAV_TAKEOFF) {
		LOG("Can't compute speed at mission item %d because it's not a waypoint\n", waypointNo);
		return -1.0;
	}

	float commandedSpeed = DEFAULT_SPEED;	// Start by assuming the default speed defined in NrcDefaults
											// in case we don't find any defined speed.

	for (i = 0; i < waypointNo; ++i) {
		switch (specificMission->items[i].command) {
		case MAV_CMD_NAV_TAKEOFF:
			// According to our NRC ICD if param 1 is set, then it defines the speed to get there
			if (specificMission->items[i].param1 > 0) {
				commandedSpeed = specificMission->items[i].param1;
			}
			break;
		case MAV_CMD_DO_CHANGE_SPEED:
			//Primary means of changing speed
			if (specificMission->items[i].param2 != -1) { // According to the ICS -1 means no speed change
				commandedSpeed = specificMission->items[i].param2;
			}
			break;
		case MAV_CMD_DO_ORBIT:
			commandedSpeed = specificMission->items[i].param2; // Commanded tangential speed
			break;
		case MAV_CMD_DO_REPOSITION:
			if (specificMission->items[i].param1 >= 0) {
				commandedSpeed = specificMission->items[i].param1;
			}
			break;
		// No default required as these are all the known commands that adjust speed
		}
	}

	return commandedSpeed;

}

/*
	timeBetweenWaypoints
	Description : This function computes the estimated time from a waypoint to another. The time returned is
				  the time we take from the center of the first waypoint to the accepted radius around the second waypoint.
				  
				  If there is acceleration/deceleration, it is considered to be a constant accel/decel.

				  The path related to the resulting time is a straight line between the two waypoints.

	Author			: Jeremi Levesque
	Date			: 03/01/2022
	Return value	: Estimated time to get from the starting waypoint to the another waypoint.
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- waypoint_start	: Starting Waypoint index in the list inside the shared mission data items list
		- waypoint_end		: Ending Waypoint index in the list inside the shared mission data items list

*/
float timeBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end) {
	const missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);
	
	double lat1;
	double lon1;
	if (!getWaypointLatLons(specificMission, waypoint_start, &lat1, &lon1)) 
		return -1.0;

	double speed_at_start = speedAtWaypoint(mission, waypoint_start);

	return timeToWaypoint(lat1, lon1, waypoint_end, speed_at_start, mission);

}

/*
	timeToWaypoint
	Description : This function computes the estimate amound of time it takes to get from a position at a specified
				  initial speed a specified waypoint. The time returned is the time we take from the center of the 
				  first waypoint to the accepted radius around the second waypoint.

				  If there is acceleration/deceleration, it is considered to be a constant accel/decel.

				  The path related to the resulting time is a straight line between the positions.

	Author			: Jeremi Levesque
	Date			: 03/01/2022
	Return value	: Estimated time to get to the specified waypoint.
	Inputs			:
		- lat1				: Latitude of the starting position
		- lon1				: Longitude of the starting position
		- waypoint_end		: Ending Waypoint index in the list inside the shared mission data items list
		- currentSpeed		: Current speed at the starting position
		- mission			: Pointer to the shared mission data structure to find the waypoint at

*/
float timeToWaypoint(double lat1, double lon1, int waypoint_end, float currentSpeed, myMissionStruct_t* mission) {
	// Uses the current position to compute the time left to the specified waypoint

	const missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);

	double lat2;
	double lon2;
	if (!getWaypointLatLons(specificMission, waypoint_end, &lat2, &lon2))
		return -1.0;

	float acceptanceRadius = specificMission->items[waypoint_end].param2;	// Param2 is acceptance radius when item is of type MAV_CMD_NAV_WAYPOINT

	float distance_between = get_distance_to_next_waypoint(lat1, lon1, lat2, lon2);
	double speed_at_start = currentSpeed;
	double speed_at_end = speedAtWaypoint(mission, waypoint_end);
	float distance_in_meters = distance_between - acceptanceRadius;

	if (distance_in_meters <= 0) {
		// We are already in the waypoint
		return 0.0;
	}

	float delta_time_in_sec;
	if (fabs(speed_at_end - speed_at_start) < EPSILON) {
		// Speed is constant
		// time = s / v
		double speed_avg = (speed_at_end + speed_at_start) / 2.0;
		delta_time_in_sec = distance_in_meters / speed_avg;
	}
	else {
		// We are accelerating or decelerating
		// We assume the acceleration is constant between the two waypoints
		// double a = (v^2 - u^2) / 2 * s
		float acceleration_in_m_s = ((speed_at_end * speed_at_end) - (speed_at_start * speed_at_start)) / (2.0 * distance_in_meters);
		printf("Acceleration = %f\n", acceleration_in_m_s);
		delta_time_in_sec = (speed_at_end - speed_at_start) / acceleration_in_m_s;
	}

	assert(delta_time_in_sec > 0);
	return delta_time_in_sec;
}

/*
	computeDirection
	Description : This function computes the compass direction from a geographic position to another.

	Author			: Jeremi Levesque
	Date			: 02/09/2022
	Return value	: Compass direction in degrees at the first position to the second geographical position.
	Inputs			:
		- lat1				: Latitude of the starting position
		- lon1				: Longitude of the starting position
		- lat2				: Latitude of the destination (end position)
		- lon2				: Longitude of the destination (end position)

*/
float computeDirection(double lat1, double lon1, double lat2, double lon2) {

	double brng_rad;
	get_bearing_to_next_waypointKE(lat1, lon1, lat2, lon2, &brng_rad);
	float brng_deg = brng_rad * (180.0 / M_PI);

	if (brng_deg < 0) {
		brng_deg = 360 - fabs(brng_deg);
	}

	return brng_deg;
}

/*
	directionBetweenWaypoints
	Description : This function computes the compass direction at a specified waypoint (start) to get
				  to another waypoint (end).

	Author			: Jeremi Levesque
	Date			: 03/01/2022
	Return value	: Compass direction in degrees at the first waypoint to the second waypoint.
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- waypoint_start	: Starting Waypoint index in the list inside the shared mission data items list
		- waypoint_end		: Ending Waypoint index in the list inside the shared mission data items list

*/
float directionBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end) {
	const missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);

	double lat1, lon1;
	double lat2, lon2;

	if (!getWaypointLatLons(specificMission, waypoint_start, &lat1, &lon1) || !getWaypointLatLons(specificMission, waypoint_end, &lat2, &lon2))
		return -1.0;

	return computeDirection(lat1, lon1, lat2, lon2);
}

/*
	timeRemainingForMission
	Description : This function computes the compass direction at a specified waypoint (start) to get
				  to another waypoint (end).

	Author			: Jeremi Levesque
	Date			: 03/01/2022
	Return value	: Time left to the mission in seconds.
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- lat_now			: Current latitude of the aircraft (or the current position in the mission)
		- lon_now			: Current longitude of the aircraft (or the current position in the mission)
		- currentSpeed		: Current speed of the aircraft (or current speed at a point during the mission)

*/
float timeRemainingForMission(myMissionStruct_t* mission, double lat_now, double lon_now, float currentSpeed) {
	missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);
	float total_time = 0.0;
	// Start calculating the time from now's position to the next waypoint relative to the current waypoint
	total_time += timeToWaypoint(lat_now, lon_now, mission->currentMissionItem, currentSpeed, mission);

	// Get the current & next waypoint, get the time between those two
	// Add the times iteratively until the next waypoint is NULL
	int prec = mission->currentMissionItem;
	int next = getNextWaypoint(specificMission, prec);

	// next = -1 when there's no next
	while (next > 0) {
		total_time += timeBetweenWaypoints(mission, prec, next);
		prec = next;
		next = getNextWaypoint(specificMission, prec);
	}

	return total_time;
}

/*
	timeForAllMission
	Description : This function computes the estimate amount of time to complete a given mission.

	Author			: Jeremi Levesque
	Date			: 03/01/2022
	Return value	: Duration of the mission in seconds.
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at

*/
float timeForAllMission(myMissionStruct_t* mission) {
	missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);

	int prec = getFirstWaypoint(specificMission);
	int next = getNextWaypoint(specificMission, prec);

	if (next < 0)
		// There is only one point or mission is empty
		return 0.0;

	double total_time = 0.0;

	while (next > 0) {
		total_time += timeBetweenWaypoints(mission, prec, next);
		prec = next;
		next = getNextWaypoint(specificMission, prec);
	}

	return total_time;
}

/*
	percentageOfMissionCompleted
	Description : This function compute the ratio of TIME completed in the mission relative to the full
				  mission expected time.

				  If we deviate from the mission a bit, will the result of the percentage be updated properly?

	Author			: Jeremi Levesque.
	Date			: 03/02/2022
	Return value	: Percentage of mission completed (btwn 0-1)
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- lat_now			: Current latitude of the aircraft (or the current position in the mission)
		- lon_now			: Current longitude of the aircraft (or the current position in the mission)
		- currentSpeed		: Current speed of the aircraft (or current speed at a point during the mission)

*/
float percentageOfTimeCompleted(myMissionStruct_t* mission, double lat_now, double lon_now, float currentSpeed) {
	float total_time = timeForAllMission(mission);
	float time_left = timeRemainingForMission(mission, lat_now, lon_now, currentSpeed);
	float percentage_left = time_left / total_time;
	float percentage_completed = 1.0 - percentage_left;
	
	if (percentage_completed < 0)
		return 0.0; // Set it to 0.0 if negative, cause we didn't complete anything from the mission.
	else
		return percentage_completed;
}

/*
	percentageOfDistanceCompleted
	Description : This function computes the current percentage of distance completed relative to the mission's
				  total distance.

				  This is a questionnable way of mesuring the completeness of the current mission. If we have a
				  great ton of distance to do, but the speed is super high, the percentage will climb faster at a higher rate.
				  If we're at a low rate, the percentage will move super slowly.

	Author			: Jeremi Levesque.
	Date			: 03/02/2022
	Return value	: Percentage of mission completed based on distance (btwn 0-1)
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- lat_now			: Current latitude of the aircraft (or the current position in the mission)
		- lon_now			: Current longitude of the aircraft (or the current position in the mission)

*/
float percentageOfDistanceCompleted(myMissionStruct_t* mission, double lat_now, double lon_now) {
	float total_distance = distanceForAllMission(mission);
	float distance_left = distanceRemainingForMission(mission, lat_now, lon_now);
	float percentage_left = distance_left / total_distance;
	float percentage_completed = 1.0 - percentage_left;
	if (percentage_completed < 0.0) {
		return 0.0;	// Set it to 0.0 if negative, cause we didn't complete anything from the mission.
	}
	else {
		return percentage_completed;
	}
}

/*
	distanceForAllMission
	Description : This function computes the distance for an entire mission based on the waypoints.
				  The distance calculated is a straight line between each leg of the mission. It doesn't
				  account for the vehicle drifting slightly off of the mission lines.

	Author			: Jeremi Levesque
	Date			: 03/02/2022
	Return value	: Distance (in meters) of the entire mission
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at

*/
float distanceForAllMission(myMissionStruct_t* mission) {
	missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);
	int prec = getFirstWaypoint(specificMission);	// Start at first waypoint
	int next = getNextWaypoint(specificMission, prec);

	if (next < 0)
		// Only one point on the mission or mission is empty.
		// No distance to travel then.
		return 0.0;

	float total_distance_in_meters = 0.0;

	while (next > 0) {
		double lat1, lon1;
		getWaypointLatLons(specificMission, prec, &lat1, &lon1);
		
		double lat2, lon2;
		getWaypointLatLons(specificMission, next, &lat2, &lon2);

		float distance = get_distance_to_next_waypoint(lat1, lon1, lat2, lon2);

		if (distance < 0)
			// Shouldn't be any negative distances, return an error value.
			return -1.0;

		total_distance_in_meters += distance;

		prec = next;
		next = getNextWaypoint(specificMission, prec);
	}

	return total_distance_in_meters;
}

/*
	distanceRemainingForMission
	Description : This function computes the current percentage of distance completed relative to the mission's
				  total distance.

				  This is a questionnable way of mesuring the completeness of the current mission. If we have a
				  great ton of distance to do, but the speed is super high, the percentage will climb faster at a higher rate.
				  If we're at a low rate, the percentage will move super slowly.

	Author			: Jeremi Levesque.
	Date			: 03/02/2022
	Return value	: Distance left (in meters) from current position to the end of the mission.
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- lat_now			: Current latitude of the aircraft (or the current position in the mission)
		- lon_now			: Current longitude of the aircraft (or the current position in the mission)

*/
float distanceRemainingForMission(myMissionStruct_t* mission, double lat_now, double lon_now) {
	missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);
	float total_distance_in_meters = 0.0;

	int prec;
	int next = mission->currentMissionItem;

	double lat_next, lon_next;
	getWaypointLatLons(specificMission, next, &lat_next, &lon_next);

	float distance_to_next = get_distance_to_next_waypoint(lat_now, lon_now, lat_next, lon_next);
	if (distance_to_next < 0)
		// Shouldn't get negative distances, return an error value.
		return -1.0;

	total_distance_in_meters += distance_to_next;

	prec = next;
	next = getNextWaypoint(specificMission, prec);

	while (next > 0) {
		double lat1, lon1;
		getWaypointLatLons(specificMission, prec, &lat1, &lon1);

		double lat2, lon2;
		getWaypointLatLons(specificMission, next, &lat2, &lon2);

		float distance = get_distance_to_next_waypoint(lat1, lon1, lat2, lon2);
		if (distance < 0)
			// Shouldn't be any negative distances, return an error value.
			return -1.0;

		total_distance_in_meters += distance;
		prec = next;
		next = getNextWaypoint(specificMission, prec);
	}

	return total_distance_in_meters;

}

float accelerationBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end) {
	missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);
	double lat1, lon1;
	getWaypointLatLons(specificMission, waypoint_start, &lat1, &lon1);

	double lat2, lon2;
	getWaypointLatLons(specificMission, waypoint_end, &lat2, &lon2);

	float distance = get_distance_to_next_waypoint(lat1, lon1, lat2, lon2);
	float speed_start = speedAtWaypoint(mission, waypoint_start);
	float speed_end = speedAtWaypoint(mission, waypoint_end);

	if (fabs(speed_end - speed_start) < EPSILON) {
		// Speed is constant between these waypoints
		return 0.0;
	}
	else {
		// There is an accel or decel between these waypoints
		float acceleration = ((speed_end * speed_end) - (speed_start * speed_start)) / (2 * distance);
		return acceleration;
	}
}

float climbRateBetweenWaypoints(myMissionStruct_t* mission, int waypoint_start, int waypoint_end) {
	missionType_t* specificMission = getSpecificMissionType(mission, MAV_MISSION_TYPE_MISSION);

	double altitude_start, altitude_end;
	if (!getWaypointAltitude(specificMission, waypoint_start, &altitude_start) || !getWaypointAltitude(specificMission, waypoint_end, &altitude_end)) {
		return -1.0;
	}

	double time_between = timeBetweenWaypoints(mission, waypoint_start, waypoint_end);
	double delta_altitude = altitude_end - altitude_start;

	if (time_between != 0)
		return delta_altitude / time_between;
	else
		return 0.0;
}

float fuelForAllMission(myMissionStruct_t* mission, double consumption_rate) {
	float time_in_sec = timeForAllMission(mission);
	float number_of_hours = time_in_sec / 3600.0;
	return number_of_hours * consumption_rate;
}

float fuelForRemainingMission(myMissionStruct_t* mission, double lat_now, double lon_now, double currentSpeed, double consumption_in_lb_per_hour) {
	float time_in_sec = timeRemainingForMission(mission, lat_now, lon_now, currentSpeed);
	float number_of_hours = time_in_sec / 3600.0;
	return number_of_hours * consumption_in_lb_per_hour;
}

/*
	buildBossWptPreviewFromMissionItem
	Description : This function builds a preview of the current and a few other mission items for display in boss.
				  Since this is preview of the next mission items, it begins at the specified item. Let's say we
				  want to build a preview from WP16, the rationale would be that the function would return the lats/lons
				  for the WPs 17, 18, 19. The number of preview can be less than WPT_PREVIEW_LENGTH if we're close to
				  the end of the mission.

	Author			: Kris Ellis? Documentation by Jeremi Levesque.
	Date			: 03/01/2021?
	Return value	: Number of lat/lons successfully written.
	Inputs			:
		- mission			: Pointer to the shared mission data structure to find the waypoint at
		- missionItemNo		: Mission item to build the start of the preview from
		- latNext			: Array of latitudes of the next mission items (Max Length of WPT_PREVIEW_LENGTH)
		- lonNext			: Array of longitudes of the next mission items (Max Length of WPT_PREVIEW_LENGTH)
*/
int buildBossWptPreviewFromMissionItem(myMissionStruct_t *myMission, int missionItemNo, double *latNext, double *lonNext) {
    
	missionType_t* specificMission = getSpecificMissionType(myMission, MAV_MISSION_TYPE_MISSION);

	if ((missionItemNo < 0) || (missionItemNo > specificMission->numMissionItems - 1)) {
		// Here we have requested a missionItem beyond the range of the mission
		printf("buildBossWptPreviewFromMissionItem received request for mission item outside of mission bounds\n");
		return -1;
	}

	//Now we want to start at missionNo and build the wpt preview
	int numPreviewItemsCompleted = 0; // We want to build a preview up to WPT_PREVIEW_LENGTH
	for (int i = missionItemNo + 1; i < specificMission->numMissionItems; i++) {
		// Note that there is a similar loop earlier, and this code could benefit from some clean-up
		int breakOutOfForLoop = 0;
		switch (specificMission->items[i].command) {
		case MAV_CMD_NAV_TAKEOFF: // 22
			printf("This mission may have more than one takeoff...this can happen if speed change was 1st item\n");
			if (i >= 1) { // Look at the previous item
				if (specificMission->items[i - 1].command == MAV_CMD_DO_CHANGE_SPEED) {
					latNext[numPreviewItemsCompleted] = specificMission->items[i].x / (1.0E7); // Convert to lat decimal degrees
					lonNext[numPreviewItemsCompleted] = specificMission->items[i].y / (1.0E7); // Convert to lat decimal degrees
					numPreviewItemsCompleted++;
				}
			}
			break;
		case MAV_CMD_NAV_LAND: // 21
			latNext[numPreviewItemsCompleted] = specificMission->items[i].x / (1.0E7); // Convert to lat decimal degrees
			lonNext[numPreviewItemsCompleted] = specificMission->items[i].y / (1.0E7); // Convert to lat decimal degrees
			numPreviewItemsCompleted++;
			breakOutOfForLoop = 1; // A land is the last item
			break;
		case MAV_CMD_NAV_WAYPOINT: // 16
			latNext[numPreviewItemsCompleted] = specificMission->items[i].x / (1.0E7); // Convert to lat decimal degrees
			lonNext[numPreviewItemsCompleted] = specificMission->items[i].y / (1.0E7); // Convert to lat decimal degrees
			numPreviewItemsCompleted++;
			break;

		}
		if (numPreviewItemsCompleted == WPT_PREVIEW_LENGTH) breakOutOfForLoop = 1; // Break if we found all the points in the preview
		if (breakOutOfForLoop == 1) break;
	}

	return numPreviewItemsCompleted;

}


