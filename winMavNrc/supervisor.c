#include <stdio.h>
#include "sleep.h"
#include <errno.h>
#include "threads.h"
#include "socket.h"
#include <stdlib.h>
#include "log.h" // for logbuf types
#include "sendSockThread.h" // For sending clear mission to BOSS
#include "CVLADtypes.h"
#include "endian.h"
#include "supervisor.h"
#include "missionManagerToSupervisorComs.h"
#include "conversions.h"
#include "updateVehicleMavLinkStatusThread.h" // for send Simple Text to GCS
#include "systime.h" // for time-stamping
#include <standard/mavlink.h>
#include "equivalenceCOREsharedMem.h" //access to shared mem and standard varaible macro definitions
#include "bossComms.h"
#include "geo.h"
#include "NrcDefaults.h" // for test case definitions
#include "conversions.h" // For Quaternions

extern int endGlobal; // winPthreadConsole.c (main)
extern int missionComplete;// winPthreadConsole.c (main)
extern bossMissionData_t bossMissionDataGlobal; // Declared in bossComms.c

socket_t NRCAUT_supervisor_send_sock = {NRCAUT_SUPERVISOR_PORT, BROADCAST_ADDRESS};
NRCAUT_supervisor_t supervisor_data;

// timers; scaled here to smaller values for the purpose of testing

//Globals local to this file
static int perceptionSystemOk = 0; // Set to not ok on first launch
static int counter=0;//counter so that the command isn't sent more than once
static int firstHvrCritMet=0; //pos and vel for a hover are met first
static double lpAcceptTimerThreshold = 0; // timer for chasing LPs
static double LPRepotimerThreshold=0; //timer for completoing the reposition to the new LP
static double hoverTimerThreshold=0; //timer the system has to spend in a hover to call it "hover achieved"
static double lpNotFoundTimerThreshold=0; //timer for not finding LP after coming to PLP
static double startMissionTmThreshold = 0;
static double startLPTimerThreshold = 0;
//I tweak those switched to test the timers
static int lastWaypointAchieved = 0; // Gets set to 1 when Mis mgr says criteria is met. Gets auto set to 0 again when mission start is Req
static int PLPachieved=0; //assumed to be set to 1 by MM when getting to PLP
static int LpCriteriaMet=0; //assumed to be set to 1 CheckHoverCriteria() when LP criteria is met
static int missionStarted=0; //Flag to indicate that mission already started
static int LPcounter=0; //counter for LPs
static int LPTMcounter = 0; //counter for LPs after 20 secs
static bool autonomyArmed=false; //autonomy is armed and ready to take off
static mavlink_mission_item_int_t PLPMissionItem; //missionItem for PLP
static mavlink_mission_item_int_t LPMissionItem; //missionItem for the newest LP
static mavlink_mission_item_int_t LastLPMissionItem; //missionItem for the last LP
static int landRequested=0, hoverRequested=0, landed=0;
static struct hoverCriteria hcriteria; // stores criteria for hover achieved

static bool acceptNewLps = false; // Set  to false at compile time, but set to true on mission start
static bool reachedEndOfMission = false; // Set to true when you get to a stop sign!
// Externs from elsewhere
extern NRCAUT_fccstatus_t NRCAUT_fccstatus_data; // FCCstatusRECV.c


// Supervisor main thread
void *supervisorMain(void *arg)
{
#ifdef _WIN32
    pthread_setname_np(pthread_self(), "Supervisor_Thread");
#else
    pthread_setname_np("Supervisor_Thread");
#endif
   
    if (Open_Send_Socket(&NRCAUT_supervisor_send_sock) < 0) {
        printf("Error opening supervisor send socket: %s\n", strerror(errno));
    }
	
	// Call the FCC spoofer so we can start missions without FCC present...
	fccStatusDataSpoofer();
    autonomyArmed=NRCAUT_fccstatus_data.fcc_status & BIT(0);
    //call this function for dummie testing; in real test it will be called from the MM
    while(!endGlobal) {


		// If the accept timer has been started and expires handle the expiration
		if ((SysTime()> lpAcceptTimerThreshold ) && (lpAcceptTimerThreshold != 0.0)) handleLpAcceptTimerExpired();
		// If the accept timer has been started and expires handle the expiration
		if ((SysTime() > LPRepotimerThreshold) && (LPRepotimerThreshold != 0.0)) handleLpRepoTimerExpired();

        //Haven't received LP, but we just came to PLP
        if (lpNotFoundTimerThreshold == 0){ //the PLP timer never started, which means that LP was not received before coming to PLP
            if ((PLPachieved==1)&&(LPcounter==0)){
                
                //need to add hover for 10 sec Mission manager to handle this
				//start orbiting
                StartOrbit(PLPMissionItem);
            }
        }
        // max time for not finding landing point acceeded
        if (lpNotFoundTimerThreshold > 0) {
         //to prevent sending this command more than once we enable counter
			if ((SIMULATE_PEREGRINE_TEST_CASE == 1) || (SIMULATE_PEREGRINE_TEST_CASE == 2)) {
				simulatePeregrineFindingLp(SIM_PEREGRINE_TIMER_RUN); // For testing the case of Peregrine finding LP during scan
			}
            if ((SysTime() > lpNotFoundTimerThreshold)&& (PLPachieved==1)&&(LPcounter ==0)&&(counter==0)) {
               handleLzScanTimerExpired();
               counter=1;
                //counter is needed to make sure we don't send the same command more than once
            }
        }

		// Adding hooks here for peregrine test cases where multiple LP's are needed
		if ((SIMULATE_PEREGRINE_TEST_CASE == 2) || (SIMULATE_PEREGRINE_TEST_CASE == 4)) {
			// Here, let's just look at the LP number in boss and if it's greater than 1 let's keep calling simulate peregrine finding LP
			// NOTE: That this code requires that the 1st LP is found elsewhere
			if (bossMissionDataGlobal.lpNo > 0) {
				simulatePeregrineFindingLp(SIM_PEREGRINE_TIMER_RUN);
			}
		}
	
       
        // if landing was requested and landing criteria met before timer exssspired
        if ((landRequested==1)&&(CheckLandedCriteria()==1)){
            landRequested=0;
            sendSimpleTextToGcs("Just landed!", MAV_SEVERITY_INFO);
            //set mission complete
			handleReachedEndOfMission();
            
        }

       ///hover was requested and hover criteria was met
        if ((hoverRequested==1)&&(CheckHoverCriteria()==1)&&(landRequested)==0){
            sendSimpleTextToGcs("Came to hover!", MAV_SEVERITY_INFO);
			if (hcriteria.manCtrlRequiredAfterCritMet == 1) { // Update the display description field to indicate that manual control is needed
				sprintf(bossMissionDataGlobal.description, "MAN CTRL"); 
				sendMissionItemToBoss(bossMissionDataGlobal);
			}
            hoverRequested=0;
			
        }

		if ((hoverRequested == 1) && (CheckHoverCriteria() == 1)&&(landRequested==1)) {
			sendSimpleTextToGcs("Came to hover!", MAV_SEVERITY_INFO);
			Land();
			hoverRequested = 0;

		}
        //hover was requested but hover criteria was not met on time; timer on hover expired
        if ((hoverRequested==1)&&(CheckHoverCriteria()==0)&&(SysTime()>hoverTimerThreshold) && (hoverTimerThreshold > 0)){
            sendSimpleTextToGcs("Unable to meet hover criteria", MAV_SEVERITY_ALERT);
            hoverRequested=0;
        }

        //this block accounts for the cases if LPs are found before 20 secs, and nothing after 20 secs, and this is where we will reposition
     
        if ((LPTMcounter == 0) && (LPcounter > 0) && ((SysTime() - startLPTimerThreshold) > LP_TIME_ACCEPT_TOLERANCE)&& (acceptNewLps == true)&&(startLPTimerThreshold!=0)) {
           
            // this is an ugly fix of Iryna to prevent doubling of Boss numbers after UPD to Rep


            bossMissionDataGlobal.lpNo--; // Decrement lp number
            
            handleLpReposition(LastLPMissionItem);
            setHoverCritInitialDistanceTol(DEFAULT_INITIAL_DISTANCE_CRIT);
            LPTMcounter++;
        
        }
        usleep(100000); // 10 Hz for now, placeholder...
		             
}

#ifdef _WIN32
	closesocket(NRCAUT_supervisor_send_sock.s); // windows version of close...
#else
	close(NRCAUT_supervisor_send_sock.s);
#endif

	return 0;
}

int sendSupervisorPacketToFcc(void) {
	long rVal;

	//Debug code
	printf("Sending command %d to FCC\n", supervisor_data.mav_command);

	Struct_ntohl((void *)(&supervisor_data), sizeof(supervisor_data));
	Swap_Double(&supervisor_data.supervisor_gps_time);
	rVal = send(NRCAUT_supervisor_send_sock.s, (const char *)&supervisor_data, sizeof(supervisor_data), 0);
	if (rVal < 0) {
		printf("Problem sending supervisor data, error#: %ld, %s\n", rVal, strerror(errno));
	}
	return (int)rVal;
}


void supervisorMissionItemPacketStuffer(mavlink_mission_item_int_t	newMissionItem){
    
    //supervisor_data.supervisor_gps_time = 0.0;
    //supervisor_data.supervisor_status = 0;
	// Something needs to tell status that there is a command
    
    clearSupervisorStatusBits();
    setSupervisorStatusBits(MAV_COMMAND);
    
	supervisor_data.supervisor_gps_time = shared_memory->hg1700.time; 
	supervisor_data.mav_command = newMissionItem.command;
    supervisor_data.param1 = newMissionItem.param1;
    supervisor_data.param2 = newMissionItem.param2;
    supervisor_data.param3 = newMissionItem.param3;
    supervisor_data.param4 = newMissionItem.param4;
    supervisor_data.latitude = newMissionItem.x;
    supervisor_data.longitude = newMissionItem.y;
    supervisor_data.altitude_msl = newMissionItem.z;
    
    return;
}

void fccStatusDataSpoofer(void) {
	// Call this when there is no FCC present so we can just assume that everything is hunky dory for autonomy purposes
	//Bill to populate
    
    // we can put any data we want in here for testing purposes. for now just indicate in the status that the fcc is ready to take control
    
    NRCAUT_fccstatus_data.fcc_status |= BIT(0);
	// could also do things like set on_ground or feedback the mode the fcc is in
    // or set command values for comparison against other things not sure
  
}

void handleRequestStartMission(void) {
	// Here is where the supervisor would check to see if all is good, and then send indication to mission mgr
	// to start. Or que the request to wait for things to become OK to start (we need to figure out what that chain of events looks like)
	// The way I see this working is that it would raise a global flag (static to this file) that would tell the supervisor thread
	// that a mission start was requested. Then, if all conditions are met the supervisor could issue sendStartMission()

	// Is FCC ready (fcc status in NRCAUT_fccstatus_data)
	// Note: As written this code prevents us from starting missions until FCC coms are handled. We may need to spoof it initially
	if ((autonomyArmed==true)&&(isCurrentMissionStarted() == 0)) {  // Autonomy armed (bit 0), FBW Engaged and 'Couple' button is on
        if (perceptionSystemOk == 0){
			sendSimpleTextToGcs("Starting mission with perception system failed ", MAV_SEVERITY_ALERT);
        }
        else {
            // Adding a check here for starting mission on ground or in air
            if (shared_memory->hg1700.mixedhgt < 10.0) {
                // Starting on or near ground...lifing off
                // Mission manager ill issue the Lifing off announcement
            }
            else {
                sendSimpleTextToGcs("Starting mission in air", MAV_SEVERITY_ALERT);
            }
            
        }
        
		sendStartMission();
        missionStarted=1;
        
	}
	else if (autonomyArmed != true) {
		sendSimpleTextToGcs("Autonomy system not ready to start mission", MAV_SEVERITY_ALERT);
	} else	if ((autonomyArmed == true)&&(isCurrentMissionStarted() == 1)) {
		sendSimpleTextToGcs("Mission re-started", MAV_SEVERITY_ALERT);
		if ((startMissionTimer > 0) && (SysTime() > startMissionTmThreshold)) {
			//this is ok for OL testing, but for CL testing we need to find a way to re-join the mission
			missionStarted = 1;
			sendStartMission();
		}
	}

}

void sendStartMission(void)
{
	lpAcceptTimerThreshold = 0; // Sys Time at which the timer is considered as expired
    LPRepotimerThreshold=0; //Time at which we start LPRepoTimer
    lpNotFoundTimerThreshold=0;
    lastWaypointAchieved = 0; // Gets set to 1 when Mis mgr says criteria is met. Gets auto set to 0 again when mission start is Req
    PLPachieved=0; //assumed to be set to 1 by MM when getting to PLP
    LpCriteriaMet=0; //assumed to be set to 1 by MM when LP criteria is met
    LPcounter=0; //counter for LPs
    LPTMcounter = 0; //counter for LPs after 20 sec
    landRequested=0;
	hoverRequested = 0;
    missionComplete=0;
	startMissionTmThreshold = 0;
    startLPTimerThreshold = 0;
    landed=0;
	counter = 0; //THIS WILL HOPEFULLY FIX THE BUG OF lz SCAN TIMER RESTART
  	bossMissionDataGlobal.isLandingLeg = 0; // Don't start opn landing leg by default
	bossMissionDataGlobal.lpNo = 0; // Ensure we start with 0 as our LP number
	acceptNewLps = true; // We want to be able to receive LP's on start of mission
	reachedEndOfMission = false; // Hey we just started the mission...surely it can't be over already
  
    
    //initialize LP, PLP, and oldLP missionitems
    LPMissionItem.x=0;
    LPMissionItem.y=0;
    LPMissionItem.z=0;
    LPMissionItem.param1=0.0;
    LPMissionItem.param2=0.0;  // in m/s
    LPMissionItem.param4=-NAN;
    LPMissionItem.command=NAN;
    
    LastLPMissionItem.x=0;
    LastLPMissionItem.y=0;
    LastLPMissionItem.z=0;
    LastLPMissionItem.param1=0.0;
    LastLPMissionItem.param2=0.0;  // in m/s
    LastLPMissionItem.param4=-NAN;
    LastLPMissionItem.command=NAN;
    
    PLPMissionItem.x=0;
    PLPMissionItem.y=0;
    PLPMissionItem.z=0;
    PLPMissionItem.param1=0.0;
    PLPMissionItem.param2=0.0;  // in m/s
    PLPMissionItem.param4=-NAN;
    PLPMissionItem.command=NAN;

	setMissionMonitorStatusTo(1); // Mission monitoring required
    startMission(); // Eventually we will need to do this via a communication protocol (e.g. UDP)
	startMissionTimer();
	
}

void handleMissedWaypoint(void)
{// As of Jan 27 this function not activated by anything...needs code in mission mgr to determine if WPTs are missed
	sendSimpleTextToGcs("Missed Waypoint...do manual direct to!", MAV_SEVERITY_ALERT);
}

void handleLpFound(double lat, double lon, float hdg)
{
	lpNotFoundTimerThreshold = 0.0;
	if (acceptNewLps == true) { //only execute this code once we are allowed to receive new LPs - accept timer is not expired
        // to specify altitude access to shm
        float curAGL, curMSL;
        curAGL = HEIGHT_AGL; //ft
        curMSL = ALT_INS; //ft
        if (curAGL > DEFAULT_HOVER_ALTITUDE_AGL) {
            LPMissionItem.z = (curMSL - curAGL + DEFAULT_HOVER_ALTITUDE_AGL)*FEET_TO_M;
        }
        else {
            LPMissionItem.z = curMSL * FEET_TO_M;
        }
        LPMissionItem.x = lat * (1E7); // Convert from Dec Deg to Mission Item Int scaled format
        LPMissionItem.y = lon * (1E7);
        LPMissionItem.param4 = hdg;
        LPMissionItem.param1 = 0.0; // put 0 m/s to arrive at the hover
        LPMissionItem.command = MAV_CMD_DO_REPOSITION;

        if (LPcounter == 0) { // Don't yet have an LP (counter is 0),
            if (lpAcceptTimerThreshold == 0) {//we haven't started the timer
                startLPAcceptTimer(); //start timer
                startLpRefineTimer();//start LP timer
                sendSimpleTextToGcs("LP timer started", MAV_SEVERITY_INFO);
            }
   
            sendSimpleTextToGcs("LP found. Holding", MAV_SEVERITY_ALERT);
           
            //increment the LP counter
   
            //Now copy new LP item to the last LP item
            LastLPMissionItem=LPMissionItem;
            //Write LP to the disk
            writeLpDataToDisk(LPcounter+1, lat, lon, hdg);

            //Now update the data to be sent to BOSS
            bossMissionDataGlobal.lpNo++; // Increment lp number
            bossMissionDataGlobal.lpLat = LPMissionItem.x / 1.0E7; // Scaled to decimal degrees
            bossMissionDataGlobal.lpLon = LPMissionItem.y / 1.0E7;
            bossMissionDataGlobal.alt = LPMissionItem.z;
            bossMissionDataGlobal.yaw = LPMissionItem.param4; // Param 4 is LP heading in deg
       
            sprintf(bossMissionDataGlobal.description, "LP UPD");
            // Where do we get the heading of the landing point?????
            sendMissionItemToBoss(bossMissionDataGlobal);
            
            broadcastLandingPoint(bossMissionDataGlobal.lpLat, bossMissionDataGlobal.lpLon, bossMissionDataGlobal.yaw);
        }
  
        else{ //not the first LP

            if (((SysTime() - startLPTimerThreshold) <= LP_TIME_ACCEPT_TOLERANCE)&& (startLPTimerThreshold != 0)) {// do not repo for the first 20 secs but display to Boss

                LastLPMissionItem = LPMissionItem;
                //Write LP to the disk
                writeLpDataToDisk(LPcounter + 1, lat, lon, hdg);
                //Now update the data to be sent to BOSS
                bossMissionDataGlobal.lpNo=LPcounter+1; // Increment lp number
                bossMissionDataGlobal.lpLat = LPMissionItem.x / 1.0E7; // Scaled to decimal degrees
                bossMissionDataGlobal.lpLon = LPMissionItem.y / 1.0E7;
                bossMissionDataGlobal.alt = LPMissionItem.z;
                bossMissionDataGlobal.yaw = LPMissionItem.param4; // Param 4 is LP heading in deg
                               
                sprintf(bossMissionDataGlobal.description, "LP UPD");
                // Where do we get the heading of the landing point?????
                sendMissionItemToBoss(bossMissionDataGlobal);
                broadcastLandingPoint(bossMissionDataGlobal.lpLat, bossMissionDataGlobal.lpLon, bossMissionDataGlobal.yaw);

            }

            else { //after the first 20 secs do reposition if distance criteria is met

                // the first LP after 20 secs
                if (LPTMcounter == 0) {

                    
                    handleLpReposition(LPMissionItem);
                    setHoverCritInitialDistanceTol(DEFAULT_INITIAL_DISTANCE_CRIT * shared_memory->cif_output.console_pot6 / 5.0);
                    //Now copy new LP item to the last LP item
                    LastLPMissionItem = LPMissionItem;
                    //Write LP data to the disk
                    writeLpDataToDisk(LPcounter + 1, lat, lon, hdg);
                
                 } else { //not the fisr LP after 20 secs, then check the distance
               //calculate distance to previous LP
                float dist_xy, dist_z;
                get_distance_to_point_global_wgs84(LastLPMissionItem.x / 1.0E7, LastLPMissionItem.y / 1.0E7, LastLPMissionItem.z * METERS_TO_FT, LPMissionItem.x / 1.0E7, LPMissionItem.y / 1.0E7, LPMissionItem.z * METERS_TO_FT, &dist_xy, &dist_z);
                //the command will be sent only if new LP is greater than some distance tolerance
                    if (dist_xy > LP_HOR_ACCEPT_TOLERANCE_DISTANCE) {
                        //only consider the new LP if it is a significant distance away...otherwise keep working on getting to the old one
                        handleLpReposition(LPMissionItem);
                        setHoverCritInitialDistanceTol(DEFAULT_INITIAL_DISTANCE_CRIT * shared_memory->cif_output.console_pot6 / 5.0);
                        //Now copy new LP item to the last LP item
                        LastLPMissionItem = LPMissionItem;
                        //Write LP data to the disk
                        writeLpDataToDisk(LPcounter + 1, lat, lon, hdg);
                    } //end not the first LP after 20 secs

                   
                } //end if within hor tolerance distance and time
                LPTMcounter++;
            }// end else for time less than 20 secs

        } //end else not the first LP
        LPcounter++;
    }// end accept new LPs
    
    else { //lpAccept timer expired because acceptNewLPs=false
            sendSimpleTextToGcs("New Lp not accepted - timer expired", MAV_SEVERITY_INFO);
    }
    
}
    
   
void handlePerceptionSystemStatusChange(int status) {
	// Status 4 means perception system is active and ready
	// All other status numbers should indicate a failure
	if (status == 4) {
		perceptionSystemOk = 1;
	} else { 
		perceptionSystemOk = 0; 
	}
}



void handleNewCurrentMissionItem(mavlink_mission_item_int_t	newMissionItem)
{ // Here is where we pass the new mission item information off to the FCC
	// Note that currently the display of the new mission item to boss is done by mission manager. Maybe that should be done by super?
	supervisorMissionItemPacketStuffer(newMissionItem);
	//maybe want to send packlet now???
	if (sendSupervisorPacketToFcc() < 0) {
		// fire down the wire
        sendSimpleTextToGcs("Supervisor to FCC comms failure", MAV_SEVERITY_ALERT);
		
	}
}


void handleLpRepoTimerExpired(void){
	
		sendSimpleTextToGcs("Repo timer expired, hovering over the last LP", MAV_SEVERITY_ALERT);	
		hoverRequested = 0; //Stop checking hover criteria...
		sprintf(bossMissionDataGlobal.description, "MAN CTRL"); // And tell boss we need human intervention
		sendMissionItemToBoss(bossMissionDataGlobal);
		LPRepotimerThreshold = 0; // Reset the timer to zere so its undefined
}
void handleLpAcceptTimerExpired(void){
	if (acceptNewLps == true) { // If we were accepting LP's we now no longer do so
		acceptNewLps = false; // No longer accept Lps after the timer has expired
		sendSimpleTextToGcs("LP accept timer expired", MAV_SEVERITY_INFO);
	}
	
}
void handleLzScanTimerExpired(void)
{
    //hover at PLP;
	sendSimpleTextToGcs("Landing point not found. Hovering over PLP", MAV_SEVERITY_ALERT);
	//set hover criteria with default crit
	float desiredHoverHdgDeg = NAN;
	//See if there is a heading associated with the PLP
	if (!isnan(PLPMissionItem.param4)) { // Is there a heading associated with the PLP
		desiredHoverHdgDeg = PLPMissionItem.param4;
	} else { // If there is no heading defined lets try to grab it from the heading that was sent to BOSS (should be direct in heading)
		desiredHoverHdgDeg = bossMissionDataGlobal.yaw; // THIS NEEDS TO BE CHECKED FOR PROPER FUNCTION
	}

	SetHoverCriteria(PLPMissionItem.x / 1.0E7, PLPMissionItem.y / 1.0E7, PLPMissionItem.z*METERS_TO_FT,
		desiredHoverHdgDeg, // Insert heading here....I note below that we aren't checking against it
		32.81,// 10 m in feet for hovering over PLP as per DDD
		10, //10 ft
		5.0, // 5 knots
		NAN, //we don't need to check heading here
		3);// 3 secs
	// We need to signal to BOSS that we are going to hover at PLP and man control required
	setHoverCritManCtrlRequired();
	sprintf(bossMissionDataGlobal.description, "PLP rep"); //reposition to PLP
	sendMissionItemToBoss(bossMissionDataGlobal);
    HoverAtLocation(PLPMissionItem); // I notice here that we simply declare the mission as over once the Hover at PLP command is given. Do we want to see if it actually gets there??
	

}

void startLPAcceptTimer(void) {
	lpAcceptTimerThreshold = SysTime() + LP_ACCEPT_TIMER; // Add the threshold 
	sendSimpleTextToGcs("Accept timer started", MAV_SEVERITY_INFO);
}
void startLPRepoTimer(void) {
	 //update the timer for reposition
	LPRepotimerThreshold = SysTime() + REPO_TIMER;
	sendSimpleTextToGcs("Repo timer started", MAV_SEVERITY_INFO);
}
void startOrbitTimer(void) {
	lpNotFoundTimerThreshold = SysTime() + ORBIT_TIMER;
	sendSimpleTextToGcs("Orbit timer started", MAV_SEVERITY_INFO);
}

void handleLpReposition(mavlink_mission_item_int_t    myLpMissionItem)
{
// this function sends command to the FCC to relocate to new LP.
	// Note that the missioin item is populated by handleLpFound, and param4 contains the heading
    sendSimpleTextToGcs("Repositioning to LP!", MAV_SEVERITY_ALERT);
	startLPRepoTimer();
    mavlink_mission_item_int_t hoverMissionItem;

    float dist_xy, dist_z;
    get_distance_to_point_global_wgs84(shared_memory->hg1700.lat, shared_memory->hg1700.lng, shared_memory->hg1700.alt,myLpMissionItem.x/1.0E7, myLpMissionItem.y/1.0E7, myLpMissionItem.z*METERS_TO_FT, &dist_xy, &dist_z);
    //we want to calculate the speed with which we do reposition
	//put the limitation on velocity
	float vel=dist_xy/REPO_TRANSIT_TIME;  //tryingto get there in 10 seconds
	if (vel > MAX_REPO_VEL*KTS_TO_MPS)vel = MAX_REPO_VEL*KTS_TO_MPS;  //Mavlink likes m/s
    if (vel < MIN_REPO_VEL * KTS_TO_MPS)vel = MAX_REPO_VEL * KTS_TO_MPS;  //Mavlink likes m/s
    
    //first do_change speed
    hoverMissionItem.x=0;
    hoverMissionItem.y=0;
    hoverMissionItem.z=0;
    hoverMissionItem.param1=0.0;
    hoverMissionItem.param2=vel;  // in m/s
    hoverMissionItem.param4=-NAN;
    hoverMissionItem.command=MAV_CMD_DO_CHANGE_SPEED;
    supervisorMissionItemPacketStuffer(hoverMissionItem);
    sendSupervisorPacketToFcc();
    
    
    //then send do reposition

	//set hover criteria with default crit
	SetHoverCriteria(myLpMissionItem.x / 1.0E7, myLpMissionItem.y / 1.0E7, myLpMissionItem.z*METERS_TO_FT, 
		myLpMissionItem.param4, // Param 4 of the passed item contains the heading
		DEFAULT_LAND_CRITERIA_HOR_DIST, 
		DEFAULT_LAND_CRITERIA_VERT_DIST, 
		DEFAULT_LAND_CRITERIA_VEL, 
		DEFAULT_LAND_CRITERIA_HDG, 
		DEFAULT_LAND_CRITERIA_TIME);
    hoverMissionItem=myLpMissionItem;
	HoverAtLocation(hoverMissionItem);
	landRequested = 1;
    
	//Now update the data to be sent to BOSS
	bossMissionDataGlobal.lpNo++; // Increment lp number
	bossMissionDataGlobal.lpLat = hoverMissionItem.x / 1.0E7; // Scaled to decimal degrees
	bossMissionDataGlobal.lpLon = hoverMissionItem.y / 1.0E7;
	bossMissionDataGlobal.alt = hoverMissionItem.z;
	bossMissionDataGlobal.yaw = hoverMissionItem.param4; // Param 4 is LP heading in deg
    //Should set heading for bossMissionData as well
	setMissionMonitorStatusTo(0); // Stop mission manager function
	sprintf(bossMissionDataGlobal.description, "LP rep");
	// Where do we get the heading of the landing point?????
	sendMissionItemToBoss(bossMissionDataGlobal);
    broadcastLandingPoint(bossMissionDataGlobal.lpLat, bossMissionDataGlobal.lpLon, bossMissionDataGlobal.yaw);

}


void handleDirectToReposition(mavlink_mission_item_int_t    myDtMissionItem, float final_velocity)
{
    // this function sends command to the FCC to perform a direct to a new waypoint or a waypoint further along in the mission.
    
    
    mavlink_mission_item_int_t DtMissionItem;
    
    float dist_xy, dist_z, initial_velocity;
    get_distance_to_point_global_wgs84(shared_memory->hg1700.lat, shared_memory->hg1700.lng, shared_memory->hg1700.alt,myDtMissionItem.x/1.0E7, myDtMissionItem.y/1.0E7, myDtMissionItem.z*METERS_TO_FT, &dist_xy, &dist_z);
    //we want to calculate the speed with which we do the direct to
    //put the limitation on velocity
    
    /* This block is for a possible future requirement to set an initial speed as well as a final speed...
    // need to sort out where these velocities come from
    initial_velocity=dist_xy/(REPO_TIMER-2.0);
    if (initial_velocity > MAX_REPO_VEL*KTS_TO_MPS)initial_velocity = MAX_REPO_VEL*KTS_TO_MPS;  //Mavlink likes m/s
    
    //first do_change speed, this is the intial speed to schedule from (speed on arrival is set in repo command)
    DtMissionItem.x=0;
    DtMissionItem.y=0;
    DtMissionItem.z=0;
    DtMissionItem.param1=0.0;
    DtMissionItem.param2=initial_velocity;  // in m/s
    DtMissionItem.param4=-NAN;
    DtMissionItem.command=MAV_CMD_DO_CHANGE_SPEED;
    supervisorMissionItemPacketStuffer(DtMissionItem);
    sendSupervisorPacketToFcc();
     */
    
    //then send do reposition
    
    DtMissionItem=myDtMissionItem;
    DtMissionItem.param1=final_velocity;
    DtMissionItem.param4=-NAN;
    DtMissionItem.command=MAV_CMD_DO_REPOSITION;
    supervisorMissionItemPacketStuffer(DtMissionItem);
    sendSupervisorPacketToFcc();
    
#ifdef KRIS_MODS_TO_DIRECT_TO_DONT_WORK
    //Now update the data to be sent to BOSS - Kris to sort out if this is needed...
    bossMissionDataGlobal.lpNo++; // Increment lp number
    bossMissionDataGlobal.lpLat = DtMissionItem.x / 1.0E7; // Scaled to decimal degrees
    bossMissionDataGlobal.lpLon = DtMissionItem.y / 1.0E7;
    bossMissionDataGlobal.alt = DtMissionItem.z;
    bossMissionDataGlobal.yaw = DtMissionItem.param4; // Param 4 is LP heading in deg
    //Should set heading for bossMissionData as well
    setMissionMonitorStatusTo(0); // Stop mission manager function
    sprintf(bossMissionDataGlobal.description, "DT rep");
    // Where do we get the heading of the landing point?????
    sendMissionItemToBoss(bossMissionDataGlobal);
#endif
}

void Land(void)
{
 	// accept no LPs
	acceptNewLps = false;
	LPRepotimerThreshold = 0; // Reset the timer to zere so its undefined
	lpAcceptTimerThreshold = 0;
	lpNotFoundTimerThreshold = 0;
	
    // check criteria that we are in a stable hover
    // if we are do the following:
    
    clearSupervisorStatusBits();
    setSupervisorStatusBits(LANDING_REQUESTED);
	sendSupervisorPacketToFcc();
 

	//KE Feb 16 2021 -- send data to Boss
	//Arguably, the x/y position of the LP shouldn't change 
	sprintf(bossMissionDataGlobal.description, "LAND"); // Change the WP description to LAND
	sendMissionItemToBoss(bossMissionDataGlobal);
	
	sendSimpleTextToGcs("Landing", MAV_SEVERITY_ALERT);
    // for takeoff check that we are on_ground then
    //clearSupervisorStatusBits();
    //setSupervisorStatusBits(TAKEOFF_REQUESTED);
}
void HoverAtLocation(mavlink_mission_item_int_t    newMissionItem)
{
    // this function sends a command to the FCC to hover over the PLP or LP
    //do reposition MAV_command
    
    mavlink_mission_item_int_t hoverMissionItem;
    hoverMissionItem=newMissionItem;
    hoverMissionItem.param1=0.0;
    hoverMissionItem.param4=-NAN;
    hoverMissionItem.command=MAV_CMD_DO_REPOSITION;
    supervisorMissionItemPacketStuffer(hoverMissionItem);
    sendSupervisorPacketToFcc();
    hoverRequested=1;
}
void StartOrbit(mavlink_mission_item_int_t    newMissionItem){
    //Sending an orbit command to the FCC
    // the mission item (PLP) gets passed to the FCC, and converted to Orbit parameters
    mavlink_mission_item_int_t orbitMissionItem;
    orbitMissionItem=newMissionItem;
    orbitMissionItem.param1=DEFAULT_ORBIT_RADIUS;
    orbitMissionItem.param2=DEFAULT_ORBIT_VELOCITY;
    orbitMissionItem.param3=DEFAULT_ORBIT_YAW_BEHAVIOUR;
    orbitMissionItem.param4=0;
    orbitMissionItem.command=MAV_CMD_DO_ORBIT;
    supervisorMissionItemPacketStuffer(orbitMissionItem);
    sendSupervisorPacketToFcc();
    //start the timer for orbiting
	startOrbitTimer();
	hoverRequested = 0;
	//KE Feb 16 2021 -- send data to Boss
	//Arguably, the x/y position of the PLP shouldn't change...but we will need to figure out how to pass the orbit parameters to Boss at some time
	setMissionMonitorStatusTo(0); // Stop mission manager function
	sprintf(bossMissionDataGlobal.description, "LZ scan"); // Change the WP description to ORBIT
	sendMissionItemToBoss(bossMissionDataGlobal);
	sendSimpleTextToGcs("Starting an orbit to scan LZ", MAV_SEVERITY_INFO);
}

int CheckHoverCriteria() {
	//Additions here Feb 17 2021
	// Check speed below threshold
	int speedCritMet = 0;
	int horPosCritMet = 0;
	int vertPosCritMet = 0;
	int hdgCritMet = 0;
	
	//We should check all applicable individual criteria before checking the combinations
	// If criteria are defined as NAN's or Zero's we will determine them as having been met

	// Horizontal velocity criteria --- vel crit is in knots
		if (isnan(hcriteria.velTolKts) || (hcriteria.velTolKts == 0)) { // WE have no speed criteria
		speedCritMet = 1; // There was no speed crit to meet
	} else { // A speed crit was defined
		if (shared_memory->fcc_project.ground_speed <= hcriteria.velTolKts) {
			speedCritMet = 1;
		}
	}

    if (!hcriteria.initialDistanceCritMet) { // Check on the tighter initial tolerance
        if (isnan(hcriteria.initialHorzDistTolFt) || (hcriteria.initialHorzDistTolFt == 0)) { // If no crit is defined
            hcriteria.initialDistanceCritMet = 1; // Reject  nonsense values for distance tolerance...just say we made the spec
        }  else {
            float dist_xy, dist_z;
            get_distance_to_point_global_wgs84(shared_memory->hg1700.lat, shared_memory->hg1700.lng, shared_memory->hg1700.alt, hcriteria.desiredLat, hcriteria.desiredLon, hcriteria.desiredAltMSL, &dist_xy, &dist_z);
            //check distance below the threshold
            if (dist_xy * METERS_TO_FT < (float)hcriteria.initialHorzDistTolFt) {  //get_distance function returns meters and the crit is in feet
                hcriteria.initialDistanceCritMet = 1; // Signal that we got to the initial position
                horPosCritMet = 1;
            }
        }
    }  else {
        // Horz Distance criteria - hz dist crit is specified in feet
        if (isnan(hcriteria.horDistTolFt) || (hcriteria.horDistTolFt == 0)) { // If no crit is defined
            horPosCritMet = 1;
        }
        else {
            float dist_xy, dist_z;
            get_distance_to_point_global_wgs84(shared_memory->hg1700.lat, shared_memory->hg1700.lng, shared_memory->hg1700.alt, hcriteria.desiredLat, hcriteria.desiredLon, hcriteria.desiredAltMSL, &dist_xy, &dist_z);
            //check distance below the threshold
            if (dist_xy * METERS_TO_FT < (float)hcriteria.horDistTolFt) {  //get_distance function returns meters and the crit is in feet
                horPosCritMet = 1;
            }
        }
    }

	// Altitude criteria
	if (isnan(hcriteria.vertDistTolFt) || (hcriteria.vertDistTolFt == 0)) { // If no crit defined
		vertPosCritMet = 1;
	} else {
		// Now check altitude against crit
		float dist_z = abs(hcriteria.desiredAltMSL - shared_memory->hg1700.alt); 
		if (dist_z < (float)hcriteria.vertDistTolFt) {
			vertPosCritMet = 1;
		}
	}
	   	 
	// Check the heading tolerance here
	if (isnan(hcriteria.hdgToleranceDeg) || (hcriteria.hdgToleranceDeg == 0)) { // If no crit is defined
		hdgCritMet = 1; 
	}	else { // Check against heading crit
		float currHdg360 = shared_memory->hg1700.hdg; // This is typically +/-180 deg
		if (currHdg360 < 0) currHdg360 += 360;
		if (currHdg360 > 360) currHdg360 -= 360;
		float hdgDiff = hcriteria.desiredHdgDeg - currHdg360;
		if (hdgDiff <-180) hdgDiff += 360; // Make the heading error at most 180 degrees
		if (hdgDiff > 180) hdgDiff -= 360;
		if (abs(hdgDiff) < hcriteria.hdgToleranceDeg) {
			hdgCritMet = 1;
		}
	}
	
	// Check if we've met criteria and need to start a timer
	if ((horPosCritMet&&vertPosCritMet&&speedCritMet&&hdgCritMet) && (firstHvrCritMet == 0)) {// all are met for the forst time
		hcriteria.timeCritFirstMet = SysTime();
		hoverTimerThreshold = SysTime() + hcriteria.timeTol;
		firstHvrCritMet = 1;
		sendSimpleTextToGcs("Hover criteria first met", MAV_SEVERITY_INFO);
	}
	// Check if we need to reset the timer
	if ((horPosCritMet == 0) || (vertPosCritMet == 0) || (speedCritMet == 0)|| (hdgCritMet == 0)) {// either of those arenot met for the forst time
		hcriteria.timeCritFirstMet = -1;	// Reset all timers and flags
		hoverTimerThreshold = 0;
		firstHvrCritMet = 0;

	}
	// Check if we've hovered for enough time
	if ((SysTime() > hoverTimerThreshold) && (hoverTimerThreshold > 0)){
		return 1;
	}
	    
	else return 0;
	
}
void SetHoverCriteria(double desiredLat, double desiredLon, float desiredAltFt, float desiredHdgTrue, float hDistTolFt, float vDistTolFt, float velTolKts, float hdgTolDeg, float timeTol){
    // NOTE: This function will default the manCrtlRequiredAfterCriteriaMet field to 0 (flase) by default
	// use setHoverCritManCtrlRequired() to set the manCtrlRequired field of the struct
    // NOTE2: This function will default the initialDistanceTolCrit to 0 and initialDistanceCritMet to 1 (met) 
    //        --if you want to override this behaviour call setHoverInitialDistanceCrit(distance)
	hcriteria.desiredLat = desiredLat;
	hcriteria.desiredLon = desiredLon;
	hcriteria.desiredAltMSL = desiredAltFt;
	hcriteria.horDistTolFt=hDistTolFt;
	hcriteria.vertDistTolFt = vDistTolFt;
    hcriteria.velTolKts=velTolKts;
	hcriteria.desiredHdgDeg = desiredHdgTrue;
	hcriteria.hdgToleranceDeg = hdgTolDeg;
    hcriteria.timeTol=timeTol;
	hcriteria.timeCritFirstMet = -1; //Set to a negative number for not yet met
    hcriteria.hoverCompleted=0;
	hcriteria.manCtrlRequiredAfterCritMet = 0;
    hcriteria.initialHorzDistTolFt = 0;
    hcriteria.initialDistanceCritMet = 1; // Default to crit met
 }

void setHoverCritManCtrlRequired(void) {
	hcriteria.manCtrlRequiredAfterCritMet = 1;
}

void setHoverCritInitialDistanceTol(float myDistTolFt) {
    // this fun to be used to set an intial distance criteria to achieve
    hcriteria.initialDistanceCritMet = 0; 
    hcriteria.initialHorzDistTolFt = myDistTolFt;
}

int CheckLandedCriteria(void){
    
    //read FCC status packet, landed bit
  //  if (NRCAUT_fccstatus_data.fcc_status & BIT(10))
	if (shared_memory->hg1700.mixedhgt < 3.0) // less that 3 ft INS height above gnd
    {
        landed=1;
    }
        else landed=0;
    
    //landed variable will be set to 1 if landed bit
 
  
    return landed;
}
void handleReachedEndOfMission(void){
    
 // The MM would know that Mission is complete by checking this flag
    missionComplete=1;
	reachedEndOfMission = true;
	acceptNewLps = false; // No longer listen for Lps
	broadcastMissionItemReached(); // let the GCS know that the last item has been reached.
    
}

void handleTerminateMission(void){
    
 // The MM would know that Mission is complete by checking this flag
    missionComplete=1;
    reachedEndOfMission = true;
    acceptNewLps = false; // No longer listen for Lps
    sendSimpleTextToGcs("Mission terminated", MAV_SEVERITY_ALERT);
   
    
}
void clearSupervisorStatusBits (void)
{
    // sets all of the status bits to zero, must be called prior to setting the required bits to prevent mode confusion
    
    supervisor_data.supervisor_status = 0;
    
}

void handleLastWaypointAchieved(mavlink_mission_item_int_t newMissionItem) {
	PLPachieved = 1;
	PLPMissionItem = newMissionItem;
}

void setSupervisorStatusBits (NRCAUT_control_mode_t new_mode)
{
    
    supervisor_data.supervisor_status |= BIT(0);  // if we are at the point where we want to set
                                                  // bits then the superisor is ready to take
                                                  // control
    
    switch (new_mode)
    {
        case LANDING_REQUESTED:
            supervisor_data.supervisor_status |= BIT(1);
            break;
            
        case TAKEOFF_REQUESTED:
            supervisor_data.supervisor_status |= BIT(2);
            break;
            
        case TRADJECTORY_CONTROL:
            supervisor_data.supervisor_status |= BIT(3);
            break;
            
        case DAA_CONTROL:
            supervisor_data.supervisor_status |= BIT(4);
            break;
            
        case MAV_COMMAND:
            supervisor_data.supervisor_status |= BIT(5);
            break;
            
        default:
            supervisor_data.supervisor_status = 0;
    }
    
}
void startMissionTimer() {
	startMissionTmThreshold = SysTime() + 5; //starts timer for starting the mission
}

void startLpRefineTimer() {
    startLPTimerThreshold = SysTime();
}
void writeLpDataToDisk(int num, double lat, double lon, float hdg){

    landingPtData_t myLPData;
    myLPData.GpsTime=shared_memory->hg1700.time;
    myLPData.LPnum=num;
    myLPData.Lat=lat;
    myLPData.Lon=lon;
    myLPData.hdg=hdg;
    // now we write to disk
    extern buffer_t supervisorLP_logbuf;
    Write_To_Buffer(&myLPData, sizeof(myLPData), &supervisorLP_logbuf);
    
}


