/*
	Replace me by your file title

	File name	: remotes.c
	Date		: 3/10/2022 2:45:28 PM
	Author		: Levesqueje
	Description :

	TODOs : Not noted.
*/

// Standard librairies includes


// External installed librairies <>


// Project documents includes ""
#include "remotes.h"
#include "globals.h"
remotes_infos_t remotesInfo;

/////////////////////
// Main content below

/*
    getRemote
    Description : This function returns a pointer to the shared memory to a specific remote information struct.
                  It will look through the remoteInfo list in the shared_memory and compare each address/port with
                  the address that we want to find in the list.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: A pointer to the specific remote information struct, or NULL on failure.
    Inputs        :
        - shm       : A pointer to the shared memory
        - to        : Address to the remote we want to find in our list
*/
remoteInfo_t* getRemote(missionData_t* mission_data, const struct sockaddr_in* to) {
    int i;

    for (i = 0; i < remotesInfo.numRemotes; ++i) {
        if (remotesInfo.remotes[i].addr.sin_addr.s_addr == to->sin_addr.s_addr && remotesInfo.remotes[i].addr.sin_port == to->sin_port) {
            return &remotesInfo.remotes[i];
        }
    }
    return NULL;
}

const remoteInfo_t* getRemoteWithSysId(const remotes_infos_t* remotes, int sysid) {
    int i;

    for (i = 0; i < remotes->numRemotes; ++i) {
        if (remotes->remotes[i].sysid == sysid)
            return &remotes->remotes[i];
    }

    return NULL;
}

/*
    getExpectedMissionAck
    Description : This function returns a type of ACK based on the mission type from the mavlink protocol.
                  It is useful for intern use since we function on the use of the eACK enum.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: A type of ACK from the eACK enumeration
    Inputs        :
        - mission_type  : The mission type from the mavlink protocol
*/
eACK getExpectedMissionAck(MAV_MISSION_TYPE mission_type) {
    switch (mission_type) {
    case MAV_MISSION_TYPE_MISSION:
        return ACK_MISSION;
    case MAV_MISSION_TYPE_FENCE:
        return ACK_FENCE;
    case MAV_MISSION_TYPE_RALLY:
        return ACK_RALLY;
    default:
        return ACK_UNEXPECTED;
    }
}

eREMOTE_PERMISSION_LEVEL getPermissionsForId(int sysid) {

    if (sysid >= 250) {      // 255 to 250
        return eREMOTE_PERMISSION_ADMIN;
    }
    else if (sysid >= 240) { // 249 to 240
        return eREMOTE_PERMISSION_SEMI;
    }
    else if (sysid >= 230) { // 239 to 230
        return eREMOTE_PERMISSION_READONLY;
    }
    else {
        return eREMOTE_PERMISSION_UNKNOWN;
    }
}
void incrementMissesForAllRemotes(remotes_infos_t* remotesInfo) {
    int i;

    for (i = 0; i < remotesInfo->numRemotes; ++i) {
        ++remotesInfo->remotes[i].misses;
    }
}
