#pragma once
#include <stdint.h>
// Some defines for the INS
#define DBL_TIME
#define LASER_RAW
#define LASER_ALT
#define BELL_412


#include "CIFtypes.h"
#include "FCCtypes.h"
#include "INStypes.h"
#include "HMUtypes.h"
#include "A429types.h"
#include "FFStypes.h"
#include "VPOTtypes.h"
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <netinet/in.h>
#endif

#pragma pack(push,4)
typedef struct { // Simulator Configuration Packet
	uint32_t overrideSimPhysics; // Set to 1 if you want to override the simulator physics engine, 0 if you want to use the simulator physics engine
	uint32_t overrideSimEngines; // Set to 1 if you want to write into the engine parameters 
	uint32_t overrideSimActuators; // allows commands to be sent to the simulator's actuators and physics engine. will only work if overrideSimPhysics is 0
	uint32_t joystickButtons; // Stuffs the joystick buttons into a 32 bit field.
} simulatorConfig_t;
#pragma pack(pop)


// This struct below used by shared memory...same length as gps_data_type in INStypes.h, but more detailed description
#pragma pack(push,4)
typedef struct { //provides raw gps data from the INS to assess gps performance
	double gps_time;
	double lattitude;
	double longitude;
	float altitude;
	float north_velocity;
	float east_velocity;
	float vertical_velocity;
	float north_sigma;
	float east_sigma;
	float vertical_sigma;
	uint32_t number_of_satelites;
	uint32_t position_type;
	float differential_age;
} ins_gps_performance_t; // called the same in Bell 205 ICD
#pragma pack(pop)

// *********************************************************** BIG GLOBAL STRUCT HERE *********************************
#pragma pack(push,4)

typedef struct {
	hg1700_t hg1700;
	ins_gps_performance_t gps_data;
	ffs_output_t ffs_output;
	ffs_configuration_t ffs_config;
	fcc_output_t fcc_output;
	fcc_project_t fcc_project;
	fcc_configuration_t fcc_configuration;
	hmu_output_t hmu_output;
	hmu_safety_t hmu_safety;
	cif_output_t cif_output;
	airdata_t air_data; // Note  in X-Plane project this was air_data_type (declared here in ciftype.h)
#ifdef BELL_205  // Define these in your project settings
	g500_data_t g500_data;
#endif
#ifdef BELL_412 // Define these in your project settings
	asra_arinc_data_t asra_arinc_data;
#endif
	fcc_vpot_t fcc_vpot_data;
	simulatorConfig_t simulatorConfig;
} shared_data_t;
#pragma pack(pop)

//Here is a struct for determining where to put files if the data logger is running
#pragma pack(push,4)
typedef struct {
    double      loggerSysTime;  // The local systime of the data logger This can be used to sync independantly logged files. Should be updated at 100Hz or so
    uint32_t    filenum; // just the number of the current data logger file
    char logfileDirectory[512]; // This should contain the directory name where logfiles are currently being written
    // The intent is that the Data logger is the only program that writes to this shared
    // memory block, and all other programs read from it. They should pause execution until a
    // directory name is read from the shared mem block
} shared_logger_directory_t;
#pragma pack(pop)

