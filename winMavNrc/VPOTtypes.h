#pragma once
#pragma pack(push,4)  // pack on 4 byte boundaries
typedef struct {
    double systime;
    uint32_t    vsw;    // virtual switches
    float   vp1_face;   // Pot face value 0-10
    float   vp2_face;
    float   vp3_face;
    float   vp4_face;
    float   vp5_face;
    float   vp6_face;
    float   vp7_face;
    float   vp8_face;
    float   vp9_face;
    float   vp10_face;
    float   vp11_face;
    float   vp12_face;
    float   vp1_value;  // Pot value in engineering units
    float   vp2_value;
    float   vp3_value;
    float   vp4_value;
    float   vp5_value;
    float   vp6_value;
    float   vp7_value;
    float   vp8_value;
    float   vp9_value;
    float   vp10_value;
    float   vp11_value;
    float   vp12_value;
    // Keep all strings at the end of the packet (for endian's sake)
    char    vp1_function[12];	// Current assigned function of pots
    char    vp2_function[12];   // Note current total #of chars is 240
    char    vp3_function[12];
    char    vp4_function[12];
    char    vp5_function[12];
    char    vp6_function[12];
    char    vp7_function[12];
    char    vp8_function[12];
    char    vp9_function[12];
    char    vp10_function[12];
    char    vp11_function[12];
    char    vp12_function[12];
    char	vsw0_function[12];
    char	vsw1_function[12];
    char	vsw2_function[12];
    char	vsw3_function[12];
    char	vsw4_function[12];
    char	vsw5_function[12];
    char	vsw6_function[12];
    char	vsw7_function[12];
} fcc_vpot_t;
#define FCC_VPOT_CHARS 240      // 12 pots and 12 chars each, 8 switches, 12 chars each

typedef struct {
    float vp1_input;
    float vp2_input;
    float vp3_input;
    float vp4_input;
    float vp5_input;
    float vp6_input;
    float vp7_input;
    float vp8_input;
    float vp9_input;
    float vp10_input;
    float vp11_input;
    float vp12_input;
    uint32_t vswitch_input;
} vcontrol_input_t;  // This type holds the input values and must be kept global

typedef struct {
    uint32_t    control_type;   // 0=vpot, 1=vswitch
    uint32_t    control_number; // Pot number, or bit field number
    float       control_value;  // Pot value (0-10) or bit field value (0-1)
} vcontrol_adjust_t;    // Used to send a change in vpot value
#pragma pack(pop) // end packing


