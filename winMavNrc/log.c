//
//  log.c
//  iNS Test
//
//  Created by Kris Ellis on 11-11-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#if defined (_WIN32) || defined (_WIN64)
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <time.h>
#include "log.h"
#include "systime.h"

#include "log_globals.h"
#include "sleep.h"


// NOTE: BIG CHANGES TO MAV PEREGRINE LOGGING HERE NOV 30 2020
// I copied log.c/h from eventLogger since the previous versions were not working on windows. But


// *************************************************************************************************
// Notes on log files
// 
//  - First call to SysTime() should initialize the timer
//  - need to import frl_file_types.h into main
//  - need to declare this variable in main: logfile_t file[MAX_LOGFILES];
//  - need to bring the file threads.h into the project


// *************************************************************************************************
// Here is a list of differences from the DRP code base and this version of the logger code
//  1) File locations are different to account for the iOS file structure rules
//  2) the variable 'file' has been moved from main.h to here
//  3) the 'end' flag declaration has been moved here -> use extern to signal it from other files
// *************************************************************************************************

extern int endGlobal; // Defined wherever main is...

static int filename_already_concatenated;
static char filenum_path[256];
static unsigned long long words_logged, max_words_logged;

int createNewFilenumFile(char *filename); // Private to log.c

// This thread handles file creation and data logging
void *Log_Thread(void *arg)
{
	int i, j, num_files, ret;
	//RT_TASK *task;
	
    printf("Launching Log Thread...\n");
    
	//task = Task_Init("LOG",LOG_PRIO);
	
	strcpy(filenum_path,logControl_global.flightDataRootDir); // This should be the FlightData directory right under the data_logger app file
	strcat(filenum_path,"/filenum.txt");
 	
    //This line below has been commented out as Reading Filenum now happens in main.c
	//Read_Filenum(filenum_path);  // Here is where the global filenumber gets read...but it is not incremented until 'Open Files()' is called
	
	num_files = Count_Files(file);
	for(i=0; i<num_files; i++) Init_FileInfo_Header(&file[i]);
	
	// Main loop for data logging
	while(!endGlobal) {
		
		// If this condition is true then we need to open the file
		if( logControl_global.start_recording && !logControl_global.recording ) {
			Open_Files(file, num_files);
			logControl_global.start_recording = 0;
		}
		
		if( logControl_global.recording ) {
			for(i=0; i<num_files; i++) {
				for(j=0; j<file[i].num_buffers; j++) {
					Log_Data(j, &file[i]);
				}
				//printf("Logging Data\n");
				// Try flushing file here
				ret = fflush(file[i].f);
				if (ret != 0) {
					printf ("There was a problem flushing logfile %d\n", i);
				}
				// Try syncing file here
#if defined (_WIN32)
				ret = 0;
				//KE Couldn't figure out the Flush Buffer method for windows, since it wants handles to files, not file pointers
				// Here is the line of code I wanted to implement:
				//ret = (int)FlushFileBuffers(fileno(file[i].f));
#else
				ret = fsync(fileno(file[i].f));
#endif
				if (ret != 0) {
					printf ("There was a problem syncing logfile %d\n", i);
				}
			}
		}        
        logControl_global.kb_logged = (float) words_logged/1024.0; // words logged is now actually bytes logged - changed June 2018 B&K
		
		if( (max_words_logged > 0) && (words_logged > max_words_logged) ) {
			printf("Maximum words have been logged...requesting stop recording.\n");
			logControl_global.stop_recording = 1;
		}
		
		// If stop_recording has been requested, then close files
		if( logControl_global.stop_recording && logControl_global.recording ) {
			printf("Stop recording requested...\n");
			Close_Files(file);
			logControl_global.recording = 0;
			logControl_global.stop_recording = 0;
		}
		
		usleep(1000000);				// ~ 10 Hz (more like every 100 s now)
	}
    
	usleep(1000000);
	
	printf("Log Thread: Terminate requested...cleaning up\n");
	if( logControl_global.recording ) {
		for(i=0; i<num_files; i++)
			for(j=0; j<file[i].num_buffers; j++)
				Log_Data(j, &file[i]);
        
		Close_Files(file);
	}
    
	// Clean up before exiting
	//rt_task_delete(task);  // Real time stuff is not supported in iOS. Instead use regular thread handling here
	
	return 0;
}



// Copy data from circular buffer to disk until read and write pointers match
void Log_Data(int n, logfile_t *file)
{
	int read_p, write_p, bufsize;
	
	// Save local copies
	read_p = file->buf[n]->read_p;
	write_p = file->buf[n]->write_p;
	bufsize = sizeof(file->buf[n]->array);
	
	// Write up to end of buffer if write_p has wrapped around circular buffer
	if( read_p > write_p ) {
		fwrite( &file->buf[n]->array[read_p], sizeof(char), bufsize - read_p, file->f );
        words_logged += bufsize - read_p;
		read_p = 0;
	}
	// Now we can finish off the write without worrying about buffer wrap-around
	if( read_p < write_p ) {
		fwrite( &file->buf[n]->array[read_p], sizeof(char), write_p - read_p, file->f );
        words_logged += write_p - read_p;
		read_p = write_p;
	}
	
	// Update read_p
	file->buf[n]->read_p = read_p;
    
	return;
}

void Open_Files(logfile_t file[], int num_files)
{
	int i, j;
	int successful = 1;
	char zero = 0;
	int hdr_size, hdr_pad;
	time_t hdr_time;
#ifndef _WIN32
	char time_str[40];
#else
	char *time_str;		// we need to use ctime for windows...no ctime_r
#endif
	
	for(i=0; i<num_files; i++) {
		//printf("Called Open Files\n");
        Create_Filename(&file[i], logControl_global.filenum); // Here is where the individual files are actually created.
        printf("Filename = %s\n", file[i].filestring);
		file[i].f = fopen(file[i].filestring,"wb");
		
		if( file[i].f == NULL ) {
			successful = 0;
            printf("Sorry..had problems opening the log filename: %s\n", file[i].filestring);
            printf("Filename = %s\n", file[i].filename);
		}
		else {
			if( file[i].hdr_reqd ) {
				// Fill in signature
				sprintf(file[i].signature, "F%sI004", file[i].call_sign);
                
				// Fill in flight number and date in file info section, and count bytes
				hdr_size = 0;
				hdr_size += sprintf(file[i].flight, "Flight number: %c%05d\n", file[i].call_sign[0], logControl_global.filenum);
				time(&hdr_time);
#ifndef _WIN32
				ctime_r(&hdr_time, time_str);		// Note:  ctime_r includes trailing "\n" -> ctime_r is not available on windows
#else
				time_str = ctime(&hdr_time);
#endif
				hdr_size += sprintf(file[i].date,"Date: %s",time_str);
				
				// Add up number of bytes in data type strings
				for(j=0; j<file[i].num_buffers; j++)
					if( file[i].buf[j]->data_hdr.mesg_major == 2 )
						hdr_size += strlen(file[i].buf[j]->data_type);
				
				// Calculate pad size to align on 32-bit boundaries
				hdr_pad = 4 - ((hdr_size-1) % 4 + 1);
				
				// Set message size for file info header
				file[i].info_hdr.mesg_size = hdr_size + hdr_pad;
				
				// Write file info section to disk
				fprintf(file[i].f, "%s", file[i].signature); // Added string "%s" to appease the compiler warning
				fwrite(&file[i].info_hdr, sizeof(msg_header_t), 1, file[i].f);
				fprintf(file[i].f, "%s",file[i].flight);    // Added string "%s" to appease the compiler warning
				fprintf(file[i].f, "%s", file[i].date);     // Added string "%s" to appease the compiler warning

                
				for(j=0; j<file[i].num_buffers; j++)
					if( file[i].buf[j]->data_hdr.mesg_major == 2 )
						fprintf(file[i].f, "%s", file[i].buf[j]->data_type);
                
				for(j=0; j<hdr_pad; j++) fwrite(&zero, sizeof(char), 1, file[i].f);
				
				// Write data def section for each buffer
				for(j=0; j<file[i].num_buffers; j++) {
					if( file[i].buf[j]->data_hdr.mesg_major == 2 ) {
						fwrite(&file[i].buf[j]->def_hdr, sizeof(msg_header_t), 1, file[i].f);
						fwrite(file[i].buf[j]->data_def, file[i].buf[j]->def_hdr.mesg_size, 1, file[i].f);
					}
				}
			}
			
			for(j=0; j<file[i].num_buffers; j++) {
				file[i].buf[j]->read_p = 0;
				file[i].buf[j]->write_p = 0;
			}
		}
	}
	
	if( successful ) {
        // Not much point in doing this as the filenumber is handled by the filenumber manager object
        char fileNumFilename[256];
        strcpy(fileNumFilename, logControl_global.flightDataRootDir);
        strcat(fileNumFilename,"/filenum.txt");
		Write_Filenum(fileNumFilename); // Writes to FlightData directory right under the app file
		//Write_Filenum("/data/filenum.txt");
        
		for(i=0; i<num_files; i++) { // This is the bit of code that automatically deletes files beyond MaxFiles
			if( (file[i].max_files > 0) && (logControl_global.filenum >= file[i].max_files) ) {
				Create_Filename(&file[i], logControl_global.filenum - file[i].max_files);
				Delete_File(file[i].filestring);
				Create_Filename(&file[i], logControl_global.filenum);
                // Now concatenate the filenumber and extension to the global variable
                if (!filename_already_concatenated) {
                    strcat(logControl_global.frl_filename, file->number);
                    strcat(logControl_global.frl_filename,file->extension);
                    filename_already_concatenated = 1;
                }

			}
		}
		logControl_global.recording = 1;
	}
	else {
		logControl_global.filenum--; // We didn't successfully open the file for writing
		logControl_global.recording = 0;
	}
	
	return;
}

// Close files to complete data logging
void Close_Files(logfile_t file[])
{
	int i;
	
	for(i=0; i<MAX_LOGFILES; i++)
		if( file[i].f != NULL)
			fclose(file[i].f);
    
	return;
}

void Delete_File(char *filename)
{
	FILE *f;
	
	f = fopen(filename,"wb");
	if( f != NULL ) {
		fclose(f);
#ifndef WIN32
		unlink(filename);
#else
		_unlink(filename);  //unlink() is deprecated, but we can use _unlink()
#endif
		
	}
	return;
}
void Write_To_Buffer(void *data, int n_bytes, buffer_t *buf)
{
	int i, pos, bufsize;
	double time;
    
	time = SysTime();   // Need to re-write SysTime to handle iphone
                        // Seems that we need to use mach-absolute time as a reference
    
	pos = buf->write_p;
	bufsize = sizeof(buf->array);
	
	if( buf->hdr_reqd ) {
		// Fill in size of data header
		buf->data_hdr.mesg_size = sizeof(double) + n_bytes;
		// Write data header first
		for(i=0; i<sizeof(msg_header_t); i++) {
			buf->array[pos] = ((unsigned char *)(&buf->data_hdr))[i];
			pos++;
			pos %= bufsize;
		}
		// Write SysTime
		for(i=0; i<sizeof(double); i++) {
			buf->array[pos] = ((unsigned char *)(&time))[i];
			pos++;
			pos %= bufsize;
		}
	}
	
	// Write output packet
	for(i=0; i<n_bytes; i++) {
		buf->array[pos] = ((unsigned char *)(data))[i];
		pos++;
		pos %= bufsize;
	}
	
	// Update write_p once a complete record has been written
	buf->write_p = pos;
	
	return;
}

int Count_Files(logfile_t file[])
{
	int i = 0;
    
	while( (i < MAX_LOGFILES) && (strlen(file[i].filename) > 0) ) i++;
	
	return i;
}

void Init_FileInfo_Header(logfile_t *file)
{
	file->info_hdr.header_word = HEADERWORD;
	file->info_hdr.mesg_major = 0;
	file->info_hdr.mesg_minor = 0;
	file->info_hdr.trailer_word = TRAILERWORD;
	
	return;
}



void Add_Buffer(logfile_t *file, buffer_t *buf)
{
	int n;
	
	n = file->num_buffers;
	
	// Copy pointer to buffer into file structure
	file->buf[n] = buf;
	// Buffer inherits the hdr_reqd property from file
	buf->hdr_reqd = file->hdr_reqd;
    
	// Fill in FRL format headers
	if( file->hdr_reqd ) {
		// Data Def header
		buf->def_hdr.header_word = HEADERWORD;
		buf->def_hdr.mesg_major = 1;
		buf->def_hdr.mesg_minor = n;
		buf->def_hdr.trailer_word = TRAILERWORD;
        
		// Data header
		buf->data_hdr.header_word = HEADERWORD;
		buf->data_hdr.mesg_major = 2;
		buf->data_hdr.mesg_minor = n;
		buf->data_hdr.trailer_word = TRAILERWORD;
	}
	
	// Update total number of buffers
	n++;
	file->num_buffers = n;
    
	return;
}

void Add_Event(logfile_t *file, buffer_t *buf)
{
	int n, m;
	
	n = file->num_buffers;
	m = file->num_events;
	
	file->buf[n] = buf;
	buf->hdr_reqd = file->hdr_reqd;
    
	if( file->hdr_reqd ) {
		// Data header
		buf->data_hdr.header_word = HEADERWORD;
		buf->data_hdr.mesg_major = 3;
		buf->data_hdr.mesg_minor = m;
		buf->data_hdr.trailer_word = TRAILERWORD;
	}
	
	// Update total number of buffers
	n++;
	file->num_buffers = n;
	// Update total number of events
	m++;
	file->num_events = m;
    
	return;
}

void Add_File(logfile_t *file, char *fname, char *ext, char *call, int hdr, int max)
{
    // Note that the DRP code base requires the user to explicitly indicate the path.
    // In iOS the fileNumberManager object will populate the path to a global variable which
    // needs to be added here
    // Here's an example of the DRP code call: Add_File(&file[0], "/data/output", ".frl", "YZV", 1, max_files);
    
    strcpy(file->filename,logControl_global.filepath_string);
    strcat(file->filename,"/"); // add the directory spacer
	strcat(file->filename, fname);

	//sprintf(file->filename,"%s",fname);
	sprintf(file->extension,"%s",ext);
	sprintf(file->call_sign,"%s",call);
	file->hdr_reqd = hdr;
	file->max_files = max;
    
    // Add the passed filename to the global frl filename variable
    strcpy(logControl_global.frl_filename,fname);
    
	return;
}

// Converts a filesize in megabytes to a number of words
void Set_Max_Storage_Size(int mb)
{
	max_words_logged = mb*1024*(1024);
	
	return;
}

void Create_Filename(logfile_t *file, int num)
{   // the filename variable is just a path and prefix. The filestring is the full path to an actual filename

	sprintf(file->number, "%d", num);
	strcpy(file->filestring, file->filename);
	strcat(file->filestring, file->number);
	strcat(file->filestring, "."); // Add the decimal for the extension
	strcat(file->filestring, file->extension);

	return;
}

void Increment_Filenum(char *filename)
{
	FILE *f;

	// filename contains the number of the last file created
	f = fopen(filename, "r");
	int filenum=0;

	if (f == NULL) {
        //Let's try creating the file
        if(createNewFilenumFile(filename)<0) {
            printf("%s not found, setting file number to %d\n", filename, filenum);
        }
	}
	else {
		fscanf(f, "%d", &filenum);
		fclose(f);
	}
	logControl_global.filenum = filenum+1; // The new filenumber should be incremented before the assignment

	return;
}

int createNewFilenumFile(char *filename) {
    // The intent is for this to be called when we cant' find our filenum file...so we try and make it
    FILE *f;
    f = fopen(filename, "w");
    if (f == NULL) {
        printf("Could not write to %s\n", filename);
        return -1; // an error occured
    }
    else {
        fprintf(f, "%d\n", 1); // Let's start at 1....
        fclose(f);
    }
    
    
    return 0; // For success
}

void Write_Filenum(char *filename)
{ //Writes the current filenum to the file

	FILE *f;

	// filename contains the number of the last file created
	f = fopen(filename, "w");
	if (f == NULL) {
		printf("Could not write to %s\n", filename);
	}
	else {
		fprintf(f, "%d\n", logControl_global.filenum); 
		fclose(f);
	}

	return;
}
