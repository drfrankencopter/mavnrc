#include "debug.h"
#include "sharedMemType.h"
#include "conversions.h"
#include "equivalenceCOREsharedMem.h" //access to shared mem and standard varaible macro definitions


extern shared_data_t *shared_memory;	// Global where we save all the data in machine native format



void pauseSimAndDebug(void) { // Insert this in your main loop
	// The intent of this is to allow a switch to be specified to pause the sim, fcc, mavnrc, boss, etc to allow for debugging

	if (FSW15) { // We will use the shutdown button to pause the sim. Ideally this would be one of the pilot switches, by something in the SCIOP was preventing me from doing this
	// We want to quickly tell X-Plane to disable physics....
		//shared_memory->simulatorConfig.overrideSimPhysics = 1;

		// DO NOT REMOVE THE BREAKPOINT.....
		// Put a breakpoint on this line....
		printf("User pressed pause button\n");
	}
	else {
		//shared_memory->simulatorConfig.overrideSimPhysics = 0;
	}

}

