//
//  recvFromRemoteThread.h
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-21.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#ifndef recvFromRemoteThread_h
#define recvFromRemoteThread_h

#include <stdio.h>

#include "globals.h"
#include "mavProtocol.h"
#include "remotes.h"

// Main thread

void recvFromRemoteThread(void* arg);

// Helper functions

int isSysIdTaken(const remotes_infos_t* remotesInfo, int sysid);
int isRemoteConnected(missionData_t* mission_data, const struct sockaddr_in* remote);
remoteInfo_t* connectRemote(missionData_t* mission_data, const struct sockaddr_in* remote, int sysid, int compid);
void sendPermissionsText(listened_message_t* lm, const remoteInfo_t* remote);


#endif /* recvFromRemoteThread_h */
