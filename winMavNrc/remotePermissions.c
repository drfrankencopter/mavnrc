//
//  remotePermissions.c
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-21.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#include "remotePermissions.h"
#include "remotes.h"

extern remotes_infos_t remotesInfo;

// Private use
int getPermissionAccorded(eREMOTE_PERMISSION_LEVEL permissionLevelNeeded, eREMOTE_PERMISSION_LEVEL permissionLevel);


/*
    hasPermission
    Description : This function checks if the specified sender of the message
                  has the rights to send this message to MavNRC.

                  The permission level should've already been attributed to
                  the sender (which is a remote). If it hasn't already been
                  attributed, the permission level is determined based on the
                  sysid from which system the message is coming from.

    Author      : Jeremi Levesque
    Date        : 02/07/2022
    Return value: 1 (TRUE) if the sender has permissions for the message, 0 (FALSE) otherwise
    Inputs      :
        - listenedMessage   : A pointer to the structure holding all the data relative to the listened message.

*/
int hasPermission(listened_message_t* lm) {
    int permissionAccorded = 0;
    eREMOTE_PERMISSION_LEVEL permissionLevel;
    char* statustext = "";
    remoteInfo_t* remote = getRemote(lm->mission_data, &lm->toAddr);

    if (remote != NULL) {
        permissionLevel = remote->permissions;
    }
    else {
        // Make sure there's not another remote that has the same sysid
        // We don't want to permit two instances with the same sysid
        if (getRemoteWithSysId(&remotesInfo, lm->msg.sysid) != NULL)
            permissionLevel = eREMOTE_PERMISSION_NO_PERMISSION;
        else
            permissionLevel = getPermissionsForId(lm->msg.sysid);

        
    }
    
    switch (lm->msg.msgid) {
        /*
         The following are allowed to all levels
         */
        case MAVLINK_MSG_ID_HEARTBEAT:
        case MAVLINK_MSG_ID_MISSION_REQUEST_LIST:
        case MAVLINK_MSG_ID_MISSION_REQUEST_INT:
        case MAVLINK_MSG_ID_COMMAND_LONG:
        case MAVLINK_MSG_ID_MISSION_ACK:
        case MAVLINK_MSG_ID_MISSION_CURRENT:
        case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
        case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
        case MAVLINK_MSG_ID_PARAM_SET:
        case MAVLINK_MSG_ID_MISSION_CLEAR_ALL:
        case MAVLINK_MSG_ID_PING:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_READONLY, permissionLevel);
            break;
        /*
         The following are reserved to semi & admin
         */
        case MAVLINK_MSG_ID_MISSION_COUNT:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_SEMI, permissionLevel);
            if (!permissionAccorded) {
                // Do the things to deny the message
                mavlink_mission_count_t count;
                mavlink_msg_mission_count_decode(&lm->msg, &count);
                sendMissionAckRemote(lm, count.mission_type, MAV_MISSION_DENIED);
                // And set a default text to show up in Gcs
                statustext = "Permission denied for mission count message\n";
            }
            break;
        case MAVLINK_MSG_ID_MISSION_ITEM_INT:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_SEMI, permissionLevel);
            if (!permissionAccorded) {
                // Do the things to deny the message
                // And set a default text to show up in Gcs
                statustext = "Permission denied for mission item int message\n";
            }
            break;
        case MAVLINK_MSG_ID_PARAM_VALUE:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_SEMI, permissionLevel);
            if (!permissionAccorded) {
                // Do the things to deny the message
                // And set a default text to show up in Gcs
                statustext = "Permission denied for param value message\n";
            }
            break;
        case MAVLINK_MSG_ID_MISSION_SET_CURRENT:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_SEMI, permissionLevel);
            if (!permissionAccorded) {
                // Do the things to deny the message
                // And set a default text to show up in Gcs
                statustext = "Permission denied for set current message\n";
            }
            break;
        /*
         The following are reserved to admin
         */
        case MAVLINK_MSG_ID_MANUAL_CONTROL:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_ADMIN, permissionLevel);
            if (!permissionAccorded) {
                statustext = "Permission denied for manual control\n";
            }
            break;
        case MAVLINK_MSG_ID_SET_MODE:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_ADMIN, permissionLevel);
            if (!permissionAccorded) {
                statustext = "Permission denied for set mode message\n";
            }
            break;

        default:
            permissionAccorded = getPermissionAccorded(eREMOTE_PERMISSION_ADMIN, permissionLevel);
            if (!permissionAccorded) {
                LOG("Unrecognized message : Couldn't determine the permissions. Permission denied.\n");
            }
            return 0;
            break;
    }   // end of switch
    
    if (!permissionAccorded) {
        sendSimpleText(lm->sockfd, statustext, MAV_SEVERITY_ALERT, &lm->toAddr); //KE Allows this spoken MSG as it should never originate from the master GCS
    }
    
    return permissionAccorded;
}


/*
    getPermissionAccorded
    Description : This function verifies if the permission level is high enough for the permission level
                  needed for an action.

    Author      : Jeremi Levesque
    Date        : 02/07/2022
    Return value: 1 (TRUE) if permission is accorded relative to the permission level, 0 (FALSE) otherwise
    Inputs      :
        - permissionLevelNeeded   : The permission level needed
        - permissionLevel         : The permission level to verify

*/
int getPermissionAccorded(eREMOTE_PERMISSION_LEVEL permissionLevelNeeded, eREMOTE_PERMISSION_LEVEL permissionLevel) {
    switch (permissionLevelNeeded) {
        case eREMOTE_PERMISSION_READONLY:
            return 1;
            break;
            
        case eREMOTE_PERMISSION_SEMI:
            return ((permissionLevel == eREMOTE_PERMISSION_SEMI) || (permissionLevel == eREMOTE_PERMISSION_ADMIN));
            break;
        
        case eREMOTE_PERMISSION_ADMIN:
            return (permissionLevel == eREMOTE_PERMISSION_ADMIN);
            break;
        case eREMOTE_PERMISSION_NO_PERMISSION:
        case eREMOTE_PERMISSION_UNKNOWN:
            return 0;
            break;
        default:
            // Should never get here
            return 0;
            break;
    }
}
