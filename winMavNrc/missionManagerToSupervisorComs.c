#include <stdio.h>

#include "missionManagerToSupervisorComs.h"
#include "CVLADtypes.h"
#include "globals.h" // For access to mission struct
#include "supervisor.h" // Note that this is included so we can call supervisor functions directly. Eventually we need to remove this dependance


void signalGcsRequestedStartMission(void) {
	//For now here we just call the supervisor's handle request start mission method
	printf("Mission manager requests to start mission...\n");
	handleRequestStartMission(); // Eventually we need to sort out a coms protocol
}

void signalNewMissionItem(mavlink_mission_item_int_t newMissionItem) {
	handleNewCurrentMissionItem(newMissionItem); // Eventually we do this via signal/coms to the supervisor
}

void signalLastWaypointAchieved(mavlink_mission_item_int_t lastMissionItem) {
	handleLastWaypointAchieved(lastMissionItem); // Eventually need to do this via signal, not direct call to super function
}

void signalDirectToReposition(mavlink_mission_item_int_t    myDtMissionItem, float final_velocity) {
	handleDirectToReposition(myDtMissionItem, final_velocity);
}

int isCurrentMissionStarted(void) {
	//Returns 1 if mission is started, 0 otherwise
	extern missionData_t* mission_data;    // Global where we save all the mission information
	if (mission_data->myCurrentMission.missionStarted == 1) {
		return 1; // Currenty running a mission
	} else {
		return 0; // Not running a mission
	}
}