
#pragma once
// NOTE: THIS FILE HAS BEEN MODIFIED FROM THE FCC CODE BASE SINCE THE ORIGINAL REFERRED TO INTERNAL VARAIBLES THAT
// ARE NOTE AVAILABLE WHEN ONLY ACCESSING SHARED MEMORY
//
// DO NOTE MERGE THIS BACK TO FCC SOURCE!!!!

// Need to include the frl_file_types form the X-Plane project
#include "sharedMemType.h"
extern shared_data_t *shared_memory;    // Global where we save all the data in machine native format
// basic truism as stated by Gubbs in the grand amalgamation of the equivalence defs...
// During the great upgrade of 2017/18...
#define TRUE  1
#define FALSE 0

// Gubbs says: Much of the variable names we use all the time are commmon between the
// FCC and HMU. Therefore, we have the common ones listed first, followed later by those 
// specific to their given machine...

// define's for the HG1700 INS data
#define INS_STATUS		(shared_memory->hg1700.status)
#define GPS_TIME		(shared_memory->hg1700.time)
#define	THETA_INS		(shared_memory->hg1700.pitch)
#define	PHI_INS			(shared_memory->hg1700.roll)
#define	PSI_INS			(shared_memory->hg1700.hdg)
#define Q_INS			(shared_memory->hg1700.q)
#define P_INS			(shared_memory->hg1700.p)
#define R_INS			(shared_memory->hg1700.r)
#define AX_INS			(shared_memory->hg1700.ax)
#define AY_INS			(shared_memory->hg1700.ay)
#define AZ_INS			(shared_memory->hg1700.az)
#define VEL_N_INS		(shared_memory->hg1700.vn)
#define VEL_E_INS		(shared_memory->hg1700.ve)
#define VEL_Z_INS		(shared_memory->hg1700.vz)
#define LAT_INS			(shared_memory->hg1700.lat)
#define LON_INS			(shared_memory->hg1700.lng)
#define ALT_INS			(shared_memory->hg1700.alt)
#define LASERALT_RAW	(shared_memory->hg1700.laser_raw)
#define LASERALT		(shared_memory->hg1700.laser_alt)
#define HEIGHT_AGL		(shared_memory->hg1700.mixedhgt)
#define INS_FAIL		(shared_memory->hg1700.status & BIT_6)


// define's for the FFS Eval Pilot input
#define LAT_CYC_DISP	(shared_memory->ffs_output.ffs_control_position_lateral)
#define LON_CYC_DISP	(shared_memory->ffs_output.ffs_control_position_long)
#define PEDALS_DISP		(shared_memory->ffs_output.ffs_control_position_pedal)
#define COLLECTIVE_DISP	(shared_memory->ffs_output.ffs_control_position_collective)
#define F_LAT			(shared_memory->ffs_output.ffs_applied_force_lateral)
#define F_LON			(shared_memory->ffs_output.ffs_applied_force_long )
#define F_PED			(shared_memory->ffs_output.ffs_applied_force_pedal)
#define SAC_LAT			(shared_memory->ffs_output.sidearm_applied_force_lateral)
#define SAC_LON			(shared_memory->ffs_output.sidearm_applied_force_long)
#define SAC_PED			(shared_memory->ffs_output.sidearm_applied_force_pedal)
#define SAC_COL			(shared_memory->ffs_output.sidearm_applied_force_collective)

// Evaluation pilot function switches
#define EPSWHATFWD		(shared_memory->ffs_output.ffs_switches & BIT_0)
#define EPSWHATAFT		(shared_memory->ffs_output.ffs_switches & BIT_1)
#define EPSWHATRGT		(shared_memory->ffs_output.ffs_switches & BIT_2)
#define EPSWHATLFT		(shared_memory->ffs_output.ffs_switches & BIT_3)
#define EPSWFRCERELEASE	(shared_memory->ffs_output.ffs_switches & BIT_4)
#define EPSWLHPB		(shared_memory->ffs_output.ffs_switches & BIT_5)
#define EPSWRHPB		(shared_memory->ffs_output.ffs_switches & BIT_6)
#define EPSWPINKEY		(shared_memory->ffs_output.ffs_switches & BIT_7)
#define EPSWTRIGGER		(shared_memory->ffs_output.ffs_switches & BIT_8)
#define EPSWCOL0		(shared_memory->ffs_output.ffs_switches & BIT_9)  
#define EPSWCOL1		(shared_memory->ffs_output.ffs_switches & BIT_10)
#define EPSWCOL2		(shared_memory->ffs_output.ffs_switches & BIT_11)
#define EPSWCOL3		(shared_memory->ffs_output.ffs_switches & BIT_12)
#define EPSWCOL4UP		(shared_memory->ffs_output.ffs_switches & BIT_13)
#define EPSWCOL4DOWN	(shared_memory->ffs_output.ffs_switches & BIT_14)
#define EPSWCOL5UP		(shared_memory->ffs_output.ffs_switches & BIT_15)
#define EPSWCOL5DOWN	(shared_memory->ffs_output.ffs_switches & BIT_16)
#define EPSWCOL6UP		(shared_memory->ffs_output.ffs_switches & BIT_17)
#define EPSWCOL6DOWN	(shared_memory->ffs_output.ffs_switches & BIT_18)
#define EPSWCOL7UP		(shared_memory->ffs_output.ffs_switches & BIT_19)
#define EPSWCOL7DOWN	(shared_memory->ffs_output.ffs_switches & BIT_20)
#define EPSWCOLLHPB		(shared_memory->ffs_output.ffs_switches & BIT_21)
#define EPSWCOLRHPB		(shared_memory->ffs_output.ffs_switches & BIT_22)
#define EPSWCOLTRIGGER	(shared_memory->ffs_output.ffs_switches & BIT_23)
#define WSFSW0			(shared_memory->ffs_output.ffs_switches & BIT_24)
#define WSFSW1			(shared_memory->ffs_output.ffs_switches & BIT_25)
#define WSFSW2			(shared_memory->ffs_output.ffs_switches & BIT_26)
#define WSFSW3			(shared_memory->ffs_output.ffs_switches & BIT_27)
#define FFSENGAGE		(shared_memory->ffs_output.ffs_switches & BIT_28)
#define EPTHUMBWHEEL    (shared_memory->ffs_output.collective_thumb_wheel_pot)

// Safety pilot control and function switches
#define FSW0    		(shared_memory->cif_output.function_switches & BIT_0)
#define FSW1    		(shared_memory->cif_output.function_switches & BIT_1)
#define FSW2    		(shared_memory->cif_output.function_switches & BIT_2)
#define FSW3    		(shared_memory->cif_output.function_switches & BIT_3)
#define FSW4	   	 	(shared_memory->cif_output.function_switches & BIT_4)
#define FSW5    		(shared_memory->cif_output.function_switches & BIT_5)
#define FSW6    		(shared_memory->cif_output.function_switches & BIT_6)
#define FSW7			(shared_memory->cif_output.function_switches & BIT_7)
#define FSW8			(shared_memory->cif_output.function_switches & BIT_8)
#define FSW9			(shared_memory->cif_output.function_switches & BIT_9)
#define FSW10			(shared_memory->cif_output.function_switches & BIT_10)
#define FSW11			(shared_memory->cif_output.function_switches & BIT_11)
#define FSW12			(shared_memory->cif_output.function_switches & BIT_12)
#define GROUND_CAL		(shared_memory->cif_output.function_switches & BIT_12)
#define FSW13			(shared_memory->cif_output.function_switches & BIT_13)
#define ACTUATOR_TEST	(shared_memory->cif_output.function_switches & BIT_13)	// Actuator test
#define FSW14			(shared_memory->cif_output.function_switches & BIT_14)
#define BYPASS			(shared_memory->cif_output.function_switches & BIT_14)
#define FSW15			(shared_memory->cif_output.function_switches & BIT_15)
#define SHUTDOWN		(shared_memory->cif_output.function_switches & BIT_15)
#define LAT_ACT_SELECT	(shared_memory->cif_output.function_switches & BIT_16)
#define LON_ACT_SELECT	(shared_memory->cif_output.function_switches & BIT_17)
#define PED_ACT_SELECT	(shared_memory->cif_output.function_switches & BIT_18)
#define COL_ACT_SELECT	(shared_memory->cif_output.function_switches & BIT_19)
#define AIR_GROUND		(shared_memory->cif_output.function_switches & BIT_20)
#define SPSW0			(shared_memory->cif_output.function_switches2 & BIT_0) 
#define SPSW1			(shared_memory->cif_output.function_switches2 & BIT_1)
#define SPSW2			(shared_memory->cif_output.function_switches2 & BIT_2)
#define SPSW3			(shared_memory->cif_output.function_switches2 & BIT_3)
#define SPSW4			(shared_memory->cif_output.function_switches2 & BIT_4)

// Safety Pilot Control positions
#define SP_LAT_CTRL		(shared_memory->cif_output.sp_control_position_lateral)
#define SP_LON_CTRL		(shared_memory->cif_output.sp_control_position_long)
#define SP_PED_CTRL		(shared_memory->cif_output.sp_control_position_pedal)
#define SP_COL_CTRL		(shared_memory->cif_output.sp_control_position_collective)
#define LAT_ACT_POS		(shared_memory->hmu_output.fbw_actuator_position_lateral)
#define LON_ACT_POS		(shared_memory->hmu_output.fbw_actuator_position_long)
#define COL_ACT_POS		(shared_memory->hmu_output.fbw_actuator_position_collective)
#define TR_ACT_POS		(shared_memory->hmu_output.fbw_actuator_position_pedal)

// Cockpit pots
#define CP1				(shared_memory->cif_output.console_pot1)
#define CP2				(shared_memory->cif_output.console_pot2)
#define CP3				(shared_memory->cif_output.console_pot3)
#define CP4				(shared_memory->cif_output.console_pot4)
#define CP5				(shared_memory->cif_output.console_pot5)
#define CP6				(shared_memory->cif_output.console_pot6)
#define CP7				(shared_memory->cif_output.console_pot7)
#define CP8				(shared_memory->cif_output.console_pot8)

// airflow angles
#define ALPHA			(shared_memory->cif_output.angle_of_attack)
#define BETA			(shared_memory->cif_output.side_slip)	// positive airflow from right

// Engage and system states
#define ENGAGED					(Project.P_vars.p_ints[0])
#define CONTROL_INDEX			(Project.P_vars.p_ints[1])
// (Project.P_vars.p_ints[2]) is engage_num in FCC and watchdog in HMU, both defined below
#define ARMED					(Project.P_vars.p_ints[3])
#define GROUND_TEST_MODE 		(Project.P_vars.p_ints[4])

// some light hearted trig...
#define SINPHI 					((float) sin(PHI_INS * DEG2RAD))
#define	COSPHI 					((float) cos(PHI_INS * DEG2RAD))	
#define	SINTHETA 				((float) sin(THETA_INS * DEG2RAD))
#define	COSTHETA 				((float) cos(THETA_INS * DEG2RAD))	
#define	SINPSI 					((float) sin(PSI_INS * DEG2RAD))
#define	COSPSI 					((float) cos(PSI_INS * DEG2RAD))
#define SINALPHA				((float) sin(ALPHA * DEG2RAD))
#define COSALPHA				((float) cos(ALPHA * DEG2RAD))
#define SINBETA					((float) sin(BETA * DEG2RAD))
#define COSBETA					((float) cos(BETA * DEG2RAD))

// these will need a more generic way of doing this for network data
#define ROTOR_RPM		(shared_memory->hmu_output.main_rotor_rpm)

// In the desktop sim with shared mem we need to map these to our shared memory area
 
#define U_GND_DOT 		(shared_memory->fcc_project.u_gnd_dot)
#define V_GND_DOT 		(shared_memory->fcc_project.v_gnd_dot)
#define W_GND_DOT 		(shared_memory->fcc_project.w_gnd_dot)
#define U_HORIZ_DOT		(shared_memory->fcc_project.u_horz_dot)
#define V_HORIZ_DOT		(shared_memory->fcc_project.v_horz_dot)
#define W_HORIZ_DOT		(shared_memory->fcc_project.w_horz_dot)
#define WIND_SPEED		(shared_memory->fcc_project.wind_speed)
#define WIND_DIRECTION	(shared_memory->fcc_project.wind_direction)
#define TRACKANGLE		(shared_memory->fcc_project.track_angle)
#define P_INS_NOTCH     (shared_memory->fcc_project.p_notch)
#define Q_INS_NOTCH     (shared_memory->fcc_project.q_notch)
#define R_INS_NOTCH     (shared_memory->fcc_project.r_notch)
#define AX_INS_NOTCH    (shared_memory->fcc_project.ax_notch)
#define AY_INS_NOTCH    (shared_memory->fcc_project.ay_notch)
#define AZ_INS_NOTCH    (shared_memory->fcc_project.az_notch)
#define P_FILT			(Project.P_vars_norecord.p_flts[36])
#define Q_FILT			(Project.P_vars_norecord.p_flts[37])
#define R_FILT			(Project.P_vars_norecord.p_flts[38])
#define AY_FILT			(Project.P_vars_norecord.p_flts[39]) // KE Doesn't know if this is assigned somewhere else (hope not). Also note that this will need to be added to regular equivalence.h
#define GNDSPEED		(shared_memory->fcc_project.ground_speed)

// Now we will define the FCC specific variables...
// the define must be set at compile time in the makefile

#define ENGAGE_NUM		(Project.P_vars.p_ints[2])  		// used only in the FCC

// Actuator commands
#define FD_LAT  		(Project.P_vars.da_out[0])			/* Final Drive to Actuators in Inches */
#define FD_LON  		(Project.P_vars.da_out[1])
#define FD_PED  		(Project.P_vars.da_out[2])
#define FD_COL  		(Project.P_vars.da_out[3])
#define FD_LAT_V  		(Project.P_vars.da_out[4])			/* Final Drive to Actuators in Volts */
#define FD_LON_V  		(Project.P_vars.da_out[5])
#define FD_PED_V  		(Project.P_vars.da_out[6])
#define FD_COL_V  		(Project.P_vars.da_out[7])

// the recorded project floats
#define P_MIX			(shared_memory->fcc_project.p_mix)
#define Q_MIX			(shared_memory->fcc_project.q_mix)
#define R_MIX			(shared_memory->fcc_project.r_mix)
#define DELTA_LAT		(Project.P_vars.p_flts[3])
#define DELTA_LON		(Project.P_vars.p_flts[4])
#define DELTA_PED		(Project.P_vars.p_flts[5])
#define DELTA_COL		(Project.P_vars.p_flts[6])
#define D_LAT_NET		(shared_memory->fcc_output.dlat_net)
#define D_LON_NET		(shared_memory->fcc_output.dlon_net)
#define D_PED_NET		(shared_memory->fcc_output.dped_net)
#define D_COL_NET		(shared_memory->fcc_output.dcol_net)
#define U_GND			(shared_memory->fcc_project.u_gnd)
#define V_GND      		(shared_memory->fcc_project.v_gnd)
#define W_GND			(shared_memory->fcc_project.w_gnd)
#define U_HORIZ			(shared_memory->fcc_project.u_horz)
#define V_HORIZ			(shared_memory->fcc_project.v_horz)
#define THETA_COMM		(shared_memory->fcc_project.theta_com)
#define PHI_COMM		(shared_memory->fcc_project.phi_com)
#define PSI_COMM		(shared_memory->fcc_project.psi_com)
#define Q_COMM			(shared_memory->fcc_project.q_com)
#define P_COMM			(shared_memory->fcc_project.p_com)
#define R_COMM			(shared_memory->fcc_project.r_com)
#define U_COMM			(shared_memory->fcc_project.u_com)
#define V_COMM			(shared_memory->fcc_project.v_com)
#define W_COMM			(shared_memory->fcc_project.w_com)
#define H_COMM			(shared_memory->fcc_project.h_com)
#define PROJ1			(shared_memory->fcc_project.proj1) 
#define PROJ2			(shared_memory->fcc_project.proj2)
#define PROJ3			(shared_memory->fcc_project.proj3)
#define PROJ4			(shared_memory->fcc_project.proj4)
#define PROJ5			(shared_memory->fcc_project.proj5)
#define PROJ6			(shared_memory->fcc_project.proj6)
#define PROJ7			(shared_memory->fcc_project.proj7)
#define PROJ8			(shared_memory->fcc_project.proj8)
#define PROJ9			(shared_memory->fcc_project.proj9)
#define PROJ10			(shared_memory->fcc_project.proj10)
#define PROJ11			(shared_memory->fcc_project.proj11)
#define PROJ12			(shared_memory->fcc_project.proj12)
#define PROJ13			(shared_memory->fcc_project.proj13)
#define PROJ14			(shared_memory->fcc_project.proj14)
#define PROJ15			(shared_memory->fcc_project.proj15)
#define PROJ16			(shared_memory->fcc_project.proj16)
#define PROJ17			(shared_memory->fcc_project.proj17)
#define PROJ18			(shared_memory->fcc_project.proj18)
#define PROJ19			(shared_memory->fcc_project.proj19)
#define PROJ20			(shared_memory->fcc_project.proj20)
#define LATITUDE_CMD	(shared_memory->fcc_project.latitude_com)
#define LONGITUDE_CMD	(shared_memory->fcc_project.longitude_com)

// the not recorded project floats and ints
#define SAC				(Project.P_vars_norecord.p_ints[0])
#define PED_NORM		(Project.P_vars_norecord.p_ints[1])
#define COL_NORM		(Project.P_vars_norecord.p_ints[2])
#define ISO_STICK		(Project.P_vars_norecord.p_ints[3])
#define DELAY_INDEX		(Project.P_vars_norecord.p_ints[4])
#define NUM_DELAY_SWITCHES (Project.P_vars_norecord.p_ints[5])
#define HIGH_FREEPLAY	(Project.P_vars_norecord.p_ints[6])
#define LOW_HEAVE_DAMP	(Project.P_vars_norecord.p_ints[7])
#define HEADHOLD		(Project.P_vars_norecord.p_ints[8])
#define HEAVE_CMD		(Project.P_vars_norecord.p_ints[9])
#define COUPLE_R_TO_P	(Project.P_vars_norecord.p_ints[10])
#define COUPLE_COL_TO_Q	(Project.P_vars_norecord.p_ints[11])
#define HEIGHTHOLD		(Project.P_vars_norecord.p_ints[12])
#define INT_TRIM		(Project.P_vars_norecord.p_ints[13])
#define DECOUPLE		(Project.P_vars_norecord.p_ints[14])
#define LAG_FILTER		(Project.P_vars_norecord.p_ints[15])
#define LEAD_FILTER		(Project.P_vars_norecord.p_ints[16])		
#define CROSS_COUPLE	(Project.P_vars_norecord.p_ints[17])
#define LAT_RCAH_ON     (Project.P_vars_norecord.p_ints[18]) // Used for fwd flight vari stab
#define PRINT_NOW		(Project.P_vars_norecord.p_ints[19]) // Global variable used for printing across various code blocks
#define EXTERNAL_HREF	(Project.P_vars_norecord.p_ints[20])
#define AS_HOLD_ON      (Project.P_vars_norecord.p_ints[21])


#define Q_PRED			(Project.P_vars_norecord.p_flts[10])
#define HH_COL_POS		(Project.P_vars_norecord.p_flts[11])
#define HH_COL_BIAS		(Project.P_vars_norecord.p_flts[12])
#define HH_A1			(Project.P_vars_norecord.p_flts[13])
#define HH_B1			(Project.P_vars_norecord.p_flts[14])
#define HH_THR_REQ		(Project.P_vars_norecord.p_flts[15])
#define HH_VI			(Project.P_vars_norecord.p_flts[16])

// Virtual Pots Go Here
#define VP1             (Project.P_vars_norecord.p_flts[17])
#define VP2             (Project.P_vars_norecord.p_flts[18])
#define VP3             (Project.P_vars_norecord.p_flts[19])
#define VP4             (Project.P_vars_norecord.p_flts[20])
#define VP5             (Project.P_vars_norecord.p_flts[21])
#define VP6             (Project.P_vars_norecord.p_flts[22])
#define VP7             (Project.P_vars_norecord.p_flts[23])
#define VP8             (Project.P_vars_norecord.p_flts[24])
#define VP9             (Project.P_vars_norecord.p_flts[25])
#define VP10            (Project.P_vars_norecord.p_flts[26])
#define VP11            (Project.P_vars_norecord.p_flts[27])
#define VP12            (Project.P_vars_norecord.p_flts[28])

#define VP1_FUNC		(shared_memory->fcc_vpot_data.vp1_function)
#define VP2_FUNC		(shared_memory->fcc_vpot_data.vp2_function)
#define VP3_FUNC		(shared_memory->fcc_vpot_data.vp3_function)
#define VP4_FUNC		(shared_memory->fcc_vpot_data.vp4_function)
#define VP5_FUNC		(shared_memory->fcc_vpot_data.vp5_function)
#define VP6_FUNC		(shared_memory->fcc_vpot_data.vp6_function)
#define VP7_FUNC		(shared_memory->fcc_vpot_data.vp7_function)
#define VP8_FUNC		(shared_memory->fcc_vpot_data.vp8_function)
#define VP9_FUNC		(shared_memory->fcc_vpot_data.vp9_function)
#define VP10_FUNC		(shared_memory->fcc_vpot_data.vp10_function)
#define VP11_FUNC		(shared_memory->fcc_vpot_data.vp11_function)
#define VP12_FUNC		(shared_memory->fcc_vpot_data.vp12_function)

#define VP1_FACEVAL     (shared_memory->fcc_vpot_data.vp1_value)
#define VP2_FACEVAL     (shared_memory->fcc_vpot_data.vp2_value)
#define VP3_FACEVAL     (shared_memory->fcc_vpot_data.vp3_value)
#define VP4_FACEVAL     (shared_memory->fcc_vpot_data.vp4_value)
#define VP5_FACEVAL     (shared_memory->fcc_vpot_data.vp5_value)
#define VP6_FACEVAL     (shared_memory->fcc_vpot_data.vp6_value)
#define VP7_FACEVAL     (shared_memory->fcc_vpot_data.vp7_value)
#define VP8_FACEVAL     (shared_memory->fcc_vpot_data.vp8_value)
#define VP9_FACEVAL     (shared_memory->fcc_vpot_data.vp9_value)
#define VP10_FACEVAL     (shared_memory->fcc_vpot_data.vp10_value)
#define VP11_FACEVAL     (shared_memory->fcc_vpot_data.vp11_value)
#define VP12_FACEVAL     (shared_memory->fcc_vpot_data.vp12_value)


#define PSI_HON2        (Project.P_vars_norecord.p_flts[29])

#define VSW_ALL			(shared_memory->fcc_vpot_data.vsw)
#define VSW0			((shared_memory->fcc_vpot_data.vsw & BIT_0) && 1)
#define VSW1			((shared_memory->fcc_vpot_data.vsw & BIT_1) && 1)
#define VSW2			((shared_memory->fcc_vpot_data.vsw & BIT_2) && 1)
#define VSW3			((shared_memory->fcc_vpot_data.vsw & BIT_3) && 1)
#define VSW4			((shared_memory->fcc_vpot_data.vsw & BIT_4) && 1)
#define VSW5			((shared_memory->fcc_vpot_data.vsw & BIT_5) && 1)
#define VSW6			((shared_memory->fcc_vpot_data.vsw & BIT_6) && 1)
#define VSW7			((shared_memory->fcc_vpot_data.vsw & BIT_7) && 1)

#define VSW0_FUNC		(shared_memory->fcc_vpot_data.vsw0_function)
#define VSW1_FUNC		(shared_memory->fcc_vpot_data.vsw1_function)
#define VSW2_FUNC		(shared_memory->fcc_vpot_data.vsw2_function)
#define VSW3_FUNC		(shared_memory->fcc_vpot_data.vsw3_function)
#define VSW4_FUNC		(shared_memory->fcc_vpot_data.vsw4_function)
#define VSW5_FUNC		(shared_memory->fcc_vpot_data.vsw5_function)
#define VSW6_FUNC		(shared_memory->fcc_vpot_data.vsw6_function)
#define VSW7_FUNC		(shared_memory->fcc_vpot_data.vsw7_function)

// FCC DIO (Rainbow in the Dark) outputs 
#define WDT1			(Project.dio_io.dio_port_a0) /* set to Port A0 to match FCC wiring was C6 */
#define FCCOK1			(Project.dio_io.dio_port_a1) /* set to Port A1 to match FCC wiring was C7 */
#define WDT2			(Project.dio_io.dio_port_a2) /* set to Port A2 to match FCC wiring was B0 */
#define FCCOK2			(Project.dio_io.dio_port_a3) /* set to Port A3 to match FCC wiring was B1 */
#define FLASH_GREEN		(Project.dio_io.dio_port_a4) /* set to Port A4 to match FCC wiring was B2 */

#define RADALT			(shared_memory->hmu_output.radar_altimeter)
#define TAS				(shared_memory->hmu_safety.cva_tas)
#define TAS_CIF			(shared_memory->cif_output.true_airspeed)
#define TAT_CIF			(shared_memory->cif_output.total_air_temp)
#define TAT				(shared_memory->hmu_output.outside_air_temperature)
#define TAT_CIF			(shared_memory->cif_output.total_air_temp)
#define BARO_ALT_CIF	(shared_memory->cif_output.barometric_altimeter)
#define TORQUE			(shared_memory->hmu_output.main_rotor_torque)
#define HMU_SAFETY_STATUS (shared_memory->hmu_safety.safety_status)

//extern hmu_output_t hmu_output_data;

// Now we will define the HUM specific variables...
// the define must be set at compile time in the makefile

#define WATCH_DOG				(Project.P_vars.p_ints[2]) // used only in the HMU, replaces FCC's engage num

// Project floats....
#define HRA						(Project.P_vars.p_flts[0])
#define LAT_CMD_RATE			(Project.P_vars.p_flts[1])
#define LON_CMD_RATE			(Project.P_vars.p_flts[2])
#define PED_CMD_RATE			(Project.P_vars.p_flts[3])
#define COL_CMD_RATE			(Project.P_vars.p_flts[4])
#define LAT_OVERRIDE			(Project.P_vars.p_flts[5])
#define LON_OVERRIDE			(Project.P_vars.p_flts[6])
#define PED_OVERRIDE			(Project.P_vars.p_flts[7])
#define COL_OVERRIDE			(Project.P_vars.p_flts[8])
#define THETA_PRED				(Project.P_vars.p_flts[21])
#define PHI_PRED				(Project.P_vars.p_flts[22])
#define ALT_PRED				(Project.P_vars.p_flts[23])
#define THETA_ENVELOPE_LIMIT	(Project.P_vars.p_flts[24])
#define PHI_ENVELOPE_LIMIT		(Project.P_vars.p_flts[25])
#define ALT_ENVELOPE_LIMIT		(Project.P_vars.p_flts[26])
#define PHI_DOT                 (Project.P_vars.p_flts[27])
#define THETA_DOT               (Project.P_vars.p_flts[28])
#define PSI_DOT                 (Project.P_vars.p_flts[29])

//#define G500_AHRS_FAILED		// Set this if we have a failed G500 AHRS
#define THETA_IPAD				(ipad_gyro_data.theta)  // Used temporarily as 2nd attitude source
#define PHI_IPAD				(ipad_gyro_data.phi)
#define THETA_INS_AT_IPAD		(Project.P_vars.p_flts[38])
#define PHI_INS_AT_IPAD			(Project.P_vars.p_flts[39])

#define THETA_GARMIN 			(asra_arinc_data.theta) /* change when ARINC code installed */
#define PHI_GARMIN 				(asra_arinc_data.phi)

#define THETA_INS_AT_GARMIN     (Project.P_vars.p_flts[36])	// These are used in sensor comparison. The comparison must take place at the G500 sample time(s) otherwise you get trips owing to the timing diffs and angular rates 
#define PHI_INS_AT_GARMIN		(Project.P_vars.p_flts[37])

#define CYCLE_TIME				(Project.P_vars.p_dble[0])

// HMU DIO Inputs
#define PADDLE					!(analog_data.dio_port_b3)
#define ENGAGE_SW				!(analog_data.dio_port_b2)
#define ARM_SW					!(analog_data.dio_port_b1)
#define ENGAGE_STATUS_1			!(analog_data.dio_port_a0)
#define ENGAGE_STATUS_2			!(analog_data.dio_port_a4)
#define ARM_STATUS_1			!(analog_data.dio_port_a1)
#define ARM_STATUS_2			!(analog_data.dio_port_a5)
#define RELAY_STATUS_1			(analog_data.dio_port_a2)
#define RELAY_STATUS_2			(analog_data.dio_port_a6)
#define ALERT_STATUS_1			(analog_data.dio_port_a3)
#define ALERT_STATUS_2			(analog_data.dio_port_a7)
#define HYD_MASTER_SOL_DRVE		(analog_data.dio_port_b0)

// HMU DIO outputs 
#define HMUOK1					(analog_data.dio_port_c0)
#define WDT_EXCITE_1			(analog_data.dio_port_c1)
#define INDICATOR_TEST			(analog_data.dio_port_c2)
#define HMUOK2					(analog_data.dio_port_c3)
#define WDT_EXCITE_2			(analog_data.dio_port_c4)


// HMU A to D inputs
#define FBW_LAT_ACT				(analog_data.analog_data.ch[0])
#define LAT_SERVO_NEG_15		(analog_data.analog_data.ch[1])

#define LAT_SERVO_POS_15		(analog_data.analog_data.ch[3])
#define FBW_LON_ACT				(analog_data.analog_data.ch[4])
#define LON_SERVO_NEG_15		(analog_data.analog_data.ch[5])

#define LON_SERVO_POS_15		(analog_data.analog_data.ch[7])
#define FBW_PED_ACT				(analog_data.analog_data.ch[8])
#define PED_SERVO_NEG_15		(analog_data.analog_data.ch[9])

#define PED_SERVO_POS_15		(analog_data.analog_data.ch[11])
#define FBW_COL_ACT				(analog_data.analog_data.ch[12])
#define COL_SERVO_NEG_15		(analog_data.analog_data.ch[13])

#define COL_SERVO_POS_15		(analog_data.analog_data.ch[15])
#define HYD_PRESS				(analog_data.analog_data.ch[16])
#define HYD_PRESS_SW			(analog_data.analog_data.ch[17])	//HYD_PRESS_SW is converted from analog (V)to a discrete by a function make_pressure_sw() in analog.c
#define HYD_TEMP				(analog_data.analog_data.ch[18])
#define TAIL_PLANE				(analog_data.analog_data.ch[31])


#define ARINC_ROTOR_RPM			(asra_arinc_data.rotor_speed)
#define HP_ON					(asra_arinc_data.discretes_adc2 & BIT_13)


#define HIND					(shared_memory->cif_output.barometric_altimeter)
#define TOTAL_TEMP				(shared_memory->cif_output.total_air_temp;)

// from the FCC
#define FD_LAT2			(fcc_output_data.final_drive_lateral)        /* final drives over Ethernet */
#define FD_LON2			(fcc_output_data.final_drive_longitudinal)
#define FD_PED2			(fcc_output_data.final_drive_pedal)
#define FD_COL2			(fcc_output_data.final_drive_collective)
#define FCC_OK1         (fcc_output_data.fccok1)   
#define FCC_OK2         (fcc_output_data.fccok2) 

#ifdef EQUIVLENCE_HMU
#define FD_LAT                    (Project.P_vars.p_flts[32])    /* Actuator commands from servo boards converted from volts to inches */
#define FD_LON                    (Project.P_vars.p_flts[33])
#define FD_PED                    (Project.P_vars.p_flts[34])
#define FD_COL                    (Project.P_vars.p_flts[35])
#define FD_LAT_V                (analog_data.analog_data.ch[2])     /* final drives over A/D in (volts)   */
#define FD_LON_V                (analog_data.analog_data.ch[6])
#define FD_PED_V                (analog_data.analog_data.ch[10])
#define FD_COL_V                (analog_data.analog_data.ch[14])
#define RADALT                    (asra_arinc_data.radalt_hi_res)
#define TORQUE                    (asra_arinc_data.mast_torque_right)
#define TAS                        (asra_arinc_data.true_airspeed)
#endif

// FCC ethernet outputs
//extern fcc_output_t fcc_output_data;


//extern project_data_t Project;


