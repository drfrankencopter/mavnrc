/*
    Mission Protocol Helper Functions
    
    File name    : missionProtocol.h
    Date        : 1/19/2022 2:06:49 PM
    Author        : Levesqueje
    Description : This file holds every function that is used to communicate the mavlink mission protocol.
                  It decodes the received information and sends back requests if needed.
    TODOS : Not noted.
*/

// Standard librairies includes
#include <string.h>
#include <pthread.h>

#ifdef _WIN32
#include <ws2tcpip.h>
#endif

// External installed librairies <>
#include <standard/mavlink.h>

// Project documents includes ""
#include "equivalenceCOREsharedMem.h" // Shared memory global access
#include "socket.h"
#include "recvFromRemoteThread.h"
#include "NrcDefaults.h"
#include "updateVehicleMavLinkStatusThread.h"

// Ugly global pointer
extern missionData_t* mission_data;
extern remotes_infos_t remotesInfo;

/*
    recvFromRemoteThread
    Description : This thread is the main thread used to listen/answer with every remote instance of QGroundControl.
                  It listens/answers back in respect of the Mavlink Protocol on the specified broadcast address.
                  
                  There are specific permissions for the remote instances of QGCs:
                    - Unique system id relative to other instances
                    - Permissions levels related to the system id
                    - MavNRC forgets (deletes) a remote information after a period of time without response (no heartbeats)

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          : None

*/
void recvFromRemoteThread(void* arg) {
    long res;
    struct sockaddr_in locAddr;
    char recvbuf[PACKET_MAX_LEN];
    listened_message_t listenedMessage;
    listenedMessage.toAddrLen = sizeof(listenedMessage.toAddr);
    listenedMessage.mission_data = mission_data; //IMPORTANT!!! Here is how the local listenedMessage points back to the global mission_data BE EXTREMELY CAREFUL
    
    printf("RECV FROM REMOTE THREAD LAUNCHED\n");
    
#ifdef _WIN32
    initialize_windows_sockets();
#endif
    
    listenedMessage.sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (listenedMessage.sockfd == -1) {
        SERROR("invalid socket for communication with remotes\n");
        return;
    }
    
    locAddr.sin_family = AF_INET;
    locAddr.sin_port = htons(0);
    locAddr.sin_addr.s_addr = INADDR_ANY;
    
    // Copy the socket into the shared_memory
    remotesInfo.sockfdRemotes = listenedMessage.sockfd;

    struct sockaddr_in* gcsAddr = &remotesInfo.gcsAddr; // Convenience pointer to the shared_memory
    gcsAddr->sin_family = AF_INET;
    gcsAddr->sin_port = htons(MAVLINK_SEND_PORT);
    inet_pton(AF_INET, LOCALHOST_ADDR, &gcsAddr->sin_addr.s_addr);

    struct sockaddr_in* broadcastAddr = &remotesInfo.broadcastAddr; // Convenience pointer to the shared memory
    broadcastAddr->sin_family = AF_INET;
    broadcastAddr->sin_port = htons(QGC_BROADCAST_PORT);
    inet_pton(AF_INET, QGC_BROADCAST_ADDR, &broadcastAddr->sin_addr.s_addr);
    
#ifdef _WIN32
    DWORD tv = 1000;
#else
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
#endif
    
    res = setsockopt(listenedMessage.sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
    if (res == -1) {
        SERROR("setsockopt error on setting the receive timeout for listening to remotes\n");
        CLOSESOCKET(listenedMessage.sockfd);
        return;
    }
    
    const int doBroadcast = 1;
    res = setsockopt(listenedMessage.sockfd, SOL_SOCKET, SO_BROADCAST, &doBroadcast, sizeof(doBroadcast));
    if (res == -1) {
        SERROR("can't broadcast\n");
        CLOSESOCKET(listenedMessage.sockfd);
        return;
    }
    
    res = bind(listenedMessage.sockfd, (const struct sockaddr*)&locAddr, sizeof(locAddr));
    if (res == -1) {
        SERROR("invalid binding for communication with remotes\n");
        CLOSESOCKET(listenedMessage.sockfd);
        return;
    }
    
    // Launch another thread to send periodic information to the remotes
    // This sending thread needs to send information from the same socket
    // it listens to.
    void* thread[1] = {updateVehicleMavLinkStatusThread};
    pthread_t pid;
    pthread_create(&pid, NULL, thread[0], &listenedMessage.sockfd);
    
    while (1) {
        long nBytes = recvfrom(listenedMessage.sockfd, recvbuf, PACKET_MAX_LEN, 0, (struct sockaddr*)&listenedMessage.toAddr, &listenedMessage.toAddrLen);
        if (nBytes > 0) {
            // We received something, otherwise we prob timed out
            if (isRemoteConnected(mission_data, &listenedMessage.toAddr)) {
                handleMessageReceived(&listenedMessage, recvbuf, nBytes);
            }
            else {
                // Parse the mavlink message to capture the sysid and compid of the remote instance
                int i;
                mavlink_message_t msg;
                mavlink_status_t status;
                remoteInfo_t* remote;
                for (i = 0; i < nBytes; ++i) {
                    if (mavlink_parse_char(MAVLINK_COMM_0, recvbuf[i], &msg, &status)) {
                        int port = htons(listenedMessage.toAddr.sin_port);
                        char adr[INET_ADDRSTRLEN];
                        inet_ntop(AF_INET, &listenedMessage.toAddr.sin_addr, adr, INET_ADDRSTRLEN);
                        
                        // We don't want two remotes with the same SYS_ID and the 255 ID is only allowed to the local instance of QGC
                        if (isSysIdTaken(&remotesInfo, msg.sysid) || msg.sysid == 255) {
                            char string[100];
                            sprintf(string, "Your SYS_ID %d is not available. Connection denied.", msg.sysid);
                            sendSimpleText(listenedMessage.sockfd, string, MAV_SEVERITY_ALERT, &listenedMessage.toAddr);
                        }
                        else {
                            // Add the remote to the list
                            remote = connectRemote(mission_data, &listenedMessage.toAddr, msg.sysid, msg.compid);
                            
                            printf("Connected a new REMOTE [%s:%d] \n", adr, port);
                            
                            // Let the remote know what permissions it has
                            sendPermissionsText(&listenedMessage, remote);
                            
                            // Handle the message as usual
                            handleMessageReceived(&listenedMessage, recvbuf, nBytes);
                        } // else
                    } // if
                } // for loop
            } // else
        }
    }
    
#ifdef _WIN32
    WSACleanup();
#endif
}

/*
    isSysIdTaken
    Description : This function verifies is a given system id is already recorded in another remote's information.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : 1 if sysid is already owned by another instance, 0 otherwise
    Inputs          :
        - remotesInfo : Struct holding the remotes list and the remotes count
        - sysid : System id to verify

*/
int isSysIdTaken(const remotes_infos_t* remotesInfo, int sysid) {
    int i;
    
    for (i = 0; i < remotesInfo->numRemotes; ++i) {
        if (remotesInfo->remotes[i].sysid == sysid) {
            return 1;
        }
    }
    
    return 0;
}

/*
    isRemoteConnected
    Description : This function searches the remotes list in the shared mission data memory
                  for the provided address. If the address is found in the list, that means
                  the remote is already connected, if not, the remote is not connected to MavNRC.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : 1 (TRUE) if remote is connected and 0 (FALSE) otherwise
    Inputs          :
        - mission_data : shared mission data struct pointer
        - remote : address of the remote to verify

*/
int isRemoteConnected(missionData_t* mission_data, const struct sockaddr_in* remote) {
    return getRemote(mission_data, remote) != NULL;
}

/*
    connectRemote
    Description : This function adds a specified address, sysid and compid into the
                  remotes list in order to remember its information for further treatments.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : Pointer to the added remote's information struct
    Inputs          :
        - mission_data : shared mission data struct pointer
        - remote : address of the remote to add
        - sysid : Mavlink system id of the remote to add
        - compid : Mavlink component id of the remote to add
*/
remoteInfo_t* connectRemote(missionData_t* mission_data, const struct sockaddr_in* remote, int sysid, int compid) {
    // Copy the remote information into a specific structure
    remoteInfo_t remInf;
    remInf.sysid = sysid;
    remInf.compid = compid;
    remInf.addrLen = sizeof(*remote);
    remInf.permissions = getPermissionsForId(sysid);
    memcpy(&remInf.addr, remote, sizeof(*remote));
    
    // Stores all the information about the remote inside the shared mission memory
    memcpy(&remotesInfo.remotes[remotesInfo.numRemotes++], &remInf, sizeof(remInf));
    
    return &remotesInfo.remotes[remotesInfo.numRemotes-1];
}

/*
    sendPermissionsText
    Description : This function sends a specified remote a status text alert to let
                  the user know what permissions it has related to its system id.

    Author          : Jeremi Levesque
    Date            : 02/07/2022
    Return value    : None
    Inputs          :
        - lm : listened message (holding all the needed data related to a mavlink message)
        - remote : address of the remote to add
*/
void sendPermissionsText(listened_message_t* lm, const remoteInfo_t* remote) {
    char* permText;
    switch (remote->permissions) {
        case eREMOTE_PERMISSION_READONLY:
            permText = "Read Only";
            break;
        case eREMOTE_PERMISSION_SEMI:
            permText = "Semi";
            break;
        case eREMOTE_PERMISSION_ADMIN:
            permText = "Admin";
            break;
        default:
            permText = "Undefined";
            break;
    }
    
    char full[MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN];
    sprintf(full, "Your permissions are : %s", permText);
    sendSimpleText(lm->sockfd, full, MAV_SEVERITY_INFO, &lm->toAddr);
}
