#pragma once
#include <standard/mavlink.h>
#include "globals.h"
// Here we'll define some properties to save in a struct
#define MAXPARAMS 600



extern int numMavParams; // Globals Found in mavParams.c
extern mavlink_param_value_t myMavParams[MAXPARAMS];

typedef struct {
    int sockfd;
    const mavlink_param_value_t* params;
    int numParams;
    struct sockaddr_in to;
    int* paramsLoaded;
} sendParametersArgs_t;

int readParamsFromDisk(void);
int findParamByName(char paraName[16]);
void* sendParametersThread(void* args);
