//
//  remotePermissions.h
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-21.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#ifndef remotePermissions_h
#define remotePermissions_h

#include "globals.h"
#include "mavProtocol.h"

int hasPermission(listened_message_t* lm);

#endif /* remotePermissions_h */
